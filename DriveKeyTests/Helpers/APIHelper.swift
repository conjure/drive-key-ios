//
//  APIHelper.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 10/08/2021.
//

import Foundation

class APIHelper {
    class func serializeJson<T>(filename fileName: String, decodingStrategy: JSONDecoder.KeyDecodingStrategy?) -> T? where T: Decodable {
        if let url = Bundle(for: APIHelper.self).url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                if decodingStrategy != nil {
                    decoder.keyDecodingStrategy = decodingStrategy!
                }
                decoder.dateDecodingStrategy = .iso8601
                return try decoder.decode(T.self, from: data)
            } catch {
                print("[APIHelper] Error: \(error)")
            }
        }

        return nil
    }
}
