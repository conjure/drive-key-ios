//
//  XCTest+Utils.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 10/08/2021.
//

import XCTest

extension XCTestCase {
    func wait(_ delay: Double) {
        let exp = expectation(description: "")
        exp.isInverted = true

        waitForExpectations(timeout: TimeInterval(delay), handler: nil)
    }
}
