//
//  BubbleProviderServiceTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 08/02/2022.
//

import XCTest
import Combine
import LocalAuthentication
import CoreLocation
@testable import DriveKey

class BubbleProviderServiceTests: XCTestCase {
    private var service: BubbleProviderService!
    private var remoteBubbleMock: BubbleProvidableMock!
    private var userDefaults: UserDefaults!
    private var mockLocationService: LocationServiceMock!
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        userDefaults = UserDefaults(suiteName: #file)
        userDefaults.removePersistentDomain(forName: #file)
        remoteBubbleMock = BubbleProvidableMock()

        mockLocationService = LocationServiceMock()
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: BiometricService(),
                                        locationService: mockLocationService,
                                        userDefaults: userDefaults)

        cancellables = []
    }

    override func tearDownWithError() throws {
        userDefaults = nil
        service = nil
        remoteBubbleMock = nil
        cancellables = nil
    }

    func testServiceLoadsWithNoBubblesOnFirstLaunch() {
        XCTAssert(service.bubbles.isEmpty)
    }

    func testServiceRefreshesAfterRemoteUpdate() {
        var multiEx: XCTestExpectation? = expectation(description: "multiple_remote_updates")

        service.syncedSubject
            .sink { bubbles in
                if !bubbles.isEmpty {
                    multiEx?.fulfill()
                }
            }
            .store(in: &cancellables)

        remoteBubbleMock.add(.vehicleReg)
        XCTAssert(service.bubbles.isEmpty)
        remoteBubbleMock.refresh()
        wait(for: [multiEx!], timeout: 3)
        XCTAssertEqual(service.bubbles.count, 1)

        multiEx = nil
        multiEx = expectation(description: "multiple_remote_updates")
        remoteBubbleMock.add(.expiredCard)
        remoteBubbleMock.refresh()
        wait(for: [multiEx!], timeout: 3)
        XCTAssertEqual(service.bubbles.count, 2)
    }

    func testRemoteIsClearedWhenServiceResets() {
        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { _ in
                ex.fulfill()
            }
            .store(in: &cancellables)

        remoteBubbleMock.add(.vehicleReg)
        XCTAssert(service.bubbles.isEmpty)
        remoteBubbleMock.refresh()
        wait(for: [ex], timeout: 3)
        XCTAssertEqual(service.bubbles.count, 1)

        service.reset()
        XCTAssert(service.bubbles.isEmpty)
        XCTAssert(remoteBubbleMock.bubbles.isEmpty)
    }

    func testServiceRefreshesAfterLocationPermissionDenied() throws {
        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { bubbles in
                if !bubbles.isEmpty {
                    ex.fulfill()
                }
            }
            .store(in: &cancellables)

        XCTAssert(service.bubbles.isEmpty)

        mockLocationService.isAuthorizedAlways = false
        mockLocationService.locationAuthSubject.send(.denied)

        wait(for: [ex], timeout: 3)
        let bubble = try XCTUnwrap(service.bubbles.first)
        XCTAssert(bubble.bubbleType == .locationPermission)
    }

    func testServiceUpdatesWhenMultipleChangesOccur() throws {
        var multiEx: XCTestExpectation? = expectation(description: "multiple_status_updates")

        service.syncedSubject
            .sink { bubbles in
                if !bubbles.isEmpty {
                    multiEx?.fulfill()
                }
            }
            .store(in: &cancellables)

        XCTAssert(service.bubbles.isEmpty)

        remoteBubbleMock.add(.vehicleReg)
        remoteBubbleMock.refresh()
        wait(for: [multiEx!], timeout: 3)
        XCTAssertEqual(service.bubbles.count, 1)

        multiEx = nil
        multiEx = expectation(description: "multiple_location_updates")

        mockLocationService.isAuthorizedAlways = false
        mockLocationService.isPreciseLocation = false
        mockLocationService.allowsBackgroundUpdates = false
        mockLocationService.locationAuthSubject.send(.authorizedWhenInUse)

        wait(for: [multiEx!], timeout: 3)
        XCTAssertEqual(service.bubbles.count, 2)
    }

    func testServiceWhenDeviceDoesNotSupportBiometrics() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                false
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { _ in
                ex.fulfill()
            }
            .store(in: &cancellables)

        XCTAssert(service.bubbles.isEmpty)
        service.refresh()
        wait(for: [ex], timeout: 3)
        XCTAssert(service.bubbles.isEmpty)
    }

    func testServiceAddsTouchIDBiometricBubble() throws {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .touchID
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { bubbles in
                if !bubbles.isEmpty {
                    ex.fulfill()
                }
            }
            .store(in: &cancellables)

        XCTAssert(service.bubbles.isEmpty)
        service.refresh()
        wait(for: [ex], timeout: 3)

        let bubble = try XCTUnwrap(service.bubbles.first)
        XCTAssert(bubble.bubbleType == .biometric)
        XCTAssertEqual(bubble.title, "bubble_biometric_title".localized)
        XCTAssertEqual(bubble.message, "bubble_biometric_message_touch".localized)
    }

    func testServiceAddsFaceIDBubble() throws {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .faceID
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { bubbles in
                if !bubbles.isEmpty {
                    ex.fulfill()
                }
            }
            .store(in: &cancellables)

        XCTAssert(service.bubbles.isEmpty)
        service.refresh()
        wait(for: [ex], timeout: 3)

        let bubble = try XCTUnwrap(service.bubbles.first)
        XCTAssert(bubble.bubbleType == .biometric)
        XCTAssertEqual(bubble.title, "bubble_biometric_title".localized)
        XCTAssertEqual(bubble.message, "bubble_biometric_message_face".localized)
    }

    func testServiceRemovesBiometricBubbleWhenApproved() throws {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { _ in
                ex.fulfill()
            }
            .store(in: &cancellables)

        userDefaults.set(BiometricStatus.granted.rawValue, forKey: UserDefaultKeys.biometricStatus)

        service.refresh()
        wait(for: [ex], timeout: 3)
        XCTAssert(service.bubbles.isEmpty)
    }

    func testServiceRemovesBiometricBubbleWhenDismissed() throws {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .touchID
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        var multiEx: XCTestExpectation? = expectation(description: "multiple_updates")

        service.syncedSubject
            .sink { _ in
                multiEx?.fulfill()
            }
            .store(in: &cancellables)

        service.refresh()
        wait(for: [multiEx!], timeout: 3)

        XCTAssert(!service.bubbles.isEmpty)

        userDefaults.set(BiometricStatus.dismissedBubble.rawValue, forKey: UserDefaultKeys.biometricStatus)

        multiEx = nil
        multiEx = expectation(description: "multiple_updates")
        service.refresh()
        wait(for: [multiEx!], timeout: 3)
        XCTAssert(service.bubbles.isEmpty)
    }

    func testServiceRemovesBiometricBubbleWhenDenied() throws {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }
        }

        let bioService = BiometricService(context: StubLAContext())
        service = BubbleProviderService(remoteBubbleService: remoteBubbleMock, biometricService: bioService, locationService: mockLocationService,
                                        userDefaults: userDefaults)

        let ex = XCTestExpectation()

        service.syncedSubject
            .sink { _ in
                ex.fulfill()
            }
            .store(in: &cancellables)

        userDefaults.set(BiometricStatus.denied.rawValue, forKey: UserDefaultKeys.biometricStatus)

        service.refresh()
        wait(for: [ex], timeout: 3)
        XCTAssert(service.bubbles.isEmpty)
    }

    class LocationServiceMock: LocationProvidable {
        var locationPublisher: PassthroughSubject<CLLocation, Never> = .init()
        var locationAuthSubject: PassthroughSubject<CLAuthorizationStatus, Never> = .init()

        var isAuthorizedAlways: Bool = true
        var isAuthorizedWhenInUse: Bool = true
        var isPreciseLocation: Bool = true
        var allowsBackgroundUpdates: Bool = true
        var isLocationServicesEnabled: Bool = true
        var lastLocation: CLLocation?
        func startMonitoringRegion(_ location: CLLocationCoordinate2D, name: String) {}
        func stopMonitoringAllRegions() {}
        var regionCount: Int = 1
    }

}
