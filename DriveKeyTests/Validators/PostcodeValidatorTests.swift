//
//  PostcodeValidatorTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 08/12/2021.
//

import XCTest
@testable import DriveKey

class PostcodeValidatorTests: XCTestCase {
    private var validator: PostCodeValidator!

    override func setUpWithError() throws {
        validator = PostCodeValidator()
    }

    override func tearDownWithError() throws {
        validator = nil
    }

    func testValiatorFailsWithNilString() {
        let passed = validator.validate(nil)
        XCTAssertFalse(passed, "Validator should fail with nil string")
    }

    func testValidatorFailsWithEmptyString() {
        let test1 = validator.validate("")
        XCTAssertFalse(test1, "Validator should fail with empty string")

        let test2 = validator.validate(" ")
        XCTAssertFalse(test2, "Validator should fail with empty string")
    }

    func testValidatorFailsWithInvalidUKPostcode() {
        let test1 = validator.validate("HA4Y EGGY")
        XCTAssertFalse(test1, "Validator should fail with invalid postcode")

        let test2 = validator.validate("UB10Random")
        XCTAssertFalse(test2, "Validator should fail with invalid postcode")

        let test3 = validator.validate("1234567")
        XCTAssertFalse(test3, "Validator should fail with invalid postcode")

        let test4 = validator.validate("INVALID")
        XCTAssertFalse(test4, "Validator should fail with invalid postcode")

        XCTAssertFalse(validator.validate("Z1A 0B1"), "Validator should fail with invalid postcode")
        XCTAssertFalse(validator.validate("A1A 0B11"), "Validator should fail with invalid postcode")
        XCTAssertFalse(validator.validate("NW6 7LLllL"), "Validator should fail with invalid postcode")
    }

    func testValidatorPassesWithValidUKPostcode() {
        let test1 = validator.validate("XM4 5HQ")
        XCTAssert(test1, "Validator should pass with valid postcode")

        let test2 = validator.validate("EC1A 1BB")
        XCTAssert(test2, "Validator should pass with valid postcode")

        let test3 = validator.validate("SE1 4YH")
        XCTAssert(test3, "Validator should pass with valid postcode")

        let test4 = validator.validate("SW1W 0NY")
        XCTAssert(test4, "Validator should pass with valid postcode")

        XCTAssert(validator.validate("PO16 7GZ"), "Validator should pass with valid postcode")
        XCTAssert(validator.validate("GU16 7HF"), "Validator should pass with valid postcode")
        XCTAssert(validator.validate("L1 8JQ"), "Validator should pass with valid postcode")
    }
    
    func testValidatorFailsWithInvalidUSAPostcode() {
        XCTAssertFalse(validator.validate("123"))
        XCTAssertFalse(validator.validate("1234"))
        XCTAssertFalse(validator.validate("123456"))
        XCTAssertFalse(validator.validate("111111"))
        XCTAssertFalse(validator.validate("0 0 0"))
        XCTAssertFalse(validator.validate("9 0 2 4 1"))
        XCTAssertFalse(validator.validate("90 241"))
    }
    
    func testValidatorPassesWithValidUSAPostcode() {
        XCTAssertTrue(validator.validate("90241"))
        XCTAssertTrue(validator.validate("86556"))
        XCTAssertTrue(validator.validate("50001"))
        XCTAssertTrue(validator.validate("01001"))
        XCTAssertTrue(validator.validate("59937"))
        XCTAssertTrue(validator.validate("73301"))
        XCTAssertTrue(validator.validate("00501"))
        XCTAssertTrue(validator.validate("12345"))
    }
}
