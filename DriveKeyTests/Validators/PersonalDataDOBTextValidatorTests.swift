//
//  PersonalDataDOBTextValidatorTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 25/10/2021.
//

@testable import DriveKey
import XCTest

class PersonalDataDOBTextValidatorTests: XCTestCase {
    private var validator: PersonalDataDOBTextValidator!

    override func setUpWithError() throws {
        validator = PersonalDataDOBTextValidator()
    }

    override func tearDownWithError() throws {
        validator = nil
    }

    func testValidatorFailsWithInvalidDateFormat() {
        let passed = validator.validate("01/01/01")
        XCTAssertFalse(passed, "Validator should fail with invalid date format")

        let validated = validator.validate("01011990")
        XCTAssertFalse(validated, "Validator should fail with invalid date format")
    }

    func testValidatorFailsWithDOBLessThan18() {
        let passed = validator.validate("01/01/2021")
        XCTAssertFalse(passed, "Validator should fail with date less than 18 years ago")
    }

    func testValidatorFailsWithDOBMoreThan100YearsOld() {
        let passed = validator.validate("14/10/1066")
        XCTAssertFalse(passed, "Validator should fail with date more than 100 years ago")
    }

    func testValidatorPassesWithValidDOB() {
        let passed = validator.validate("01/01/1990")
        XCTAssertTrue(passed, "Validator should pass with valid DOB")
    }
}
