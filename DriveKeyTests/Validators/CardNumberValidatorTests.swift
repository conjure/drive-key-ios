//
//  CardNumberValidatorTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 04/11/2022.
//

import XCTest
@testable import DriveKey

class CardNumberValidatorTests: XCTestCase {
    private var validator: CardNumberValidator!
    
    override func setUpWithError() throws {
        validator = CardNumberValidator()
    }
    
    override func tearDownWithError() throws {
        validator = nil
    }

    func testFailNil() throws {
        XCTAssertFalse(validator.validate(nil))
    }
    
    func testFailWhitespaces() throws {
        XCTAssertFalse(validator.validate(""))
        XCTAssertFalse(validator.validate(" "))
        XCTAssertFalse(validator.validate("         "))
    }
    
    func testFailNonNumericString() throws {
        XCTAssertFalse(validator.validate("abcd efgh abcd efgh"))   // 16
        XCTAssertFalse(validator.validate("abcdefghabcdefgh"))
        
        XCTAssertFalse(validator.validate("abcd efgh abcd efg"))    // 15
        XCTAssertFalse(validator.validate("abcdefghabcdefg"))
        
        XCTAssertFalse(validator.validate("abcd efgh ijkl mnop"))   // 16
        XCTAssertFalse(validator.validate("abcdefghijklmnop"))
        
        XCTAssertFalse(validator.validate("abcd efgh ijkl mno"))    // 15
        XCTAssertFalse(validator.validate("abcdefghijklmno"))
        
        XCTAssertFalse(validator.validate("1234 efgh 5678 mnop"))   // 16
        XCTAssertFalse(validator.validate("1234efgh5678mnop"))
        
        XCTAssertFalse(validator.validate("1234 efgh 5678 mno"))    // 15
        XCTAssertFalse(validator.validate("1234efgh5678mno"))
    }
    
    func testFailNumericString() throws {
        XCTAssertFalse(validator.validate("0"))
        XCTAssertFalse(validator.validate("1"))
        XCTAssertFalse(validator.validate("15"))
        XCTAssertFalse(validator.validate("16"))
        
        XCTAssertFalse(validator.validate("12345678901234"))        // 14
        XCTAssertFalse(validator.validate("1234 5678 9012 34"))     // 14
        
        XCTAssertFalse(validator.validate("1234 5678 9012 3456 7")) // 17
        XCTAssertFalse(validator.validate("12345678901234567"))
    }
    
    func testSuccessNumericString() throws {
        XCTAssertTrue(validator.validate("1234 5678 9012 3456"))    // 16
        XCTAssertTrue(validator.validate("1234567890123456"))
        
        XCTAssertTrue(validator.validate("1234 5678 9012 345"))     // 15
        XCTAssertTrue(validator.validate("123456789012345"))
    }
    
    func testMastercard() throws {
        XCTAssertTrue(validator.validate("5200 8282 8282 8210"))
        XCTAssertTrue(validator.validate("5105 1051 0510 5100"))
    }
    
    func testVisa() throws {
        XCTAssertTrue(validator.validate("4242 4242 4242 4242"))
        XCTAssertTrue(validator.validate("4000 0566 5566 5556"))
    }
    
    func testAmericanExpress() throws {
        XCTAssertTrue(validator.validate("3782 822463 10005"))
        XCTAssertTrue(validator.validate("3714 496353 98431"))
    }
}
