//
//  CardCVVValidatorTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 04/11/2022.
//

import XCTest
@testable import DriveKey

class CardCVVValidatorTests: XCTestCase {
    private var validator: CardCVVValidator!
    
    override func setUpWithError() throws {
        validator = CardCVVValidator()
    }
    
    override func tearDownWithError() throws {
        validator = nil
    }

    func testFailNil() throws {
        XCTAssertFalse(validator.validate(nil))
    }
    
    func testFailWhitespaces() throws {
        XCTAssertFalse(validator.validate(""))
        XCTAssertFalse(validator.validate(" "))
        XCTAssertFalse(validator.validate("         "))
    }
    
    func testFailNonNumericString() throws {
        XCTAssertFalse(validator.validate("a b c d"))
        XCTAssertFalse(validator.validate("abcd"))
        
        XCTAssertFalse(validator.validate("a b c"))
        XCTAssertFalse(validator.validate("abc"))
    }
    
    func testFailNumericString() throws {
        XCTAssertFalse(validator.validate("0"))
        XCTAssertFalse(validator.validate("1"))
        XCTAssertFalse(validator.validate("3"))
        XCTAssertFalse(validator.validate("4"))
        
        XCTAssertFalse(validator.validate("12"))
        XCTAssertFalse(validator.validate("1 2 3 4 5"))
        XCTAssertFalse(validator.validate("1 2  3  4 5"))
        
        XCTAssertFalse(validator.validate("1 2"))
        XCTAssertFalse(validator.validate("1  "))
        
        XCTAssertFalse(validator.validate("1  2"))
        XCTAssertFalse(validator.validate("1 2 "))
        XCTAssertFalse(validator.validate("1   "))
    }
    
    func testSuccessNumericString() throws {
        XCTAssertTrue(validator.validate("123"))
        XCTAssertTrue(validator.validate("1 2 3"))
        XCTAssertTrue(validator.validate("1  2  3"))
        
        XCTAssertTrue(validator.validate("1234"))
        XCTAssertTrue(validator.validate("1 2 3 4"))
        XCTAssertTrue(validator.validate("1  2  3  4"))
    }
}

