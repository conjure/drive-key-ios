//
//  PricePerMileFormatterTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 02/09/2022.
//

import XCTest
@testable import DriveKey

class PricePerMileFormatterTests: XCTestCase {
    
    func testPounds() throws {
        let symbol = "£"
        let fomatter = PricePerMileFormatter(symbol)
        
        let valuesShort = [1, 0.123, 123, 0]
        for value in valuesShort {
            let valueStr = String(value)
            XCTAssertEqual(fomatter.format(valueStr), symbol + valueStr)
        }
        
        let valuesLong = [1.00001, 0.12345, 12345.67, 0.000001]
        for value in valuesLong {
            let valueStr = String(value).prefix(5)
            XCTAssertEqual(fomatter.format(String(valueStr)), symbol + valueStr)
        }
    }
    
    func testDollars() throws {
        let symbol = "$"
        let fomatter = PricePerMileFormatter(symbol)
        
        let valuesShort = [1, 0.123, 123, 0]
        for value in valuesShort {
            let valueStr = String(value)
            XCTAssertEqual(fomatter.format(valueStr), symbol + valueStr)
        }
        
        let valuesLong = [1.00001, 0.12345, 12345.67, 0.000001]
        for value in valuesLong {
            let valueStr = String(value).prefix(5)
            XCTAssertEqual(fomatter.format(String(valueStr)), symbol + valueStr)
        }
    }
}
