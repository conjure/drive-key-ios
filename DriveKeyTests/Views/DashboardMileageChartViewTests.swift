//
//  DashboardMileageChartViewTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 07/09/2022.
//

import XCTest
@testable import DriveKey

class DashboardMileageChartViewTests: XCTestCase {
    private var chartView: DashboardMileageChartView!
    private let monthFormatter = DateFormatter()
    
    override func setUpWithError() throws {
        monthFormatter.dateFormat = "MMMM"
        chartView = DashboardMileageChartView()
    }

    override func tearDownWithError() throws {
        chartView = nil
    }

    func testDataCount() throws {
        let summary_1 = MileageSummary.generateMonths(1, milesStep: 10)
        chartView.update(summary_1, unit: .miles)
        
        XCTAssertEqual(monthFormatter.string(from: summary_1.months![0].startDate!), chartView.labelsBottom[2].text)
        XCTAssertEqual(chartView.lines.count, 1)
        XCTAssertEqual(chartView.labelsTop.count, 3)
        
        let summary_2 = MileageSummary.generateMonths(2, milesStep: 10)
        chartView.update(summary_2, unit: .miles)
        
        XCTAssertEqual(monthFormatter.string(from: summary_2.months![0].startDate!), chartView.labelsBottom[2].text)
        XCTAssertEqual(monthFormatter.string(from: summary_2.months![1].startDate!), chartView.labelsBottom[1].text)
        XCTAssertEqual(chartView.lines.count, 2)
        XCTAssertEqual(chartView.labelsTop.count, 3)
        
        let summary_3 = MileageSummary.generateMonths(3, milesStep: 10)
        chartView.update(summary_3, unit: .miles)
        
        XCTAssertEqual(monthFormatter.string(from: summary_3.months![0].startDate!), chartView.labelsBottom[2].text)
        XCTAssertEqual(monthFormatter.string(from: summary_3.months![1].startDate!), chartView.labelsBottom[1].text)
        XCTAssertEqual(monthFormatter.string(from: summary_3.months![2].startDate!), chartView.labelsBottom[0].text)
        XCTAssertEqual(chartView.lines.count, 3)
        XCTAssertEqual(chartView.labelsTop.count, 3)
    }
}
