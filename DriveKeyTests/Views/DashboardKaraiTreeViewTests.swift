//
//  DashboardKaraiTreeViewTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 08/03/2022.
//

import XCTest
@testable import DriveKey

class DashboardKaraiTreeViewTests: XCTestCase {
    private var treeView: DashboardKaraiTreeView!

    override func setUpWithError() throws {
        let vc = UIViewController()
        vc.view.backgroundColor = .appColor(.navyBlue)
        treeView = DashboardKaraiTreeView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        vc.view.addSubview(treeView)
        let nav = UINavigationController(rootViewController: vc)

        // attach them to a real window
        let window = UIWindow()
        window.rootViewController = nav
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        treeView = nil
    }

    func testTreeViewPresentsCorrectInitialState() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-green"))
        XCTAssertEqual(treeView.imageView.alpha, 0.3, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonNegative() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.negative(1))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-green"))
        XCTAssertEqual(treeView.imageView.alpha, 0.3, accuracy: 0.01)

        treeView.update(.negative(0.25))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-green"))
        XCTAssertEqual(treeView.imageView.alpha, 0.3, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonNeutral() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.neutral)
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-green"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveLessThan100Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveSingle(0.25, value: 25))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-green"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor100Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(1, value: 10))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-1"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveForLessThan200Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(1, value: 1))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-1"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)

        treeView.update(.positiveMulti(1, value: 12))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-1"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)

        treeView.update(.positiveMulti(1, value: 17))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-1"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor200Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(2, value: 200))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-2"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor300Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(3, value: 300))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-3"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor400Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(4, value: 400))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-4"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor500Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(5, value: 500))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-5"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor600Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(6, value: 600))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-6"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor700Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(7, value: 700))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-7"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

    func testTreeViewIsCorrectForCarbonPositiveFor1000Percent() {
        treeView.layoutIfNeeded()
        RunLoop.current.run(until: Date())

        treeView.update(.positiveMulti(10, value: 1000))
        XCTAssertEqual(treeView.imageView.image, UIImage(named: "tracker-tree-blue-7"))
        XCTAssertEqual(treeView.imageView.alpha, 1, accuracy: 0.01)
    }

}
