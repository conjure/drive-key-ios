//
//  OneTimePaymentSummary+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 01/03/2022.
//

import Foundation
@testable import DriveKey

extension OneTimePaymentSummary {
    static var mock: OneTimePaymentSummary {
        return .init(totalCo2Generated: 167, netCo2Price: 5, co2UnitPrice: 0.03, currency: "GBP", paymentMethodExist: true, dkMargin: 0.3, minTransactionFee: 1, totalPrice: 6.5)
    }

    static var mockEmpty: OneTimePaymentSummary {
        return .init(totalCo2Generated: 0, netCo2Price: 0, co2UnitPrice: 0.03, currency: "GBP", paymentMethodExist: true, dkMargin: 0.3, minTransactionFee: 1, totalPrice: 0)
    }
}
