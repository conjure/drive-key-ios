//
//  BubbleProvidable+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 08/02/2022.
//

import Foundation
import Combine
@testable import DriveKey

class BubbleProvidableMock: BubbleProvidable {
    var bubbles: [Bubble] = []

    var syncedSubject: PassthroughSubject<[Bubble], Never> = .init()

    func sync(for user: DrivekeyUser) {}

    func refresh() {
        syncedSubject.send(bubbles)
    }

    func reset() {
        self.bubbles = []
    }

    func add(_ bubbleType: BubbleType) {
        bubbles.append(.init(bubbleType: bubbleType, title: "Test Bubble", message: "Test Bubble Message"))
    }
}
