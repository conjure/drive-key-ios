//
//  VehicleData+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 20/07/2022.
//

import Foundation
@testable import DriveKey

extension VehicleData {
    static func mockVehicles(_ active: Int, inactive: Int) -> [VehicleData] {
        var result = [VehicleData]()
        
        for i in 0..<active {
            let id = "Id " + String(i)
            let plate = "Plate " + String(i)
            let model = "Model " + String(i)
            let make = "Make " + String(i)
            let country = "Country " + String(i)
            let countryCode = "C" + String(i)
            let isPrimary = i == 1
            
            let vehicle = VehicleData(vehicleId: id,
                                      licencePlate: plate,
                                      vrm: plate,
                                      co2Emissions: nil,
                                      model: model,
                                      year: nil,
                                      weight: nil,
                                      fuelType: nil,
                                      fuelConsumptionMin: nil,
                                      fuelConsumptionMax: nil,
                                      bhp: nil,
                                      engineCapacity: nil,
                                      isElectricVehicle: nil,
                                      make: make,
                                      country: country,
                                      countryCode: countryCode,
                                      state: nil,
                                      status: .active,
                                      primary: isPrimary,
                                      vehicleRegistration: nil)
            
            result.append(vehicle)
        }
        
        for i in 0..<inactive {
            let id = "Id " + String(inactive) + String(i)
            let plate = "Plate " + String(inactive) + String(i)
            let model = "Model " + String(inactive) + String(i)
            let make = "Make " + String(inactive) + String(i)
            let country = "Country " + String(inactive) + String(i)
            let countryCode = "C" + String(inactive) + String(i)
            
            let vehicle = VehicleData(vehicleId: id,
                                      licencePlate: plate,
                                      vrm: plate,
                                      co2Emissions: nil,
                                      model: model,
                                      year: nil,
                                      weight: nil,
                                      fuelType: nil,
                                      fuelConsumptionMin: nil,
                                      fuelConsumptionMax: nil,
                                      bhp: nil,
                                      engineCapacity: nil,
                                      isElectricVehicle: nil,
                                      make: make,
                                      country: country,
                                      countryCode: countryCode,
                                      state: nil,
                                      status: .inactive,
                                      primary: false,
                                      vehicleRegistration: nil)
            
            result.append(vehicle)
        }
        
        return result
    }
    
    static var mockVehicleNoVrm: VehicleData {
        let id = "Id 1"
        let plate = "Plate 1"
        let model = "Model 1"
        let make = "Make 1"
        let country = "Country 1"
        let countryCode = "C1"
        let isPrimary = true
        
        let vehicle = VehicleData(vehicleId: id,
                                  licencePlate: plate,
                                  vrm: nil,
                                  co2Emissions: nil,
                                  model: model,
                                  year: nil,
                                  weight: nil,
                                  fuelType: nil,
                                  fuelConsumptionMin: nil,
                                  fuelConsumptionMax: nil,
                                  bhp: nil,
                                  engineCapacity: nil,
                                  isElectricVehicle: nil,
                                  make: make,
                                  country: country,
                                  countryCode: countryCode,
                                  state: nil,
                                  status: .active,
                                  primary: isPrimary,
                                  vehicleRegistration: nil)
        
        return vehicle
    }
    
    static var mockVehicleNoLicencePlates: VehicleData {
        let id = "Id 1"
        let plate = "Plate 1"
        let model = "Model 1"
        let make = "Make 1"
        let country = "Country 1"
        let countryCode = "C1"
        let isPrimary = true
        
        let vehicle = VehicleData(vehicleId: id,
                                  licencePlate: nil,
                                  vrm: plate,
                                  co2Emissions: nil,
                                  model: model,
                                  year: nil,
                                  weight: nil,
                                  fuelType: nil,
                                  fuelConsumptionMin: nil,
                                  fuelConsumptionMax: nil,
                                  bhp: nil,
                                  engineCapacity: nil,
                                  isElectricVehicle: nil,
                                  make: make,
                                  country: country,
                                  countryCode: countryCode,
                                  state: nil,
                                  status: .active,
                                  primary: isPrimary,
                                  vehicleRegistration: nil)
        
        return vehicle
    }
    
    static var mockVehicleData: VehicleData {
        let id = "Id 1"
        let plate = "Plate 1"
        let model = "Model 1"
        let make = "Make 1"
        let country = "Country 1"
        let countryCode = "C1"
        let isPrimary = true
        
        let vehicle = VehicleData(vehicleId: id,
                                  licencePlate: plate,
                                  vrm: plate,
                                  co2Emissions: 100,
                                  model: model,
                                  year: "2022",
                                  weight: 2000,
                                  fuelType: "PETROL",
                                  fuelConsumptionMin: 20,
                                  fuelConsumptionMax: 40,
                                  bhp: 200,
                                  engineCapacity: "2000",
                                  isElectricVehicle: false,
                                  make: make,
                                  country: country,
                                  countryCode: countryCode,
                                  state: nil,
                                  status: .active,
                                  primary: isPrimary,
                                  vehicleRegistration: nil)
        
        return vehicle
    }
}
