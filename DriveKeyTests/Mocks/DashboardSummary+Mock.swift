//
//  DashboardSummary+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 23/09/2022.
//

import Foundation
@testable import DriveKey

extension DashboardSummary {
    static var mockSummary: DashboardSummary {
        let result = DashboardSummary(miles: 20,
                                      trips: 5,
                                      overallEcoDrivingScore: 0.73,
                                      co2PerKiloGram: 50,
                                      co2OffsetPerKiloGram: 150,
                                      hardBrake: 0.7,
                                      hardAcceleration: 0.3,
                                      hardTurn: 0.45,
                                      kiloMeters: 32)
        
        return result
    }
}
