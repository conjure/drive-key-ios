//
//  APIMock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 10/08/2021.
//

@testable import DriveKey
import Foundation

enum APIMockError: Error {
    case mockError
}

class APIMock: APIProtocol {
    var failRequest: Bool = false
    func oneTimePaymentSummary(_ completion: @escaping (Result<OneTimePaymentSummary, Error>) -> Void) {
        guard !failRequest else {
            completion(.failure(APIMockError.mockError))
            return
        }
        completion(.success(.mockEmpty))
    }

    func oneTimePayment(data: OneTimePaymentSummary, completion: @escaping (Result<OneTimePaymentAPIResponse, Error>) -> Void) {
        
    }

    func removePaymentMethod(_ id: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }

    func lookupVehicle(_ vrm: String, country: String, countryCode: String, state: String?, completion: @escaping (Result<VehicleDataLookupResponse, Error>) -> Void) {
        let data: VehicleDataLookupResponse? = APIHelper.serializeJson(filename: "vehicle-data", decodingStrategy: nil)
        completion(.success(data!))
    }

    func registerVehicle(_: VehicleData, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }

    func vehicleData(_: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }
    
    func removeVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func primaryVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func changeVehicle(_ tripId: String, vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func removeTrip(_ tripId: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func changeTrip(_ tripId: String, type: TripType, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }

    func userLinking(_: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }
    
    func authCode(_ completion: @escaping (Result<APIAuthCodeMessage, Error>) -> Void) {
        performRequest(APIAuthCodeMessage.mockResponse, completion: completion)
    }

    func userData(_: UpdateUserPayload, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func disableAccount(_ authCode: String?, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }

    func dismiss(_: Bubble, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }

    func paymentAttach(_: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        completion(.success(APIMessage.mockResponse))
    }

    func paymentAttach(paymentMethodId _: String, type _: String, completion: @escaping (Result<MembershipAttachResponse, Error>) -> Void) {
        completion(.failure(APIMockError.mockError))
    }

    var paymentMethodType: PaymentMethodType = .none
    func paymentMethods(_: String, completion: @escaping (Result<PaymentMethodContainer, Error>) -> Void) {
        switch paymentMethodType {
        case .visa:
            completion(.success(PaymentMethodContainer(paymentMethods: [StripePaymentMethod.visaMock])))
        case .applePay:
            completion(.success(PaymentMethodContainer(paymentMethods: [StripePaymentMethod.applePayMock])))
        case .none:
            completion(.failure(APIMockError.mockError))
        }
    }

    func pay(_: String, type _: String, completion: @escaping (Result<MembershipPayResponse, Error>) -> Void) {
        completion(.failure(APIMockError.mockError))
    }

    func confirm(_: String, completion: @escaping (Result<ConfirmIntentResponse, Error>) -> Void) {
        completion(.failure(APIMockError.mockError))
    }

    var voucherState: VoucherMockResponse = .valid
    
    func redeemVoucher(_ voucherCode: String, completion: @escaping (Result<VoucherAPIResponse, Error>) -> Void) {
        switch voucherState {
        case .valid:
            let mock = VoucherAPIResponse(voucher: "Test-123", amount: 25, status: "active", type: "mock-type")
            completion(.success(mock))
        case .invalid:
            completion(.failure(NetworkManagerError.errorMessage(APIErrorMessage.invalidVoucherResponse.message)))
        case .expired:
            completion(.failure(NetworkManagerError.errorMessage(APIErrorMessage.expiredVoucherResponse.message)))
        case .previouslyRedeemed:
            completion(.failure(NetworkManagerError.errorMessage(APIErrorMessage.redeemedVoucherResponse.message)))
        }
    }
    
    func generateExpense(_ trips: [String], completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    func updateBusiness(_ tripId: String, rate: Double, notes: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        performRequest(APIMessage.mockResponse, completion: completion)
    }
    
    private func performRequest<T>(_ response: T, completion: @escaping (Result<T, Error>) -> Void) where T: Decodable {
        if failRequest {
            completion(.failure(APIMockError.mockError))
        } else {
            completion(.success(response))
        }
    }
}
