//
//  DrivekeyUser+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 05/10/2021.
//

@testable import DriveKey
import Foundation

extension DrivekeyUser {
    static var bob: DrivekeyUser {
        let result = DrivekeyUser(userID: "123",
                                  firstName: "Bob",
                                  lastName: "Bob",
                                  email: "bob@karai.com",
                                  dateOfBirth: nil,
                                  addressLineOne: nil,
                                  addressLineTwo: nil,
                                  addressLineThree: nil,
                                  region: nil,
                                  postcode: nil,
                                  country: nil,
                                  createdAt: Date(),
                                  currencyCode: "GBP",
                                  defaultTripType: .business)
        
        return result
    }
    
    static var mockUser: DrivekeyUser {
        let result = DrivekeyUser(userID: "mock id",
                                  firstName: "Mock",
                                  lastName: "Mock123",
                                  email: "mock@email.com",
                                  dateOfBirth: nil,
                                  addressLineOne: nil,
                                  addressLineTwo: nil,
                                  addressLineThree: nil,
                                  region: nil,
                                  postcode: nil,
                                  country: nil,
                                  createdAt: nil)

        return result
    }
    
    static func mockUserUnits(_ unit: UnitDistance) -> DrivekeyUser {
        let units = Units(distance: unit)
        let result = DrivekeyUser(userID: "mock id",
                                  firstName: "Mock",
                                  lastName: "Mock123",
                                  email: "mock@email.com",
                                  dateOfBirth: nil,
                                  addressLineOne: nil,
                                  addressLineTwo: nil,
                                  addressLineThree: nil,
                                  region: nil,
                                  postcode: nil,
                                  country: nil,
                                  createdAt: nil,
                                  units: units)

        return result
    }
    
    static var mockUserActiveVehicle: DrivekeyUser {
        let result = DrivekeyUser(userID: "mock id",
                                  firstName: "Mock",
                                  lastName: "Mock123",
                                  email: "mock@email.com",
                                  dateOfBirth: nil,
                                  addressLineOne: nil,
                                  addressLineTwo: nil,
                                  addressLineThree: nil,
                                  region: nil,
                                  postcode: nil,
                                  country: nil,
                                  createdAt: nil,
                                  currency: "US Dollar",
                                  currencyCode: "USD")
        
        let vehicle = VehicleData.mockVehicles(1, inactive: 0).first
        result.activeVehicle = vehicle

        return result
    }
    
    static var mockUserCurrencyUSD: DrivekeyUser {
        let result = DrivekeyUser(userID: "mock id",
                                  firstName: "Mock",
                                  lastName: "Mock123",
                                  email: "mock@email.com",
                                  dateOfBirth: nil,
                                  addressLineOne: nil,
                                  addressLineTwo: nil,
                                  addressLineThree: nil,
                                  region: nil,
                                  postcode: nil,
                                  country: nil,
                                  createdAt: nil,
                                  currency: "US Dollar",
                                  currencyCode: "USD")

        return result
    }

    static var mockedDashboardSummary: DrivekeyUser {
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = DashboardSummary(miles: 20,
                                                 trips: 5,
                                                 overallEcoDrivingScore: 0.73,
                                                 co2PerKiloGram: 0,
                                                 co2OffsetPerKiloGram: 0,
                                                 hardBrake: 0.7,
                                                 hardAcceleration: 0.3,
                                                 hardTurn: 0.45,
                                                 kiloMeters: 32)
        return user
    }

    static var mockedNegativeOffset: DrivekeyUser {
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = DashboardSummary(miles: 20,
                                                 trips: 5,
                                                 overallEcoDrivingScore: 0.73,
                                                 co2PerKiloGram: 50,
                                                 co2OffsetPerKiloGram: 10,
                                                 hardBrake: 0.7,
                                                 hardAcceleration: 0.3,
                                                 hardTurn: 0.45,
                                                 kiloMeters: 32)
        return user
    }

    static var mockedNeutralOffset: DrivekeyUser {
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = DashboardSummary(miles: 20,
                                                 trips: 5,
                                                 overallEcoDrivingScore: 0.73,
                                                 co2PerKiloGram: 50,
                                                 co2OffsetPerKiloGram: 50,
                                                 hardBrake: 0.7,
                                                 hardAcceleration: 0.3,
                                                 hardTurn: 0.45,
                                                 kiloMeters: 32)
        return user
    }

    static var mockedPositiveOffset: DrivekeyUser {
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = DashboardSummary(miles: 20,
                                                 trips: 5,
                                                 overallEcoDrivingScore: 0.73,
                                                 co2PerKiloGram: 50,
                                                 co2OffsetPerKiloGram: 233,
                                                 hardBrake: 0.7,
                                                 hardAcceleration: 0.3,
                                                 hardTurn: 0.45,
                                                 kiloMeters: 32)
        return user
    }
}
