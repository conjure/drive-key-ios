//
//  Currency+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 31/05/2022.
//

import Foundation
@testable import DriveKey

extension Currency {
    static var mockCurrencies: [Currency] {
        [
            Currency(code: "GBP", name: "Britain Pound Sterling"),
            Currency(code: "USD", name: "US Dollars")
        ]
    }
}
