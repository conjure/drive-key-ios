//
//  APIResponse+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 05/10/2021.
//

@testable import DriveKey
import Foundation

extension APIMessage {
    static var mockResponse: APIMessage {
        let result = APIMessage(message: "APIResponse mock")
        return result
    }
}

extension APIAuthCodeMessage {
    static var mockResponse: APIAuthCodeMessage {
        let result = APIAuthCodeMessage(authCode: "dummy-code")
        return result
    }
}
