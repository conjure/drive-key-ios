//
//  Voucher+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

@testable import DriveKey
import Foundation

enum VoucherMockResponse {
    case valid
    case invalid
    case expired
    case previouslyRedeemed
}

extension APIErrorMessage {
    static var invalidVoucherResponse: APIErrorMessage {
        return .init(code: 404, message: "Invalid voucher code CARBON-16F5", success: false)
    }

    static var expiredVoucherResponse: APIErrorMessage {
        return .init(code: 400, message: "Expired voucher code CARBON-16F5", success: false)
    }

    static var redeemedVoucherResponse: APIErrorMessage {
        return .init(code: 400, message: "You have redeemed the voucher CARBON-16F5 already", success: false)
    }
}
