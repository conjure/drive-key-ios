//
//  AppDataProvidable+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 24/05/2022.
//

import Foundation
import Combine
@testable import DriveKey

enum AppDataError: Error {
    case genericError
}

class AppDataProvidableMock: AppDataProvidable {
    var isUpdateDefaultCurrencyCalled = false
    var dataUpdated = PassthroughSubject<Bool, Never>()
    
    var fetchedCountries = [Country]()
    var fetchedCurrencies = [Currency]()
    
    func updateDefaultCurrency(_ user: DrivekeyUser) {
        isUpdateDefaultCurrencyCalled = true
    }
    
    var countries: [Country] {
        fetchedCountries
    }
    
    var currencies: [Currency] {
        fetchedCurrencies
    }
}
