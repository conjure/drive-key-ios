//
//  UITableViewMock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 20/07/2022.
//

import UIKit

class UITableViewMock: UITableView {
    var isReloadCalled = false
    var selectedRows = [IndexPath]()
    
    override func reloadData() {
        isReloadCalled = true
    }
    
    func selectRow(_ indexPath: IndexPath) {
        selectedRows.append(indexPath)
        delegate?.tableView?(self, didSelectRowAt: indexPath)
    }
    
    func deselectRow(_ indexPath: IndexPath) {
        if let index = selectedRows.firstIndex(of: indexPath) {
            selectedRows.remove(at: index)
        }
        delegate?.tableView?(self, didDeselectRowAt: indexPath)
    }
    
    override var indexPathsForSelectedRows: [IndexPath]? {
        selectedRows
    }
}
