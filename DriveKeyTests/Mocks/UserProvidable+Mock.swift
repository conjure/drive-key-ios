//
//  UserProvidable+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 02/02/2022.
//

import Foundation
import Combine
@testable import DriveKey

class UserProvidableMock: UserProvidable {
    var currentUser: DrivekeyUser?

    var lastTrip: Trip?
    var vehicles: [VehicleData]?
    var vehiclesActive: [VehicleData]?
    
    var lastMotionTrip: DriveKey.MotionTrip?

    var syncedSubject: PassthroughSubject<DrivekeyUser, Never>

    init() {
        self.syncedSubject = PassthroughSubject<DrivekeyUser, Never>()

        let user = DrivekeyUser.bob
        setUser(user)
    }

    func setUser(_ user: DrivekeyUser) {
        self.currentUser = user
        self.syncedSubject.send(user)
    }

    func setVehicle(_ vehicle: VehicleData) {
        self.currentUser?.activeVehicle = vehicle
    }
    
    func setVehicles(_ vehicles: [VehicleData]) {
        self.vehicles = vehicles
    }
    
    func setVehiclesActive(_ vehicles: [VehicleData]) {
        self.vehiclesActive = vehicles
    }

    func reset() {
        self.currentUser = nil
    }
    
    var mileageSummary: MileageSummary? {
        nil
    }
}
