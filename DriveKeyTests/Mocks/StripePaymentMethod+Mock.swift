//
//  StripePaymentMethod+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 18/02/2022.
//

// swiftlint:disable force_try

import Foundation
@testable import DriveKey

enum PaymentMethodType {
    case visa
    case applePay
    case none
}

extension StripePaymentMethod {
    static var visaMock: StripePaymentMethod {
        let jsonString = """
{
      "id": "pm_1EUmyr2x6R10KRrhlYS3l97f",
      "object": "payment_method",
      "billing_details": {
        "address": {
          "city": null,
          "country": null,
          "line1": null,
          "line2": null,
          "postal_code": null,
          "state": null
        },
        "email": null,
        "name": null,
        "phone": null
      },
      "card": {
        "brand": "visa",
        "checks": {
          "address_line1_check": null,
          "address_postal_code_check": null,
          "cvc_check": null
        },
        "country": "US",
        "exp_month": 8,
        "exp_year": 2020,
        "fingerprint": "0Kibh5yAgbiiwPEL",
        "funding": "credit",
        "generated_from": null,
        "last4": "4242",
        "networks": {
          "available": [
            "visa"
          ],
          "preferred": null
        },
        "three_d_secure_usage": {
          "supported": true
        },
        "wallet": null
      },
      "created": 1556596209,
      "customer": null,
      "livemode": false,
      "metadata": {},
      "type": "card"
}
"""

        let jsonData = Data(jsonString.utf8)
        let decoder = JSONDecoder()

        let method = try! decoder.decode(StripePaymentMethod.self, from: jsonData)
        return method
    }

    static var applePayMock: StripePaymentMethod {
        let jsonString = """
{
   "id":"pm_1KVbqcIUJIhCdmPffoGGk7Xp",
   "object":"payment_method",
   "billing_details":{
      "address":{
         "city":"London",
         "country":"GB",
         "line1":"16 Cole Street",
         "line2":null,
         "postal_code":"SE1 4YH",
         "state":null
      },
      "email":null,
      "name":"Bob Nelson",
      "phone":null
   },
   "card":{
      "brand":"visa",
      "checks":{
         "address_line1_check":"pass",
         "address_postal_code_check":"pass",
         "cvc_check":null
      },
      "country":"US",
      "exp_month":12,
      "exp_year":2023,
      "fingerprint":"7TES2n2T5XtXBpML",
      "funding":"credit",
      "generated_from":null,
      "last4":"4242",
      "networks":{
         "available":[
            "visa"
         ],
         "preferred":null
      },
      "three_d_secure_usage":{
         "supported":true
      },
      "wallet":{
         "apple_pay":{

         },
         "dynamic_last4":"4242",
         "type":"apple_pay"
      }
   },
   "created":1645449758,
   "customer":"cus_L45VLG3FP15IlZ",
   "livemode":false,
   "metadata":{

   },
   "type":"card"
}
"""

        let jsonData = Data(jsonString.utf8)
        let decoder = JSONDecoder()

        let method = try! decoder.decode(StripePaymentMethod.self, from: jsonData)
        return method
    }
}
