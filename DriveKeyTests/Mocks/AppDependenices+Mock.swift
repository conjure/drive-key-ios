//
//  AppDependenices+Mock.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 10/02/2022.
//

import Foundation
@testable import DriveKey

extension AppDependencies {
    static var MockObject: AppDependencies {
        let apiMock = APIMock()
        let dynamicLinkService = DynamicLinkService()
        let locationService = LocationService()
        let motionService = MotionService()
        let permissionService = PermissionService(location: locationService,
                                                  notification: NotificationService(),
                                                  motion: motionService)
        let dependencies = AppDependencies(api: apiMock,
                                           authService: AuthService(),
                                           dynamicLinkService: dynamicLinkService,
                                           biometricService: BiometricService(),
                                           userService: UserProvidableMock(),
                                           bubbleService: BubbleProvidableMock(),
                                           permissionService: permissionService,
                                           appDataService: AppDataProvidableMock(),
                                           tripsMonitor: TripsMonitor(locationService, motion: motionService, tripUploader: TripsUploader()))
        return dependencies
    }
}
