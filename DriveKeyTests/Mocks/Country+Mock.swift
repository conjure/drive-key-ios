//
//  Country+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 30/05/2022.
//

import Foundation
@testable import DriveKey

extension Country {
    static var mockCountries: [Country] {
        [
            Country(code: "c1", name: "n1", states: [CountryState]()),
            Country(code: "c2", name: "n2", states: [CountryState]()),
            Country(code: "us", name: "United States", states: [CountryState]()),
            Country(code: "gb", name: "United Kingdom", states: [CountryState]())
        ]
    }
}
