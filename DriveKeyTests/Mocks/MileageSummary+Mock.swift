//
//  MileageSummary+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 07/09/2022.
//

import Foundation
import Firebase
@testable import DriveKey

extension MileageSummary {
    
    static func generateMonths(_ pastMonths: Int, milesStep: Double) -> MileageSummary {
        var result = [MonthSummary]()
        
        let today = Date()
        for i in 0..<pastMonths {
            let monthValue = -i
            let monthDate = Calendar.current.date(byAdding: .month, value: monthValue, to: today)!
            
            let miles = 10 + Double(i) * milesStep
            let total = miles * 0.1
            let timestamp = Timestamp(date: monthDate)
            
            let monthSummary = MonthSummary(currencyCode: "GBP",
                                            kiloMeters: 0,
                                            miles: miles,
                                            month: nil,
                                            total: total,
                                            start: nil,
                                            startAt: timestamp,
                                            tripId: "\(i)",
                                            tripIds: nil,
                                            trips: 1,
                                            uid: nil,
                                            unitsOfDistance: nil,
                                            year: nil)
            result.append(monthSummary)
        }
        
        return MileageSummary(months: result)
    }
}
