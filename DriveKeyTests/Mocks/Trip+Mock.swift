//
//  Trip+Mock.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 02/08/2022.
//

import Foundation
import Firebase
@testable import DriveKey

extension Trip {
    static var mockTrip: Trip {
        let result = Trip(id: "1",
                          start: "s1",
                          startAt: nil,
                          end: "e1",
                          endAt: nil,
                          ecoDriverScore: 1,
                          miles: 2,
                          kiloMeters: 3.22,
                          tripEvents: 1,
                          adjustedCo2PerKiloGram: 1,
                          co2EstPrice: 0.03,
                          end_location: nil,
                          estimatedRate: Trip.EstimatedRate(currencyCode: "GBP", rate: 0.5, total: 1, unitsOfDistance: .miles),
                          start_location: nil,
                          licencePlate: "",
                          vehicleId: "v1",
                          tripType: .personal,
                          businessNotes: nil,
                          businessReason: nil,
                          distance: 3.22, waypoints: nil)
        return result
    }
    
    static var mockTripBusiness: Trip {
        let result = Trip(id: "1",
                          start: "s1",
                          startAt: nil,
                          end: "e1",
                          endAt: nil,
                          ecoDriverScore: 1,
                          miles: 2,
                          kiloMeters: 3.22,
                          tripEvents: 1,
                          adjustedCo2PerKiloGram: 1, co2EstPrice: 0.03,
                          end_location: nil,
                          estimatedRate: Trip.EstimatedRate(currencyCode: "GBP", rate: 0.5, total: 1, unitsOfDistance: .miles),
                          start_location: nil,
                          licencePlate: "",
                          vehicleId: "v1",
                          tripType: .business,
                          businessNotes: "notes",
                          businessReason: "reason",
                          distance: 3.22, waypoints: nil)
        return result
    }
    
    static var tripNoStartAt: Trip {
        let result = Trip(id: "1",
                          start: "2023-06-15T12:00:00+01:00",
                          startAt: nil,
                          end: "2023-06-15T13:00:00+01:00",
                          endAt: nil,
                          ecoDriverScore: 1,
                          miles: 2,
                          kiloMeters: 3.22,
                          tripEvents: 1,
                          adjustedCo2PerKiloGram: 1,
                          co2EstPrice: 0.03,
                          end_location: nil,
                          estimatedRate: Trip.EstimatedRate(currencyCode: "GBP", rate: 0.5, total: 1, unitsOfDistance: .miles),
                          start_location: nil,
                          licencePlate: "",
                          vehicleId: "v1",
                          tripType: .personal,
                          businessNotes: nil,
                          businessReason: nil,
                          distance: 3.22, waypoints: nil)
        return result
    }
    
    static var tripNoStart: Trip {
        let start = DateFormatter.yyyyMMddTHHmmssXXXXX.date(from: "2023-06-15T12:00:00+01:00")!
        let end = DateFormatter.yyyyMMddTHHmmssXXXXX.date(from: "2023-06-15T13:00:00+01:00")!
        
        let result = Trip(id: "1",
                          start: nil,
                          startAt: Timestamp(date: start),
                          end: nil,
                          endAt: Timestamp(date: end),
                          ecoDriverScore: 1,
                          miles: 2,
                          kiloMeters: 3.22,
                          tripEvents: 1,
                          adjustedCo2PerKiloGram: 1,
                          co2EstPrice: 0.03,
                          end_location: nil,
                          estimatedRate: Trip.EstimatedRate(currencyCode: "GBP", rate: 0.5, total: 1, unitsOfDistance: .miles),
                          start_location: nil,
                          licencePlate: "",
                          vehicleId: "v1",
                          tripType: .personal,
                          businessNotes: nil,
                          businessReason: nil,
                          distance: 3.22, waypoints: nil)
        return result
    }
    
    static var tripNoDates: Trip {
        let result = Trip(id: "1",
                          start: nil,
                          startAt: nil,
                          end: nil,
                          endAt: nil,
                          ecoDriverScore: 1,
                          miles: 2,
                          kiloMeters: 3.22,
                          tripEvents: 1,
                          adjustedCo2PerKiloGram: 1,
                          co2EstPrice: 0.03,
                          end_location: nil,
                          estimatedRate: Trip.EstimatedRate(currencyCode: "GBP", rate: 0.5, total: 1, unitsOfDistance: .miles),
                          start_location: nil,
                          licencePlate: "",
                          vehicleId: "v1",
                          tripType: .personal,
                          businessNotes: nil,
                          businessReason: nil,
                          distance: 3.22, waypoints: nil)
        return result
    }
    
    static func generateTrips(_ pastMonths: Int, countPerMonth: Int) -> [Trip] {
        var result = [Trip]()
        
        let today = Date()
        for i in 0..<pastMonths {
            let monthValue = -i
            let monthDate = Calendar.current.date(byAdding: .month, value: monthValue, to: today)!
            let monthStr = DateFormatter.yyyyMMddTHHmmssXXXXX.string(from: monthDate)
            
            for j in 0..<countPerMonth {
                let id = String(i) + String(j)
                let location = TripLocation(longitude: 1, latitude: 1)
                let trip = Trip(id: id,
                                start: monthStr,
                                startAt: nil,
                                end: monthStr,
                                endAt: nil,
                                ecoDriverScore: 1,
                                miles: 1,
                                kiloMeters: 1.6,
                                tripEvents: 1,
                                adjustedCo2PerKiloGram: 1,
                                co2EstPrice: 0.03,
                                end_location: location,
                                estimatedRate: nil,
                                start_location: location,
                                licencePlate: "license-plate-" + id,
                                vehicleId: "vehicle-id-" + id,
                                tripType: .personal,
                                businessNotes: nil,
                                businessReason: nil,
                                distance: 3.22, waypoints: nil)
                result.append(trip)
            }
        }
        
        return result
    }
}
