//
//  MotionTrip+Extension.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 12/12/2023.
//

import Foundation
import Firebase
@testable import DriveKey

extension MotionTrip {
    static var mockTrip: MotionTrip {
        let startDate = Date().addingTimeInterval(-30*60)
        let endDate = Date()

        let trip = MotionTrip(Timestamp(date: startDate),
                                      endAt: Timestamp(date: endDate),
                                      waypoints: [Waypoint](),
                                      type: MotionActivityType.automotive)
        return trip
    }
}
