//
//  ProfileViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import XCTest
@testable import DriveKey
import Combine
import LocalAuthentication

class ProfileViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        let vc = ViewController()
        self.navController = NavigationController(rootViewController: vc)
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)

        // attach them to a real window
        let window = UIWindow()
        window.rootViewController = navController
        window.makeKeyAndVisible()
        vc.loadViewIfNeeded()

        cancellables = []
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
        cancellables = nil
    }

    func testProfileHeaderShowsCorrectName() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)

        appCoordinator?.presentProfile()
        RunLoop.current.run(until: Date())

        // run the assertions
        let presentedViewController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedViewController)
        let profileVC = presentedViewController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileVC)
        profileVC?.loadViewIfNeeded()

        XCTAssertEqual(profileVC?.headerView.name, "Mock")
    }

    func testProfileHeaderShowsCorrectEmail() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)

        appCoordinator?.presentProfile()
        RunLoop.current.run(until: Date())

        // run the assertions
        let presentedViewController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedViewController)
        let profileVC = presentedViewController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileVC)
        profileVC?.loadViewIfNeeded()

        XCTAssertEqual(profileVC?.headerView.email, "mock@email.com")
    }

    func testProfileHeaderUpdatesAfterChangingName() {
        let navigationController = appCoordinator?.navigationController

        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)

        appCoordinator?.presentProfile()
        RunLoop.current.run(until: Date())

        // run the assertions
        let presentedViewController = navigationController?.presentedViewController as? NavigationController
        let profileVC = presentedViewController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileVC)
        profileVC?.loadViewIfNeeded()
        profileVC?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        XCTAssertEqual(profileVC?.headerView.name, "Mock")

        let ex: XCTestExpectation? = expectation(description: "updating_user_name")

        profileVC?.viewModel.userService.syncedSubject
            .sink { user in
                print(user.firstName ?? "")
                ex?.fulfill()
            }
            .store(in: &cancellables)

        let updatedUser = DrivekeyUser(userID: "mock id_2",
                                                    firstName: "Karai",
                                                    lastName: "Mock123",
                                                    email: "mock@email.com",
                                                    dateOfBirth: nil,
                                                    addressLineOne: nil,
                                                    addressLineTwo: nil,
                                                    addressLineThree: nil,
                                                    region: nil,
                                                    postcode: nil,
                                                    country: nil,
                                                    createdAt: nil)
        appCoordinator.dependencies.userService.setUser(updatedUser)
        wait(for: [ex!], timeout: 1)

        let newEx: XCTestExpectation? = expectation(description: "checking_user_name")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            XCTAssertEqual(profileVC?.headerView.name, "Karai")
            newEx?.fulfill()
        }
        wait(for: [newEx!], timeout: 2)
    }

    func testProfileDoesNotShowBiometricsSectionWhenNotPresent() {
        // A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.biometryNotEnrolled))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                false
            }

            override var biometryType: LABiometryType {
                .none
            }
        }

        let navigationController = appCoordinator?.navigationController

        appCoordinator.dependencies.biometricService = BiometricService(context: StubLAContext())
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)

        appCoordinator?.presentProfile()
        RunLoop.current.run(until: Date())

        // run the assertions
        let presentedViewController = navigationController?.presentedViewController as? NavigationController
        let profileVC = presentedViewController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileVC)
        profileVC?.loadViewIfNeeded()
        profileVC?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        let sections = profileVC?.viewModel.sections.filter({$0 == .security})
        XCTAssert(sections?.isEmpty == true)
    }

    func testProfileDoesShowBiometricsSectionWhenPresent() {
        // A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(true, nil)
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }
        }

        let navigationController = appCoordinator?.navigationController

        appCoordinator.dependencies.biometricService = BiometricService(context: StubLAContext())
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)

        appCoordinator?.presentProfile()
        RunLoop.current.run(until: Date())

        // run the assertions
        let presentedViewController = navigationController?.presentedViewController as? NavigationController
        let profileVC = presentedViewController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileVC)
        profileVC?.loadViewIfNeeded()
        profileVC?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        let sections = profileVC?.viewModel.sections.filter({$0 == .security})
        XCTAssertEqual(sections?.count, 1)
    }

}
