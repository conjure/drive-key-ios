//
//  PaymentMethodListViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 18/02/2022.
//

import XCTest
@testable import DriveKey

class PaymentMethodListViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!

    override func setUpWithError() throws {
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
    }

    func testPaymentMethodsScreenHasCorrectTitle() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.debugShowPaymentMethods()
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? PaymentMethodListViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertEqual(paymentMethodsList?.title, "profile.account.payment".localized)
    }

    func testPaymentMethodsScreenHasTableView() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.debugShowPaymentMethods()
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? PaymentMethodListViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertNotNil(paymentMethodsList?.paymentMethodTableView,
                            "Controller should have a tableview")
    }

    func testPaymentMethodsScreenIsEmptyWhenNoCustomerId() {
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.debugShowPaymentMethods()
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? PaymentMethodListViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        let emptyCells = paymentMethodsList?.dataSource.paymentMethods.isEmpty
        XCTAssert(emptyCells == true)
    }

    func testPaymentMethodsScreenShowsPaymentMethodWhenPresent() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .visa

        appCoordinator?.debugShowPaymentMethods()
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? PaymentMethodListViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        let sectionCount = paymentMethodsList?.paymentMethodTableView.numberOfSections
        XCTAssertEqual(sectionCount, 1)

        let rowCount = paymentMethodsList?.paymentMethodTableView.numberOfRows(inSection: 0)
        XCTAssertEqual(rowCount, 1)
    }
}
