//
//  ExistingPaymentMethodViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 28/02/2022.
//

import XCTest
@testable import DriveKey

class ExistingPaymentMethodViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!

    override func setUpWithError() throws {
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
    }

    func testExistingPaymentMethodsScreenHasCorrectTitle() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertEqual(paymentMethodsList?.title, "profile.account.payment".localized)
    }

    func testExistingPaymentMethodsScreenHasTableView() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertNotNil(paymentMethodsList?.tableView,
                            "Controller should have a tableview")
    }

    func testExistingPaymentMethodsShowsAddButton() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .visa

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertNotNil(paymentMethodsList?.tableView.tableFooterView as? AddPaymentMethodFooterView)
    }

    func testExistingPaymentMethodsHidesAddButtonWhenNoData() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .none

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertNil(paymentMethodsList?.tableView.tableFooterView as? AddPaymentMethodFooterView)
    }

    func testExistingPaymentMethodsScreenShowsPaymentMethodWhenPresent() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .visa

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        let sectionCount = paymentMethodsList?.tableView.numberOfSections
        XCTAssertEqual(sectionCount, 1)

        let rowCount = paymentMethodsList?.tableView.numberOfRows(inSection: 0)
        XCTAssertEqual(rowCount, 1)
    }

    func testExistingPaymentMethodsScreenShowsCorrectCardNumber() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .visa

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        let tableView = paymentMethodsList!.tableView!
        let cell = tableView.dataSource?.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? PaymentMethodCell
        XCTAssertNotNil(cell)
        XCTAssertEqual(cell?.nameLabel.text, "\u{2022}\u{2022}\u{2022}\u{2022} 4242")
        XCTAssertTrue(cell?.disclosureIcon.isHidden == true)
    }

    func testExistingPaymentMethodsScreenHidesApplePayButtonWhenVisa() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .visa

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertTrue(paymentMethodsList?.applePayButton.isHidden == true)
    }

    func testExistingPaymentMethodsScreenShowsApplePayButtonWhenCurrentMethodIsApplePay() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .applePay

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        XCTAssertNotNil(paymentMethodsList?.applePayButton)
        XCTAssertTrue(paymentMethodsList?.applePayButton.isHidden == false)
    }

    func testExistingPaymentMethodsScreenShowsCorrectButtonTitle() {
        let navigationController = appCoordinator?.navigationController
        let user = DrivekeyUser.mockUser
        user.customerId = "mock123"
        appCoordinator.dependencies.userService.setUser(user)
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.paymentMethodType = .applePay

        appCoordinator?.presentExistingPaymentMethod(with: .mock)
        RunLoop.current.run(until: Date())

        let paymentMethodsList = navigationController?.topViewController as? ExistingPaymentMethodViewController
        XCTAssertNotNil(paymentMethodsList)
        paymentMethodsList?.loadViewIfNeeded()

        let buttonTitle = paymentMethodsList?.payButton.title(for: .normal)
        XCTAssertEqual(buttonTitle, "Pay £6.50")
    }
}
