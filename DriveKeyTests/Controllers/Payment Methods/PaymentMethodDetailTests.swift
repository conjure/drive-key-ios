//
//  PaymentMethodDetailTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 21/02/2022.
//

import XCTest
@testable import DriveKey

class PaymentMethodDetailTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!

    override func setUpWithError() throws {
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
    }

    func testDetailsScreenShowsBrandAsTitle() {
        XCTAssertNotNil(appCoordinator)

        let paymentMethod = PaymentMethod(StripePaymentMethod.visaMock)
        appCoordinator?.presentPaymentMethod(paymentMethod)
        RunLoop.current.run(until: Date())

        let paymentMethodVC = navController?.topViewController as? PaymentMethodDetailViewController
        XCTAssertNotNil(paymentMethodVC)
        paymentMethodVC?.loadViewIfNeeded()

        XCTAssertEqual(paymentMethodVC?.title, "Visa")
    }

    func testDetailsScreenShowsApplePayAsTitle() {
        XCTAssertNotNil(appCoordinator)

        let paymentMethod = PaymentMethod(StripePaymentMethod.applePayMock)
        appCoordinator?.presentPaymentMethod(paymentMethod)
        RunLoop.current.run(until: Date())

        let paymentMethodVC = navController?.topViewController as? PaymentMethodDetailViewController
        XCTAssertNotNil(paymentMethodVC)
        paymentMethodVC?.loadViewIfNeeded()

        XCTAssertEqual(paymentMethodVC?.title, "Apple Pay")
    }

    func testDetailsScreenShowsCorrectDataForVisaCard() {
        XCTAssertNotNil(appCoordinator)

        let paymentMethod = PaymentMethod(StripePaymentMethod.visaMock)
        appCoordinator?.presentPaymentMethod(paymentMethod)
        RunLoop.current.run(until: Date())

        let paymentMethodVC = navController?.topViewController as? PaymentMethodDetailViewController
        XCTAssertNotNil(paymentMethodVC)
        paymentMethodVC?.loadViewIfNeeded()

        XCTAssertFalse(paymentMethodVC?.cardNumberLabel.isHidden == true)
        XCTAssertFalse(paymentMethodVC?.expiryDateLabel.isHidden == true)

        let cardNumber = paymentMethodVC?.cardNumberLabel.text
        let expiryDate = paymentMethodVC?.expiryDateLabel.text

        XCTAssertEqual(cardNumber, "\u{2022}\u{2022}\u{2022}\u{2022} 4242")
        XCTAssertEqual(expiryDate, "08/20")
    }

    func testDetailsScreenShowsCorrectDataForApplePayCard() {
        XCTAssertNotNil(appCoordinator)

        let paymentMethod = PaymentMethod(StripePaymentMethod.applePayMock)
        appCoordinator?.presentPaymentMethod(paymentMethod)
        RunLoop.current.run(until: Date())

        let paymentMethodVC = navController?.topViewController as? PaymentMethodDetailViewController
        XCTAssertNotNil(paymentMethodVC)
        paymentMethodVC?.loadViewIfNeeded()

        XCTAssertFalse(paymentMethodVC?.cardNumberLabel.isHidden == true)
        XCTAssertFalse(paymentMethodVC?.expiryDateLabel.isHidden == true)

        let cardNumber = paymentMethodVC?.cardNumberLabel.text
        let expiryDate = paymentMethodVC?.expiryDateLabel.text

        XCTAssertEqual(cardNumber, "\u{2022}\u{2022}\u{2022}\u{2022} 4242")
        XCTAssertEqual(expiryDate, "12/23")
    }

}
