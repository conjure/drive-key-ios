//
//  VehicleDetailsTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 26/07/2022.
//

import XCTest
import OverlayContainer
@testable import DriveKey

class VehicleDetailsTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navigationController: NavigationController!
    
    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        navigationController = NavigationController()
        appCoordinator = AppCoordinator(navigationController, dependencies: AppDependencies.MockObject)
        
        let window = UIWindow()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navigationController = nil
    }

    func testVehicleDetailsShowFlow() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let profileViewController = presentedNavigationController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileViewController)
        profileViewController?.loadViewIfNeeded()
        
        profileViewController?.delegate?.profileViewController(profileViewController!, didSelect: .myProfile)
        wait(0.1)

        let details = presentedNavigationController?.topViewController as? ProfileDetailViewController
        XCTAssertNotNil(details)
        details?.loadViewIfNeeded()

        details?.delegate?.profileDetailViewController(details!, didTapRow: .myVehicles(plate: ""))
        wait(0.1)
        
        let vehicles = VehicleData.mockVehicles(5, inactive: 0)
        let vehicle = vehicles.first!
        let vehicleListViewController = presentedNavigationController?.topViewController as? VehicleListViewController
        XCTAssertNotNil(vehicleListViewController)
        vehicleListViewController?.delegate?.vehicleListViewController(vehicleListViewController!, didSelect: vehicle, activeVehicles: vehicles)
        wait(0.1)
        
        let vehicleDetailsViewController = presentedNavigationController?.topViewController as? VehicleDetailsViewController
        XCTAssertNotNil(vehicleDetailsViewController)
    }
    
    func testVehicleDetailsRemoveTray() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let vehicles = VehicleData.mockVehicles(5, inactive: 0)
        let vehicle = vehicles.first!
        appCoordinator.presentVehicleDetails(vehicle, activeVehicles: vehicles)
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let vehicleDetailsViewController = presentedNavigationController?.topViewController as? VehicleDetailsViewController
        XCTAssertNotNil(vehicleDetailsViewController)
        vehicleDetailsViewController?.onRemove()
        wait(0.2)
        
        let overlayViewController = vehicleDetailsViewController?.presentedViewController as? OverlayContainerViewController
        XCTAssertNotNil(overlayViewController)
        let removeTray = overlayViewController?.viewControllers.first as? RemoveTrayViewController
        XCTAssertNotNil(removeTray)
    }
    
    func testVehicleDetailsRemoveLastActiveTray() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let vehicles = VehicleData.mockVehicles(1, inactive: 0)
        let vehicle = vehicles.first!
        appCoordinator.presentVehicleDetails(vehicle, activeVehicles: vehicles)
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let vehicleDetailsViewController = presentedNavigationController?.topViewController as? VehicleDetailsViewController
        XCTAssertNotNil(vehicleDetailsViewController)
        vehicleDetailsViewController?.onRemove()
        wait(0.2)
        
        let overlayViewController = vehicleDetailsViewController?.presentedViewController as? OverlayContainerViewController
        XCTAssertNotNil(overlayViewController)
        let removeLastTray = overlayViewController?.viewControllers.first as? RemoveLastTrayViewController
        XCTAssertNotNil(removeLastTray)
    }
    
    func testVehicleDetailsPrimaryTray() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let vehicles = VehicleData.mockVehicles(5, inactive: 0)
        let vehicle = vehicles.first!
        appCoordinator.presentVehicleDetails(vehicle, activeVehicles: vehicles)
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let vehicleDetailsViewController = presentedNavigationController?.topViewController as? VehicleDetailsViewController
        XCTAssertNotNil(vehicleDetailsViewController)
        vehicleDetailsViewController?.onPrimary()
        wait(0.2)
        
        let overlayViewController = vehicleDetailsViewController?.presentedViewController as? OverlayContainerViewController
        XCTAssertNotNil(overlayViewController)
        let primaryTray = overlayViewController?.viewControllers.first as? PrimaryTrayViewController
        XCTAssertNotNil(primaryTray)
        XCTAssertEqual(vehicle.vehicleId, primaryTray?.preselectedVehicle?.vehicleId)
        XCTAssertEqual(true, primaryTray?.shouldDismiss)
        XCTAssertEqual(true, primaryTray?.isUpdate)
    }
}
