//
//  VehicleLookupTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 10/08/2021.
//

@testable import DriveKey
import XCTest

class VehicleLookupTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!

    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
    }

    func testVehicleLookupViewControllerShowData() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.debugShowVehicleLookup(isBubble: false, isProfile: false)
        RunLoop.current.run(until: Date())

        let vehicleLookupViewController = navigationController?.topViewController as? VehicleLookupViewController
        XCTAssertNotNil(vehicleLookupViewController)
        vehicleLookupViewController?.loadViewIfNeeded()
        vehicleLookupViewController?.beginAppearanceTransition(true, animated: false)
        wait(0.1)
        
        let viewModel = vehicleLookupViewController?.viewModel
        XCTAssertNotNil(viewModel)

        vehicleLookupViewController?.vrmTextField.value = "1234567"
        XCTAssertNotNil(vehicleLookupViewController?.vrmTextField.text)
        vehicleLookupViewController?.onSearch()
        wait(0.1)

        let vehicleLookupDataViewController = vehicleLookupViewController?.children.first(where: { $0 is VehicleLookupDataViewController }) as? VehicleLookupDataViewController
        XCTAssertNotNil(vehicleLookupDataViewController)
        let viewModelData = vehicleLookupDataViewController?.viewModel
        XCTAssertNotNil(viewModelData)

        XCTAssertEqual(viewModelData?.co2Emissions, "0.11 kg/km")
        XCTAssertEqual(viewModelData?.fuelType, "DIESEL")
        XCTAssertEqual(viewModelData?.fuelConsumption, "62.8-76.4 mpg")
        XCTAssertEqual(viewModelData?.year, "2015")
        XCTAssertEqual(viewModelData?.bhp, "118")
    }
}
