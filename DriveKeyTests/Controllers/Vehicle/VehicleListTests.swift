//
//  VehicleListTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 21/07/2022.
//

import XCTest
@testable import DriveKey

class VehicleListTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navigationController: NavigationController!
    
    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        navigationController = NavigationController()
        appCoordinator = AppCoordinator(navigationController, dependencies: AppDependencies.MockObject)
        
        let window = UIWindow()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navigationController = nil
    }

    func testVehicleListShowFlow() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let profileViewController = presentedNavigationController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileViewController)
        profileViewController?.loadViewIfNeeded()
        
        profileViewController?.delegate?.profileViewController(profileViewController!, didSelect: .myProfile)
        wait(0.1)

        let details = presentedNavigationController?.topViewController as? ProfileDetailViewController
        XCTAssertNotNil(details)
        details?.loadViewIfNeeded()

        details?.delegate?.profileDetailViewController(details!, didTapRow: .myVehicles(plate: ""))
        wait(0.1)
        
        let vehicleListViewController = presentedNavigationController?.topViewController as? VehicleListViewController
        XCTAssertNotNil(vehicleListViewController)
    }
}
