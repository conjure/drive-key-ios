//
//  AddCurrencyViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 22/08/2022.
//

import XCTest
@testable import DriveKey

class AddCurrencyViewControllerTests: XCTestCase {
    var appCoordinator: AppCoordinator!
    var navigationController: NavigationController!

    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        navigationController = NavigationController()
        appCoordinator = AppCoordinator(navigationController, dependencies: AppDependencies.MockObject)

        let window = UIWindow()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navigationController = nil
    }

    func testCurrencyShowFlowFromLoginRoute() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentLoginRoute()
        wait(0.1)

        let loginRoute = navigationController?.topViewController as? LoginRouteViewController
        XCTAssertNotNil(loginRoute)
        loginRoute?.loadViewIfNeeded()

        loginRoute?.delegate?.loginRouteDidSignupWithApple(loginRoute!)
        wait(0.1)

        let currencyViewController = navigationController?.topViewController as? AddCurrencyViewController
        XCTAssertNotNil(currencyViewController)
        wait(0.1)
    }

    func testCurrencyShowFlowFromCreateAccount() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentLoginRoute()
        wait(0.1)

        let loginRoute = navigationController?.topViewController as? LoginRouteViewController
        XCTAssertNotNil(loginRoute)
        loginRoute?.loadViewIfNeeded()

        loginRoute?.delegate?.loginRouteWillSignup(loginRoute!)
        wait(0.1)

        let signup = navigationController?.topViewController as? SignupViewController
        XCTAssertNotNil(signup)
        signup?.loadViewIfNeeded()

        signup?.delegate?.signupViewControllerDidSignUp(signup!)
        wait(0.1)

        let currencyViewController = navigationController?.topViewController as? AddCurrencyViewController
        XCTAssertNotNil(currencyViewController)
        wait(0.1)
    }

    func testCurrencyShowFlowFromLoginScreen() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentLoginRoute()
        wait(0.1)

        let loginRoute = navigationController?.topViewController as? LoginRouteViewController
        XCTAssertNotNil(loginRoute)
        loginRoute?.loadViewIfNeeded()

        loginRoute?.delegate?.loginRouteWillLogin(loginRoute!)
        wait(0.1)

        let login = navigationController?.topViewController as? LoginViewController
        XCTAssertNotNil(login)
        login?.loadViewIfNeeded()

        login?.delegate?.loginViewControllerDidSignupWithApple(login!)
        wait(0.1)

        let currencyViewController = navigationController?.topViewController as? AddCurrencyViewController
        XCTAssertNotNil(currencyViewController)
        wait(0.1)
    }

}
