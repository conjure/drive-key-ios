//
//  TripHistoryViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 03/08/2022.
//

import XCTest
import Pulley
@testable import DriveKey

class TripHistoryViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navigationController: NavigationController!

    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        navigationController = NavigationController()
        appCoordinator = AppCoordinator(navigationController, dependencies: AppDependencies.MockObject)
        
        let window = UIWindow()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navigationController = nil
    }
    
    func testTripHistoryShowFlow() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUserActiveVehicle)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appDataService?.fetchedCurrencies = Currency.mockCurrencies
        UserDefaults.standard.set(true, forKey: UserDefaultKeys.loggedIn)
        let userService = appCoordinator.dependencies.userService as? UserProvidableMock
        userService?.lastTrip = Trip.mockTrip
        appCoordinator?.start()
        wait(0.1)
                
        let pulleyViewController = navigationController.topViewController as? PulleyViewController
        XCTAssertNotNil(pulleyViewController)
        
        let dashboardViewController = pulleyViewController?.primaryContentViewController as? DashboardViewController
        XCTAssertNotNil(dashboardViewController)
        dashboardViewController?.dashboardContentViewDidTapLastTrip(DashboardContentView())
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let tripHistoryViewController = presentedNavigationController?.topViewController as? TripHistoryViewController
        XCTAssertNotNil(tripHistoryViewController)
    }
}
