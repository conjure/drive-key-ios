//
//  CurrencyViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 31/05/2022.
//

import XCTest
@testable import DriveKey

class CurrencyViewControllerTests: XCTestCase {
    var appCoordinator: AppCoordinator!
    var navigationController: NavigationController!
    
    override func setUpWithError() throws {
        UIView.setAnimationsEnabled(false)
        navigationController = NavigationController()
        appCoordinator = AppCoordinator(navigationController, dependencies: AppDependencies.MockObject)
        
        let window = UIWindow()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navigationController = nil
    }

    func testCurrencyShowFlow() throws {
        appCoordinator.dependencies.userService.setUser(DrivekeyUser.mockUser)
        let appDataService = appCoordinator.dependencies.appDataService as? AppDataProvidableMock
        appDataService?.fetchedCountries = Country.mockCountries
        appCoordinator?.presentProfile()
        wait(0.1)
        
        let presentedNavigationController = navigationController?.presentedViewController as? NavigationController
        XCTAssertNotNil(presentedNavigationController)
        let profileViewController = presentedNavigationController?.topViewController as? ProfileViewController
        XCTAssertNotNil(profileViewController)
        profileViewController?.loadViewIfNeeded()
        
        profileViewController?.delegate?.profileViewController(profileViewController!, didSelect: .payments)
        wait(0.1)

        let details = presentedNavigationController?.topViewController as? ProfileDetailViewController
        XCTAssertNotNil(details)
        details?.loadViewIfNeeded()

        details?.delegate?.profileDetailViewController(details!, didTapRow: .currency("USD"))
        wait(0.1)
        
        let currencyViewController = presentedNavigationController?.topViewController as? CurrencyViewController
        XCTAssertNotNil(currencyViewController)
        wait(0.1)
    }

}
