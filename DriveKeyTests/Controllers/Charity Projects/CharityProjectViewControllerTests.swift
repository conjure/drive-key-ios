//
//  CharityProjectViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 08/03/2022.
//

import XCTest
import Combine
import SafariServices
@testable import DriveKey

class CharityProjectViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!
    private var picker: ProjectPickerDelegate!
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)
        self.picker = .init()
        self.cancellables = []

        // attach them to a real window
        let window = UIWindow()
        window.rootViewController = navController
        window.makeKeyAndVisible()

        appCoordinator.presentCharityProject(.brazilianAmazon, picker: picker, source: .dashboard)
        RunLoop.current.run(until: Date())
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
        picker = nil
        cancellables = nil
    }

    func testScreenLoadsWithCorrectTitle() {
        let nav = navController?.presentedViewController as? NavigationController
        let vc = nav?.topViewController as? CharityProjectViewController
        XCTAssertNotNil(vc)
        vc!.loadViewIfNeeded()
        XCTAssertEqual(vc!.title, CharityProject.brazilianAmazon.screenTitle)
    }

    func testScreenLoadsKaraiWebsiteWhenLinkTapped() {
        let nav = navController?.presentedViewController as? NavigationController
        let vc = nav?.topViewController as? CharityProjectViewController
        XCTAssertNotNil(vc)
        vc!.loadViewIfNeeded()
        vc!.delegate?.charityProjectViewController(vc!, didTap: vc!.viewModel.ctaURL!)

        wait(0.5)
        let safariVC = nav?.presentedViewController as? SFSafariViewController
        XCTAssertNotNil(safariVC)
    }

    func testScreenUpdatesWhenProjectCarouselTapped() {
        let nav = navController?.presentedViewController as? NavigationController
        let vc = nav?.topViewController as? CharityProjectViewController
        XCTAssertNotNil(vc)
        vc!.loadViewIfNeeded()
        XCTAssertEqual(vc!.title, "projects.amazon.detail.title".localized)

        let ex: XCTestExpectation? = expectation(description: "updating_project")

        picker.didChange.sink { _ in
            ex?.fulfill()
        }.store(in: &cancellables)

        picker.project = .midilli

        wait(for: [ex!], timeout: 1)

        XCTAssertEqual(vc!.title, CharityProject.midilli.screenTitle)
    }

}
