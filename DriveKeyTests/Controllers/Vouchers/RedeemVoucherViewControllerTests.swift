//
//  RedeemVoucherViewControllerTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import XCTest
@testable import DriveKey

class RedeemVoucherViewControllerTests: XCTestCase {
    private var appCoordinator: AppCoordinator!
    private var navController: NavigationController!

    override func setUpWithError() throws {
        self.navController = NavigationController()
        self.appCoordinator = AppCoordinator(navController, dependencies: AppDependencies.MockObject)

        // attach them to a real window
        let window = UIWindow()
        window.rootViewController = navController
        window.makeKeyAndVisible()
    }

    override func tearDownWithError() throws {
        appCoordinator = nil
        navController = nil
    }

    func testRedeemVoucherScreenHasCorrectTitles() {
        XCTAssertNotNil(appCoordinator)
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()

        XCTAssertEqual(vc?.title, "voucher.redeem.title".localized)
        XCTAssertEqual(vc?.voucherTextField.name, "voucher.textfield.title".localized)
        XCTAssertEqual(vc?.redeemButton.title(for: .normal), "voucher.redeem.button".localized)
    }

    func testRedeemVoucherDisablesButtonWhenNoText() {
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()

        XCTAssertFalse(vc?.redeemButton.isEnabled == true)
    }

    func testRedeemValidVoucherShowsNoError() {
        let navigationController = appCoordinator?.navigationController

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()
        vc?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        vc?.voucherTextField.textField.text = "CARBON-16CF"
        vc?.redeemVoucher(self)
        RunLoop.current.run(until: Date())

        XCTAssertTrue(vc?.voucherTextField.isError == false)
    }

    func testRedeemInvalidVoucherShowsError() {
        let navigationController = appCoordinator?.navigationController
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.voucherState = .invalid

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()
        vc?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        vc?.voucherTextField.textField.text = "fg-16CF"
        vc?.redeemVoucher(self)
        RunLoop.current.run(until: Date())

        XCTAssertTrue(vc?.voucherTextField.isError == true)
    }

    func testRedeemExpiredVoucherShowsError() {
        let navigationController = appCoordinator?.navigationController
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.voucherState = .expired

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()
        vc?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        vc?.voucherTextField.textField.text = "CARBON-16CF"
        vc?.redeemVoucher(self)
        RunLoop.current.run(until: Date())

        XCTAssertTrue(vc?.voucherTextField.isError == true)
    }

    func testRedeemPreviouslyUsedVoucherShowsError() {
        let navigationController = appCoordinator?.navigationController
        let mock = appCoordinator.dependencies.api as? APIMock
        mock?.voucherState = .previouslyRedeemed

        appCoordinator?.presentVoucherRedeem()
        RunLoop.current.run(until: Date())

        let vc = navigationController?.topViewController as? RedeemVoucherViewController
        XCTAssertNotNil(vc)
        vc?.loadViewIfNeeded()
        vc?.beginAppearanceTransition(true, animated: true)
        RunLoop.current.run(until: Date())

        vc?.voucherTextField.textField.text = "CARBON-16CF"
        vc?.redeemVoucher(self)
        RunLoop.current.run(until: Date())

        XCTAssertTrue(vc?.voucherTextField.isError == true)
    }

}
