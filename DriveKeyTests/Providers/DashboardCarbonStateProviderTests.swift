//
//  DashboardCarbonStateProviderTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 07/03/2022.
//

import XCTest
@testable import DriveKey

class DashboardCarbonStateProviderTests: XCTestCase {
    var provider: DashboardCarbonStateProvider!

    override func setUpWithError() throws {
        self.provider = .init()
    }

    override func tearDownWithError() throws {
        self.provider = nil
    }

    func testProviderReturnsNoDataStateWhenBrandNewUser() {
        provider.updateCarbonData(nil, offset: nil)
        XCTAssertEqual(provider.state, .noData)
    }

    func testProviderReturnsNegativeWhenGeneratedMoreThanOffset() {
        provider.updateCarbonData(1, offset: nil)
        XCTAssertEqual(provider.state, .negative(1))

        provider.updateCarbonData(100, offset: nil)
        XCTAssertEqual(provider.state, .negative(1))

        provider.updateCarbonData(100, offset: 50)
        XCTAssertEqual(provider.state, .negative(0.5))

        provider.updateCarbonData(10, offset: 9)
        XCTAssertEqual(provider.state, .negative(0.9))
    }

    func testProviderReturnsNeutralWhenOffsetIsEqual() {
        provider.updateCarbonData(10, offset: 10)
        XCTAssertEqual(provider.state, .neutral)

        provider.updateCarbonData(1234, offset: 1234)
        XCTAssertEqual(provider.state, .neutral)
    }

    func testProviderReturnsPositiveWhenOffsetIsGreater() {
        provider.updateCarbonData(10, offset: 15)
        XCTAssertEqual(provider.state, .positiveSingle(0.5, value: 5))

        provider.updateCarbonData(10, offset: 20)
        XCTAssertEqual(provider.state, .positiveMulti(1, value: 10))
    }

    func testProviderReturnsPositiveMultipleWhenOffsetGreater100Percent() {
        provider.updateCarbonData(10, offset: 22)
        XCTAssertEqual(provider.state, .positiveMulti(1, value: 12))

        provider.updateCarbonData(10, offset: 30)
        XCTAssertEqual(provider.state, .positiveMulti(2, value: 20))

        provider.updateCarbonData(10, offset: 40)
        XCTAssertEqual(provider.state, .positiveMulti(3, value: 30))

        provider.updateCarbonData(10, offset: 50)
        XCTAssertEqual(provider.state, .positiveMulti(4, value: 40))

        provider.updateCarbonData(10, offset: 60)
        XCTAssertEqual(provider.state, .positiveMulti(5, value: 50))

        provider.updateCarbonData(10, offset: 70)
        XCTAssertEqual(provider.state, .positiveMulti(6, value: 60))

        provider.updateCarbonData(10, offset: 80)
        XCTAssertEqual(provider.state, .positiveMulti(7, value: 70))

        provider.updateCarbonData(10, offset: 90)
        XCTAssertEqual(provider.state, .positiveMulti(8, value: 80))

        provider.updateCarbonData(10, offset: 1000)
        XCTAssertEqual(provider.state, .positiveMulti(99, value: 990))
    }

    func testProviderReturnsPositiveWhenOffsetCarbonWithoutGenerating() {
        provider.updateCarbonData(0, offset: 22)
        XCTAssertEqual(provider.state, .positiveMulti(1, value: 22))

        provider.updateCarbonData(0, offset: 323)
        XCTAssertEqual(provider.state, .positiveMulti(1, value: 323))
    }

}
