//
//  VehicleEnterEmissionsViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 05/10/2022.
//

import XCTest
@testable import DriveKey

class VehicleEnterEmissionsViewModelTests: XCTestCase {
    let userService = UserProvidableMock()
    
    func testFirstVehicleFail() throws {
        let userActiveVehicle = DrivekeyUser.mockUserActiveVehicle
        userService.setUser(userActiveVehicle)
        let viewModel = VehicleEnterEmissionsViewModel(userService, screenSource: .profile, vehicleData: nil, vrm: nil, country: nil, state: nil)
        XCTAssertFalse(viewModel.isFirstVehicle)
    }
    
    func testFirstVehicleSuccess() throws {
        let userNoActiveVehicle = DrivekeyUser.mockUser
        userService.setUser(userNoActiveVehicle)
        let viewModel = VehicleEnterEmissionsViewModel(userService, screenSource: .profile, vehicleData: nil, vrm: nil, country: nil, state: nil)
        XCTAssertTrue(viewModel.isFirstVehicle)
    }

}
