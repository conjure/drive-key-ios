//
//  VehicleLookupDataViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 31/10/2022.
//

import XCTest
@testable import DriveKey

final class VehicleLookupDataViewModelTests: XCTestCase {
    private var viewModel: VehicleLookupDataViewModel!
    private let vehicle = VehicleData.mockVehicleData
    private let numberFormatter = NumberFormatter()
    
    override func setUpWithError() throws {
        viewModel = VehicleLookupDataViewModel(vehicleData: vehicle)
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 0
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    func testVehicleData() throws {
        XCTAssertEqual(viewModel.co2Emissions, "0.10 kg/km")
        XCTAssertEqual(viewModel.model, "Make 1 Model 1")
        XCTAssertEqual(viewModel.year, "2022")
        XCTAssertEqual(viewModel.weight, "2000 Kg")
        XCTAssertEqual(viewModel.fuelType, "PETROL")
        XCTAssertEqual(viewModel.bhp, "200")
        XCTAssertEqual(viewModel.engineCapacity, "2000 cc")
    }
    
    func testVehicleCO2Heading() throws {
        XCTAssertEqual(viewModel.shouldHideCO2CalculationHeading, false)
        
        let viewModelNoHeading = VehicleLookupDataViewModel(vehicleData: VehicleData.mockVehicleNoVrm)
        XCTAssertEqual(viewModelNoHeading.shouldHideCO2CalculationHeading, true)
    }
}
