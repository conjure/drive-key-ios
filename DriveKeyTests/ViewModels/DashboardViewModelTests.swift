//
//  DashboardViewModelTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 07/02/2022.
//

import XCTest
import Combine
@testable import DriveKey

class DashboardViewModelTests: XCTestCase {
    private var viewModel: DashboardViewModel!
    private var userService: UserProvidableMock!
    private var bubbleService: BubbleProvidableMock!
    private let api = APIMock()
    private var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        userService = UserProvidableMock()
        bubbleService = BubbleProvidableMock()
        viewModel = DashboardViewModel(userService: userService,
                                       notificationService: NotificationService(),
                                       api: api,
                                       bubbleProvider: bubbleService, tripsMonitor: AppDependencies.MockObject.tripsMonitor)
    }

    override func tearDownWithError() throws {
        self.viewModel = nil
        self.userService = nil
        self.bubbleService = nil
    }
    
    func testVehicleChangeSuccess() throws {
        api.failRequest = false
        userService.lastTrip = Trip.mockTrip
        
        let ex = XCTestExpectation(description: "vehicle-change-1")
        viewModel.$vehicleChanged
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeLastTripVehicle("vehicle-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testVehicleChangeFail() throws {
        api.failRequest = true
        userService.lastTrip = Trip.mockTrip
        
        let ex = XCTestExpectation(description: "vehicle-change-2")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeLastTripVehicle("vehicle-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testVehicleCount() throws {
        let vehicles = VehicleData.mockVehicles(4, inactive: 2)
        userService.setVehicles(vehicles)
        XCTAssertEqual(4, viewModel.vehicles.count)
    }

    func testSubHeadingWithEmptyVehicle() {
        let heading = viewModel.subHeading
        XCTAssert(heading.isEmpty, "There should be no subheading if a vehicle has not been added")
    }

    func testSubHeadingWithVehicleNoVrm() {
        let vehicle = VehicleData.mockVehicleNoVrm
        let user = userService.currentUser!
        user.activeVehicle = vehicle
        userService.setUser(user)
        
        XCTAssertEqual(viewModel.subHeading, vehicle.licencePlate)
        XCTAssertNil(vehicle.vrm)
    }
    
    func testSubHeadingWithVehicleNoPlates() {
        let vehicle = VehicleData.mockVehicleNoLicencePlates
        let user = userService.currentUser!
        user.activeVehicle = vehicle
        userService.setUser(user)
        
        XCTAssertEqual(viewModel.subHeading, vehicle.vrm)
        XCTAssertNil(vehicle.licencePlate)
    }
    
    func testSubHeadingWithNoUser() {
        self.userService.reset()
        let heading = viewModel.subHeading
        XCTAssert(heading.isEmpty, "There should be no subheading if there is no current user")
    }

    func testSubHeadingWithValidVehicle() {
        let vehicle = VehicleData(vehicleId: "dummy id",
                                  licencePlate: "asdf",
                                  vrm: "asdf",
                                  co2Emissions: 0.25,
                                  model: "Test",
                                  year: "2022",
                                  weight: 200,
                                  fuelType: "",
                                  fuelConsumptionMin: 0,
                                  fuelConsumptionMax: 100,
                                  bhp: 125,
                                  engineCapacity: "",
                                  isElectricVehicle: false,
                                  make: "",
                                  country: "United Kingdom",
                                  countryCode: "UK",
                                  state: nil,
                                  status: .active,
                                  primary: true,
                                  vehicleRegistration: nil)
        self.userService.setVehicle(vehicle)
        let heading = viewModel.subHeading
        XCTAssertEqual(heading, "asdf", "License Plate should match with added vehicle")
    }

    func testCorrectMorningWelcomeMessage() throws {
        var components = DateComponents()
        components.hour = 10
        components.minute = 44
        let date = try XCTUnwrap(Calendar.current.date(from: components))
        let message = viewModel.welcomeMessage(for: date)
        XCTAssertEqual(message, "welcome.message.morning".localized)

        components.hour = 00
        components.minute = 00
        let date2 = try XCTUnwrap(Calendar.current.date(from: components))
        let message2 = viewModel.welcomeMessage(for: date2)
        XCTAssertEqual(message2, "welcome.message.morning".localized)

        components.hour = 06
        components.minute = 44
        let date3 = try XCTUnwrap(Calendar.current.date(from: components))
        let message3 = viewModel.welcomeMessage(for: date3)
        XCTAssertEqual(message3, "welcome.message.morning".localized)

        components.hour = 11
        components.minute = 59
        let date4 = try XCTUnwrap(Calendar.current.date(from: components))
        let message4 = viewModel.welcomeMessage(for: date4)
        XCTAssertEqual(message4, "welcome.message.morning".localized, "\(date4) should return morning message")
    }

    func testCorrectAfternoonWelcomeMessage() throws {
        var components = DateComponents()
        components.hour = 12
        components.minute = 44
        let date = try XCTUnwrap(Calendar.current.date(from: components))
        let message = viewModel.welcomeMessage(for: date)
        XCTAssertEqual(message, "welcome.message.afternoon".localized)

        components.hour = 15
        components.minute = 00
        let date2 = try XCTUnwrap(Calendar.current.date(from: components))
        let message2 = viewModel.welcomeMessage(for: date2)
        XCTAssertEqual(message2, "welcome.message.afternoon".localized)

        components.hour = 16
        components.minute = 59
        let date3 = try XCTUnwrap(Calendar.current.date(from: components))
        let message3 = viewModel.welcomeMessage(for: date3)
        XCTAssertEqual(message3, "welcome.message.afternoon".localized)
    }

    func testCorrectEveningWelcomeMessage() throws {
        var components = DateComponents()
        components.hour = 17
        components.minute = 44
        let date = try XCTUnwrap(Calendar.current.date(from: components))
        let message = viewModel.welcomeMessage(for: date)
        XCTAssertEqual(message, "welcome.message.evening".localized)

        components.hour = 17
        components.minute = 00
        let date2 = try XCTUnwrap(Calendar.current.date(from: components))
        let message2 = viewModel.welcomeMessage(for: date2)
        XCTAssertEqual(message2, "welcome.message.evening".localized)

        components.hour = 23
        components.minute = 59
        let date3 = try XCTUnwrap(Calendar.current.date(from: components))
        let message3 = viewModel.welcomeMessage(for: date3)
        XCTAssertEqual(message3, "welcome.message.evening".localized)
    }

    func testEcoScore() {
        self.userService.setUser(DrivekeyUser.mockedDashboardSummary)
        let score = viewModel.ecoScore
        XCTAssertEqual(score, 0.73)
    }

    func testCO2TotalWithNoUser() {
        self.userService.reset()
        let co2 = viewModel.co2VTotalValue
        XCTAssertEqual(co2, 0)
    }

    func testCO2TotalWithNoSummary() {
        let co2 = viewModel.co2VTotalValue
        XCTAssertEqual(co2, 0)
    }

    func testCO2TotalWithValidUser() {
        self.userService.setUser(DrivekeyUser.mockedNegativeOffset)
        let co2 = viewModel.co2VTotalValue
        XCTAssertEqual(co2, 50)
    }

    func testCO2OffsetWithNoUser() {
        self.userService.reset()
        let co2 = viewModel.co2OffsetValue
        XCTAssertEqual(co2, 0)
    }

    func testCO2OffsetWithNoSummary() {
        let co2 = viewModel.co2OffsetValue
        XCTAssertEqual(co2, 0)
    }

    func testCO2OffsetWithValidUser() {
        self.userService.setUser(DrivekeyUser.mockedDashboardSummary)
        let co2 = viewModel.co2OffsetValue
        XCTAssertEqual(co2, 0)
    }

    func testNetValueWhenOffsetNothing() {
        self.userService.setUser(DrivekeyUser.mockedDashboardSummary)
        let net = viewModel.co2NetValue
        XCTAssertEqual(net, 0)
    }

    func testNetValueWhenOffsetPartial() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 15,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let net = viewModel.co2NetValue
        XCTAssertEqual(net, 35)
    }

    func testNetValueWhenOffsetEverything() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 50,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let net = viewModel.co2NetValue
        XCTAssertEqual(net, 0)
    }

    func testNetValueWhenPositiveOffset() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 150,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let net = viewModel.co2NetValue
        XCTAssertEqual(net, 0)
    }

    func testGetTripsWithNoSummary() {
        let trips = viewModel.trips
        XCTAssertEqual(trips, 0)
    }

    func testGetTripsWithValidUser() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 150,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let trips = viewModel.trips
        XCTAssertEqual(trips, 5)
    }

    func testGetMilesWithNoSummary() {
        let miles = viewModel.distance
        XCTAssertEqual(miles, 0)
    }

    func testGetMiles() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 150,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let miles = viewModel.distance
        XCTAssertEqual(miles, 20)
    }

    func testGetScoresWithNoSummary() {
        let scores = viewModel.scores
        XCTAssert(scores.isEmpty)
    }

    func testGetScoresWithValidSummary() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0.73,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 150,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let scores = viewModel.scores
        XCTAssert(!scores.isEmpty)
        XCTAssertEqual(scores[0], 0.73)
        XCTAssertEqual(scores[1], 0.7)
        XCTAssertEqual(scores[2], 0.3)
        XCTAssertEqual(scores[3], 0.45)
    }

    func testGetScoresWithNoEcoScore() {
        let summary = DashboardSummary(miles: 20,
                                       trips: 5,
                                       overallEcoDrivingScore: 0,
                                       co2PerKiloGram: 50,
                                       co2OffsetPerKiloGram: 150,
                                       hardBrake: 0.7,
                                       hardAcceleration: 0.3,
                                       hardTurn: 0.45,
                                       kiloMeters: 32)
        let user = DrivekeyUser.mockUser
        user.dashboardSummary = summary
        self.userService.setUser(user)

        let scores = viewModel.scores
        XCTAssert(scores.isEmpty)
    }
    
    func testUnits() {
        let summary = DashboardSummary.mockSummary
        
        let userMiles = DrivekeyUser.mockUserUnits(.miles)
        userMiles.dashboardSummary = summary
        userService.setUser(userMiles)
        XCTAssertEqual(viewModel.distance, Int(summary.miles!))
        
        let userKilometers = DrivekeyUser.mockUserUnits(.kilometers)
        userKilometers.dashboardSummary = summary
        userService.setUser(userKilometers)
        XCTAssertEqual(viewModel.distance, Int(summary.kiloMeters!))
    }

    func testHasTrayBubblesWithNoBubbles() {
        XCTAssertFalse(viewModel.hasTrayBubbles)
    }

    func testHasTrayBubblesWithOnlyVehicleRegBubble() {
        bubbleService.add(.vehicleReg)
        XCTAssertFalse(viewModel.hasTrayBubbles)
    }

    func testHasTrayBubblesWithOnlyBiometricBubble() {
        bubbleService.add(.biometric)
        XCTAssert(viewModel.hasTrayBubbles)
    }

    func testHasTrayBubblesWithDashboardBubbles() {
        bubbleService.add(.vehicleReg)
        bubbleService.add(.locationPermission)
        XCTAssertFalse(viewModel.hasTrayBubbles)
    }

    func testHasTrayBubblesWithMixtureBubbles() {
        bubbleService.add(.vehicleReg)
        bubbleService.add(.locationPermission)
        bubbleService.add(.biometric)
        bubbleService.add(.expiredCard)
        XCTAssert(viewModel.hasTrayBubbles)
    }

}
