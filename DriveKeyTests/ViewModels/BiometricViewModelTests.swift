//
//  BiometricViewModelTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 09/08/2021.
//

import Combine
@testable import DriveKey
import LocalAuthentication
import XCTest

class BiometricViewModelTests: XCTestCase {
    private var viewModel: BiometricViewModel!
    private var userDefaults: UserDefaults!
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        userDefaults = UserDefaults(suiteName: #file)
        userDefaults.removePersistentDomain(forName: #file)

        cancellables = []
    }

    override func tearDownWithError() throws {
        userDefaults = nil
        viewModel = nil
        cancellables = nil
    }

    func testSuccessfullAuthentication() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(true, nil)
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }
        }

        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        var didAccept = false
        var error: Error?
        let expectation = self.expectation(description: "Biometric Authentication")

        viewModel.enableBiometrics()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(encounteredError):
                    error = encounteredError
                }

                // Fullfilling our expectation to unblock
                // our test execution:
                expectation.fulfill()
            }, receiveValue: { value in
                didAccept = value
            })
            .store(in: &cancellables)

        // Awaiting fulfilment of our expecation before
        // performing our asserts:
        waitForExpectations(timeout: 10)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        // Asserting that our Combine pipeline yielded the
        // correct output:
        XCTAssertNil(error)
        XCTAssertTrue(didAccept)
        XCTAssertEqual(bioStatus, 1, "Biometric Status should be 1 on success")
    }

    func testUserFallbackUpdatesBiometricStatus() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.userFallback))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }
        }

        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        var didAccept = false
        var error: Error?
        let expectation = self.expectation(description: "Biometric Authentication")

        viewModel.enableBiometrics()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(encounteredError):
                    error = encounteredError
                }

                // Fullfilling our expectation to unblock
                // our test execution:
                expectation.fulfill()
            }, receiveValue: { value in
                didAccept = value
            })
            .store(in: &cancellables)

        // Awaiting fulfilment of our expecation before
        // performing our asserts:
        waitForExpectations(timeout: 10)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        // Asserting that our Combine pipeline yielded the
        // correct output:
        XCTAssertNil(error)
        XCTAssertFalse(didAccept)
        XCTAssertEqual(bioStatus, 0, "Biometric Status should be -1 on user fallback")
    }

    func testDeviceNotEnrolledUpdatesBiometricStatus() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.biometryNotEnrolled))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                false
            }

            override var biometryType: LABiometryType {
                .none
            }
        }

        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        var error: Error?
        let expectation = self.expectation(description: "Biometric Authentication")

        viewModel.enableBiometrics()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(encounteredError):
                    error = encounteredError
                }

                // Fullfilling our expectation to unblock
                // our test execution:
                expectation.fulfill()
            }, receiveValue: { _ in
            })
            .store(in: &cancellables)

        // Awaiting fulfilment of our expecation before
        // performing our asserts:
        waitForExpectations(timeout: 10)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        XCTAssertNotNil(error)
        XCTAssertEqual(bioStatus, -2, "Biometric Status should be -2 when device not enrolled")
    }

    func testDeviceDeclinedFaceID() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.biometryNotAvailable))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .faceID
            }
        }

        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        var didAccept = false
        var error: Error?
        let expectation = self.expectation(description: "Biometric Authentication")

        viewModel.enableBiometrics()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(encounteredError):
                    error = encounteredError
                }

                // Fullfilling our expectation to unblock
                // our test execution:
                expectation.fulfill()
            }, receiveValue: { value in
                didAccept = value
            })
            .store(in: &cancellables)

        // Awaiting fulfilment of our expecation before
        // performing our asserts:
        waitForExpectations(timeout: 10)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        // Asserting that our Combine pipeline yielded the
        // correct output:
        XCTAssertNotNil(error)
        XCTAssertFalse(didAccept)
        XCTAssertEqual(bioStatus, -2, "Biometric Status should be -2 when device declined Permission")
        XCTAssertEqual(viewModel.biometricType, .faceID)
    }

    func testCancelDoesNotUpdateStatus() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.userCancel))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .faceID
            }
        }

        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        // Asserting that our Combine pipeline yielded the
        // correct output:
        XCTAssertEqual(bioStatus, 0, "Biometric Status should be 0 when user cancelled")
        XCTAssertEqual(viewModel.biometricType, .faceID)
    }

    func testDelayingPermissionUpdatesStatus() {
        viewModel = BiometricViewModel(biometricService: BiometricService(context: LAContext()), userDefaults: userDefaults, fromBubble: false)
        viewModel.delayPermission()
        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)
        XCTAssertEqual(bioStatus, 0, "Biometric Status should be -1 when user delays granting permission")
    }

    func testAuthenticationFailureTimeout() {
        /// A class faking Touch ID availability and successful user verification
        class StubLAContext: LAContext {
            override func evaluatePolicy(_: LAPolicy, localizedReason _: String, reply: @escaping (Bool, Error?) -> Void) {
                reply(false, LAError(LAError.biometryLockout))
            }

            override func canEvaluatePolicy(_: LAPolicy, error _: NSErrorPointer) -> Bool {
                true
            }

            override var biometryType: LABiometryType {
                .faceID
            }
        }

        var didAccept = false
        var error: Error?
        let expectation = self.expectation(description: "Biometric Authentication")
        viewModel = BiometricViewModel(biometricService: BiometricService(context: StubLAContext()), userDefaults: userDefaults, fromBubble: false)

        viewModel.enableBiometrics()
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case let .failure(encounteredError):
                    error = encounteredError
                }

                // Fullfilling our expectation to unblock
                // our test execution:
                expectation.fulfill()
            }, receiveValue: { value in
                didAccept = value
            })
            .store(in: &cancellables)

        // Awaiting fulfilment of our expecation before
        // performing our asserts:
        waitForExpectations(timeout: 10)

        let bioStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        // Asserting that our Combine pipeline yielded the
        // correct output:
        XCTAssertNotNil(error)
        XCTAssertFalse(didAccept)
        XCTAssertEqual(bioStatus, -2, "Biometric Status should be -2 when device performed too many requests")
        XCTAssertEqual(viewModel.biometricType, .faceID)
    }
}
