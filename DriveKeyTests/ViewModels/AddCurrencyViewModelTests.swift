//
//  AddCurrencyViewModelTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 22/08/2022.
//

import XCTest
@testable import DriveKey

class AddCurrencyViewModelTests: XCTestCase {
    var viewModel: AddCurrencyViewModel!
    var api: APIMock!
    var userService: UserProvidableMock!

    override func setUpWithError() throws {
        api = APIMock()
        userService = UserProvidableMock()
        userService.setUser(DrivekeyUser.mockUserCurrencyUSD)
        viewModel = AddCurrencyViewModel(api, userService: userService, appService: AppDataProvidableMock())
    }

    override func tearDownWithError() throws {
        api = nil
        userService = nil
        viewModel = nil
    }

    func testUserUpdateSuccess() throws {
        let currencies = Currency.mockCurrencies
        viewModel.selectedCurrency = currencies.first
        
        let ex = XCTestExpectation(description: "update-success")
        viewModel.updateUser { error in
            XCTAssertNil(error)
            ex.fulfill()
        }
        wait(for: [ex], timeout: 0.5)
    }

    func testUserUpdateFail() throws {
        let ex = XCTestExpectation(description: "update-fail")
        viewModel.selectedCurrency = nil
        viewModel.updateUser { error in
            XCTAssertNotNil(error)
            XCTAssert(error is AddCurrencyViewModel.AddCurrencyError)
            ex.fulfill()
        }
        wait(for: [ex], timeout: 0.5)
    }

    func testUserUpdateError() throws {
        api.failRequest = true
        let ex = XCTestExpectation(description: "update-error")
        viewModel.updateUser { error in
            XCTAssertNotNil(error)
            ex.fulfill()
        }
        wait(for: [ex], timeout: 0.5)
    }

}
