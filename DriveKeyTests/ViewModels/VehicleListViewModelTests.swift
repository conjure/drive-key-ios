//
//  VehicleListViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 20/07/2022.
//

import XCTest
import Combine
@testable import DriveKey

class VehicleListViewModelTests: XCTestCase {
    var viewModel: VehicleListViewModel!
    let api = APIMock()
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        let user = DrivekeyUser.mockUser
        viewModel = VehicleListViewModel(user, api: api, vehicleFetcher: VehicleFetcher())
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    func testPrimarySuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "set-primary")
        viewModel.primaryUpdated
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.updatePrimary("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testPrimaryError() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "set-primary")
        viewModel.error
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.updatePrimary("some-id")
        wait(for: [ex], timeout: 0.1)
    }
}
