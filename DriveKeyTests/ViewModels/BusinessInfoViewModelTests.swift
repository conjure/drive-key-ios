//
//  BusinessInfoViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 02/09/2022.
//

import XCTest
import Combine
@testable import DriveKey

class BusinessInfoViewModelTests: XCTestCase {
    var viewModel: BusinessInfoViewModel!
    let userService = UserProvidableMock()
    let api = APIMock()
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        viewModel = BusinessInfoViewModel(api, userService: userService)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    func testUserData() throws {
        let userBob = DrivekeyUser.bob
        XCTAssertEqual(viewModel.currency, userBob.currencyCode)
        XCTAssertEqual(viewModel.currencySymbol, "£")
        XCTAssertEqual(viewModel.rate, nil)
        XCTAssertTrue(viewModel.isDefaultBusiness)
    }
    
    func testUpdateUserDataFail() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "user-update")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.update(1, isBusiness: true)
        wait(for: [ex], timeout: 0.1)
    }

    func testUpdateUserDataSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "user-update")
        viewModel.$dataUpdated
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.update(1, isBusiness: true)
        wait(for: [ex], timeout: 0.1)
    }
}
