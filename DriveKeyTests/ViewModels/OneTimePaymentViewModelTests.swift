//
//  OneTimePaymentViewModelTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 02/03/2022.
//

import XCTest
@testable import DriveKey
import Combine

class OneTimePaymentViewModelTests: XCTestCase {
    private var viewModel: OneTimePaymentViewModel!
    private var api: APIMock!
    private var cancellables: Set<AnyCancellable>!

    override func setUpWithError() throws {
        self.api = APIMock()
        viewModel = OneTimePaymentViewModel(user: .mockedDashboardSummary, api: api)
        cancellables = []
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.viewModel = nil
        cancellables = nil
    }

    func testViewModelInitialStateIsIdle() {
        XCTAssertEqual(viewModel.state, .idle)
    }

    func testViewModelStateWhenAPIFails() {
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .failed)
            expectation.fulfill()
        }.store(in: &cancellables)

        api.failRequest = true
        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)
    }

    func testViewModelStateWhenAPISucceeds() {
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)
    }

    func testViewModelProvidesCorrectOffsetDescriptionWhenProducedNothing() {
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        let description = viewModel.additionalOffsetDescription
        XCTAssertEqual(description, "one.time.payment.noCarbon.generated".localized.localized)
    }

    func testViewModelProvidesCorrectOffsetDescriptionWhenNegative() {
        self.viewModel = OneTimePaymentViewModel(user: .mockedNegativeOffset, api: api)
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        let description = viewModel.additionalOffsetDescription
        XCTAssertEqual(description, "one.time.payment.carbon.generated".localized)
    }

    func testViewModelProvidesCorrectOffsetDescriptionWhenNeutral() {
        self.viewModel = OneTimePaymentViewModel(user: .mockedNeutralOffset, api: api)
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        let description = viewModel.additionalOffsetDescription
        XCTAssertEqual(description, "one.time.payment.carbon.neutral".localized)
    }

    func testViewModelProvidesCorrectOffsetDescriptionWhenPositive() {
        self.viewModel = OneTimePaymentViewModel(user: .mockedPositiveOffset, api: api)
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        let description = viewModel.additionalOffsetDescription
        XCTAssertEqual(description, "one.time.payment.carbon.positive".localized)
    }

    func testViewModelUpdatesCorrectValuesWhenAddingAdditional() {
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        XCTAssertEqual(viewModel.generatedCO2, "0kg")
        XCTAssertEqual(viewModel.totalOffset, "£0")
        XCTAssertEqual(viewModel.additionalOffsetPrice, "£0")
        XCTAssertEqual(viewModel.driveKeyPrice, "£0")
        XCTAssertEqual(viewModel.totalString, "£0")
        XCTAssertEqual(viewModel.karaiFeeText, "karai.fee.none".localized)

        viewModel.update(with: 5)
        XCTAssertEqual(viewModel.generatedCO2, "167kg")
        XCTAssertEqual(viewModel.totalOffset, "£0")
        XCTAssertEqual(viewModel.additionalOffsetPrice, "£5")
        XCTAssertEqual(viewModel.driveKeyPrice, "£1.50")
        XCTAssertEqual(viewModel.totalString, "£6.50")
        XCTAssertEqual(viewModel.karaiFeeText, "Karai fee (30%)")

        viewModel.update(with: 2)
        XCTAssertEqual(viewModel.generatedCO2, "67kg")
        XCTAssertEqual(viewModel.totalOffset, "£0")
        XCTAssertEqual(viewModel.additionalOffsetPrice, "£2")
        XCTAssertEqual(viewModel.driveKeyPrice, "£1")
        XCTAssertEqual(viewModel.totalString, "£3")
        XCTAssertEqual(viewModel.karaiFeeText, "karai.fee.minimum".localized)
    }

    func testViewModelUpdatesCheckoutSummaryWhenAddingAdditional() {
        let expectation = self.expectation(description: "Fetching payment summary")

        let tokenPublisher = viewModel.$state
                    .dropFirst()
                    .collect(2)
                    .first()

        tokenPublisher.sink { arrays in
            XCTAssertEqual(arrays.first, .loading)
            XCTAssertEqual(arrays.last, .loaded)
            expectation.fulfill()
        }.store(in: &cancellables)

        viewModel.fetchData()
        wait(for: [expectation], timeout: 1)

        viewModel.update(with: 1)
        var state = viewModel.getCheckoutState()
        var summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 33)
        XCTAssertEqual(summary?.totalPrice, 2)

        viewModel.update(with: 2)
        state = viewModel.getCheckoutState()
        summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 67)
        XCTAssertEqual(summary?.totalPrice, 3)

        viewModel.update(with: 5)
        state = viewModel.getCheckoutState()
        summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 167)
        XCTAssertEqual(summary?.totalPrice, 6.50)

        viewModel.update(with: 10)
        state = viewModel.getCheckoutState()
        summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 333)
        XCTAssertEqual(summary?.totalPrice, 13)

        viewModel.update(with: 20)
        state = viewModel.getCheckoutState()
        summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 667)
        XCTAssertEqual(summary?.totalPrice, 26)

        viewModel.update(with: 25)
        state = viewModel.getCheckoutState()
        summary = state.updatedSummary
        XCTAssertNotNil(summary)
        XCTAssertEqual(summary?.totalCo2Generated, 833)
        XCTAssertEqual(summary?.totalPrice, 32.5)
    }

}
