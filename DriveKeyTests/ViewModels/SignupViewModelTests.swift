//
//  SignupViewModelTests.swift
//  DriveKeyTests
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

@testable import DriveKey
import XCTest

class SignupViewModelTests: XCTestCase {
    private var viewModel: SignupViewModel!

    override func setUpWithError() throws {
        viewModel = SignupViewModel(authService: AuthService(), userService: UserProvidableMock())
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    func testIsValidPassesWithValidEmailAndPassword() {
        XCTAssertTrue(viewModel.isValid("bob@drivekey.com", password: "123"))
    }

    func testIsValidFailsWithEmptyPassword() {
        XCTAssertFalse(viewModel.isValid("bob@drivekey.com", password: ""))
    }

    func testIsValidFailsWithInvalidEmail() {
        XCTAssertFalse(viewModel.isValid("", password: "123"))
        XCTAssertFalse(viewModel.isValid("din", password: "123"))
        XCTAssertFalse(viewModel.isValid("bob@drivekey@", password: "123"))
        XCTAssertFalse(viewModel.isValid("bob@drivekey@doc.", password: "123"))
    }

    func testSignupFailsWithEmptyPassword() {
        XCTAssertFalse(viewModel.validatePassword(""))
        XCTAssertFalse(viewModel.validatePassword("         "))
    }

    func testSignupFailsWithPasswordLessThan8Characters() {
        XCTAssertFalse(viewModel.validatePassword("Dr!"))
    }

    func testSignupFailsWithPasswordMissingDigit() {
        XCTAssertFalse(viewModel.validatePassword("DriveKey!"))
    }

    func testSignupFailsWithPasswordMissingUppercase() {
        XCTAssertFalse(viewModel.validatePassword("drivekey@2"))
    }

    func testSignupFailsWithPasswordMissingSpecialCharacter() {
        XCTAssertFalse(viewModel.validatePassword("DriveKey123"))
    }

    func testSignupPassesWithValidPassword() {
        XCTAssert(viewModel.validatePassword("DriveKey1412!"))
        XCTAssert(viewModel.validatePassword("DriveKey1412!   "))
    }
}
