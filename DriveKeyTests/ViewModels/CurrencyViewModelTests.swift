//
//  CurrencyViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 31/05/2022.
//

import XCTest
import Combine
@testable import DriveKey

class CurrencyViewModelTests: XCTestCase {
    var viewModel: CurrencyViewModel!
    var api: APIMock!
    var userService: UserProvidableMock!
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        api = APIMock()
        userService = UserProvidableMock()
        userService.setUser(DrivekeyUser.mockUserCurrencyUSD)
        viewModel = CurrencyViewModel(userService, currencies: Currency.mockCurrencies)
    }

    override func tearDownWithError() throws {
        api = nil
        userService = nil
        viewModel = nil
    }

    func testSelectedCurrency() throws {
        XCTAssertEqual(viewModel.selectedCurrency?.code, "USD")
    }
}
