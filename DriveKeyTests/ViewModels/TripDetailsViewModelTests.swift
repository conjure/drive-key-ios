//
//  TripDetailsViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 16/09/2022.
//

import XCTest
import Combine
@testable import DriveKey

class TripDetailsViewModelTests: XCTestCase {
    var viewModel: TripDetailsViewModel!
    let userService = UserProvidableMock()
    let api = APIMock()
    let trip = Trip.mockTripBusiness
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        viewModel = TripDetailsViewModel(userService, api: api, trip: trip)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }
    
    func testTripData() throws {
        XCTAssertEqual(viewModel.isBusiness, trip.tripType == .business)
        XCTAssertEqual(viewModel.notes, trip.businessNotes)
        XCTAssertEqual(viewModel.reason, trip.businessReason)
        let symbol = String.symbolForCurrencyCode(code: trip.estimatedRate!.currencyCode!)
        XCTAssertEqual(viewModel.currencySymbol, symbol)
    }
    
    func testVehicles() throws {
        let vehicles = VehicleData.mockVehicles(6, inactive: 0)
        userService.setVehiclesActive(vehicles)
        XCTAssertEqual(viewModel.vehicles?.count, 6)
    }
    
    func testBusinessDataUpdateFail() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "data-update")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.update(1, notes: "notes", reason: "reason")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testBusinessDataUpdateSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "data-update")
        viewModel.$businessDataUpdated
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.update(1, notes: "notes", reason: "reason")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testChangeVehicleSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "change-vehicle")
        viewModel.$vehicleChanged
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeTripVehicle("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testChangeVehicleError() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "change-vehicle")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeTripVehicle("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testRemoveTripSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "remove-trip")
        viewModel.$tripRemoved
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.removeTrip("some-reason")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testRemoveTripError() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "remove-trip")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.removeTrip("some-reason")
        wait(for: [ex], timeout: 0.1)
    }
}
