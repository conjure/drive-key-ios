//
//  VehicleDetailsViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 26/07/2022.
//

import XCTest
import Combine
@testable import DriveKey

class VehicleDetailsViewModelTests: XCTestCase {
    var viewModel: VehicleDetailsViewModel!
    var cancellables = Set<AnyCancellable>()
    let vehicles = VehicleData.mockVehicles(5, inactive: 0)
    var vehicle: VehicleData!
    let api = APIMock()
    
    override func setUpWithError() throws {
        let countries = Country.mockCountries
        vehicle = vehicles[1]
        viewModel = VehicleDetailsViewModel(vehicle, activeVehicles: vehicles, countries: countries, api: api)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    func testRemoveSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "remove-vehicle")
        viewModel.$removed
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.removeVehicle("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testRemoveError() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "remove-vehicle")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.removeVehicle("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testPrimarySuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "primary-update")
        viewModel.$primaryUpdated
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.updatePrimary("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testPrimaryError() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "primary-update")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.updatePrimary("some-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testVehicleData() {
        XCTAssertEqual("Id 1", viewModel.vehicleId)
        XCTAssertEqual("Plate 1", viewModel.licensePlate)
        XCTAssertEqual("Make 1 Model 1", viewModel.model)
        XCTAssertEqual("Country 1", viewModel.country)
        XCTAssertEqual(true, viewModel.isPrimary)
        XCTAssertEqual(true, viewModel.isActive)
        XCTAssertEqual(nil, viewModel.state)
        XCTAssertEqual(false, viewModel.isCountryUS)
        XCTAssertEqual(true, viewModel.isActive)
    }
}
