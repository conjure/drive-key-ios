//
//  VehicleLookupViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 05/10/2022.
//

import XCTest
@testable import DriveKey

class VehicleLookupViewModelTests: XCTestCase {
    let userService = UserProvidableMock()
    
    func testFirstVehicleFail() throws {
        let userActiveVehicle = DrivekeyUser.mockUserActiveVehicle
        userService.setUser(userActiveVehicle)
        let viewModel = VehicleLookupViewModel(userService, screenSource: .profile, country: Country.other, state: nil)
        XCTAssertFalse(viewModel.isFirstVehicle)
    }
    
    func testFirstVehicleSuccess() throws {
        let userNoActiveVehicle = DrivekeyUser.mockUser
        userService.setUser(userNoActiveVehicle)
        let viewModel = VehicleLookupViewModel(userService, screenSource: .profile, country: Country.other, state: nil)
        XCTAssertTrue(viewModel.isFirstVehicle)
    }
}
