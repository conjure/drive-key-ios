//
//  TripHistoryViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 02/08/2022.
//

import XCTest
import Combine
@testable import DriveKey

class TripHistoryViewModelTests: XCTestCase {
    var viewModel: TripHistoryViewModel!
    let userService = UserProvidableMock()
    let api = APIMock()
    var cancellables = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        let user = DrivekeyUser.mockUser
        viewModel = TripHistoryViewModel(userService: userService, api: api, tripsFetcher: TripHistoryFetcher(user: user), filter: nil)
    }

    override func tearDownWithError() throws {
        viewModel = nil
    }

    func testVehicleChangeSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "vehicle-change")
        viewModel.$vehicleChanged
            .dropFirst()
            .sink { success in
                XCTAssertTrue(success)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeTripVehicle("trip-id", vehicleId: "vehicle-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testVehicleChangeFail() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "vehicle-change")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.changeTripVehicle("trip-id", vehicleId: "vehicle-id")
        wait(for: [ex], timeout: 0.1)
    }
    
    func testGenerateReportFail() throws {
        api.failRequest = true
        
        let ex = XCTestExpectation(description: "generate-report")
        viewModel.$error
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.generateReport(["trip-id"])
        wait(for: [ex], timeout: 0.1)
    }
    
    func testGenerateReportSuccess() throws {
        api.failRequest = false
        
        let ex = XCTestExpectation(description: "generate-report")
        viewModel.$reportGenerated
            .dropFirst()
            .sink { error in
                XCTAssertNotNil(error)
                ex.fulfill()
            }
            .store(in: &cancellables)
        viewModel.generateReport(["trip-id"])
        wait(for: [ex], timeout: 0.1)
    }
    
    func testVehicleCount() throws {
        let vehicles = VehicleData.mockVehicles(4, inactive: 0)
        userService.setVehiclesActive(vehicles)
        XCTAssertEqual(4, viewModel.vehicles?.count)
    }
    
    func testUserEmail() throws {
        let userBob = DrivekeyUser.bob
        XCTAssertEqual(userBob.email, viewModel.email)
    }
}
