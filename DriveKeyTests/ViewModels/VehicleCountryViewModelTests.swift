//
//  VehicleCountryViewModelTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 24/05/2022.
//

import XCTest
import Combine
@testable import DriveKey

class VehicleCountryViewModelTests: XCTestCase {
    var viewModel: VehicleCountryViewModel!
    var countries: [Country]!
    
    override func setUpWithError() throws {
        countries = Country.mockCountries
        viewModel = VehicleCountryViewModel(countries, screenSource: .onboarding)
    }

    override func tearDownWithError() throws {
        viewModel = nil
        countries = nil
    }
    
    func testShouldShowStates() throws {
        XCTAssertFalse(viewModel.shouldShowStates(countries[0]))
        XCTAssertFalse(viewModel.shouldShowStates(countries[1]))
        XCTAssertTrue(viewModel.shouldShowStates(countries[2]))
        XCTAssertFalse(viewModel.shouldShowStates(Country.other))
    }
    
    func testDefaultCountry() throws {
        XCTAssertEqual(viewModel.defaultCountryIndex, 3)
    }
    
    func testValidateSuccess() throws {
        let state = CountryState(code: "s1", name: "ns1")
        
        viewModel.selectedCountry = countries[0]
        XCTAssertTrue(viewModel.validate())
        
        viewModel.selectedCountry = countries[1]
        XCTAssertTrue(viewModel.validate())
        
        viewModel.selectedCountry = Country.other
        XCTAssertTrue(viewModel.validate())
        
        viewModel.selectedCountry = countries[2]
        viewModel.selectedState = state
        XCTAssertTrue(viewModel.validate())
    }
    
    func testValidateFail() throws {
        viewModel.selectedCountry = countries[2]
        XCTAssertFalse(viewModel.validate())
    }
}
