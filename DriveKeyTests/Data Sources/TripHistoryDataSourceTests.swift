//
//  TripHistoryDataSourceTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 26/08/2022.
//

import XCTest
@testable import DriveKey

class TripHistoryDataSourceTests: XCTestCase {
    let tableView = UITableViewMock()
    var dataSource: TripHistoryDataSource!
    var didSelectCellCalled = false
    var didSelectMonthCalled = false
    
    override func setUpWithError() throws {
        dataSource = TripHistoryDataSource(tableView, unit: .miles, cellProvider: { _, _, _ in
            return UITableViewCell()
        })
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
    }

    override func tearDownWithError() throws {
        dataSource = nil
    }
    
    func testDataCount() throws {
        let trips_12_2 = Trip.generateTrips(12, countPerMonth: 2)
        let tripsMore = Trip.generateTrips(1, countPerMonth: 4)
        dataSource.updateTrips(trips_12_2 + tripsMore)
        XCTAssertEqual(12, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(6, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertEqual(2, dataSource.tableView(tableView, numberOfRowsInSection: 1))
        
        let trips_2_6 = Trip.generateTrips(2, countPerMonth: 20)
        dataSource.updateTrips(trips_2_6)
        XCTAssertEqual(2, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(20, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertEqual(20, dataSource.tableView(tableView, numberOfRowsInSection: 1))
        
        let trips_1_1 = Trip.generateTrips(1, countPerMonth: 1)
        dataSource.updateTrips(trips_1_1)
        XCTAssertEqual(1, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(1, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        
        dataSource.updateTrips([Trip]())
        XCTAssertEqual(0, dataSource.numberOfSections(in: tableView))
        
        let trips_4_2 = Trip.generateTrips(4, countPerMonth: 2)
        dataSource.updateTrips(trips_4_2)
        XCTAssertEqual(4, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(2, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertEqual(2, dataSource.tableView(tableView, numberOfRowsInSection: 1))
    }
    
    func testTripDates() throws {
        let tripNoStart = Trip.tripNoStart
        var trips = [tripNoStart]
        dataSource.updateTrips(trips)
        XCTAssertEqual(1, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(1, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertNotNil(tripNoStart.startDate)
        XCTAssertNotNil(tripNoStart.endDate)
        
        let tripNoStartAt = Trip.tripNoStartAt
        trips = [tripNoStartAt]
        dataSource.updateTrips(trips)
        XCTAssertEqual(1, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(1, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertNotNil(tripNoStartAt.startDate)
        XCTAssertNotNil(tripNoStartAt.endDate)
        
        let tripNoDates = Trip.tripNoDates
        trips = [tripNoDates]
        dataSource.updateTrips(trips)
        XCTAssertEqual(0, dataSource.numberOfSections(in: tableView))
        XCTAssertEqual(0, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertNil(tripNoDates.startDate)
        XCTAssertNil(tripNoDates.endDate)
    }
    
    func testCellSelect() throws {
        let trips = Trip.generateTrips(4, countPerMonth: 2)
        dataSource.updateTrips(trips)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        let cell_00 = dataSource.itemIdentifier(for: indexPath_00)
        XCTAssertFalse(cell_00!.isSelected)
        tableView.selectRow(indexPath_00)
        XCTAssertTrue(cell_00!.isSelected)
        tableView.deselectRow(indexPath_00)
        XCTAssertFalse(cell_00!.isSelected)
        
        let indexPath_31 = IndexPath(row: 1, section: 3)
        let cell_31 = dataSource.itemIdentifier(for: indexPath_31)
        XCTAssertFalse(cell_31!.isSelected)
        tableView.selectRow(indexPath_31)
        XCTAssertTrue(cell_31!.isSelected)
        tableView.deselectRow(indexPath_31)
        XCTAssertFalse(cell_31!.isSelected)
    }
    
    func testSectionSelect() throws {
        let trips = Trip.generateTrips(4, countPerMonth: 2)
        dataSource.updateTrips(trips)
        
        let sectionView = dataSource.tableView(tableView, viewForHeaderInSection: 0)
        let tapRecognizer = sectionView?.gestureRecognizers?.first
        
        let section = dataSource.sections[0]
        let cell_00 = dataSource.itemIdentifier(for: IndexPath(row: 0, section: 0))
        let cell_01 = dataSource.itemIdentifier(for: IndexPath(row: 1, section: 0))
        
        tapRecognizer?.execute()
        XCTAssertTrue(section.isSelected)
        XCTAssertTrue(cell_00!.isSelected)
        XCTAssertTrue(cell_01!.isSelected)
        
        tapRecognizer?.execute()
        XCTAssertFalse(section.isSelected)
        XCTAssertFalse(cell_00!.isSelected)
        XCTAssertFalse(cell_01!.isSelected)
    }
    
    func testSectionSelectByCellOneItem() throws {
        let trips = Trip.generateTrips(1, countPerMonth: 1)
        dataSource.updateTrips(trips)
        
        let section = dataSource.sections[0]
        let indexPath_00 = IndexPath(row: 0, section: 0)
        let cell_00 = dataSource.itemIdentifier(for: indexPath_00)
        XCTAssertFalse(section.isSelected)
        XCTAssertFalse(cell_00!.isSelected)
        
        tableView.selectRow(indexPath_00)
        XCTAssertTrue(section.isSelected)
        XCTAssertTrue(cell_00!.isSelected)
        
        tableView.deselectRow(indexPath_00)
        XCTAssertFalse(section.isSelected)
        XCTAssertFalse(cell_00!.isSelected)
    }
    
    func testSectionSelectByCellTwoItems() throws {
        let trips = Trip.generateTrips(2, countPerMonth: 2)
        dataSource.updateTrips(trips)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        let indexPath_01 = IndexPath(row: 1, section: 0)
        
        let section = dataSource.sections[0]
        let cell_00 = dataSource.itemIdentifier(for: indexPath_00)
        let cell_01 = dataSource.itemIdentifier(for: indexPath_01)
        XCTAssertFalse(section.isSelected)
        XCTAssertFalse(cell_00!.isSelected)
        XCTAssertFalse(cell_01!.isSelected)
        
        tableView.selectRow(indexPath_00)
        XCTAssertFalse(section.isSelected)
        XCTAssertTrue(cell_00!.isSelected)
        XCTAssertFalse(cell_01!.isSelected)
        
        tableView.selectRow(indexPath_01)
        XCTAssertTrue(section.isSelected)
        XCTAssertTrue(cell_00!.isSelected)
        XCTAssertTrue(cell_01!.isSelected)
        
        tableView.deselectRow(indexPath_00)
        XCTAssertFalse(section.isSelected)
        XCTAssertFalse(cell_00!.isSelected)
        XCTAssertTrue(cell_01!.isSelected)
    }
    
    func testReportData() throws {
        let trips = Trip.generateTrips(4, countPerMonth: 4)
        dataSource.updateTrips(trips)
        dataSource.showSelection(true)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        tableView.selectRow(indexPath_00)
        
        let report_1 = dataSource.report
        XCTAssertEqual(trips[0].miles, report_1?.distance)
        let co2Str = "\(trips[0].co2Value!.formattedCO2)" + "tray.report.tile.co2.kg".localized
        XCTAssertEqual(co2Str, report_1?.co2)
    }
    
    func testReportUnits() throws {
        let dataSourceMi = TripHistoryDataSource(tableView, unit: .miles, cellProvider: { _, _, _ in
            return UITableViewCell()
        })
        let dataSourceKm = TripHistoryDataSource(tableView, unit: .kilometers, cellProvider: { _, _, _ in
            return UITableViewCell()
        })
        
        let trips = Trip.generateTrips(4, countPerMonth: 4)
        dataSourceMi.updateTrips(trips)
        dataSourceKm.updateTrips(trips)
        
        dataSourceMi.showSelection(true)
        dataSourceKm.showSelection(true)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        tableView.selectRow(indexPath_00)
        
        let reportMi = dataSourceMi.report
        XCTAssertEqual(trips[0].miles, reportMi?.distance)
        
        let reportKm = dataSourceKm.report
        XCTAssertEqual(trips[0].kiloMeters, reportKm?.distance)
    }
    
    func testCellUnits() throws {
        let dataSourceMi = TripHistoryDataSource(tableView, unit: .miles, cellProvider: { _, _, _ in
            return UITableViewCell()
        })
        let dataSourceKm = TripHistoryDataSource(tableView, unit: .kilometers, cellProvider: { _, _, _ in
            return UITableViewCell()
        })
        
        let trips = Trip.generateTrips(4, countPerMonth: 4)
        dataSourceMi.updateTrips(trips)
        dataSourceKm.updateTrips(trips)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        let modelMi = dataSourceMi.itemIdentifier(for: indexPath_00)
        let modelKm = dataSourceKm.itemIdentifier(for: indexPath_00)
        
        let trip_00 = trips[0]
        let distanceStrMi = " " + UnitDistance.miles.localizedShort.capitalizingFirstLetter()
        XCTAssertEqual(trip_00.miles, Double(modelMi!.distance.replacingOccurrences(of: distanceStrMi, with: "")))
        let distanceStrKm = " " + UnitDistance.kilometers.localizedShort.capitalizingFirstLetter()
        XCTAssertEqual(trip_00.kiloMeters, Double(modelKm!.distance.replacingOccurrences(of: distanceStrKm, with: "")))
    }
    
    func testSelectedTrips() throws {
        let trips = Trip.generateTrips(4, countPerMonth: 4)
        dataSource.updateTrips(trips)
        dataSource.showSelection(true)
        
        let indexPath_00 = IndexPath(row: 0, section: 0)
        let indexPath_31 = IndexPath(row: 1, section: 3)
        tableView.selectRow(indexPath_00)
        tableView.selectRow(indexPath_31)
        
        let selectedTrips = dataSource.selectedTrips!
        XCTAssertEqual(selectedTrips.count, 2)
        XCTAssertEqual(selectedTrips[0], "00")
        XCTAssertEqual(selectedTrips[1], "31")
    }
    
    func testCellSelectDelegate() throws {
        dataSource.delegate = self
        
        let trips = Trip.generateTrips(1, countPerMonth: 1)
        dataSource.updateTrips(trips)
        
        tableView.selectRow(IndexPath(row: 0, section: 0))
        XCTAssertTrue(didSelectCellCalled)
    }
    
    func testMonthSelectDelegate() throws {
        dataSource.delegate = self
        
        let trips = Trip.generateTrips(1, countPerMonth: 1)
        dataSource.updateTrips(trips)
        
        let sectionView = dataSource.tableView(tableView, viewForHeaderInSection: 0)
        let tapRecognizer = sectionView?.gestureRecognizers?.first
        tapRecognizer?.execute()
        
        XCTAssertTrue(didSelectMonthCalled)
    }
}

extension TripHistoryDataSourceTests: TripHistoryDataSourceDelegate {
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didScroll scrollView: UIScrollView) {
        //
    }
    
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect tripId: String) {
        didSelectCellCalled = true
    }
    
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect month: DateComponents) {
        didSelectMonthCalled = true
    }
}
