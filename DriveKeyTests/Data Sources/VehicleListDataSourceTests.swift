//
//  VehicleListDataSourceTests.swift
//  DriveKeyTests
//
//  Created by Piotr Wilk on 20/07/2022.
//

import XCTest
@testable import DriveKey

class VehicleListDataSourceTests: XCTestCase {
    let tableView = UITableViewMock()
    var dataSource: VehicleListDataSource!
    var didSelectCalled = false
    var didSelectPrimaryCalled = false
    
    override func setUpWithError() throws {
        dataSource = VehicleListDataSource(tableView)
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        
        tableView.register(VehicleListActiveTableViewCell.self, forCellReuseIdentifier: VehicleListActiveTableViewCell.className)
        tableView.register(VehicleListInactiveTableViewCell.self, forCellReuseIdentifier: VehicleListInactiveTableViewCell.className)
    }

    override func tearDownWithError() throws {
        dataSource = nil
    }

    func testReload() throws {
        let vehicles = [VehicleData]()
        dataSource.updateData(vehicles)
        XCTAssertTrue(tableView.isReloadCalled)
        
        tableView.isReloadCalled = false
        let toggleView = dataSource.tableView(tableView, viewForHeaderInSection: 1) as! VehicleListPreviousTableViewSection
        let toggleSwitch = toggleView.subviews.first(where: { $0 is UISwitch }) as? UISwitch
        toggleSwitch?.isOn = true
        toggleSwitch?.sendActions(for: .valueChanged)
        XCTAssertTrue(tableView.isReloadCalled)
    }
    
    func testSelectCell() throws {
        dataSource.delegate = self
        let vehicles = VehicleData.mockVehicles(3, inactive: 0)
        dataSource.updateData(vehicles)
        tableView.selectRow(IndexPath(row: 0, section: 0))
        XCTAssertTrue(didSelectCalled)
    }
    
    func testSelectCellPrimary() throws {
        dataSource.delegate = self
        let vehicles = VehicleData.mockVehicles(3, inactive: 0)
        dataSource.updateData(vehicles)
        
        let cell = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! VehicleListActiveTableViewCell
        cell.onPrimary?()
        
        XCTAssertTrue(didSelectPrimaryCalled)
    }
    
    func testCount() throws {
        let vehicles = VehicleData.mockVehicles(2, inactive: 4)
        dataSource.updateData(vehicles)
        
        XCTAssertEqual(2, dataSource.tableView(tableView, numberOfRowsInSection: 0))
        XCTAssertEqual(0, dataSource.tableView(tableView, numberOfRowsInSection: 1))
        
        let toggleView = dataSource.tableView(tableView, viewForHeaderInSection: 1) as! VehicleListPreviousTableViewSection
        let toggleSwitch = toggleView.subviews.first(where: { $0 is UISwitch }) as? UISwitch
        toggleSwitch?.isOn = true
        toggleSwitch?.sendActions(for: .valueChanged)
        XCTAssertEqual(4, dataSource.tableView(tableView, numberOfRowsInSection: 1))
    }

    func testCell() throws {
        let vehicles = VehicleData.mockVehicles(3, inactive: 3)
        dataSource.updateData(vehicles)
        
        let cellActive = dataSource.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? VehicleListActiveTableViewCell
        XCTAssertNotNil(cellActive)
    }
}

extension VehicleListDataSourceTests: VehicleListDataSourceDelegate {
    func vehicleListDataSource(_ dataSource: VehicleListDataSource, didSelect vehicle: VehicleData) {
        didSelectCalled = true
    }
    
    func vehicleListDataSourceDidTapPrimary(_ dataSource: VehicleListDataSource) {
        didSelectPrimaryCalled = true
    }
}
