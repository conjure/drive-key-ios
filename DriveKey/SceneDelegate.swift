//
//  SceneDelegate.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    var appCoordinator: AppCoordinator?
    var dynamicLinkService: DynamicLinkService?

    func scene(_ scene: UIScene, willConnectTo _: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        guard let windowScene = (scene as? UIWindowScene) else { return }

        dynamicLinkService = DynamicLinkService()
        let locationService = LocationService()
        let motionService = MotionService()
        let permissionService = PermissionService(location: locationService,
                                                  notification: NotificationService(),
                                                  motion: motionService)

        let userService = (UIApplication.shared.delegate as! AppDelegate).userService
        let biometricService = BiometricService()
        let bubbleProvider = BubbleProviderService(remoteBubbleService: FirebaseBubbleService(), biometricService: biometricService)
        let tripsMonitor = TripsMonitor(locationService,
                                        motion: motionService,
                                        tripUploader: TripsUploader.shared)
        let dependencies = AppDependencies(api: NetworkManager.shared,
                                           authService: AuthService(),
                                           dynamicLinkService: dynamicLinkService!,
                                           biometricService: biometricService,
                                           userService: userService,
                                           bubbleService: bubbleProvider,
                                           permissionService: permissionService,
                                           appDataService: AppDataService(NetworkManager.shared),
                                           tripsMonitor: tripsMonitor)
        let navigationController = NavigationController()
        let coordinator = AppCoordinator(navigationController, dependencies: dependencies)
        coordinator.start()
        appCoordinator = coordinator
        
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        // motion logger
        _ = MotionDataLogger.shared
        
        // debug
        // appCoordinator?.debugShowTripHistory()
        
        // Dynamic Link
        if let userActivity = connectionOptions.userActivities.first {
            if let incomingURL = userActivity.webpageURL {
                dynamicLinkService?.handleUniversalLink(incomingURL)
            }
        }
    }

    func sceneDidDisconnect(_: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        appCoordinator?.dependencies.permissionService.fetchAuthorizationData()
    }

    func sceneWillResignActive(_: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }

    func scene(_: UIScene, continue userActivity: NSUserActivity) {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
              let url = userActivity.webpageURL
        else {
            return
        }

        dynamicLinkService?.handleUniversalLink(url)
    }
    
    func logDetectionStatus() {
        let dependencies = appCoordinator?.dependencies
        let tripsMonitor = dependencies?.tripsMonitor
        tripsMonitor?.logStatus()
    }
}
