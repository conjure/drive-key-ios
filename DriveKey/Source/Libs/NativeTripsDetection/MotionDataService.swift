//
//  MotionDataService.swift
//  DriveKey
//
//  Created by Piotr Wilk on 01/11/2023.
//

import Foundation
import CoreMotion
import Combine

class MotionDataService: MotionDataProvidable {    
    static let shared = MotionDataService()
    
    let motionManager = CMMotionManager()
    let accelerometerData = PassthroughSubject<CMAcceleration, Never>()
    let rotationData = PassthroughSubject<CMRotationRate, Never>()
    let magneticFieldData = PassthroughSubject<CMMagneticField, Never>()
    
    let interval: TimeInterval = 1/30
    
    init() {
        motionManager.accelerometerUpdateInterval = interval
        motionManager.gyroUpdateInterval = interval
        motionManager.magnetometerUpdateInterval = interval
        motionManager.deviceMotionUpdateInterval = interval
        
        startAllSensors()
    }
    
    func startAllSensors() {
        startAccelerometer()
        startGyro()
        startMagnetometer()
    }
    
    //TODO: Check if ever should be used instead of sensors separately
    func startDeviceMotion() {
        motionManager.startDeviceMotionUpdates(to: .main) { data, error in
            if let error {
                print("[MotionDataService] startDeviceMotionUpdates error: \(error)")
                return
            }
            
            if let data {
                print("[MotionDataService] DeviceMotion: \(data.userAcceleration)")
            }
        }
    }
    
    func startAccelerometer() {
        motionManager.startAccelerometerUpdates(to: .main) { data, error in
            if let error {
                print("[MotionDataService] startAccelerometerUpdates error: \(error)")
                return
            }
            
            if let data {
                //let t = DateFormatter.motionTimestampLocal.string(from: Date())
                //print("[MotionDataService] T: \(t) Acc: \(data)")
                self.accelerometerData.send(data.acceleration)
            }
        }
    }
    
    func startGyro() {
        motionManager.startGyroUpdates(to: .main) { data, error in
            if let error {
                print("[MotionDataService] startGyroUpdates error: \(error)")
                return
            }
            
            if let data {
                //print("[MotionDataService] Gyro: \(data)")
                self.rotationData.send(data.rotationRate)
            }
        }
    }
    
    func startMagnetometer() {
        motionManager.startMagnetometerUpdates(to: .main) { data, error in
            if let error {
                print("[MotionDataService] startMagnetometerUpdates error: \(error)")
                return
            }
            
            if let data {
                //print("[MotionDataService] Mag: \(data)")
                self.magneticFieldData.send(data.magneticField)
            }
        }
    }
}
