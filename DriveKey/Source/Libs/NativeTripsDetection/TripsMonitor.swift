//
//  TripsMonitor.swift
//  TripTracker
//
//  Created by Piotr Wilk on 13/09/2023.
//

import Foundation
import Combine
import CoreMotion
import CoreLocation

class TripsMonitor {
    private(set) var locationProvider: LocationProvidable
    private(set) var motionProvider: MotionActivityProvidable
    private let tripsUploader: TripsUploadable
    private let userDefaults: UserDefaults!

    private var shouldTrack = false
    private var monitorActivity: MotionActivity?
    private var currentActivity: MotionActivity?
    private var previousTrip: MotionTrip?
    private var newActivityCount: Int = 0
    private var subscriptions = Set<AnyCancellable>()

    struct Settings {
        static let modes: [MotionActivityType]          = [.automotive, .cycling]
        static let activityCount: Int                   = 1
        static let activityMinDuration: TimeInterval    = 60*3
        static let activityMinDistance: Double          = 804 // half mile
        static let mergeMaxInterval: TimeInterval       = 60*5
        static let regionMaxCount: Int                  = 20
    }
    
    init(_ location: LocationProvidable, motion: MotionActivityProvidable, tripUploader: TripsUploadable,
         userDefaults: UserDefaults = .standard) {
        self.locationProvider = location
        self.motionProvider = motion
        self.tripsUploader = tripUploader
        self.userDefaults = userDefaults

        setupPublishers()
        logStatus()

        // initialise should track based on user defaults
        self.shouldTrack = userDefaults.bool(forKey: UserDefaultKeys.shouldTrackTrips)
    }

    func startMonitoring() {
        shouldTrack = true
    }

    func stopMonitoring() {
        shouldTrack = false
    }

    private func setupPublishers() {
        locationProvider.locationPublisher
            .sink { self.handleLocation($0) }
            .store(in: &subscriptions)
        
        motionProvider.activityPublisher
            .sink { self.handleActivity($0) }
            .store(in: &subscriptions)
    }
    
    private func handleLocation(_ location: CLLocation) {
        monitorActivity?.waypoints.append(.init(location))
        currentActivity?.waypoints.append(.init(location))
    }

    private func handleActivity(_ activity: CMMotionActivity) {
        guard shouldTrack else { return }

        if activity.type == .unknown {
            //log("NEXT DROP ", activity: activity, desc: "FLAGS: \(activity.flags)", track: false)
            return
        }
        
        if let monitorActivity {
            if !monitorActivity.isSame(activity) {
                self.monitorActivity = nil
                newActivityCount = 0
                log("NEXT COUNT RESET NEW TYPE: \(activity.type.rawValue)")
            }
            
            if monitorActivity.isSame(activity) && newActivityCount <= Settings.activityCount {
                newActivityCount += 1
                log("NEXT COUNT UP \(newActivityCount) TYPE: \(activity.type.rawValue) FLAGS: \(activity.flags)")
            }
            
            if newActivityCount >= Settings.activityCount {
                startActivity(activity)
                newActivityCount = 0
            }
        } else {
            startActivity(activity)
        }
    }
    
    private func startActivity(_ activity: CMMotionActivity) {
        if let currentActivity, currentActivity.isSame(activity) {
            log("[-- START MONITOR DROP --] SAME ACTIVITY: \(activity.type.rawValue)")
            self.monitorActivity = nil
            return
        }
        startRegion(activityStart: true)
        
        if newActivityCount < Settings.activityCount {
            monitorActivity = MotionActivity(activity)
            log("[-- START MONITOR \(activity.type.rawValue) --] ", activity: activity, desc: "FLAGS: \(activity.flags)")
        } else {
            if let currentActivity, !currentActivity.isSame(activity) {
                endActivity()
            }
            
            currentActivity = MotionActivity(activity)
            if let monitorActivity {
                currentActivity?.startDate = monitorActivity.startDate
                currentActivity?.waypoints.append(contentsOf: monitorActivity.waypoints)
            }
            monitorActivity = nil
            log("[-- START \(activity.type.rawValue) --] W: \(currentActivity?.waypoints.count ?? 0) ", activity: activity, desc: "FLAGS: \(activity.flags)")
        }
    }
    
    private func endActivity() {
        guard let activity = currentActivity else {
            log("END NIL ACTIVITY", track: false)
            startRegion(activityStart: false)
            return
        }
        
        activity.endDate = Date()
        log("[-- END \(activity.type.rawValue) --] W: \(activity.waypoints.count) ", activity: activity)
        createTrip(activity)
        
        startRegion(activityStart: false)
        currentActivity = nil
    }
    
    private func startRegion(activityStart: Bool) {
        if locationProvider.regionCount > Settings.regionMaxCount {
            locationProvider.stopMonitoringAllRegions()
        }
        
        var coordinate: CLLocationCoordinate2D? = currentActivity?.waypoints.last?.coordinate
        if  coordinate == nil {
            coordinate = locationProvider.lastLocation?.coordinate
        }
        if  coordinate == nil {
            coordinate = monitorActivity?.waypoints.last?.coordinate
        }
        
        let namePrefix = activityStart ? "START" : "END"
        let activityType = currentActivity != nil ? currentActivity!.type.rawValue : "NIL"
        if let coordinate = coordinate {
            let name = "\(activityType) \(namePrefix) Lat: \(coordinate.latitude.rounded(2)) Lon: \(coordinate.longitude.rounded(2))"
            locationProvider.startMonitoringRegion(coordinate, name: name)
            log("START REGION: \(name)", track: false)
        } else {
            log("START REGION NO DATA: \(namePrefix) \(activityType)", track: false)
        }
    }
    
    private func stopAllRegions() {
        locationProvider.stopMonitoringAllRegions()
    }
    
    private func createTrip(_ activity: MotionActivity) {
        checkPreviousTrip(activity)
        
        if Settings.modes.contains(activity.type) {
            let trip = MotionTrip(activity)
            log("CREATE TRIP TYPE: \(trip.type) W: \(activity.waypoints.count)")
            processTrip(trip)
        } else {
            log("CREATE TRIP NOT SUPPORTED TYPE: \(activity.type.rawValue) W: \(activity.waypoints.count)")
        }
    }
    
    func checkPreviousTrip(_ activity: MotionActivity) {
        if let previousTrip {
            let diff = activity.startDate.timeIntervalSince(previousTrip.endAt.dateValue())
            log("CHECK PREV TYPE: \(previousTrip.type) CURR TYPE: \(activity.type.rawValue) DIFF: \(diff)")
            if diff > Settings.mergeMaxInterval {
                log("CHECK PREV DIFF BIGGER THAN MERGE UPLOAD TYPE: \(previousTrip.type)")
                uploadTrip(previousTrip)
                self.previousTrip = nil
            }
        }
    }
    
    private func processTrip(_ trip: MotionTrip) {
        if let previousTrip {
            log("PROCESS PREV TYPE: \(previousTrip.type) S: \(previousTrip.startStr) E: \(previousTrip.endStr) W: \(previousTrip.waypoints.count) | PROCESS CURR TYPE: \(trip.type) S: \(trip.startStr) E: \(trip.endStr) W: \(trip.waypoints.count)")
            if previousTrip.type == trip.type {
                let diff = trip.startAt.dateValue().timeIntervalSince(previousTrip.endAt.dateValue())
                if diff < Settings.mergeMaxInterval {
                    let mergedTrip = merge(previousTrip, tripLast: trip)
                    self.previousTrip = mergedTrip
                } else {
                    log("PROCESS UPLOAD PREV NO MERGE TIME")
                    uploadTrip(previousTrip)
                    self.previousTrip = trip
                }
            } else {
                log("PROCESS UPLOAD PREV NO TYPE MATCH")
                uploadTrip(previousTrip)
                self.previousTrip = trip
            }
        } else {
            if Settings.modes.contains(trip.type) {
                log("PROCESS ASSIGN PREV TYPE: \(trip.type)")
                self.previousTrip = trip
            } else {
                log("PROCESS ASSIGN PREV NO MODES MATCH TYPE: \(trip.type)")
            }
        }
    }
    
    private func merge(_ tripFirst: MotionTrip, tripLast: MotionTrip) -> MotionTrip {
        let waypoints = tripFirst.waypoints + tripLast.waypoints
        let trip = MotionTrip(tripFirst.startAt, endAt: tripLast.endAt, waypoints: waypoints, type: tripFirst.type)
        log("MERGE TYPE: \(trip.type) START: \(trip.startStr) END: \(trip.endStr) W: \(waypoints.count)")

        return trip
    }
    
    private func uploadTrip(_ trip: MotionTrip) {
        if checkMinimumRequirements(trip) {
            tripsUploader.uploadTrip(trip)
            log("UPLOAD TRIP TYPE: \(trip.type)", track: false)
        } else {
            log("UPLOAD TRIP CANCEL NO MIN REQ TYPE: \(trip.type)", track: false)
        }
        
        locationProvider.stopMonitoringAllRegions()
    }
    
    private func checkMinimumRequirements(_ trip: MotionTrip) -> Bool {
        let duration: TimeInterval = trip.endAt.dateValue().timeIntervalSince(trip.startAt.dateValue())
        log("CHECK MIN DURATION: \(String(format: "%.0f", duration)) TYPE: \(trip.type)")
        if duration < Settings.activityMinDuration {
            return false
        }
        
        guard trip.waypoints.count > 1 else { return false }
        
        let distance = trip.waypoints.distanceInMeters
        log("CHECK MIN DISTANCE: \(String(format: "%.0f", distance)) TYPE: \(trip.type)")
        
        return distance > Settings.activityMinDistance
    }
    
    func logStatus() {
        var log = "Location Always: \(locationProvider.isAuthorizedAlways), "
        log += "Location WhenInUse: \(locationProvider.isAuthorizedWhenInUse), "
        log += "Location Precise: \(locationProvider.isPreciseLocation), "
        log += "Motion Authorized: \(motionProvider.isAuthorized), "
        log += "Motion Modes: \(Settings.modes.map({ $0.rawValue })), "
        log += "Activity Count: \(Settings.activityCount)"
        print("[TripsMonitor] STATUS: \(log)")

        //TODO: Bring back trackEvent(log, event: .status)
    }
}

extension TripsMonitor {
    private func log(_ message: String, activity: CMMotionActivity, desc: String = "", track: Bool = true) {
        var logStr = message + activity.type.rawValue + " " + DateFormatter.HHmmssLocal.string(from: activity.startDate)
        if !desc.isEmpty {
            logStr += " " + desc
        }
        log(logStr, track: track)
    }
    
    private func log(_ message: String, activity: MotionActivity, desc: String = "", track: Bool = true) {
        var logStr = message + activity.type.rawValue + " " + DateFormatter.HHmmssLocal.string(from: activity.startDate)
        if !desc.isEmpty {
            logStr += " " + desc
        }
        log(logStr, track: track)
    }
    
    private func log(_ message: String, track: Bool = true) {
        LOGGER(message)
        print("[TripsMonitor] \(message)")
    }
}

extension TripsMonitor {
    func dummyCreateTrip(_ type: MotionActivityType = .automotive) {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date().addingTimeInterval(-60*30),
                                      unknown: false,
                                      stationary: type == .stationary,
                                      walking: type == .walking,
                                      running: type == .running,
                                      automotive: type == .automotive,
                                      cycling: type == .cycling)
        activity.endDate = Date()
        
        createTrip(activity)
    }
    
    func dummyUploadTrip(_ type: MotionActivityType = .automotive) {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date().addingTimeInterval(-60*50),
                                      unknown: false,
                                      stationary: type == .stationary,
                                      walking: type == .walking,
                                      running: type == .running,
                                      automotive: type == .automotive,
                                      cycling: type == .cycling)
        activity.endDate = Date()
        activity.waypoints = MotionTrip.dummyWaypoints
        
        let trip = MotionTrip(activity)

        uploadTrip(trip)
    }
}
