//
//  MotionTrip.swift
//  TripTracker
//
//  Created by Piotr Wilk on 15/09/2023.
//

import Foundation
import FirebaseFirestore
import MapKit

struct Waypoint: Codable {
    let latitude: Double
    let longitude: Double
    let timestamp: String
}

struct MotionTrip: Decodable, Encodable {
    var id = UUID().uuidString
    let startAt: Timestamp
    let endAt: Timestamp
    let deviceType: String
    let type: MotionActivityType
    let distance: Double
    let waypoints: [Waypoint]

    init(_ activity: MotionActivity) {
        self.startAt = Timestamp(date: activity.startDate)
        self.endAt = Timestamp(date: activity.endDate)
        self.deviceType = "iOS"
        self.type = activity.type
        self.waypoints = activity.waypoints
        self.distance = activity.waypoints.distanceInMeters
    }
    
    init(_ startAt: Timestamp, endAt: Timestamp, waypoints: [Waypoint], type: MotionActivityType) {
        self.startAt = startAt
        self.endAt = endAt
        self.deviceType = "iOS"
        self.type = type
        self.waypoints = waypoints
        self.distance = waypoints.distanceInMeters.rounded(0)
    }

    var coordinates: [CLLocationCoordinate2D] {
        waypoints.map({ CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude) })
    }
}

extension MotionTrip: Hashable {
    static func == (lhs: MotionTrip, rhs: MotionTrip) -> Bool {
        lhs.id == rhs.id && lhs.startAt == rhs.startAt && lhs.endAt == rhs.endAt
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
        hasher.combine(startAt)
        hasher.combine(endAt)
    }
}

extension MotionTrip: Identifiable {}

extension MotionTrip {
    var startStr: String {
        DateFormatter.HHmmss.string(from: startAt.dateValue())
    }
    
    var endStr: String {
        DateFormatter.HHmmss.string(from: endAt.dateValue())
    }
}

extension Waypoint {
    init(_ location: CLLocation) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        self.timestamp = DateFormatter.yyyyMMddTHHmmssSSSXXXXX.string(from: location.timestamp)
    }
    
    var coordinate: CLLocationCoordinate2D {
        .init(latitude: latitude, longitude: longitude)
    }
    
    static func distanceInMiles(_ coordinates: [CLLocationCoordinate2D]) -> Double {
        guard coordinates.count > 1 else { return 0 }
        
        var totalKilometers: Double = 0
        for i in 0..<coordinates.count-1 {
            let coordiante1 = coordinates[i]
            let coordiante2 = coordinates[i + 1]
            
            let location1 = CLLocation(latitude: coordiante1.latitude, longitude: coordiante1.longitude)
            let location2 = CLLocation(latitude: coordiante2.latitude, longitude: coordiante2.longitude)
            totalKilometers += location1.distance(from: location2) / 1000
        }
        
        let distanceKilometers = Measurement(value: totalKilometers, unit: UnitLength.kilometers)
        return distanceKilometers.converted(to: .miles).value
    }
}

extension Array where Element == Waypoint {
    var distanceInMeters: Double {
        guard count > 1 else { return 0 }
        
        var distance: Double = 0
        
        let coordinates = map({ $0.coordinate })
        for i in 0..<coordinates.count-1 {
            let location1 = CLLocation(latitude: coordinates[i].latitude, longitude: coordinates[i].longitude)
            let location2 = CLLocation(latitude: coordinates[i + 1].latitude, longitude: coordinates[i + 1].longitude)
            distance += location1.distance(from: location2)
        }
        
        return distance.rounded(0)
    }
}

extension MotionTrip {
    static var dummyWaypoints: [Waypoint] {
        let coordinates = [
            (51.5074, -0.1278), 
                (51.5125, -0.1280),
                (51.5165, -0.1285),
                (51.5205, -0.1287),
                (51.5246, -0.1289),
                (51.5287, -0.1290),
                (51.5327, -0.1292),
                (51.5368, -0.1294),
                (51.3979776,-0.0886325)
        ]
        
        var result = [Waypoint]()
        
        let now = Date()
        for (index, item) in coordinates.enumerated() {
            let dateIndex = Date(timeInterval: -Double(index * 7), since: now)
            let location = CLLocation(coordinate: CLLocationCoordinate2D(latitude: item.0, longitude: item.1),
                                      altitude: 0,
                                      horizontalAccuracy: 0,
                                      verticalAccuracy: 0,
                                      timestamp: dateIndex)
            let waypoint = Waypoint(location)
            result.append(waypoint)
        }
        
        return result
    }
}
