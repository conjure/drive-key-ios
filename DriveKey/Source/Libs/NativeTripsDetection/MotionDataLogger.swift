//
//  MotionDataLogger.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/11/2023.
//

import Foundation
import Combine
import CoreMotion
import LRUCache
import CSV

typealias SensorsData = (t: String, 
                         ax: Double, ay: Double, az: Double,
                         gx: Double, gy: Double, gz: Double,
                         mx: Double, my: Double, mz: Double, 
                         gf: Double)

class MotionDataLogger {
    static let shared = MotionDataLogger()
    
    private let cache = LRUCache<String, SensorsData>()
    private let dataProvider: MotionDataProvidable = MotionDataService.shared
    private var cancellables = Set<AnyCancellable>()
    
    private init() {
        cache.countLimit = 60 * 30 * 2 //2mins
        setupPublishers()
    }
    
    private func setupPublishers() {
        Publishers.CombineLatest3(dataProvider.accelerometerData, dataProvider.rotationData, dataProvider.magneticFieldData)
            .removeDuplicates(by: { dataA, dataB in
                if dataA.0 == dataB.0 || dataA.1 == dataB.1 || dataA.2 == dataB.2 {
                    return true
                }
                
                return false
            })
            .map {
                let gForce = sqrt($0.x * $0.x + $0.y * $0.y + $0.z * $0.z)
                return SensorsData(DateFormatter.motionTimestampLocal.string(from: Date()),
                                   $0.x.rounded(6), $0.y.rounded(6), $0.z.rounded(6),
                                   $1.x.rounded(6), $1.y.rounded(6), $1.z.rounded(6),
                                   $2.x.rounded(6), $2.y.rounded(6), $2.z.rounded(6),
                                   gForce.rounded(6))
            }
            .receive(on: DispatchQueue.global())
            .sink { [weak self] data in
                self?.cache(data)
            }
            .store(in: &cancellables)
    }
    
    private func cache(_ data: SensorsData) {
        cache.setValue(data, forKey: data.t)
    }
    
    private func generateCSVText() -> String {
        var result = ""
        
        do {
            let csv = try CSVWriter(stream: .toMemory(), newline: .lf)
            
            try? csv.write(row: ["timestamp", "acc_x", "acc_y", "acc_z", "gyro_x", "gyro_y", "gyro_z", "mag_x", "mag_y", "mag_z", "g_force"])
            let items = cache.allValues
            for item in items {
                csv.beginNewRow()
                try? csv.write(row: [item.t, 
                                     String(item.ax), String(item.ay), String(item.az),
                                     String(item.gx), String(item.gy), String(item.gz),
                                     String(item.mx), String(item.my), String(item.mz),
                                     String(item.gf)])
            }
            csv.stream.close()
            let log = "SHARE MOTION DATA COUNT: \(items.count)"

            let csvData = csv.stream.property(forKey: .dataWrittenToMemoryStreamKey) as? Data
            if let csvData, let csvString = String(data: csvData, encoding: .utf8) {
                result = csvString
            }
        } catch (let error) {
            let log = "SHARE MOTION DATA ERROR: \(error)"
        }
        
        return result
    }
    
    func generateCSV() -> URL {
        let title = "DK-" + DateFormatter.motionTimeLocal.string(from: Date())
        let text = generateCSVText()
        
        let temporaryFolder = FileManager.default.temporaryDirectory
        let fileName = title + ".csv"
        let temporaryFileURL = temporaryFolder.appendingPathComponent(fileName)
        do {
            try text.write(to: temporaryFileURL, atomically: true, encoding: .utf8)
        } catch {
            let log = "SHARE MOTION CSV ERROR: \(error)"
        }
        
        return temporaryFileURL
    }
}

extension Double {
    func rounded(_ places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

extension CMAcceleration: Equatable {
    public static func == (lhs: CMAcceleration, rhs: CMAcceleration) -> Bool {
        lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z
    }
}

extension CMRotationRate: Equatable {
    public static func == (lhs: CMRotationRate, rhs: CMRotationRate) -> Bool {
        lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z
    }
}

extension CMMagneticField: Equatable {
    public static func == (lhs: CMMagneticField, rhs: CMMagneticField) -> Bool {
        lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z
    }
}
