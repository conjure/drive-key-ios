//
//  TripsUploader.swift
//  TripTracker
//
//  Created by Piotr Wilk on 15/09/2023.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

protocol TripsUploadable {
    func uploadTrip(_ trip: MotionTrip)
}

class TripsUploader: TripsUploadable {
    static let shared = TripsUploader()
    private let db = Firestore.firestore()

    func uploadTrip(_ trip: MotionTrip) {
        guard let user = Auth.auth().currentUser else { return }
        let encoder = Firestore.Encoder()

        do {
            var data = try encoder.encode(trip)
            let collectionRef = db.collection("users").document(user.uid).collection("trip-upload").document(trip.id)

            collectionRef.setData(data) { error in
                print("[TripsUploader] setData error: \(String(describing: error))")

                if let error = error {
                    let log = "[TripsUploader] UPLOAD TRIP ERROR: \(error) TYPE: \(trip.type)"
                    LOGGER(log)
                } else {
                    let log = "[TripsUploader] UPLOAD TRIP TYPE: \(trip.type) ID: \(trip.id)"
                    LOGGER(log)
                }
            }
        } catch {
            let log = "TRIP SERIALIZATION ERROR: \(error) TYPE: \(trip.type)"
            LOGGER(log)
        }
    }
}
