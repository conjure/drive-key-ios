//
//  MotionDataProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 01/11/2023.
//

import Foundation
import CoreMotion
import Combine

protocol MotionDataProvidable {
    var accelerometerData: PassthroughSubject<CMAcceleration, Never> { get }
    var rotationData: PassthroughSubject<CMRotationRate, Never> { get }
    var magneticFieldData: PassthroughSubject<CMMagneticField, Never> { get }
}
