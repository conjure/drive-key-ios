//
//  MotionActivity.swift
//  TripTracker
//
//  Created by Piotr Wilk on 14/09/2023.
//

import Foundation
import CoreMotion
import CoreLocation

enum MotionActivityType: String, Decodable, Encodable {
    case automotive = "AUTOMOTIVE"
    case cycling    = "CYCLING"
    case running    = "RUNNING"
    case walking    = "WALKING"
    case stationary = "STATIONARY"
    case unknown    = "UNKNOWN"
}

class MotionActivity {
    let confidence: CMMotionActivityConfidence
    var startDate: Date
    let unknown: Bool
    let stationary: Bool
    let walking: Bool
    let running: Bool
    let automotive: Bool
    let cycling: Bool
    var endDate: Date
    var waypoints: [Waypoint]
    
    init(confidence: CMMotionActivityConfidence, startDate: Date, unknown: Bool, stationary: Bool, walking: Bool, running: Bool, automotive: Bool, cycling: Bool) {
        self.confidence = confidence
        self.startDate = startDate
        self.unknown = unknown
        self.stationary = stationary
        self.walking = walking
        self.running = running
        self.automotive = automotive
        self.cycling = cycling
        self.endDate = Date()
        self.waypoints = [Waypoint]()
    }
    
    /*
     *    For example, if you're in a car stopped at a stop sign the state might
     *    look like:
     *       stationary = YES, walking = NO, running = NO, automotive = YES
     *
     *    Or a moving vehicle,
     *       stationary = NO, walking = NO, running = NO, automotive = YES
     */
    func isSame(_ activity: CMMotionActivity) -> Bool {
        if activity.automotive { return automotive }
        if activity.cycling { return cycling }
        if activity.running { return running }
        if activity.walking { return walking }
        if activity.stationary { return stationary }
        if activity.unknown { return unknown }
        
        return false
    }
}

extension MotionActivity {
    convenience init(_ activity: CMMotionActivity) {
        self.init(confidence: activity.confidence,
                  startDate: activity.startDate,
                  unknown: activity.unknown,
                  stationary: activity.stationary,
                  walking: activity.walking,
                  running: activity.running,
                  automotive: activity.automotive,
                  cycling: activity.cycling)
    }
}

extension MotionActivity {
    var type: MotionActivityType {
        if automotive { return .automotive }
        if cycling { return .cycling }
        if running { return .running }
        if walking { return .walking }
        if stationary { return .stationary }
        if unknown { return .unknown }
        
        return .unknown
    }
}

extension CMMotionActivity {
    var type: MotionActivityType {
        if automotive { return .automotive }
        if cycling { return .cycling }
        if running { return .running }
        if walking { return .walking }
        if stationary { return .stationary }
        if unknown { return .unknown }
        
        return .unknown
    }
}

extension CMMotionActivity {
    var flags: String {
        "CONF=\(confidence.description) A=\(automotive) C=\(cycling) R=\(running) W=\(walking) S=\(stationary) U=\(unknown)"
    }
}

extension CMMotionActivityConfidence: CustomStringConvertible {
    public var description: String {
        switch self {
        case .low:
            return "low"
        case .medium:
            return "medium"
        case .high:
            return "high"
        @unknown default:
            return "unknown"
        }
    }
}
