//
//  MotionActivityProvidable.swift
//  TripTracker
//
//  Created by Piotr Wilk on 15/09/2023.
//

import Foundation
import Combine
import CoreMotion

protocol MotionActivityProvidable {
    var activityPublisher: PassthroughSubject<CMMotionActivity, Never> { get }
    var isAuthorized: Bool { get }
}
