//
//  DebugMotionActivityProvider.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/12/2023.
//

import Foundation
import CoreMotion
import Combine

class DebugMotionActivityProvider: MotionActivityProvidable {
    var activityPublisher = PassthroughSubject<CMMotionActivity, Never>()
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        startPublishing()
    }
    
    func startPublishing() {
        let timerPublisher = Timer.publish(every: 2, on: .main, in: .default).autoconnect()
        Publishers.Zip(activities.publisher, timerPublisher)
            .sink { data in
                let activity = data.0 as DebugMotionActivity
                activity.motionActivity.startDate = Date()
                self.activityPublisher.send(activity)
            }
            .store(in: &subscriptions)
    }
    
    private var activities: [DebugMotionActivity] {
        [
            .automotive,
            .automotive,
            .unknown,
            .automotive,
            .automotive,
            .automotive,
            .automotive,
            .unknown,
            .automotive,
            .automotive,
            .walking,
            .walking,
            .walking
        ]
    }
    
    var isAuthorized: Bool {
        true
    }
}

class DebugMotionActivity: CMMotionActivity {
    var motionActivity: MotionActivity
    
    init(motionActivity: MotionActivity) {
        self.motionActivity = motionActivity
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("[DebugMotionActivity] init(coder:) has not been implemented")
    }
    
    override var startDate: Date {
        motionActivity.startDate
    }
    
    override var automotive: Bool {
        motionActivity.automotive
    }
    
    override var cycling: Bool {
        motionActivity.cycling
    }
    
    override var running: Bool {
        motionActivity.running
    }
    
    override var walking: Bool {
        motionActivity.walking
    }
    
    override var stationary: Bool {
        motionActivity.stationary
    }
    
    override var unknown: Bool {
        motionActivity.unknown
    }
}

extension DebugMotionActivity {
    static var automotive: DebugMotionActivity {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date(),
                                      unknown: false,
                                      stationary: false,
                                      walking: false,
                                      running: false,
                                      automotive: true,
                                      cycling: false)
        return DebugMotionActivity(motionActivity: activity)
    }
    
    static var cycling: DebugMotionActivity {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date(),
                                      unknown: false,
                                      stationary: false,
                                      walking: false,
                                      running: false,
                                      automotive: false,
                                      cycling: true)
        return DebugMotionActivity(motionActivity: activity)
    }
    
    static var walking: DebugMotionActivity {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date(),
                                      unknown: false,
                                      stationary: false,
                                      walking: true,
                                      running: false,
                                      automotive: false,
                                      cycling: false)
        return DebugMotionActivity(motionActivity: activity)
    }
    
    static var stationary: DebugMotionActivity {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date(),
                                      unknown: false,
                                      stationary: true,
                                      walking: false,
                                      running: false,
                                      automotive: false,
                                      cycling: false)
        return DebugMotionActivity(motionActivity: activity)
    }
    
    static var unknown: DebugMotionActivity {
        let activity = MotionActivity(confidence: .high,
                                      startDate: Date(),
                                      unknown: true,
                                      stationary: false,
                                      walking: false,
                                      running: false,
                                      automotive: false,
                                      cycling: false)
        return DebugMotionActivity(motionActivity: activity)
    }
}
