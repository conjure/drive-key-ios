//
//  LocationProvidable.swift
//  TripTracker
//
//  Created by Piotr Wilk on 15/09/2023.
//

import Foundation
import Combine
import CoreLocation

protocol LocationProvidable {
    var locationPublisher: PassthroughSubject<CLLocation, Never> { get }
    var locationAuthSubject: PassthroughSubject<CLAuthorizationStatus, Never> { get }
    var isAuthorizedAlways: Bool { get }
    var isAuthorizedWhenInUse: Bool { get }
    var isPreciseLocation: Bool { get }
    var allowsBackgroundUpdates: Bool { get }
    var isLocationServicesEnabled: Bool { get }
    var lastLocation: CLLocation? { get }
    
    func startMonitoringRegion(_ location: CLLocationCoordinate2D, name: String)
    func stopMonitoringAllRegions()
    var regionCount: Int { get }
}
