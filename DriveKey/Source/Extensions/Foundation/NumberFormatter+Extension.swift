//
//  NumberFormatter+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2023.
//

import Foundation

extension NumberFormatter {
    static let distance: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return formatter
    }()
}
