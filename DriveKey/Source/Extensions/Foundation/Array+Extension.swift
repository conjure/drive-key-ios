//
//  Array+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 09/09/2021.
//

import Foundation

extension Array where Element: FloatingPoint {
    func sum() -> Element {
        reduce(0, +)
    }

    func avg() -> Element {
        sum() / Element(count)
    }

    func std() -> Element {
        let mean = avg()
        let v = reduce(0) { $0 + ($1 - mean) * ($1 - mean) }
        return sqrt(v / (Element(count) - 1))
    }
}
