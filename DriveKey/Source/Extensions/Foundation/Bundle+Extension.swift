//
//  Bundle+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/10/2021.
//

import Foundation

extension Bundle {
    class func serializeJson<T>(filename fileName: String, decodingStrategy: JSONDecoder.KeyDecodingStrategy?) -> T? where T: Decodable {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                if decodingStrategy != nil {
                    decoder.keyDecodingStrategy = decodingStrategy!
                }
                decoder.dateDecodingStrategy = .iso8601WithOptionalMilliseconds
                return try decoder.decode(T.self, from: data)
            } catch {
                print("[APIHelper] Error: \(error)")
            }
        }

        return nil
    }
    
    class var appVersion: String {
        var result = ""
        if let versionShort = main.infoDictionary?["CFBundleShortVersionString"] as? String,
           let version = main.infoDictionary?["CFBundleVersion"] as? String {
            result = versionShort + " (\(version))"
        }
        
        return result
    }
}
