//
//  String+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Foundation
import UIKit

extension String {
    func capitalizingFirstLetter() -> String {
        prefix(1).uppercased() + lowercased().dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = capitalizingFirstLetter()
    }

    var trimmed: String {
        trimmingCharacters(in: .whitespaces)
    }

    var localized: String {
        NSLocalizedString(self, comment: "")
    }

    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }

    func removeAllSpaces() -> String {
        replacingOccurrences(of: " ", with: "")
    }

    func removingWhitespaces() -> String {
        components(separatedBy: .whitespaces).joined()
    }
    
    func nsRange(of str: String) -> NSRange {
        (self as NSString).range(of: str)
    }
    
    // https://stackoverflow.com/a/44616772
    func subscriptCO2(_ font: UIFont) -> NSAttributedString {
        let resultAttStr = NSMutableAttributedString(string: self)
        
        var nextStartIndex = startIndex
        while let range = range(of: "co2".localized, options: [.literal, .caseInsensitive], range: nextStartIndex..<endIndex) {
            if let from = range.lowerBound.samePosition(in: utf16) {
                let start = utf16.distance(from: utf16.startIndex, to: from)
                resultAttStr.addAttribute(.font, value: font, range: NSRange(location: start + 2, length: 1))
            }
            nextStartIndex = range.upperBound
        }
        
        return resultAttStr
    }

    func underline() -> NSAttributedString {
        let range = nsRange(of: self)
        let resultAttStr = NSMutableAttributedString(string: self)
        resultAttStr.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)

        return resultAttStr
    }
    
    var isNumeric: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
    
    // https://stackoverflow.com/a/49340156
    static func symbolForCurrencyCode(code: String) -> String {
        var candidates: [String] = []
        let locales: [String] = NSLocale.availableLocaleIdentifiers
        for localeID in locales {
            guard let symbol = findMatchingSymbol(localeID: localeID, currencyCode: code) else {
                continue
            }
            if symbol.count == 1 {
                return symbol
            }
            candidates.append(symbol)
        }
        let sorted = sortAscByLength(list: candidates)
        if sorted.count < 1 {
            return ""
        }
        return sorted[0]
    }
    
    private static func findMatchingSymbol(localeID: String, currencyCode: String) -> String? {
        let locale = Locale(identifier: localeID as String)
        guard let code = locale.currencyCode else {
            return nil
        }
        if code != currencyCode {
            return nil
        }
        guard let symbol = locale.currencySymbol else {
            return nil
        }
        return symbol
    }
    
    private static func sortAscByLength(list: [String]) -> [String] {
        return list.sorted(by: { $0.count < $1.count })
    }
}
