//
//  Notification+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 21/01/2022.
//

import Foundation

extension Notification.Name {
    static let closedDrawerNotification = Notification.Name("closedDrawerNotification")
}

@objc extension NSNotification {
    public static let closedDrawerNotification = Notification.Name.closedDrawerNotification
}
