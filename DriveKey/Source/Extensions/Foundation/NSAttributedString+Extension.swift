//
//  NSAttributedString+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/09/2021.
//

import UIKit

extension NSMutableAttributedString {
    
    // TODO: Remove in favor of `subscriptCO2` from String extension
    func updateCO2(for font: UIFont, colour: UIColor) {
        // Set new attributed string
        let coString = "karai.emissions.co".localized
        let twoStr = "karai.emissions.subscript.2".localized

        let newAttributes: [NSAttributedString.Key: Any] = [.font: font.withSize(10), .foregroundColor: colour]
        let newAttributedString = NSMutableAttributedString(string: coString, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: colour])
        newAttributedString.append(NSAttributedString(string: twoStr, attributes: newAttributes))

        // Get range of text to replace
        guard let range = string.range(of: "CO2") else { return }
        let nsRange = NSRange(range, in: string)

        // Replace content in range with the new content
        replaceCharacters(in: nsRange, with: newAttributedString)
    }

    func underline(_ str: String) -> Self {
        let range = string.nsRange(of: str)
        addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: range)

        return self
    }

    func applyFont(_ font: UIFont, str: String) -> Self {
        let range = string.nsRange(of: str)
        addAttributes([NSAttributedString.Key.font: font], range: range)

        return self
    }
}
