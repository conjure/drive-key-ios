//
//  Date+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/08/2021.
//

import Foundation

extension Date {
    func daysInMonth() -> Int {
        let calendar = Calendar.current
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count

        return numDays
    }

    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        calendar.dateComponents(Set(components), from: self)
    }

    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        calendar.component(component, from: self)
    }

    func days(sinceDate: Date, calendar: Calendar = Calendar.current) -> Int? {
        // Replace the hour (time) of both dates with 00:00
        let date1 = calendar.startOfDay(for: sinceDate)
        let date2 = calendar.startOfDay(for: self)

        let components = calendar.dateComponents([.day], from: date1, to: date2)

        return components.day
    }

    var removeTime: Date? {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: self)) else {
            return nil
        }

        return date
    }
}
