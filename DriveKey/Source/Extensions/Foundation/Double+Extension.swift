//
//  Double+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/10/2021.
//

import Foundation

extension Double {
    var formattedCO2: String {
        if self == 0 {
            return "0"
        } else if self < 1 {
            return String(format: "%.2f", self)
        } else {
            return String(format: "%.0f", self)
        }
    }
    
    func formatForCurrency(_ currency: String?) -> String? {
        let numberFormatter = NumberFormatter()
        
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyCode = currency?.uppercased()
        numberFormatter.currencySymbol = String.symbolForCurrencyCode(code: currency?.uppercased() ?? "")
        
        let result = numberFormatter.string(from: self as NSNumber)
        let emptySuffix = numberFormatter.decimalSeparator + "00"
        return result?.replacingOccurrences(of: emptySuffix, with: "")
    }
}
