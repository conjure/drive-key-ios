//
//  JSONDecoder+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Foundation

extension JSONDecoder.DateDecodingStrategy {
    static let iso8601WithOptionalMilliseconds = JSONDecoder.DateDecodingStrategy.custom { decoder in
        let container = try decoder.singleValueContainer()
        let dateStr = try container.decode(String.self)

        var date: Date?
        date = DateFormatter.yyyyMMddTHHmmssXXXXX.date(from: dateStr)
        if date == nil {
            date = DateFormatter.yyyyMMddTHHmmssSSSXXXXX.date(from: dateStr)
        }
        if date == nil {
            date = DateFormatter.yyyyMMddTHHmmssSSSZZZZZ.date(from: dateStr)
        }

        guard let date = date else {
            throw DecodingError.dataCorruptedError(in: container,
                                                   debugDescription: "[DateDecodingStrategy] invalid date formatt: " + dateStr)
        }

        return date
    }
}

extension JSONDecoder.KeyDecodingStrategy {
    static let convertFromUpperCamelCase = JSONDecoder.KeyDecodingStrategy.custom { keys in
        let codingKey = keys.last!
        let firstLetter = codingKey.stringValue.prefix(1).lowercased()
        let newCodingKey = firstLetter + codingKey.stringValue.dropFirst()
        return AnyCodingKey(stringValue: newCodingKey)!
    }
}

struct AnyCodingKey: CodingKey {
    var stringValue: String
    var intValue: Int?

    init?(stringValue: String) {
        intValue = nil
        self.stringValue = stringValue
    }

    init?(intValue: Int) {
        stringValue = String(intValue)
        self.intValue = intValue
    }
}
