//
//  UIStoryboard+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

public extension UIStoryboard {
    class func instantiateViewController<T: UIViewController>() -> T {
        let storyboard = UIStoryboard(name: T.className, bundle: .main)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: T.className) as? T else {
            fatalError("Could not instantiate initial storyboard with name: \(T.className)")
        }

        return viewController
    }
}
