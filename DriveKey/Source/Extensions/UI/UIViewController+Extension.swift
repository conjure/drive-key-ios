//
//  UIViewController+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit
import SwiftUI

extension UIViewController {
    func add(child: UIViewController, to: UIView? = nil, animated: Bool = false) {
        addChild(child)

        let parentView = to ?? view
        parentView?.addToFill(child.view)
        child.didMove(toParent: self)
        if animated {
            child.view.alpha = 0
            UIView.animate(withDuration: 0.2) {
                child.view.alpha = 1
            }
        }
    }

    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }

    func dismissAllPresentedModals(_ animated: Bool = false) {
        var viewControllers = [UIViewController]()
        var currentViewController = self

        while let presentedVC = currentViewController.presentedViewController {
            viewControllers.append(currentViewController)
            currentViewController = presentedVC
        }
        viewControllers.forEach { $0.dismiss(animated: animated, completion: nil) }
    }

    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    /// Add a SwiftUI `View` as a child of the input `UIView`.
    /// - Parameters:
    ///   - swiftUIView: The SwiftUI `View` to add as a child.
    ///   - view: The `UIView` instance to which the view should be added.
    func addSwiftUIView<Content>(_ swiftUIView: Content, to view: UIView, hideNavBar: Bool = false) where Content: View {
        let hostingController = HostingController(wrappedView: swiftUIView, hideNavigationBar: hideNavBar)

        /// Add as a child of the current view controller.
        addChild(hostingController)

        /// Add the SwiftUI view to the view controller view hierarchy.
        view.addSubview(hostingController.view)

        /// Setup the contraints to update the SwiftUI view boundaries.
        hostingController.view.backgroundColor = .clear
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            hostingController.view.topAnchor.constraint(equalTo: view.topAnchor),
            hostingController.view.leftAnchor.constraint(equalTo: view.leftAnchor),
            view.bottomAnchor.constraint(equalTo: hostingController.view.bottomAnchor),
            view.rightAnchor.constraint(equalTo: hostingController.view.rightAnchor)
        ]

        NSLayoutConstraint.activate(constraints)

        /// Notify the hosting controller that it has been moved to the current view controller.
        hostingController.didMove(toParent: self)
    }
}
