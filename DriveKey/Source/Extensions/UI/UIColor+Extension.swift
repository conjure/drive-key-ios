//
//  UIColor+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

extension UIColor {
    convenience init(hex: Int) {
        let rgb = (
            R: CGFloat((hex >> 16) & 0xFF) / 255,
            G: CGFloat((hex >> 08) & 0xFF) / 255,
            B: CGFloat((hex >> 00) & 0xFF) / 255
        )

        self.init(red: rgb.R, green: rgb.G, blue: rgb.B, alpha: 1)
    }
}
