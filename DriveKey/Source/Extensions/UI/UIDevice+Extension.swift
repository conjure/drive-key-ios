//
//  UIDevice+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 12/10/2021.
//

import UIKit

extension UIDevice {
    var hasNotch: Bool {
        let keyWindow = UIApplication.shared.windows.filter(\.isKeyWindow).first
        return keyWindow?.safeAreaInsets.bottom ?? 0 > 0
    }
}
