//
//  GMSMapView+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/01/2022.
//

import GoogleMaps

extension GMSMapView {
    func setStyle(_ jsonName: String) {
        if let stylePath = Bundle.main.path(forResource: jsonName, ofType: nil) {
            let styleURL = URL(fileURLWithPath: stylePath)
            do {
                try mapStyle = GMSMapStyle(contentsOfFileURL: styleURL)
            } catch {
                print("[GMSMapView] Style json failed to load error: \(error)")
            }
        }
    }
}
