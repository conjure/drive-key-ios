//
//  UILabel+Extension.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 07/03/2022.
//

import UIKit

extension UILabel {
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor, labelFrame: CGRect?) {
        let readMoreText: String = trailingText + moreText

        let rect = labelFrame ?? self.frame
        let lengthForVisibleString: Int = self.visibleTextLength(for: rect)
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString)
            .replacingCharacters(in: NSRange(location: lengthForVisibleString,
                                             length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString)
            .replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength),
                                             length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore,
                                                         attributes: [NSAttributedString.Key.font: self.font!])
        let readMoreAttributed = NSMutableAttributedString(string: moreText,
                                                           attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }

    typealias LabelFont = (font: UIFont, textColor: UIColor)

    func addTrailingWithAttrSections(firstSectionFont: LabelFont,
                                     secondSectionFont: LabelFont,
                                     trailingText: String,
                                     moreText: String,
                                     trailingFont: LabelFont, labelFrame: CGRect?) {
        let readMoreText: String = trailingText + moreText

        let rect = labelFrame ?? self.frame
        let lengthForVisibleString: Int = self.visibleTextLength(for: rect)
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString)
            .replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength),
                                        length: readMoreLength), with: "") + trailingText

        let separatedTrimmedString = trimmedForReadMore.components(separatedBy: "\n\r")
        guard !separatedTrimmedString.isEmpty else { return }
        let infoAttrStr = NSMutableAttributedString(string: separatedTrimmedString[0],
                                                    attributes: [.font: firstSectionFont.font,
                                                                 .foregroundColor: firstSectionFont.textColor])

        if separatedTrimmedString.count == 2 {
            infoAttrStr.append(.init(string: "\n\r\(separatedTrimmedString[1])",
                                     attributes: [.font: secondSectionFont.font,
                                                  .foregroundColor: secondSectionFont.textColor]))
        }

        let readMoreAttributed = NSMutableAttributedString(string: moreText,
                                                           attributes: [NSAttributedString.Key.font: trailingFont.font, NSAttributedString.Key.foregroundColor: trailingFont.textColor])
        infoAttrStr.append(readMoreAttributed)
        self.attributedText = infoAttrStr
    }

    func visibleTextLength(for labelFrame: CGRect) -> Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = labelFrame.width
        let labelHeight: CGFloat = labelFrame.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)

        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key: Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)

        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString)
                .substring(to: index).boundingRect(with: sizeConstraint,
                                                   options: .usesLineFragmentOrigin,
                                                   attributes: attributes as? [NSAttributedString.Key: Any],
                                                   context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}
