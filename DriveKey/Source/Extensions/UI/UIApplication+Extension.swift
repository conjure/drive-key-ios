//
//  UIApplication+Extension.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

extension UIApplication {
    class var safeAreaHeightTop: CGFloat {
        guard let window = UIApplication.shared.windows.first else { return 0 }
        let safeAreaFrame = window.safeAreaLayoutGuide.layoutFrame

        return safeAreaFrame.minY
    }

    class var safeAreaHeightBottom: CGFloat {
        guard let window = UIApplication.shared.windows.first else { return 0 }
        let safeAreaFrame = window.safeAreaLayoutGuide.layoutFrame

        return window.frame.maxY - safeAreaFrame.maxY
    }
    
    static var appWindow: UIWindow? {
        UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .compactMap({$0 as? UIWindowScene})
            .first?.windows
            .filter({ $0.isKeyWindow }).first
    }
}
