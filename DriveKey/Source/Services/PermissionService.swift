//
//  PermissionService.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/10/2021.
//

import Foundation

class PermissionService {
    let locationService: PermissionProvidable
    let notificationService: PermissionProvidable
    let motionService: PermissionProvidable

    init(location: PermissionProvidable, notification: PermissionProvidable, motion: PermissionProvidable) {
        locationService = location
        notificationService = notification
        motionService = motion
    }

    func requestPermissionLocation() {
        locationService.requestPermission()
    }

    func requestPermissionNotification() {
        notificationService.requestPermission()
    }

    func requestPermissionMotion() {
        motionService.requestPermission()
    }

    func fetchAuthorizationData() {
        let group = DispatchGroup()

        group.enter()
        locationService.isPermissionGranted { granted in
            print("[PermissionService] location granted: \(granted)")
            group.leave()
        }

        group.enter()
        notificationService.isPermissionGranted { granted in
            print("[PermissionService] notification granted: \(granted)")
            group.leave()
        }

        group.enter()
        motionService.isPermissionGranted { granted in
            print("[PermissionService] motion granted: \(granted)")
            group.leave()
        }

        group.notify(queue: .main) {
            print("[PermissionService] fetchAuthorizationData finished")
        }
    }

    var isLocationAuthorized: Bool {
        locationService.isAuthorized
    }

    var isNotificationAuthorized: Bool {
        notificationService.isAuthorized
    }

    var isMotionAuthorized: Bool {
        motionService.isAuthorized
    }

    var shouldShowPermissions: Bool {
        print("[PermissionService] shouldShowPermissions asked location: \(locationService.isPermissionAsked)")
        print("notification: \(notificationService.isPermissionAsked)")
        print("motion: \(motionService.isPermissionAsked)")
        return !locationService.isPermissionAsked || !notificationService.isPermissionAsked || !motionService.isPermissionAsked
    }
}
