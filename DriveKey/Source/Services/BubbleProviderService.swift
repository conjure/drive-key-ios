//
//  BubbleProviderService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 08/02/2022.
//

import Foundation
import Combine
import CoreMotion

class BubbleProviderService: BubbleProvidable {
    private let remoteBubbleService: BubbleProvidable!
    private let biometricService: BiometricService!
    private let userDefaults: UserDefaults
    private let locationService: LocationProvidable

    private var tasks = Set<AnyCancellable>()
    var bubbles: [Bubble] = []
    var syncedSubject: PassthroughSubject<[Bubble], Never> = PassthroughSubject<[Bubble], Never>()

    init(remoteBubbleService: BubbleProvidable,
         biometricService: BiometricService,
         locationService: LocationProvidable = LocationService(),
         userDefaults: UserDefaults = .standard) {
        self.remoteBubbleService = remoteBubbleService
        self.biometricService = biometricService
        self.locationService = locationService
        self.userDefaults = userDefaults

        setupPublishers()
    }

    func sync(for user: DrivekeyUser) {
        remoteBubbleService.sync(for: user)
    }

    func reset() {
        self.remoteBubbleService.reset()
        self.bubbles = []
    }

    func refresh() {
        self.prepareBubbles()
    }

    private func setupPublishers() {
        remoteBubbleService.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.prepareBubbles()
            }
            .store(in: &tasks)

        locationService.locationAuthSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.prepareBubbles()
            }
            .store(in: &tasks)
    }

    private func prepareBubbles() {
         var backendBubbles = remoteBubbleService.bubbles

         if let bubble = createLocationServiceBubbles() {
             backendBubbles.append(bubble)
         }
         // Biometrics
         if biometricService.canEvaluate(), userDefaults.integer(forKey: UserDefaultKeys.biometricStatus) == BiometricStatus.unknown.rawValue {
             let message = biometricService.sourceType == .faceID ? "bubble_biometric_message_face".localized : "bubble_biometric_message_touch".localized
             let bubble = Bubble(bubbleType: .biometric, title: "bubble_biometric_title".localized, message: message)
             backendBubbles.append(bubble)
         }

         self.bubbles = backendBubbles.sorted()
         self.syncedSubject.send(self.bubbles)
     }
     
    private func createLocationServiceBubbles() -> Bubble? {
        if !locationService.isLocationServicesEnabled {
            let title = "bubble_location_service_title".localized
            let message = "bubble_location_service_message".localized
            return Bubble(bubbleType: .locationPermission, title: title, message: message)
        }

        if !(locationService.isAuthorizedAlways && locationService.allowsBackgroundUpdates) || !locationService.isPreciseLocation {
            let title = "bubble_location_permission_title".localized
            let message = "bubble_location_permission_message".localized
            return Bubble(bubbleType: .locationPermission, title: title, message: message)
        }

        return nil
    }
}
