//
//  AuthService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 28/07/2021.
//

import AuthenticationServices
import Combine
import CryptoKit
import FirebaseAuth
import Foundation

class AuthService: NSObject, ObservableObject {
    private let userDefaults: UserDefaults
    private var handler: AuthStateDidChangeListenerHandle?
    private var authToken: String?
    private let providerIdApple = "apple.com"

    var authSubject = PassthroughSubject<(user: DrivekeyUser, newUser: Bool), Never>()
    var authCodeSubject = PassthroughSubject<String, Never>()
    var errorSubject = PassthroughSubject<Error, Never>()
    var verificationSubject = PassthroughSubject<Bool, Never>()

    init(userDefaults: UserDefaults = .standard) {
        self.userDefaults = userDefaults

        super.init()

        if !userDefaults.bool(forKey: UserDefaultKeys.didLaunchAppBefore) {
            // Clear any previous cached users
            _ = logout()
            userDefaults.set(true, forKey: UserDefaultKeys.didLaunchAppBefore)
        }

        reloadUser()
        registerHandler()

        NotificationCenter.default.addObserver(self, selector: #selector(reloadUser), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    private func registerHandler() {
        handler = Auth.auth().addStateDidChangeListener { [weak self] _, user in
            self?.userDefaults.setValue(user != nil, forKey: UserDefaultKeys.loggedIn)
        }
    }

    var isUserLoggedIn: Bool {
        userDefaults.bool(forKey: UserDefaultKeys.loggedIn)
    }

    var isUserEmailVerified: Bool {
        Auth.auth().currentUser?.isEmailVerified ?? false
    }

    @objc func reloadUser() {
        Auth.auth().currentUser?.reload(completion: { error in
            if let error = error {
                print("[AuthService] - Error reloading user - \(error)")
            } else {
                self.verificationSubject.send(self.isUserEmailVerified)
            }
        })
    }

    func signup(with email: String, password: String) -> AnyPublisher<DrivekeyUser, AuthError> {
        Future<DrivekeyUser, AuthError> { promise in
            Auth.auth().createUser(withEmail: email, password: password) { result, error in
                if let error = error {
                    if let err = error as NSError? {
                        return promise(.failure(self.parseFirebaseError(err)))
                    }
                    print("Error signing user up - \(error)")
                    return promise(.failure(AuthError.unknown(reason: error.localizedDescription)))
                } else if let user = result?.user {
                    // Profile updated.
                    let currentUser = DrivekeyUser(userID: user.uid, email: email)
                    self.sendEmailVerification()
                    return promise(.success(currentUser))
                }
            }
        }.eraseToAnyPublisher()
    }

    func login(with email: String, password: String) -> AnyPublisher<DrivekeyUser, AuthError> {
        Future<DrivekeyUser, AuthError> { promise in
            Auth.auth().signIn(withEmail: email, password: password) { result, error in
                if let error = error {
                    if let err = error as NSError? {
                        return promise(.failure(self.parseFirebaseError(err)))
                    }
                    print("Error logging user in - \(error)")
                    return promise(.failure(AuthError.unknown(reason: error.localizedDescription)))
                }

                if let user = result?.user {
                    let firstName = user.displayName?.components(separatedBy: " ").first
                    let currentUser = DrivekeyUser(userID: user.uid, firstName: firstName, email: email)
                    return promise(.success(currentUser))
                }
            }
        }.eraseToAnyPublisher()
    }

    func sendResetPasswordEmail(for email: String) -> AnyPublisher<Bool, AuthError> {
        Future<Bool, AuthError> { promise in
            let actionCodeSettings = ActionCodeSettings()
            actionCodeSettings.url = URL(
                string: Configuration.DynamicLinkResetPassword
            )
            actionCodeSettings.dynamicLinkDomain = Configuration.DynamicLinkDomain
            actionCodeSettings.handleCodeInApp = true
            actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)

            Auth.auth().sendPasswordReset(withEmail: email, actionCodeSettings: actionCodeSettings) { error in
                if let error = error {
                    if let err = error as NSError? {
                        return promise(.failure(self.parseFirebaseError(err)))
                    }
                    print("Error sending password reset - \(error)")
                    return promise(.failure(AuthError.unknown(reason: error.localizedDescription)))
                }
                return promise(.success(true))
            }
        }.eraseToAnyPublisher()
    }

    func resetPassword(newPassword: String, oobCode: String) -> AnyPublisher<Bool, AuthError> {
        Future<Bool, AuthError> { promise in
            Auth.auth().confirmPasswordReset(withCode: oobCode, newPassword: newPassword) { error in
                if let error = error {
                    if let err = error as NSError? {
                        return promise(.failure(self.parseFirebaseError(err)))
                    }
                    print("Error resetting password - \(error)")
                    return promise(.failure(AuthError.unknown(reason: error.localizedDescription)))
                }
                return promise(.success(true))
            }
        }.eraseToAnyPublisher()
    }

    func sendEmailVerification(_ completion: ((Error?) -> Void)? = nil) {
        Auth.auth().currentUser?.sendEmailVerification { error in
            if let error = error {
                print("Error sending email verification - \(error)")
                completion?(error)
            } else {
                print("Sent email verification for user \(Auth.auth().currentUser?.email ?? "")")
                completion?(nil)
            }
        }
    }

    private func parseFirebaseError(_ error: NSError) -> AuthError {
        switch error.code {
        case FirebaseAuth.AuthErrorCode.userDisabled.rawValue:
            return AuthError.userDisabled
        case FirebaseAuth.AuthErrorCode.userNotFound.rawValue:
            return AuthError.userNotFound
        case FirebaseAuth.AuthErrorCode.wrongPassword.rawValue:
            return AuthError.wrongPassword
        case FirebaseAuth.AuthErrorCode.invalidEmail.rawValue:
            return AuthError.invalidEmail
        case FirebaseAuth.AuthErrorCode.emailAlreadyInUse.rawValue:
            return AuthError.emailInUse
        case FirebaseAuth.AuthErrorCode.expiredActionCode.rawValue:
            return (AuthError.expiredResetLink)
        case FirebaseAuth.AuthErrorCode.weakPassword.rawValue:
            return (AuthError.weakPassword)
        case FirebaseAuth.AuthErrorCode.invalidActionCode.rawValue:
            return AuthError.invalidResetLink
        case FirebaseAuth.AuthErrorCode.networkError.rawValue:
            return AuthError.networkError
        default:
            return .unknown(reason: error.localizedFailureReason ?? error.localizedDescription)
        }
    }

    // Unhashed nonce.
    fileprivate var currentNonce: String?

    func generateAppleIDRequest() -> ASAuthorizationAppleIDRequest {
        let nonce = randomNonceString()
        currentNonce = nonce
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        request.nonce = sha256(nonce)

        return request
    }

    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            String(format: "%02x", $0)
        }.joined()

        return hashString
    }

    // Adapted from https://auth0.com/docs/api-auth/tutorials/nonce#generate-a-cryptographically-random-nonce
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: [Character] =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length

        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }

            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }

                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }

        return result
    }

    private func registerNewAccount(credential: ASAuthorizationAppleIDCredential) {
        guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
        }
        guard let appleIDToken = credential.identityToken else {
            print("Unable to fetch identity token")
            errorSubject.send(AuthError.unknown(reason: "Unable to fetch identity token"))
            return
        }

        guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            errorSubject.send(AuthError.unknown(reason: "Unable to serialize token string from data: \(appleIDToken.debugDescription)"))
            return
        }
        // Initialize a Firebase credential.
        let firebaseCred = OAuthProvider.credential(withProviderID: providerIdApple, idToken: idTokenString, rawNonce: nonce)

        Auth.auth().signIn(with: firebaseCred) { result, error in
            if let error = error {
                if let err = error as NSError? {
                    self.errorSubject.send(self.parseFirebaseError(err))
                    return
                }

                print("Error logging user in - \(error)")
                self.errorSubject.send(error)
                return
            } else if let user = result?.user, user.displayName == nil {
                guard let name = credential.fullName, let firstName = name.givenName, let secondName = name.familyName, let email = credential.email else {
                    print("Unable to fetch full name")
                    return
                }
                // update name
                let currentUser = DrivekeyUser(userID: user.uid, firstName: firstName, lastName: secondName, email: email)
                self.authSubject.send((currentUser, true))
            }
        }
    }
    
    var isAppleSignIn: Bool {
        if Auth.auth().currentUser?.providerData.first(where: { $0.providerID.contains(providerIdApple)}) != nil {
            return true
        }
        
        return false
    }
    
    private func extractAuthCode(credential: ASAuthorizationAppleIDCredential) {
        guard currentNonce != nil else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
        }
        guard let authCode = credential.authorizationCode else {
            print("Unable to fetch authorization code")
            errorSubject.send(AuthError.unknown(reason: "Unable to fetch authorization code "))
            return
        }
        
        guard let authCodeString = String(data: authCode, encoding: .utf8) else {
            errorSubject.send(AuthError.unknown(reason: "Unable to serialize authorization code string from data: \(authCode.debugDescription)"))
            return
        }
        
        authCodeSubject.send(authCodeString)
    }

    private func signInWithExistingAccount(credential: ASAuthorizationAppleIDCredential) {
        guard let nonce = currentNonce else {
            fatalError("Invalid state: A login callback was received, but no login request was sent.")
        }
        guard let appleIDToken = credential.identityToken else {
            print("Unable to fetch identity token")
            errorSubject.send(AuthError.unknown(reason: "Unable to fetch identity token"))
            return
        }

        guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
            errorSubject.send(AuthError.unknown(reason: "Unable to serialize token string from data: \(appleIDToken.debugDescription)"))
            return
        }
        // Initialize a Firebase credential.
        let firebaseCred = OAuthProvider.credential(withProviderID: providerIdApple, idToken: idTokenString, rawNonce: nonce)

        Auth.auth().signIn(with: firebaseCred) { result, error in
            if let error = error {
                if let err = error as NSError? {
                    self.errorSubject.send(self.parseFirebaseError(err))
                    return
                }
                print("Error logging user in - \(error)")
                self.errorSubject.send(error)
                return
            } else if let user = result?.user, let email = user.email {
                let currentUser = DrivekeyUser(userID: user.uid, email: email)
                self.authSubject.send((currentUser, false))
            }
        }
    }

    func logout() -> AnyPublisher<Bool, AuthError> {
        Future<Bool, AuthError> { promise in
            do {
                try Auth.auth().signOut()
                return promise(.success(true))
            } catch {
                print("Could not sign out due to \(error)")
                if let err = error as NSError? {
                    return promise(.failure(self.parseFirebaseError(err)))
                }
                return promise(.failure(AuthError.unknown(reason: error.localizedDescription)))
            }
        }.eraseToAnyPublisher()
    }
}

extension AuthService: ASAuthorizationControllerDelegate {
    func authorizationController(controller _: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIdCredential as ASAuthorizationAppleIDCredential:
            if Auth.auth().currentUser == nil {
                if appleIdCredential.email != nil, appleIdCredential.fullName != nil {
                    registerNewAccount(credential: appleIdCredential)
                } else {
                    signInWithExistingAccount(credential: appleIdCredential)
                }
            } else {
                extractAuthCode(credential: appleIdCredential)
            }
        default:
            break
        }
    }

    func authorizationController(controller _: ASAuthorizationController, didCompleteWithError error: Error) {
        print("Error signing in with Apple - \(error.localizedDescription)")
        errorSubject.send(error)
    }
}
