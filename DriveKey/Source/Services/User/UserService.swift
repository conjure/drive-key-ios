//
//  UserService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 10/08/2021.
//

import Combine
import FirebaseFirestore
import Foundation

class UserService: UserProvidable {
    private let db = Firestore.firestore()
    private var userListener: ListenerRegistration?
    private var tripsListener: ListenerRegistration?
    private var motionTripsListener: ListenerRegistration?
    private var summaryListener: ListenerRegistration?
    private let userDefaults: UserDefaults!
    private let vehicleFetcher: VehicleFetcher
    
    var syncedSubject: PassthroughSubject<DrivekeyUser, Never> = PassthroughSubject<DrivekeyUser, Never>()
    private var cancellables = Set<AnyCancellable>()
    
    var currentUser: DrivekeyUser?
    private(set) var lastTrip: Trip?
    private(set) var mileageSummary: MileageSummary?
    private(set) var vehicles: [VehicleData]?
    
    init(userDefaults: UserDefaults = .standard, vehicleFetcher: VehicleFetcher) {
        self.userDefaults = userDefaults
        self.vehicleFetcher = vehicleFetcher
        
        registerSubscriptions()
        
        if let user = loadCache() {
            setupListeners(for: user.uid)
        }
    }
    
    private func registerSubscriptions() {
        vehicleFetcher.data
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                self?.vehicles = data
            }
            .store(in: &cancellables)
    }
    
    func setUser(_ user: DrivekeyUser) {
        setupListeners(for: user.uid)
    }
    
    private func setupListeners(for userID: String) {
        setupUserListener(for: userID)
        setupTripsListener(for: userID)
        setupSummaryListener(for: userID)
        vehicleFetcher.registerListener(userID)
    }
    
    private func setupUserListener(for userID: String) {
        let userRef = db.collection("users").document(userID)
        userListener = userRef
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("[UserService] Error fetching user document: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("[UserService] User Document data was empty.")
                    return
                }
                
                let decoder = Firestore.Decoder()
                
                do {
                    let user = try decoder.decode(DrivekeyUser.self, from: data)
                    self.sync(user: user)
                } catch {
                    print("[UserService] Could not parse user - \(error)")
                }
            }
    }
    
    private func setupTripsListener(for userID: String) {
        let tripsRef = db.collection("users").document(userID).collection("trips")
            .order(by: "startAt", descending: true)
            .whereField("active", isEqualTo: true)
            .limit(to: 1)
        tripsListener = tripsRef
            .addSnapshotListener { snap, error in
                if let error = error {
                    print("[UserService] Error setting trips listener: \(error)")
                } else {
                    if let data = snap?.documents.first {
                        let decoder = Firestore.Decoder()
                        do {
                            let trip = try decoder.decode(Trip.self, from: data.data())
                            self.sync(trip: trip)
                        } catch let error {
                            print("[UserService] Could not parse last trip - \(error)")
                        }
                    } else {
                        print("[UserService] No last trip data")
                        self.lastTrip = nil
                        self.sync(trip: nil)
                    }
                }
            }
    }
    
    private func setupSummaryListener(for userId: String) {
        let tripsRef = db.collection("users").document(userId).collection("business-trips-summary")
        summaryListener = tripsRef
            .addSnapshotListener { querySnapshot, error in
                if let error = error {
                    print("[UserService] Error setting summary listener: \(error)")
                } else {
                    guard let documents = querySnapshot?.documents else {
                        print("[UserService] Error fetching documents: \(error!)")
                        return
                    }
                    
                    var months = [MonthSummary]()
                    
                    let decoder = Firestore.Decoder()
                    for document in documents {
                        do {
                            let summaryMonth = try decoder.decode(MonthSummary.self, from: document.data())
                            months.append(summaryMonth)
                        } catch let error {
                            print("[UserService] Could not parse summary - \(error)")
                        }
                    }
                    
                    let summary = MileageSummary(months: months)
                    self.sync(summary: summary)
                }
            }
    }
    
    private func loadCache() -> DrivekeyUser? {
        let decoder = JSONDecoder()
        // Load last trip
        if let savedTrip = userDefaults.object(forKey: UserDefaultKeys.lastTrip) as? Data {
            do {
                let loadedTrip = try decoder.decode(Trip.self, from: savedTrip)
                print("[UserService] loading trip data from cache - \(loadedTrip)")
                lastTrip = loadedTrip
            } catch {
                print("[UserService] Error decoding cached trip - \(error)")
            }
        }
        
        // mileage summary
        if let savedSummary = userDefaults.object(forKey: UserDefaultKeys.mileageSummary) as? Data {
            do {
                let loadedSummary = try decoder.decode(MileageSummary.self, from: savedSummary)
                print("[UserService] loading summary data from cache - \(loadedSummary)")
                mileageSummary = loadedSummary
            } catch {
                print("[UserService] Error decoding cached summary - \(error)")
            }
        }
        
        // Load User
        if let savedPerson = userDefaults.object(forKey: UserDefaultKeys.currentUser) as? Data {
            do {
                let loadedPerson = try decoder.decode(DrivekeyUser.self, from: savedPerson)
                print("[UserService] loading user data from cache - \(loadedPerson)")
                currentUser = loadedPerson
                return loadedPerson
            } catch {
                print("[UserService] Error decoding cached user - \(error)")
                return nil
            }
        }
        return nil
    }
    
    private func sync(user: DrivekeyUser? = nil, trip: Trip? = nil, summary: MileageSummary? = nil, motionTrip: MotionTrip? = nil) {
        let encoder = JSONEncoder()
        
        if let user = user, let encoded = try? encoder.encode(user) {
            userDefaults.set(encoded, forKey: UserDefaultKeys.currentUser)
            currentUser = user
            userDefaults.setValue(user.activeVehicle != nil, forKey: UserDefaultKeys.shouldTrackTrips)
            print("[UserService] User synced")
        }
        
        if let trip = trip, let encoded = try? encoder.encode(trip) {
            userDefaults.set(encoded, forKey: UserDefaultKeys.lastTrip)
            lastTrip = trip
            print("[UserService] Last Trip synced")
        }
        
        if let summary = summary, let encoded = try? encoder.encode(summary) {
            userDefaults.set(encoded, forKey: UserDefaultKeys.mileageSummary)
            mileageSummary = summary
            print("[UserService] Summary synced")
        }
        
        if let user = currentUser {
            self.syncedSubject.send(user)
        }
    }
    
    var vehiclesActive: [VehicleData]? {
        guard let vehicles = vehicles else { return nil }
        
        return vehicles.filter({ $0.status == .active })
    }
    
    func reset() {
        userListener?.remove()
        userListener = nil
        tripsListener?.remove()
        tripsListener = nil
        motionTripsListener?.remove()
        motionTripsListener = nil
        summaryListener?.remove()
        summaryListener = nil
        userDefaults.removeObject(forKey: UserDefaultKeys.currentUser)
        userDefaults.removeObject(forKey: UserDefaultKeys.lastTrip)
        userDefaults.removeObject(forKey: UserDefaultKeys.mileageSummary)
        currentUser = nil
        lastTrip = nil
    }
}
