//
//  VehicleFetcher.swift
//  DriveKey
//
//  Created by Piotr Wilk on 29/07/2022.
//

import Foundation
import Combine
import FirebaseFirestore

class VehicleFetcher: VehicleProvidable {
    private let db = Firestore.firestore()
    private var dataListener: ListenerRegistration?
    private(set) var data = PassthroughSubject<[VehicleData], Never>()
    private(set) var error = PassthroughSubject<Error, Never>()
    
    func registerListener(_ userId: String) {
        unregisterListener()
        
        let path = "users" + "/" + userId
        registerDataListener(path)
    }
    
    private func registerDataListener(_ path: String) {
        let collectionsRef = db.document(path).collection("vehicles")
        dataListener = collectionsRef
            .addSnapshotListener { documentSnapshot, error in
                if let error = error {
                    print("[VehicleFetcher] Listener error: \(error)")
                    self.error.send(error)
                } else {
                    print("[VehicleFetcher] Listener snapshot")
                    var result = [VehicleData]()
                    
                    guard let snapshot = documentSnapshot else {
                        self.data.send(result)
                        return
                    }
                    
                    let decoder = Firestore.Decoder()
                    for snapshot in snapshot.documents {
                        do {
                            let model = try decoder.decode(VehicleData.self, from: snapshot.data())
                            result.append(model)
                        } catch let error {
                            print("[VehicleFetcher] Listener decode error: \(error)")
                        }
                    }
                    
                    self.data.send(result)
                }
            }
    }
    
    func unregisterListener() {
        dataListener?.remove()
        dataListener = nil
    }
}
