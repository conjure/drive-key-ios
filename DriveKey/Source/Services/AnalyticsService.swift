//
//  AnalyticsService.swift
//  DriveKey
//
//  Created by Piotr Wilk on 27/05/2022.
//

import Foundation
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseAuth
import Firebase

enum TripEvent: String {
    case appState
    case detection
    case data
    case status
    case geofence
}

class AnalyticsService {
    static func trackScreenEvent(_ screenName: String) {
        Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName: screenName])
    }

    static func trackEvent(_ event: TrackingEvent) {
        print("[AnalyticsService] tracking event - \(event)")

        // Firebase
        if let name = event.eventName {
            Analytics.logEvent(name, parameters: event.eventParams)
        }
    }
}
