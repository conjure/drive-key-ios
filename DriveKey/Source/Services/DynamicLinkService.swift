//
//  DynamicLinkService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import FirebaseDynamicLinks
import Foundation

enum DynamicLinkEvent {
    case resetPassword(oobCode: String)
}

class DynamicLinkService {
    var linkActionSubject = PassthroughSubject<DynamicLinkEvent, Never>()

    func verifyCustomURLScheme(_ url: URL) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            handleIncomingDynamicLink(dynamicLink)
            return true
        }

        return false
    }

    func handleUniversalLink(_ link: URL) {
        DynamicLinks.dynamicLinks().handleUniversalLink(link) { dynamicLink, error in
            guard error == nil else {
                print("Found an error with dynamic link - \(error!.localizedDescription)")
                return
            }

            if let dynamicLink = dynamicLink {
                self.handleIncomingDynamicLink(dynamicLink)
            }
        }
    }

    private func handleIncomingDynamicLink(_ dynamicLink: DynamicLink) {
        guard let url = dynamicLink.url else {
            print("Dynamic link has no url")
            return
        }

        print("Your incoming link parameter is \(url.absoluteString)")

        guard dynamicLink.matchType == .unique || dynamicLink.matchType == .default else {
            print("Not a strong enough match to continue")
            return
        }

        guard let queryItems = url.queryParameters else { return }

        if queryItems["continueUrl"] != nil {
            handleResetPasswordLink(queryItems)
        }
    }

    private func handleResetPasswordLink(_ queryItems: [String: String]) {
        guard let oobCode = queryItems["oobCode"], let continueUrl = queryItems["continueUrl"] else { return }

        switch continueUrl {
        case Configuration.DynamicLinkResetPassword:
            linkActionSubject.send(.resetPassword(oobCode: oobCode))
        default:
            return
        }
    }
}
