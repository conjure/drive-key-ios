//
//  LocationService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 11/08/2021.
//

import Combine
import CoreLocation
import Foundation

class LocationService: NSObject {
    private let locationManager: CLLocationManager!
    private var currentLocation: CLLocation?

    var locationSubject = PassthroughSubject<CLLocation?, Error>()
    var locationAuthSubject = PassthroughSubject<CLAuthorizationStatus, Never>()
    var locationPublisher = PassthroughSubject<CLLocation, Never>()
    
    private let regionRadius: Double = 200

    override init() {
        locationManager = CLLocationManager()
        super.init()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
        // locationManager.activityType = .automotiveNavigation
        
        if isAuthorized {
            startUpdates()
        }
    }
    
    private func startUpdates() {
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.startMonitoringVisits()
    }

    func requestLocation() {
        locationManager.requestAlwaysAuthorization()
    }

    func getCurrentLocation() {
        let status = locationManager.authorizationStatus

        if status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled() {
            return
        }

        // if haven't show location permission dialog before, show it to user
        if status == .notDetermined {
            locationManager.requestAlwaysAuthorization()
            return
        }

        // at this point the authorization status is authorized
        // request location data once
        locationManager.requestLocation()
    }

    var isAuthorized: Bool {
        let status = locationManager.authorizationStatus
        return !(status == .notDetermined || status == .denied)
    }

    var isPermissionAsked: Bool {
        let status = locationManager.authorizationStatus
        return !(status == .notDetermined)
    }
    
    var regionCount: Int {
        locationManager.monitoredRegions.count
    }
    
    var lastLocation: CLLocation? {
        currentLocation
    }
    
    private func log(_ message: String) {
        LOGGER(message)
        print("[LocationService] \(message)")
    }
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationAuthSubject.send(status)
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            print("[LocationService] user allow app to get location data when app is active or in background")
            locationSubject.send(CLLocation())
            startUpdates()
            return
        case .denied:
            print("[LocationService] user tap 'disallow' on the permission dialog, cant get location data")
            locationSubject.send(completion: .finished)
        case .restricted:
            print("[LocationService] parental control setting disallow location data")
        case .notDetermined:
            print("[LocationService] the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }

    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
            locationPublisher.send(location)
        }
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        locationSubject.send(completion: .failure(error))
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        log("EnterRegion: \(region.identifier) COUNT: \(regionCount)")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        log("ExitRegion: \(region.identifier) COUNT: \(regionCount)")
    }
}

extension LocationService: PermissionProvidable {
    func requestPermission() {
        requestLocation()
    }

    func isPermissionGranted(_ completion: ((Bool) -> Void)?) {
        completion?(isAuthorized)
    }
}

extension LocationService: LocationProvidable {
    var isAuthorizedAlways: Bool {
        locationManager.authorizationStatus == .authorizedAlways
    }
    
    var isAuthorizedWhenInUse: Bool {
        locationManager.authorizationStatus == .authorizedWhenInUse
    }
    
    var isPreciseLocation: Bool {
        locationManager.accuracyAuthorization == .fullAccuracy
    }

    var isLocationServicesEnabled: Bool {
        CLLocationManager.locationServicesEnabled()
    }

    var allowsBackgroundUpdates: Bool {
        locationManager.allowsBackgroundLocationUpdates
    }

    func startMonitoringRegion(_ location: CLLocationCoordinate2D, name: String) {
        let region = CLCircularRegion(center: location, radius: regionRadius, identifier: name)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        locationManager.startMonitoring(for: region)
        log("StartMonitoring: \(name) COUNT: \(regionCount)")
    }
    
    func stopMonitoringAllRegions() {
        log("StopMonitoring All COUNT: \(regionCount)")
        locationManager.monitoredRegions.forEach({ locationManager.stopMonitoring(for: $0) })
    }
}
