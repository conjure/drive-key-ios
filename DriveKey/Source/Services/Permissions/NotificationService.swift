//
//  NotificationService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 11/08/2021.
//

import Combine
import FirebaseMessaging
import UIKit

class NotificationService: NSObject {
    private(set) var isAuthorized = false
    private var shouldAsk = true
    private(set) var fcmToken: String?

    var tokenSubject = PassthroughSubject<String?, Error>()

    override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
    }

    func requestNotification() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions) { granted, error in
                self.shouldAsk = false

                if let error = error {
                    // Handle the error here.
                    print("[NotificationService] Error granting push permission - \(error)")
                    self.isAuthorized = false
                    self.tokenSubject.send(completion: .failure(error))
                }

                if !granted {
                    self.isAuthorized = false
                    self.tokenSubject.send(completion: .finished)
                } else {
                    self.isAuthorized = true
                    #if targetEnvironment(simulator)
                        self.tokenSubject.send(completion: .finished)
                    #else
                        self.tokenSubject.send(self.getDeviceToken())
                    #endif
                }
            }
        UIApplication.shared.registerForRemoteNotifications()
    }

    func getDeviceToken() -> String? {
        fcmToken
    }

    private func isNotificationGranted(_ completion: @escaping (_ granted: Bool) -> Void) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            var result = false
            self.shouldAsk = false

            switch settings.authorizationStatus {
            case .authorized, .provisional:
                print("[NotificationService] Permission .authorized, .provisional")
                result = true
            case .denied:
                print("[NotificationService] Permission .denied")
                result = false
            case .notDetermined:
                print("[NotificationService] Permission .notDetermined")
                result = false
                self.shouldAsk = true
            default:
                print("[NotificationService] Permission fallback granted = false ")
                result = false
            }
            
            completion(result)
        }
    }

    var isPermissionAsked: Bool {
        !shouldAsk
    }
}

extension NotificationService: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _: UNUserNotificationCenter,
        willPresent _: UNNotification,
        withCompletionHandler completionHandler:
        @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        completionHandler([.banner, .list, .sound])
    }

    func userNotificationCenter(
        _: UNUserNotificationCenter,
        didReceive _: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        completionHandler()
    }
}

extension NotificationService: MessagingDelegate {
    func messaging(
        _: Messaging,
        didReceiveRegistrationToken fcmToken: String?
    ) {
        print("[NotificationService] didReceiveRegistrationToken \(fcmToken ?? "nil")")
        self.fcmToken = fcmToken
    }
}

extension NotificationService: PermissionProvidable {
    func requestPermission() {
        requestNotification()
    }

    func isPermissionGranted(_ completion: ((Bool) -> Void)?) {
        isNotificationGranted { granted in
            self.isAuthorized = granted
            print("[NotificationService] isPermissionGranted: \(granted)")
            completion?(granted)
        }
    }
}
