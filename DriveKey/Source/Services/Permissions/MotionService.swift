//
//  MotionService.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/10/2021.
//

import UIKit
import Combine
import CoreMotion

class MotionService {
    private let manager: CMMotionActivityManager!
    
    var authorizationSubject = PassthroughSubject<Bool, Error>()
    
    var activityPublisher = PassthroughSubject<CMMotionActivity, Never>()
    var statusPublisher = CurrentValueSubject<CMAuthorizationStatus, Never>(.notDetermined)
    var errorPublisher = PassthroughSubject<Error, Never>()
    
    init() {
        manager = CMMotionActivityManager()
        updateStatus()
        
        if isAuthorized {
            listenToUpdates()
        }
    }
    
    func requestMotion() {
        registerMotion()
    }
    
    var isAuthorized: Bool {
        let status = CMMotionActivityManager.authorizationStatus()
        return status == .authorized
    }

    var isPermissionAsked: Bool {
        let status = CMMotionActivityManager.authorizationStatus()
        return !(status == .notDetermined)
    }
    
    func registerMotion() {
        let today = Date()

        manager.queryActivityStarting(from: today, to: today, to: OperationQueue.main, withHandler: { (_: [CMMotionActivity]?, error: Error?) in
            if error != nil {
                print("[MotionService] registerMotion error \(error!)")
                self.errorPublisher.send(error!)
                self.authorizationSubject.send(completion: .finished)
            } else {
                print("[MotionService] registerMotion Authorized")
                self.updateStatus()
                self.authorizationSubject.send(true)
            }
            
            self.manager.stopActivityUpdates()
            runOnMainThreadAsyncAfter(1) {
                self.listenToUpdates()
            }
        })
    }
    
    private func updateStatus() {
        statusPublisher.send(CMMotionActivityManager.authorizationStatus())
    }
    
    private func listenToUpdates() {
        manager.startActivityUpdates(to: .main) { activity in
            if let activity = activity {
                self.activityPublisher.send(activity)
            }
        }
    }
}

extension MotionService: MotionActivityProvidable {}

extension MotionService: PermissionProvidable {
    func requestPermission() {
        registerMotion()
    }

    func isPermissionGranted(_ completion: ((Bool) -> Void)?) {
        completion?(isAuthorized)
    }
}
