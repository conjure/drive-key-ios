//
//  AppDataService.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import Foundation
import Combine
import FirebaseFirestore
import FirebaseAuth

class AppDataService: AppDataProvidable {
    private let api: APIProtocol
    private let db: Firestore
    private var handler: AuthStateDidChangeListenerHandle?
    private var userListener: ListenerRegistration?
    private var _countries = [Country]()
    private var _currencies = [Currency]()
    
    var dataUpdated = PassthroughSubject<Bool, Never>()
    
    private let defaultCurrencyCode = "USD"
    
    private struct Paths {
        static let countries = "countries"
        static let currencies = "currencies"
    }
    
    init(_ api: APIProtocol, firestore: Firestore = Firestore.firestore()) {
        self.api = api
        self.db = firestore
        registerHandler()
    }
    
    private func registerHandler() {
        handler = Auth.auth().addStateDidChangeListener { [weak self] _, user in
            if user != nil {
                self?.fetchAllData({ success in
                    if success {
                        self?.setupUserListener(user!.uid)
                        self?.dataUpdated.send(true)
                    } else {
                        print("[AppDataService] fetchAllData failed")
                        self?.dataUpdated.send(false)
                    }
                })
            }
        }
    }
    
    private func setupUserListener(_ userID: String) {
        let userRef = db.collection("users").document(userID)
        userListener = userRef
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("[AppDataService] setupUserListener Error: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("[AppDataService] setupUserListener User Document data was empty.")
                    return
                }
                
                let decoder = Firestore.Decoder()
                
                do {
                    let user = try decoder.decode(DrivekeyUser.self, from: data)
                    print("[AppDataService] setupUserListener user: \(user.uid)")
                } catch {
                    print("[AppDataService] setupUserListener Could not parse user - \(error)")
                }
            }
    }
    
    private func removeListeners() {
        userListener?.remove()
        userListener = nil
    }
    
    private func fetchAllData(_ completion: @escaping (Bool) -> Void) {
        let group = DispatchGroup()
        
        var successCountries = false
        group.enter()
        fetchCountries { fetched in
            successCountries = fetched
            group.leave()
        }
        
        var successCurrencies = false
        group.enter()
        self.fetchCurrencies { fetched in
            successCurrencies = fetched
            group.leave()
        }
        
        group.notify(queue: .global(qos: .background)) {
            completion(successCountries && successCurrencies)
        }
    }
    
    private func fetchCountries(_ completion: @escaping (Bool) -> Void) {
        fetchData(Paths.countries) { (result: Result<[Country], Error>) in
            switch result {
            case .success(var data):
                data.append(Country.other)
                self._countries = data
                print("[AppDataService] fetchCountries success")
                completion(true)
            case .failure(let error):
                print("[AppDataService] fetchCountries error: \(error)")
                completion(false)
            }
        }
    }
    
    private func fetchCurrencies(_ completion: @escaping (Bool) -> Void) {
        fetchData(Paths.currencies) { (result: Result<[Currency], Error>) in
            switch result {
            case .success(let data):
                self._currencies = data
                print("[AppDataService] fetchCurrencies success")
                completion(true)
            case .failure(let error):
                print("[AppDataService] fetchCurrencies error: \(error)")
                completion(false)
            }
        }
    }
    
    private func fetchData<T: Codable>(_ path: String, completion: @escaping (Result<[T], Error>) -> Void) {
        let dataRef = db.collection(path)
        dataRef.getDocuments { (querySnapshot, err) in
            if let err = err {
                completion(.failure(err))
            } else {
                var result = [T]()
                if let snapshot = querySnapshot {
                    for document in snapshot.documents {
                        let decoder = Firestore.Decoder()
                        do {
                            let data = try decoder.decode(T.self, from: document.data())
                            result.append(data)
                        } catch let error {
                            completion(.failure(error))
                        }
                    }
                }
                completion(.success(result))
            }
        }
    }
    
    var countries: [Country] {
        _countries
    }
    
    var currencies: [Currency] {
        _currencies
    }
}
