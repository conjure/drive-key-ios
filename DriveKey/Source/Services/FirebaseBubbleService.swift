//
//  FirebaseBubbleService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/08/2021.
//

import Combine
import FirebaseFirestore
import Foundation

class FirebaseBubbleService: BubbleProvidable {
    private var currentBubbles = [Bubble]()
    private let db = Firestore.firestore()
    private var bubbleListener: ListenerRegistration?

    var syncedSubject = PassthroughSubject<[Bubble], Never>()

    var bubbles: [Bubble] {
        currentBubbles
    }

    func sync(for user: DrivekeyUser) {
        listenForRemoteBubbles(user.uid)
    }

    private func listenForRemoteBubbles(_ userID: String) {
        bubbleListener = db.collection("users").document(userID).collection("bubbles").addSnapshotListener { collectionSnapshot, error in
            guard let bubbles = collectionSnapshot else {
                print("[Bubble Service] Error fetching user bubbles: \(error!)")
                return
            }

            var tempBubbles: [Bubble] = []
            for document in bubbles.documents {
                let data = document.data()
                let decoder = Firestore.Decoder()
                do {
                    let bubble = try decoder.decode(Bubble.self, from: data)
                    if bubble.bubbleType != nil, !bubble.dismissed { tempBubbles.append(bubble) }
                } catch {
                    print("[Bubble Service] Could not parse bubble - \(error)")
                }
            }

            self.currentBubbles = tempBubbles
            print("[Bubble Service] synced bubbles - \(self.currentBubbles.count)")
            self.syncedSubject.send(tempBubbles)
        }
    }

    func reset() {
        bubbleListener?.remove()
        self.currentBubbles = []
        self.bubbleListener = nil
    }
}
