//
//  BiometricService.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Foundation
import LocalAuthentication

class BiometricService {
    private let context: LAContext
    private let policy: LAPolicy
    private let localizedReason: String

    private var error: NSError?

    init(context: LAContext = LAContext(),
         policy: LAPolicy = .deviceOwnerAuthenticationWithBiometrics,
         localizedReason: String = "biometric.alert.touch.reason".localized,
         localizedFallbackTitle: String = "biometric.alert.delay".localized,
         localizedCancelTitle: String = "biometric.alert.cancel".localized) {
        self.context = context
        self.policy = policy
        self.localizedReason = localizedReason
        context.localizedFallbackTitle = localizedFallbackTitle
        context.localizedCancelTitle = localizedCancelTitle

        _ = canEvaluate()
    }

    var sourceType: BiometricType {
        _ = context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)

        switch context.biometryType {
        case .none: return .none
        case .touchID: return .touchID
        case .faceID: return .faceID
        default: return .unknown
        }
    }

    func canEvaluate() -> Bool {
        return context.canEvaluatePolicy(policy, error: &error)
    }

    func evaluate(completion: @escaping (Bool, BiometricError?) -> Void) {
        // Asks Context to evaluate a Policy with a LocalizedReason
        context.evaluatePolicy(policy, localizedReason: localizedReason) { [weak self] success, error in
            // Moves to the main thread because completion triggers UI changes
            DispatchQueue.main.async {
                if success {
                    // Context successfully evaluated the Policy
                    completion(true, nil)
                } else {
                    // Unwraps Error
                    // If not available, sends false for Success & nil for BiometricError
                    guard let error = error else { return completion(false, nil) }

                    // Maps error to our BiometricError
                    let bioError = self?.biometricError(from: error as NSError)
                    completion(false, bioError)
                }
            }
        }
    }

    func authenticateWithPasscode(completion: @escaping (Bool, BiometricError?) -> Void) {
        let ctx = LAContext()
        ctx.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: localizedReason) { success, error in
            DispatchQueue.main.async {
                if success {
                    // Context successfully evaluated the Policy
                    completion(true, nil)
                } else {
                    // Unwraps Error
                    // If not available, sends false for Success & nil for BiometricError
                    guard let error = error else { return completion(false, nil) }

                    // Maps error to our BiometricError
                    let bioError = self.biometricError(from: error as NSError)
                    completion(false, bioError)
                }
            }
        }
    }

    private func biometricType(for type: LABiometryType) -> BiometricType {
        switch type {
        case .none: return .none
        case .touchID: return .touchID
        case .faceID: return .faceID
        default: return .unknown
        }
    }

    private func biometricError(from nsError: NSError) -> BiometricError {
        let error: BiometricError

        switch nsError {
        case LAError.authenticationFailed:
            error = .authenticationFailed
        case LAError.userCancel:
            error = .userCancel
        case LAError.userFallback:
            error = .userFallback
        case LAError.biometryNotAvailable:
            error = .biometryNotAvailable
        case LAError.biometryNotEnrolled:
            error = .biometryNotEnrolled
        case LAError.biometryLockout:
            error = .biometryLockout
        default:
            error = .unknown
        }

        return error
    }
}
