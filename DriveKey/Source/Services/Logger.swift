//
//  Logger.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/12/2023.
//

import UIKit

func LOGGER(_ message: String) {
    Logger.shared.log(message)
}

class Logger {
    static let shared = Logger()
    private var storage = [String]()
    
    init() {
        addAppVersion()
    }
    
    func log(_ message: String) {
        #if DEBUG
        logWithTimestamp(message)
        #endif
    }
    
    func reset() {
        storage.removeAll()
        addAppVersion()
    }
    
    var logs: [String] {
        storage
    }
    
    func generateTXT() -> URL {
        let title = "DK-" + DateFormatter.motionTimeLocal.string(from: Date())
        let text = logs.joined(separator: "\n")
        
        let temporaryFolder = FileManager.default.temporaryDirectory
        let fileName = title + ".txt"
        let temporaryFileURL = temporaryFolder.appendingPathComponent(fileName)
        do {
            try text.write(to: temporaryFileURL, atomically: true, encoding: .utf8)
        } catch {
            let log = "SHARE MOTION CSV ERROR: \(error)"
        }
        
        return temporaryFileURL
    }
    
    private func logWithTimestamp(_ message: String) {
        let timestamp = DateFormatter.loggerLocal.string(from: Date())
        let result = timestamp + " " + message
        storage.append(result)
        print(result)
    }
    
    private func addAppVersion() {
        let versionApp = "App version: \(Bundle.appVersion)"
        let versionSystem = "iOS: \(UIDevice.current.systemVersion)"
        let deviceModel = "Model: \(modelIdentifier)"
        let bundleId = "\nBundleId: \(Bundle.main.bundleIdentifier ?? "nil")"
        let log = versionApp + " " + versionSystem + " " + deviceModel + " " + bundleId
        storage.append(log)
    }
    
    //https://stackoverflow.com/a/30075200
    private var modelIdentifier: String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
            return simulatorModelIdentifier
        }
        
        var sysinfo = utsname()
        uname(&sysinfo) // ignore return value
        return String(bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
    }
}
