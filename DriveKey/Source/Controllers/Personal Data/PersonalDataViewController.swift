//
//  PersonalDataViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2021.
//

import UIKit

class PersonalDataViewController: ViewController, PickerPresentable {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var firstNameTextField: NamedTextField!
    @IBOutlet private var lastNameTextField: NamedTextField!
    @IBOutlet private var emailTextField: NamedTextField!
    @IBOutlet private var dobTextField: NamedTextField!
    @IBOutlet private var addressLine1TextField: NamedTextField!
    @IBOutlet private var addressLine2TextField: NamedTextField!
    @IBOutlet private var postcodeTextField: NamedTextField!
    @IBOutlet private var countryPickerField: NamedPickerField!
    @IBOutlet private var statePickerField: NamedPickerField!

    @IBOutlet private var dobBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var postcodeBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var stateHeightConstraint: NSLayoutConstraint!
    private let spacingNoError: CGFloat = -8
    private let spacingError: CGFloat = 12

    var viewModel: PersonalDataViewModel!
    private var editButton: UIButton?
    var api: APIProtocol!

    private var textFields = [NamedTextField]()
    private let keyboardView = KeyboardAccessoryView(frame: CGRect.zero)
    private var isEditMode = false
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?

    deinit {
        stopObservingKeyboardChanges()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "personal.data.title".localized
        
        editButton = SmallButton(type: .custom)
        editButton?.translatesAutoresizingMaskIntoConstraints = false
        editButton?.setTitle("edit".localized, for: .normal)
        editButton?.addTarget(self, action: #selector(onEdit), for: .touchUpInside)
        editButton?.setTitleColor(.black, for: .normal)
        editButton?.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .highlighted)
        editButton?.titleLabel?.font = UIFont.K2DMedium(16)
        editButton?.adjustsImageWhenHighlighted = true
        let editItem = UIBarButtonItem(customView: editButton!)
        navigationItem.rightBarButtonItem = editItem

        firstNameTextField.name = "personal.data.name.label".localized
        firstNameTextField.value = viewModel.name
        firstNameTextField.textField.keyboardType = .asciiCapable
        textFields.append(firstNameTextField)

        lastNameTextField.name = "personal.data.last.name.label".localized
        lastNameTextField.value = viewModel.lastName
        lastNameTextField.textField.keyboardType = .asciiCapable
        textFields.append(lastNameTextField)

        emailTextField.name = "personal.data.email.label".localized
        emailTextField.value = viewModel.email
        emailTextField.textField.keyboardType = .asciiCapable
        emailTextField.isDisabled = true

        dobTextField.name = "personal.data.dob.label".localized
        dobTextField.textField.placeholder = "personal.data.dob.label.placeholder".localized
        dobTextField.value = viewModel.dob
        dobTextField.textField.keyboardType = .numberPad
        dobTextField.validator = PersonalDataDOBTextValidator()
        dobTextField.errorMessage = "personal.data.dob.error.message".localized
        dobTextField.formatter = PersonalDataDOBTextFormatter()
        textFields.append(dobTextField)

        addressLine1TextField.name = "personal.data.address.line.1.label".localized
        addressLine1TextField.value = viewModel.addressLine1
        addressLine1TextField.textField.keyboardType = .asciiCapable
        textFields.append(addressLine1TextField)

        addressLine2TextField.name = "personal.data.address.line.2.label".localized
        addressLine2TextField.value = viewModel.addressLine2
        addressLine2TextField.textField.keyboardType = .asciiCapable
        textFields.append(addressLine2TextField)

        postcodeTextField.value = viewModel.postcode
        postcodeTextField.textField.keyboardType = .asciiCapable
        postcodeTextField.textField.autocapitalizationType = .allCharacters
        postcodeTextField.textField.textContentType = .postalCode
        postcodeTextField.validator = PersonalDataPostcodeTextValidator()
        textFields.append(postcodeTextField)
        updatePostcodeCaption()
        
        countryPickerField.name = "personal.data.country.label".localized
        countryPickerField.textField.placeholder = "personal.data.country.placeholder".localized
        countryPickerField.placeholderColor = .black
        countryPickerField.addRightIcon(UIImage(named: "picker-arrow-down")!, size: CGSize(width: 8, height: 16))
        countryPickerField.onRightButton = { [weak self] in
            self?.onCountry()
        }
        
        statePickerField.name = "personal.data.state.label".localized
        statePickerField.textField.placeholder = "personal.data.state.placeholder".localized
        statePickerField.placeholderColor = .black
        statePickerField.addRightIcon(UIImage(named: "picker-arrow-down")!, size: CGSize(width: 8, height: 16))
        statePickerField.onRightButton = { [weak self] in
            self?.onState()
        }

        enableEditing(false)

        for (index, textfield) in textFields.enumerated() {
            textfield.textField.tag = index
            textfield.textField.delegate = self

            if index == textFields.count - 1 {
                textfield.textField.returnKeyType = .done
            } else {
                textfield.textField.returnKeyType = .next
            }
        }

        keyboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        keyboardView.buttonTitle = "next".localized
        keyboardView.onButton = { [weak self] in
            guard let strongSelf = self else { return }
            _ = strongSelf.textFieldShouldReturn(strongSelf.dobTextField.textField)
        }
        dobTextField.textField.inputAccessoryView = keyboardView

        startObservingKeyboardChanges()
        setupHideKeyboardOnTap()
        
        if let indexCountry = viewModel.selectedCountryIndex {
            updateDataCountry(indexCountry)
        }
        
        if let indexState = viewModel.selectedStateIndex {
            updateDataState(indexState)
        } else {
            showStatePickerField(false)
        }
        
        if viewModel.isCountryUS {
            showStatePickerField(true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (navigationController as? NavigationController)?.setProfileBlackTheme()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presentedViewController?.dismiss(animated: false)
    }

    private func enableEditing(_ enable: Bool) {
        isEditMode = enable
        let editText = enable ? "save".localized : "edit".localized
        editButton?.setTitle(editText, for: .normal)
        textFields.forEach { $0.isEnabled = enable }
        enablePickerFields(enable)
    }
    
    private func enablePickerFields(_ enable: Bool) {
        let color = enable ? UIColor(hex: 0xE6E8EA) : UIColor.clear
        countryPickerField.isEnabled = enable
        countryPickerField.highlightColor = color
        statePickerField.isEnabled = enable
        statePickerField.highlightColor = color
    }
    
    private func showStatePickerField(_ show: Bool) {
        statePickerField.isHidden = !show
        stateHeightConstraint.constant = show ? 80 : 0
        view.layoutIfNeeded()
        
        if show {
            let bottomOffsetY = max(scrollView.contentSize.height - scrollView.bounds.size.height, 0)
            scrollView.setContentOffset(CGPoint(x: 0, y: bottomOffsetY), animated: true)
        }
    }

    private func performSaveData() {
        showSpinner()
        view.endEditing(true)

        let dobDate = viewModel.getDOBDate(dobTextField.text)
        let state = viewModel.selectedState?.code ?? ""
        let updateData = UpdateUserPayload(firstName: firstNameTextField.text,
                                           lastName: lastNameTextField.text,
                                           email: emailTextField.text,
                                           dateOfBirth: dobDate,
                                           addressLineOne: addressLine1TextField.text,
                                           addressLineTwo: addressLine2TextField.text,
                                           postcode: postcodeTextField.text,
                                           country: viewModel.selectedCountry?.name ?? "",
                                           countryCode: viewModel.selectedCountry?.code ?? "",
                                           state: state)
        print("[PersonalDataViewController] UpdateUserPayload: \(updateData)")

        api.userData(updateData) { result in
            self.hideSpinner()

            switch result {
            case .success:
                self.proceed()
            case let .failure(error):
                self.showAlertError(error: error)
            }
        }
    }

    private func proceed() {
        enableEditing(false)
    }

    private func didTapSave() {
        if postcodeTextField.validate() == false || dobTextField.validate() == false {
            view.endEditing(true)
            return
        }

        performSaveData()
    }
    
    private func presentPicker(_ data: [Country]) {
        showPicker(data.map({ $0.name })) { [weak self] index in
            self?.updateDataCountry(index)
        }
    }
    
    private func presentPicker(_ data: [CountryState]) {
        showPicker(data.map({ $0.name })) { [weak self] index in
            self?.updateDataState(index)
        }
    }
    
    private func updateDataCountry(_ index: Int) {
        let countries = viewModel.countries
        guard countries.count > index else { return }
        let country = countries[index]

        // clear selection
        if viewModel.shouldClearSelection(for: country) {
            countryPickerField.value = nil
            viewModel.selectedCountry = nil
        } else {
            countryPickerField.value = country.name
            viewModel.selectedCountry = country
        }
        
        let showState = viewModel.shouldShowStates(country)
        showStatePickerField(showState)
        if !showState {
            statePickerField.value = nil
            viewModel.selectedState = nil
        }
        
        updatePostcodeCaption()
    }
    
    private func updateDataState(_ index: Int) {
        guard let country = viewModel.selectedCountry, country.states.count > index else { return }
        let state = country.states[index]
        statePickerField.value = state.name
        viewModel.selectedState = state
    }
    
    private func updatePostcodeCaption() {
        let name = viewModel.isCountryUS ? "personal.data.postcode.label.us".localized : "personal.data.postcode.label".localized
        let error = viewModel.isCountryUS ? "invalid.postcode.error.message.us".localized : "invalid.postcode.error.message".localized
        postcodeTextField.name = name
        postcodeTextField.errorMessage = error
    }
    
    private func onCountry() {
        let countries = viewModel.countries
        guard !countries.isEmpty else { return }
        presentPicker(countries)
    }
    
    private func onState() {
        guard let country = viewModel.selectedCountry, !country.states.isEmpty else { return }
        presentPicker(country.states)
    }

    @objc
    func onEdit() {
        if isEditMode {
            didTapSave()
        } else {
            enableEditing(true)
        }
    }

    @discardableResult
    private func validate() -> Bool {
        let resultDob = dobTextField.validate()
        let constraintValueDob: CGFloat = resultDob == true ? spacingNoError : spacingError
        dobBottomConstraint.constant = constraintValueDob

        let resultPostcode = postcodeTextField.validate()
        let constraintValuePostcode: CGFloat = resultPostcode == true ? spacingNoError : spacingError
        postcodeBottomConstraint.constant = constraintValuePostcode

        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }

        return (resultDob && resultPostcode) == true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension PersonalDataViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }

    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height - keyboardView.bounds.height + 80, right: 0)
        scrollView.contentInset = contentInset
    }

    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}

extension PersonalDataViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == dobTextField.textField {
            validate()
        }

        if textField == postcodeTextField.textField {
            validate()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextIndex = textField.tag + 1
        if nextIndex >= textFields.count {
            textField.resignFirstResponder()
        } else {
            let nextTextField = textFields[nextIndex]
            nextTextField.textField.becomeFirstResponder()
        }

        return true
    }
}
