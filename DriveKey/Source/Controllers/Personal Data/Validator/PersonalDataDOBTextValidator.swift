//
//  PersonalDataDOBTextValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/09/2021.
//

import Foundation

class PersonalDataDOBTextValidator: TextValidatable {
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }

    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text, !text.isEmpty else {
                // assuming empty field is valid
                return true
            }

            let components = text.components(separatedBy: "/")
            guard components.count == 3 else { return false }

            let currentYear = Calendar.current.component(.year, from: Date())
            let yearInt = Int(components[2]) ?? currentYear + 1
            guard currentYear >= yearInt else { return false }

            let diff = currentYear - yearInt
            guard diff >= 18 else { return false }
            guard diff <= 100 else { return false }

            let dayInt = Int(components[0])
            let monthInt = Int(components[1])

            var dateComponents = DateComponents()
            dateComponents.month = monthInt
            dateComponents.year = yearInt
            dateComponents.day = dayInt
            dateComponents.calendar = Calendar.current

            return dateComponents.isValidDate
        })
    }
}
