//
//  PersonalDataDOBTextFormatter.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/09/2021.
//

import AnyFormatKit
import Foundation

class PersonalDataDOBTextFormatter: TextFormatable {
    let formatter = DefaultTextInputFormatter(textPattern: "##/##/####")

    func format(_ str: String?) -> String? {
        let unformattedText = formatter.unformat(str)
        return formatter.format(unformattedText)
    }
}
