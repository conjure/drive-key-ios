//
//  PersonalDataViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2021.
//

import Foundation

class PersonalDataViewModel {
    let userData: DrivekeyUser?
    let countries: [Country]
    private let dateFormatter = DateFormatter()
    private let statePickerCountry = "us"
    private let countryPlaceholder = Country(code: "placeholder", name: "personal.data.country.placeholder".localized, states: [CountryState]())
    
    var selectedCountry: Country?
    var selectedState: CountryState?
    
    init(userData: DrivekeyUser?, countries: [Country]) {
        self.userData = userData
        var countriesNoOther = countries.filter({ $0.code != Country.other.code })
        countriesNoOther.insert(countryPlaceholder, at: 0)
        self.countries = countriesNoOther
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if let user = userData {
            if let countryCode = user.countryCode {
                self.selectedCountry = self.countries.first(where: { $0.code.lowercased() == countryCode.lowercased() })
            } else if let vehicleData = user.activeVehicle, let countryCode = vehicleData.countryCode {
                self.selectedCountry = self.countries.first(where: { $0.code.lowercased() == countryCode.lowercased() })
            }
            
            if let country = selectedCountry {
                if let stateCode = user.state {
                    self.selectedState = country.states.first(where: { $0.code.lowercased() == stateCode.lowercased() })
                } else if let vehicleData = user.activeVehicle, let stateCode = vehicleData.state {
                    self.selectedState = country.states.first(where: { $0.code.lowercased() == stateCode.lowercased() })
                }
            }
        }
    }
    
    func getDOBDate(_ text: String?) -> Date? {
        guard let text = text else { return nil }
        return dateFormatter.date(from: text)
    }
    
    var name: String {
        userData?.firstName ?? ""
    }
    
    var lastName: String {
        userData?.lastName ?? ""
    }
    
    var email: String {
        userData?.email ?? ""
    }
    
    var dob: String {
        if let dobDate = userData?.dateOfBirthDate {
            return dateFormatter.string(from: dobDate)
        }
        
        return ""
    }
    
    var addressLine1: String {
        userData?.addressLineOne ?? ""
    }
    
    var addressLine2: String {
        userData?.addressLineTwo ?? ""
    }
    
    var region: String {
        userData?.region ?? ""
    }
    
    var postcode: String {
        userData?.postcode ?? ""
    }
    
    var country: String {
        "add.payment.method.billing.country.value".localized
    }
    
    func shouldShowStates(_ country: Country) -> Bool {
        country.code.lowercased() == statePickerCountry
    }
    
    var selectedCountryIndex: Int? {
        if let country = selectedCountry {
            return countries.firstIndex(where: { $0.code == country.code })
        }
        
        return nil
    }
    
    var selectedStateIndex: Int? {
        if let country = selectedCountry, let state = selectedState {
            return country.states.firstIndex(where: { $0.code == state.code })
        }
        
        return nil
    }
    
    var isCountryUS: Bool {
        guard let country = selectedCountry else { return false }
        
        return country.code.lowercased() == "us"
    }

    func shouldClearSelection(for country: Country) -> Bool {
        return country.code == countryPlaceholder.code
    }
}
