//
//  ForgotPasswordViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import UIKit

protocol ForgotPasswordViewControllerDelegate: AnyObject {
    func forgotPasswordViewControllerWillShowConfirmation(_ vc: ForgotPasswordViewController)
}

class ForgotPasswordViewController: ViewController {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var emailField: NamedTextField!
    @IBOutlet private var sendButton: RoundedButton!
    @IBOutlet private var errorView: ErrorView!

    // MARK: - Properties

    private var tasks = Set<AnyCancellable>()

    var viewModel: ForgotPasswordViewModel!
    weak var delegate: ForgotPasswordViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        headingLabel.text = "forgot.password.heading.label".localized

        emailField.name = "login.email.heading".localized
        emailField.textField.textContentType = .emailAddress
        emailField.textField.autocapitalizationType = .none
        emailField.textField.keyboardType = .emailAddress
        emailField.textField.autocorrectionType = .no
        emailField.textField.returnKeyType = .done
        emailField.validator = EmailAddressValidator()
        emailField.errorMessage = "login.email.error".localized
        emailField.textField.delegate = self

        sendButton.setTitle("forgot.password.button.label".localized, for: .normal)
        sendButton.isEnabled = false

        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        // Publishers
        emailField.textField.textPublisher()
            .map { _ in self.emailField.validate() }
            .receive(on: DispatchQueue.main)
            .assign(to: \.isEnabled, on: sendButton)
            .store(in: &tasks)

        viewModel.$sentEmail
            .receive(on: DispatchQueue.main)
            .sink { sent in
                self.hideSpinner()
                if sent {
                    self.delegate?.forgotPasswordViewControllerWillShowConfirmation(self)
                }
            }.store(in: &tasks)

        viewModel.$authError
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.hideSpinner()
                if let error = error {
                    self.showError(error)
                }
            }.store(in: &tasks)
    }

    private func showError(_ error: Error) {
        errorView.configure(message: error.localizedDescription)
        errorView.isHidden = false
    }

    private func removeSubscriptions() {
        tasks.removeAll()
    }

    @IBAction private func sendEmail(_: Any) {
        showSpinner()
        viewModel.sendResetPasswordEmail(to: emailField.text!)
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
}
