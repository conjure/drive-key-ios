//
//  ForgotPasswordViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import Foundation

class ForgotPasswordViewModel: ObservableObject {
    private let authService: AuthService
    private var tasks = Set<AnyCancellable>()

    @Published var sentEmail: Bool = false
    @Published var authError: Error?

    init(authService: AuthService) {
        self.authService = authService
    }

    func sendResetPasswordEmail(to email: String) {
        authService.sendResetPasswordEmail(for: email)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case let .failure(error):
                    self?.authError = error
                default:
                    break
                }
            } receiveValue: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.sentEmail = true
            }
            .store(in: &tasks)
    }
}
