//
//  LoginRouteViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import AuthenticationServices
import Combine
import UIKit

protocol LoginRouteViewControllerDelegate: AnyObject {
    func loginRouteWillLogin(_ vc: LoginRouteViewController)
    func loginRouteWillSignup(_ vc: LoginRouteViewController)
    func loginRouteDidSignupWithApple(_ vc: LoginRouteViewController)
    func loginRouteDidSigninWithApple(_ vc: LoginRouteViewController)
}

class LoginRouteViewController: ViewController {
    @IBOutlet private var loginButton: RoundedButton!
    @IBOutlet private var loginLabel: UILabel!
    @IBOutlet private var signupLabel: UILabel!
    @IBOutlet private var signupButton: UIButton!
    @IBOutlet private var signupAppleButton: AppleSignInButton!

    var viewModel: LoginRouteViewModel!

    weak var delegate: LoginRouteViewControllerDelegate?
    private var tasks = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = false

        loginLabel.text = "login.route.login.label".localized
        signupLabel.text = "login.route.signup.label".localized

        loginButton.setTitle("login.route.login.button".localized, for: .normal)
        signupButton.setTitle("signup.button.label".localized, for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        viewModel.registerSubscription()

        viewModel.loggedIn
            .receive(on: DispatchQueue.main)
            .sink { loggedIn in
                self.hideSpinner()
                if loggedIn {
                    self.delegate?.loginRouteDidSigninWithApple(self)
                }
            }.store(in: &tasks)

        viewModel.signedUp
            .receive(on: DispatchQueue.main)
            .sink { signedUp in
                self.hideSpinner()
                if signedUp {
                    self.delegate?.loginRouteDidSignupWithApple(self)
                }
            }.store(in: &tasks)

        viewModel.authError
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.hideSpinner()
                if let err = error as? ASAuthorizationError {
                    switch err.code {
                    case .canceled, .unknown:
                        return
                    default:
                        break
                    }
                }
                self.showAlertError(title: "login.error.alert".localized, error: error)

            }.store(in: &tasks)
    }

    private func removeSubscriptions() {
        tasks.removeAll()
        viewModel.removeSubscriptions()
    }

    @IBAction private func willLoginWithEmail(_: Any) {
        delegate?.loginRouteWillLogin(self)
    }

    @IBAction private func willSignupWithEmail(_: Any) {
        delegate?.loginRouteWillSignup(self)
    }

    @IBAction private func willSignupWithApple(_: Any) {
        showSpinner()
        viewModel.loginWithApple(from: self)
    }
}

extension LoginRouteViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for _: ASAuthorizationController)
        -> ASPresentationAnchor {
        view.window!
    }
}
