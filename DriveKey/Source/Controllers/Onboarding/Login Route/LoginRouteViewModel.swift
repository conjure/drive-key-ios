//
//  LoginRouteViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 10/08/2021.
//

import AuthenticationServices
import Combine
import Foundation

class LoginRouteViewModel: ObservableObject {
    private let authService: AuthService
    private let userService: UserProvidable
    private let api: APIProtocol
    private var tasks = Set<AnyCancellable>()

    private var userToUpdate: DrivekeyUser?

    var signedUp = PassthroughSubject<Bool, Never>()
    var loggedIn = PassthroughSubject<Bool, Never>()
    var authError = PassthroughSubject<Error, Never>()

    init(authService: AuthService, userService: UserProvidable, api: APIProtocol) {
        self.authService = authService
        self.userService = userService
        self.api = api
    }

    func loginWithApple(from: ASAuthorizationControllerPresentationContextProviding) {
        let request = authService.generateAppleIDRequest()
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = authService
        authorizationController.presentationContextProvider = from

        authorizationController.performRequests()
    }

    func registerSubscription() {
        userService.syncedSubject.receive(on: DispatchQueue.main)
            .sink { _ in
                guard self.userToUpdate != nil else { return }
                self.updateName()
                self.userToUpdate = nil
            }
            .store(in: &tasks)

        authService.authSubject
            .receive(on: DispatchQueue.main)
            .debounce(for: 1.0, scheduler: RunLoop.main)
            .sink { user, didSignup in
                print("Signed in Apple user - \(user)")
                if didSignup {
                    AnalyticsService.trackEvent(.accountCreatedApple)
                    self.userToUpdate = user
                    self.userService.setUser(user)
                } else {
                    AnalyticsService.trackEvent(.signInApple)
                    self.fetchUser(user)
                }
            }
            .store(in: &tasks)

        authService.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.authError.send(error)
            }
            .store(in: &tasks)
    }

    func removeSubscriptions() {
        tasks.removeAll()
    }

    private func fetchUser(_ user: DrivekeyUser) {
        self.userService.setUser(user)
        loggedIn.send(true)
    }

    private func updateName() {
        guard let user = userToUpdate else { return }

        let payload = UpdateUserPayload(firstName: user.firstName, lastName: user.lastName)
        api.userData(payload) { result in
            switch result {
            case let .success(data):
                print("[LoginRouteViewModel] updateUserData: \(data)")
                self.signedUp.send(true)
                self.userToUpdate = nil
            case let .failure(error):
                print("[LoginRouteViewModel] updateUserData error: \(error)")
                self.authError.send(error)
            }
        }
    }
}
