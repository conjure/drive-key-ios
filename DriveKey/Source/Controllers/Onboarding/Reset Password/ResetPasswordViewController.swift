//
//  ResetPasswordViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import UIKit

protocol ResetPasswordViewControllerDelegate: AnyObject {
    func resetPasswordViewControllerDidResetPassword(_ vc: ResetPasswordViewController)
}

class ResetPasswordViewController: ViewController {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var passwordTextField: NamedTextField!
    @IBOutlet private var confirmPasswordTextField: NamedTextField!
    @IBOutlet private var confirmButton: RoundedButton!
    @IBOutlet private var errorView: ErrorView!

    // MARK: - Properties

    private var tasks = Set<AnyCancellable>()
    var viewModel: ResetPasswordViewModel!
    weak var delegate: ResetPasswordViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // textfields
        passwordTextField.name = "login.password.heading".localized
        passwordTextField.textField.textContentType = .oneTimeCode
        passwordTextField.textField.autocapitalizationType = .none
        passwordTextField.textField.autocorrectionType = .no
        passwordTextField.textField.returnKeyType = .next
        passwordTextField.textField.tag = 0
        passwordTextField.textField.delegate = self
        passwordTextField.textField.enablePasswordToggle()

        confirmPasswordTextField.name = "reset.password.confirm.label".localized
        confirmPasswordTextField.textField.autocapitalizationType = .none
        confirmPasswordTextField.textField.autocorrectionType = .no
        confirmPasswordTextField.textField.returnKeyType = .done
        confirmPasswordTextField.textField.tag = 1
        confirmPasswordTextField.textField.delegate = self
        confirmPasswordTextField.textField.enablePasswordToggle()
        confirmPasswordTextField.errorMessage = "reset.password.error.label".localized
        confirmPasswordTextField.validator = TextValidator(validationClosure: { [weak self] text in
            if text?.isEmpty == false {
                return text == self?.passwordTextField.text
            }
            return true
        })

        headingLabel.text = "reset.password.heading.label".localized

        confirmButton.setTitle("reset.password.button".localized, for: .normal)
        confirmButton.isEnabled = false

        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        // Publishers
        Publishers.CombineLatest(passwordTextField.textField.textPublisher(), confirmPasswordTextField.textField.textPublisher())
            .map { ($0 == $1) && (!$0.isEmpty && !$1.isEmpty) }
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { enabled in
                self.confirmPasswordTextField.validate()
                self.confirmButton.isEnabled = enabled
            })
            .store(in: &tasks)

        viewModel.$resetPassword
            .receive(on: DispatchQueue.main)
            .sink { reset in
                self.hideSpinner()
                if reset {
                    self.delegate?.resetPasswordViewControllerDidResetPassword(self)
                }
            }.store(in: &tasks)

        viewModel.$authError
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.hideSpinner()
                if let error = error {
                    self.showError(error)
                }
            }.store(in: &tasks)
    }

    private func removeSubscriptions() {
        tasks.removeAll()
    }

    @IBAction private func resetPassword(_: Any) {
        showSpinner()
        viewModel.resetPassword(with: passwordTextField.text!)
    }

    private func showError(_ error: Error) {
        errorView.configure(message: error.localizedDescription)
        errorView.isHidden = false
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if nextTag == 1 {
            confirmPasswordTextField.textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
}
