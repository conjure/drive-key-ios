//
//  ResetPasswordViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import Foundation

class ResetPasswordViewModel: ObservableObject {
    private var authService: AuthService
    private var oobCode: String
    private var tasks = Set<AnyCancellable>()

    @Published var resetPassword = false
    @Published var authError: Error?

    init(authService: AuthService, oobCode: String) {
        self.authService = authService
        self.oobCode = oobCode
    }

    private func validatePassword(_ password: String) -> Bool {
        let rawPassword = password.trimmingCharacters(in: .whitespaces)

        let isValid = NSPredicate(format: "SELF MATCHES %@ ", "^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.*[$@$#!%*?&]).{8,}$")

        return isValid.evaluate(with: rawPassword)
    }

    func resetPassword(with newPassword: String) {
        guard validatePassword(newPassword) else {
            authError = AuthError.weakPassword
            return
        }

        authService.resetPassword(newPassword: newPassword, oobCode: oobCode)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case let .failure(error):
                    self?.authError = error
                default:
                    break
                }
            } receiveValue: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.resetPassword = true
            }
            .store(in: &tasks)
    }
}
