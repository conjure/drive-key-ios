//
//  PermissionsViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 11/08/2021.
//

import Combine
import UIKit

protocol PermissionsViewControllerDelegate: AnyObject {
    func permissionsViewControllerWillProceed(_ vc: PermissionsViewController)
}

class PermissionsViewController: ViewController {
    @IBOutlet private var locationLabel: UILabel!
    @IBOutlet private var pushLabel: UILabel!
    @IBOutlet private var motionLabel: UILabel!
    @IBOutlet private var allowButton: RoundedButton!

    // MARK: - Properties

    private var tasks = Set<AnyCancellable>()
    var viewModel: PermissionsViewModel!
    weak var delegate: PermissionsViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        locationLabel.text = "permissions.location.label".localized
        pushLabel.attributedText = "permissions.push.label".localized.subscriptCO2(.K2DMedium(8))
        motionLabel.text = "permissions.motion.label".localized
        allowButton.setTitle("permissions.allow.button".localized, for: .normal)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        // Publishers
        viewModel.$shouldProceed
            .receive(on: DispatchQueue.main)
            .sink { loggedIn in
                self.hideSpinner()
                if loggedIn {
                    self.delegate?.permissionsViewControllerWillProceed(self)
                }
            }.store(in: &tasks)

        viewModel.$permissionError
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.hideSpinner()
                if let error = error {
                    self.showAlertError(title: "permission.error.alert".localized, error: error)
                }
            }.store(in: &tasks)
    }

    private func removeSubscriptions() {
        tasks.removeAll()
    }

    @IBAction private func showPermissions(_: Any) {
        showSpinner()
        viewModel.requestPermissions()
    }
}
