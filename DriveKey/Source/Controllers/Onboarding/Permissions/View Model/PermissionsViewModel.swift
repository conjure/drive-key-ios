//
//  PermissionsViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 11/08/2021.
//

import Foundation
import Combine

class PermissionsViewModel: ObservableObject {
    private let permissionService: PermissionService!
    private var tasks = Set<AnyCancellable>()

    @Published var shouldProceed: Bool = false
    @Published var permissionError: Error?

    init(permissionService: PermissionService) {
        self.permissionService = permissionService
    }

    private func registerForLocation() {
        let locationService = permissionService.locationService as! LocationService
        locationService.locationSubject
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case let .failure(error):
                    self.permissionError = error
                case .finished:
                    self.requestNextPermission()
                }
            } receiveValue: { _ in
                self.requestNextPermission()
            }
            .store(in: &tasks)

        locationService.locationAuthSubject
            .sink { status in
                if status == .authorizedAlways {
                    AnalyticsService.trackEvent(.allowLocation)
                }
            }
            .store(in: &tasks)
    }

    private func registerForNotification() {
        let notificationService = permissionService.notificationService as! NotificationService
        notificationService.tokenSubject
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case let .failure(error):
                    self.permissionError = error
                case .finished:
                    self.requestNextPermission()
                }
            } receiveValue: { tok in
                if tok != nil { AnalyticsService.trackEvent(.allowPush) }
                self.requestNextPermission()
            }
            .store(in: &tasks)
    }

    private func registerForMotion() {
        let motionService = permissionService.motionService as! MotionService
        motionService.authorizationSubject
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case let .failure(error):
                    self.permissionError = error
                case .finished:
                    self.requestNextPermission()
                }
            } receiveValue: { granted in
                if granted { AnalyticsService.trackEvent(.allowMotion) }
                self.requestNextPermission()
            }
            .store(in: &tasks)
    }

    private func requestNextPermission() {
        if !permissionService.locationService.isPermissionAsked {
            requestLocation()
        } else if !permissionService.notificationService.isPermissionAsked {
            requestNotification()
        } else if !permissionService.motionService.isPermissionAsked {
            requestMotion()
        } else {
            shouldProceed = true
        }
    }

    func requestPermissions() {
        requestNextPermission()
    }

    private func requestLocation() {
        registerForLocation()
        permissionService.requestPermissionLocation()
    }

    private func requestNotification() {
        registerForNotification()
        permissionService.requestPermissionNotification()
    }

    private func requestMotion() {
        registerForMotion()
        permissionService.requestPermissionMotion()
    }
}
