//
//  CarouselViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 26/08/2021.
//

import UIKit

protocol CarouselViewControllerDelegate: AnyObject {
    func walkthroughViewControllerDidFinish(_ viewController: CarouselViewController)
}

class CarouselViewController: ViewController {
    @IBOutlet private var karaiView: UIImageView!
    @IBOutlet private var continueButton: RoundedButton!

    weak var delegate: CarouselViewControllerDelegate?

    lazy var page0: UIView = {
        let view = UIView()
        let label = ContentLabel()
        label.text = "walkthrough.content.1".localized
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 55).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -55).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 30).isActive = true
        return view
    }()

    lazy var page1: UIView = {
        let view = UIView()
        let label = ContentLabel()
        label.text = "walkthrough.content.2".localized
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 55).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -55).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 30).isActive = true
        return view
    }()

    lazy var page2: UIView = {
        let view = UIView()
        let label = ContentLabel()
        label.text = "walkthrough.content.3".localized
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 55).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -55).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 30).isActive = true
        return view
    }()

    lazy var views = [page0, page1, page2]

    lazy var footer: UIImageView = {
        let image = UIImage(named: "walkthrough_footer")
        let view = UIImageView(image: image)
        view.contentMode = .scaleAspectFill
        return view
    }()

    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: view.frame.height)

        for i in 0 ..< views.count {
            scrollView.addSubview(views[i])
            views[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
        }

        scrollView.delegate = self

        return scrollView
    }()

    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.numberOfPages = views.count
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = .white
        pageControl.tintColor = UIColor.white.withAlphaComponent(0.25)
        pageControl.addTarget(self, action: #selector(pageControlTapHandler(sender:)), for: .touchUpInside)
        return pageControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(scrollView)
        scrollView.edgeTo(view: view)

        view.addSubview(pageControl)

        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -25).isActive = true

        continueButton.backgroundColor = .white
        continueButton.setTitle("continue".localized, for: .normal)
        view.bringSubviewToFront(continueButton)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // footer
        if footer.superview == nil {
            scrollView.addSubview(footer)
            let footerHeight = (253 / 812) * UIScreen.main.bounds.height
            footer.frame = CGRect(x: 0, y: view.frame.maxY - footerHeight, width: scrollView.contentSize.width, height: footerHeight)
        }
    }

    @objc
    func pageControlTapHandler(sender: UIPageControl) {
        scrollView.scrollTo(horizontalPage: sender.currentPage, animated: true)
    }

    @IBAction private func didPressContinue(_: Any) {
        delegate?.walkthroughViewControllerDidFinish(self)
    }
}

extension CarouselViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}

private class ContentLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        font = UIFont.K2DRegular(16)
        textColor = .white
        textAlignment = .center
        numberOfLines = 0
        adjustsFontSizeToFitWidth = true
        minimumScaleFactor = 0.5
    }
}
