//
//  LoginViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

import AuthenticationServices
import Combine
import UIKit

protocol LoginViewControllerDelegate: AnyObject {
    func loginViewControllerDidLogin(_ vc: LoginViewController)
    func loginViewControllerDidSignupWithApple(_ vc: LoginViewController)
    func loginViewControllerDidForgotPassword(_ vc: LoginViewController)
}

class LoginViewController: ViewController {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var loginButton: RoundedButton!
    @IBOutlet private var forgotPasswordButton: SmallButton!
    @IBOutlet private var signupAppleButton: AppleSignInButton!
    @IBOutlet private var errorView: ErrorView!
    @IBOutlet private var orLabel: UILabel!
    @IBOutlet private var emailField: NamedTextField!
    @IBOutlet private var passwordField: NamedTextField!
    @IBOutlet private var scrollView: UIScrollView!

    @IBOutlet private var signinAppleButtonTopConstraint: NSLayoutConstraint!

    // MARK: - Properties

    private var tasks = Set<AnyCancellable>()

    var viewModel: LoginViewModel!
    weak var delegate: LoginViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // textfields
        emailField.name = "login.email.heading".localized
        emailField.textField.textContentType = .emailAddress
        emailField.textField.autocapitalizationType = .none
        emailField.textField.keyboardType = .emailAddress
        emailField.textField.autocorrectionType = .no
        emailField.textField.returnKeyType = .next
        emailField.textField.tag = 0
        emailField.textField.delegate = self
        emailField.validator = EmailAddressValidator()
        emailField.errorMessage = "login.email.error".localized

        passwordField.name = "login.password.heading".localized
        passwordField.textField.textContentType = .oneTimeCode
        passwordField.textField.autocapitalizationType = .none
        passwordField.textField.autocorrectionType = .no
        passwordField.textField.returnKeyType = .done
        passwordField.textField.tag = 1
        passwordField.textField.delegate = self
        passwordField.textField.enablePasswordToggle()

        headingLabel.text = "login.heading.label".localized

        let attributes = [.underlineStyle: NSUnderlineStyle.single.rawValue,
                          .font: UIFont.link,
                          .foregroundColor: UIColor.black] as [NSAttributedString.Key: Any]
        let attrButtonTitle = NSMutableAttributedString(string: "login.forgot.password.button".localized, attributes: attributes)
        forgotPasswordButton.setAttributedTitle(attrButtonTitle, for: .normal)

        loginButton.setTitle("login.button".localized, for: .normal)
        loginButton.isEnabled = false

        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        // Publishers
        emailField.textField.textPublisher()
            .map { _ in self.emailField.validate() }
            .sink { _ in }
            .store(in: &tasks)

        Publishers.CombineLatest(emailField.textField.textPublisher(), passwordField.textField.textPublisher())
            .map { self.viewModel.isValid($0, password: $1) }
            .receive(on: DispatchQueue.main)
            .assign(to: \.isEnabled, on: loginButton)
            .store(in: &tasks)

        viewModel.loggedIn
            .receive(on: DispatchQueue.main)
            .sink { loggedIn in
                self.showLoadingState(false)
                if loggedIn {
                    self.delegate?.loginViewControllerDidLogin(self)
                }
            }.store(in: &tasks)

        viewModel.signedUp
            .receive(on: DispatchQueue.main)
            .sink { signedUp in
                self.showLoadingState(false)
                if signedUp {
                    self.delegate?.loginViewControllerDidSignupWithApple(self)
                }
            }.store(in: &tasks)

        viewModel.authError
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.showLoadingState(false)
                if let err = error as? ASAuthorizationError {
                    switch err.code {
                    case .canceled, .unknown:
                        return
                    default:
                        break
                    }
                }
                self.showError(error)
            }.store(in: &tasks)
    }

    private func showError(_ error: Error) {
        errorView.configure(message: error.localizedDescription)
        orLabel.isHidden = true
        errorView.isHidden = false
        signinAppleButtonTopConstraint.constant = 55.0
        scrollView.flashScrollIndicators()
    }

    private func showLoadingState(_ enabled: Bool) {
        if enabled {
            showSpinner()
            loginButton.isEnabled = false
        } else {
            hideSpinner()
            loginButton.isEnabled = true
        }
        signupAppleButton.isEnabled = !enabled
    }

    private func removeSubscriptions() {
        tasks.removeAll()
        viewModel.removeSubscriptions()
    }

    @IBAction private func login(_: Any) {
        self.showLoadingState(true)
        viewModel.login(with: emailField.text!, password: passwordField.text!)
    }

    @IBAction private func willSignupWithApple(_: Any) {
        self.showLoadingState(true)
        viewModel.loginWithApple(from: self)
    }

    @IBAction private func forgotPassword(_: Any) {
        delegate?.loginViewControllerDidForgotPassword(self)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if nextTag == 1 {
            passwordField.textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
}

extension LoginViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for _: ASAuthorizationController)
        -> ASPresentationAnchor {
        view.window!
    }
}
