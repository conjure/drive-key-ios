//
//  LoginViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

import AuthenticationServices
import Combine
import Foundation

class LoginViewModel: ObservableObject {
    private let authService: AuthService
    private let userService: UserProvidable
    private let api: APIProtocol
    private var tasks = Set<AnyCancellable>()

    private var userToUpdate: DrivekeyUser?
    private var shouldFetchUser = false

    var signedUp = PassthroughSubject<Bool, Never>()
    var loggedIn = PassthroughSubject<Bool, Never>()
    var authError = PassthroughSubject<Error, Never>()

    init(authService: AuthService, userService: UserProvidable, api: APIProtocol) {
        self.authService = authService
        self.userService = userService
        self.api = api

        registerSubscription()
    }

    func isValid(_ email: String, password: String) -> Bool {
        let rawEmail = email.trimmingCharacters(
            in: .whitespaces
        )

        var isValidEmail: Bool {
            NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: rawEmail)
        }
        return isValidEmail && !password.isEmpty
    }

    func login(with email: String, password: String) {
        authService.login(with: email, password: password)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case let .failure(err):
                    self?.authError.send(err)
                default:
                    break
                }
            } receiveValue: { [weak self] user in
                guard let strongSelf = self else { return }
                AnalyticsService.trackEvent(.signInEmail)
                strongSelf.fetchUserForLogin(user)
                strongSelf.shouldFetchUser = true
            }
            .store(in: &tasks)
    }

    func loginWithApple(from: ASAuthorizationControllerPresentationContextProviding) {
        let request = authService.generateAppleIDRequest()
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = authService
        authorizationController.presentationContextProvider = from

        authorizationController.performRequests()
    }

    private func registerSubscription() {
        userService.syncedSubject.receive(on: DispatchQueue.main)
            .sink { _ in
                if self.shouldFetchUser {
                    self.shouldFetchUser = false
                    self.loggedIn.send(true)
                }
                guard self.userToUpdate != nil else { return }
                self.updateName()
                self.userToUpdate = nil
            }
            .store(in: &tasks)

        authService.authSubject
            .receive(on: DispatchQueue.main)
            .debounce(for: 1.0, scheduler: RunLoop.main)
            .sink { user, didSignup in
                print("Signed in Apple user - \(user)")
                if didSignup {
                    AnalyticsService.trackEvent(.accountCreatedApple)
                    self.userToUpdate = user
                    self.userService.setUser(user)
                } else {
                    AnalyticsService.trackEvent(.signInApple)
                    self.fetchUser(user)
                }
            }
            .store(in: &tasks)

        authService.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.authError.send(error)
            }
            .store(in: &tasks)
    }

    func removeSubscriptions() {
        tasks.removeAll()
    }

    private func updateName() {
        guard let user = userToUpdate else { return }

        let payload = UpdateUserPayload(firstName: user.firstName, lastName: user.lastName)
        api.userData(payload) { result in
            switch result {
            case let .success(data):
                print("[LoginViewModel] updateUserData: \(data)")
                self.signedUp.send(true)
                self.userToUpdate = nil
            case let .failure(error):
                print("[LoginViewModel] updateUserData error: \(error)")
                self.authError.send(error)
            }
        }
    }

    private func fetchUserForLogin(_ user: DrivekeyUser) {
        self.userService.setUser(user)
    }
    
    private func fetchUser(_ user: DrivekeyUser) {
        self.userService.setUser(user)
        loggedIn.send(true)
    }
}
