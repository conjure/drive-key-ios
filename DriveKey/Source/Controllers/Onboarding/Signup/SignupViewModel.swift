//
//  SignupViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

import Combine
import Foundation

class SignupViewModel: ObservableObject {
    private let authService: AuthService
    private let userService: UserProvidable

    private var tasks = Set<AnyCancellable>()

    var authSubject = PassthroughSubject<Bool, Never>()
    var errorSubject = PassthroughSubject<Error, Never>()

    init(authService: AuthService, userService: UserProvidable) {
        self.authService = authService
        self.userService = userService
    }

    func isValid(_ email: String, password: String) -> Bool {
        let rawEmail = email.trimmingCharacters(
            in: .whitespaces
        )

        var isValidEmail: Bool {
            NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: rawEmail)
        }
        return isValidEmail && !password.isEmpty
    }

    func validatePassword(_ password: String) -> Bool {
        let rawPassword = password.trimmingCharacters(in: .whitespaces)

        let isValid = NSPredicate(format: "SELF MATCHES %@ ", "^(?=.*[a-z])(?=.*[0-9])(?=.*[A-Z])(?=.*[$@$#!%*?&]).{8,}$")

        return isValid.evaluate(with: rawPassword)
    }

    func signup(with email: String, password: String) {
        guard validatePassword(password) else {
            errorSubject.send(AuthError.weakPassword)
            return
        }

        userService.syncedSubject.receive(on: DispatchQueue.main)
            .sink { _ in
                AnalyticsService.trackEvent(.accountCreatedEmail)
                self.authSubject.send(true)
                self.removeSubscriptions() 
            }
            .store(in: &tasks)

        authService.signup(with: email, password: password)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case let .failure(err):
                    self?.errorSubject.send(err)
                default:
                    break
                }
            } receiveValue: { [weak self] user in
                guard let strongSelf = self else { return }
                strongSelf.userService.setUser(user)
            }
            .store(in: &tasks)
    }

    func removeSubscriptions() {
        tasks.removeAll()
    }
}
