//
//  SignupViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

import Combine
import UIKit

protocol SignupViewControllerDelegate: AnyObject {
    func signupViewControllerDidSignUp(_ vc: SignupViewController)
    func signupViewControllerWillShowHTMLContent(_ vc: SignupViewController, content: HTMLContentType)
}

class SignupViewController: ViewController {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var termsConditionLabel: UILabel!
    @IBOutlet private var signupButton: RoundedButton!
    @IBOutlet private var emailField: NamedTextField!
    @IBOutlet private var passwordField: NamedTextField!
    @IBOutlet private var errorView: ErrorView!

    // MARK: - Properties
    private var tasks = Set<AnyCancellable>()
    private var isProcessing: Bool = false

    var viewModel: SignupViewModel!
    weak var delegate: SignupViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // textfields
        emailField.name = "login.email.heading".localized
        emailField.textField.textContentType = .emailAddress
        emailField.textField.autocapitalizationType = .none
        emailField.textField.keyboardType = .emailAddress
        emailField.textField.autocorrectionType = .no
        emailField.textField.returnKeyType = .next
        emailField.textField.tag = 0
        emailField.textField.delegate = self
        emailField.validator = EmailAddressValidator()
        emailField.errorMessage = "login.email.error".localized

        passwordField.name = "login.password.heading".localized
        passwordField.textField.textContentType = .oneTimeCode
        passwordField.textField.autocapitalizationType = .none
        passwordField.textField.autocorrectionType = .no
        passwordField.textField.returnKeyType = .done
        passwordField.textField.tag = 1
        passwordField.textField.delegate = self
        passwordField.textField.enablePasswordToggle()

        headingLabel.text = "signup.heading.label".localized

        // create attributed string
        let signupStrTerms = "signup.terms.label".localized
        let signupStrTc = "signup.terms.link.tc".localized
        let signupStrPrivacy = "signup.terms.link.privacy".localized
        let attrString = NSMutableAttributedString(string: signupStrTerms)
            .underline(signupStrTc)
            .underline(signupStrPrivacy)
        attrString.addAttribute(.font, value: UIFont.K2DRegular(14)! as Any, range: signupStrTerms.nsRange(of: signupStrTerms))
        termsConditionLabel.textColor = .black
        termsConditionLabel.lineBreakMode = .byWordWrapping
        termsConditionLabel.numberOfLines = 2
        termsConditionLabel.isUserInteractionEnabled = true
        termsConditionLabel.textAlignment = .center
        termsConditionLabel.attributedText = attrString
        termsConditionLabel.adjustsFontSizeToFitWidth = true
        termsConditionLabel.minimumScaleFactor = 0.5

        signupButton.setTitle("signup.button.label".localized, for: .normal)
        signupButton.isEnabled = false

        // terms and conditions
        let tap = UITapGestureRecognizer(target: self, action: #selector(loadTermsConditions))
        termsConditionLabel.isUserInteractionEnabled = true
        termsConditionLabel.addGestureRecognizer(tap)

        hideKeyboardWhenTappedAround()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerSubscriptions()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeSubscriptions()
    }

    private func registerSubscriptions() {
        // Publishers
        emailField.textField.textPublisher()
            .map { _ in self.emailField.validate() }
            .sink { _ in }
            .store(in: &tasks)

        Publishers.CombineLatest(emailField.textField.textPublisher(), passwordField.textField.textPublisher())
            .map { self.viewModel.isValid($0, password: $1) }
            .receive(on: DispatchQueue.main)
            .assign(to: \.isEnabled, on: signupButton)
            .store(in: &tasks)

        viewModel.authSubject
            .receive(on: DispatchQueue.main)
            .sink { signedUp in
                self.showLoadingState(false)
                self.errorView.isHidden = true
                if signedUp {
                    self.delegate?.signupViewControllerDidSignUp(self)
                }
            }.store(in: &tasks)

        viewModel.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { error in
                self.showLoadingState(false)
                self.showError(error)
            }.store(in: &tasks)
    }

    private func removeSubscriptions() {
        tasks.removeAll()
        viewModel.removeSubscriptions()
    }

    private func showError(_ error: Error) {
        errorView.configure(message: error.localizedDescription)
        errorView.isHidden = false
    }

    @IBAction private func signup(_: Any) {
        showLoadingState(true)
        viewModel.signup(with: emailField.text!, password: passwordField.text!)
    }

    private func showLoadingState(_ enabled: Bool) {
        isProcessing = enabled
        if enabled {
            showSpinner()
            signupButton.isEnabled = false
        } else {
            hideSpinner()
            signupButton.isEnabled = true
        }
    }

    @objc private func loadTermsConditions(_ gesture: UITapGestureRecognizer) {
        guard let label = gesture.view as? UILabel, let text = label.attributedText?.string else { return }

        guard !isProcessing else { return }

        let stringTc = "signup.terms.link.tc".localized
        let rangeTc = text.nsRange(of: stringTc)
        let stringPrivacy = "signup.terms.link.privacy".localized
        let rangePrivacy = text.nsRange(of: stringPrivacy)

        if gesture.didTapAttributedTextInLabel(label: label, inRange: rangeTc) {
            delegate?.signupViewControllerWillShowHTMLContent(self, content: .termsOfUse)
        } else if gesture.didTapAttributedTextInLabel(label: label, inRange: rangePrivacy) {
            delegate?.signupViewControllerWillShowHTMLContent(self, content: .privacyPolicy)
        }
    }
}

extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        if nextTag == 1 {
            passwordField.textField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }

        return true
    }
}
