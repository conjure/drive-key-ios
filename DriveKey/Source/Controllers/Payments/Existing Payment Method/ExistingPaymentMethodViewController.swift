//
//  ExistingPaymentMethodViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 28/02/2022.
//

import UIKit
import PassKit
import Stripe

protocol ExistingPaymentMethodViewControllerDelegate: AnyObject {
    func existingPaymentMethodViewControllerWillAddNewMethod(_ vc: ExistingPaymentMethodViewController, summary: OneTimePaymentSummary, previousMethod: PaymentMethod)
    func existingPaymentMethodViewControllerDidCompletePayment(_ vc: ExistingPaymentMethodViewController, amount: Double)
    func existingPaymentMethodViewControllerDidError(_ vc: ExistingPaymentMethodViewController, error: Error)
    func existingPaymentMethodViewControllerWillShowHTMLContent(_ vc: ExistingPaymentMethodViewController, content: HTMLContentType)
}

class ExistingPaymentMethodViewController: ViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet private var termsConditionLabel: UILabel!
    @IBOutlet private var buttonStackView: UIStackView!
    @IBOutlet var payButton: RoundedButton!
    
    var applePayButton: PKPaymentButton!
    var viewModel: ExistingPaymentMethodViewModel!
    private var dataSource = ExistingPaymentMethodDataSource(paymentMethods: [])

    private let paymentTermsAndPrivacyStr = "payment.terms.label".localized
    private let termsStr = "payment.terms.str".localized
    private let privacyStr = "payment.privacy.str".localized
    private var isProcessing: Bool = false

    weak var delegate: ExistingPaymentMethodViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
            navigationController.navigationBar.prefersLargeTitles = true
        }

        title = "profile.account.payment".localized

        // terms and conditions
        // create attributed string
        let attrString = NSMutableAttributedString(string: paymentTermsAndPrivacyStr)
            .underline(termsStr)
            .underline(privacyStr)
        attrString.addAttribute(.font, value: UIFont.K2DSemiBold(10)! as Any, range: paymentTermsAndPrivacyStr.nsRange(of: paymentTermsAndPrivacyStr))
        termsConditionLabel.textColor = .black.withAlphaComponent(0.5)
        termsConditionLabel.lineBreakMode = .byWordWrapping
        termsConditionLabel.numberOfLines = 2
        termsConditionLabel.isUserInteractionEnabled = true
        termsConditionLabel.textAlignment = .center
        termsConditionLabel.attributedText = attrString

        let tap = UITapGestureRecognizer(target: self, action: #selector(loadTermsConditions))
        termsConditionLabel.addGestureRecognizer(tap)

        tableView.dataSource = self.dataSource
        tableView.delegate = self
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.register(PaymentMethodCell.self, forCellReuseIdentifier: PaymentMethodCell.reuseidentifier)

        applePayButton = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .whiteOutline)
        applePayButton.cornerRadius = 8.0
        applePayButton.translatesAutoresizingMaskIntoConstraints = false
        applePayButton.addTarget(self, action: #selector(applePayTapped), for: .touchUpInside)

        NSLayoutConstraint.activate([
            applePayButton.heightAnchor.constraint(equalToConstant: 55)
        ])
        buttonStackView.insertArrangedSubview(applePayButton, at: 0)
        applePayButton.isHidden = true

        showLoadingState(true)
        self.viewModel.fetchPaymentMethods { error in
            self.showLoadingState(false)
            if let error = error {
                self.showAlertError(error: error)
            } else {
                self.updatePaymentMethods()
            }
        }
    }

    private func reload() {
        if viewModel.paymentMethods.isEmpty {
            tableView.tableFooterView = UIView()
        } else {
            let footerView = AddPaymentMethodFooterView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50.0))
            tableView.tableFooterView = footerView
            footerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapAddMethod)))
        }
        
        tableView.reloadData()

        payButton.isHidden = viewModel.isApplePay
        applePayButton.isHidden = !viewModel.isApplePay

        let amountStr = viewModel.amountString
        let payStr = "one.time.payment.button.text".localized.replacingOccurrences(of: PlaceholderKeys.amount, with: amountStr)
        payButton.setTitle(payStr, for: .normal)
    }

    private func updatePaymentMethods() {
        self.dataSource = ExistingPaymentMethodDataSource(paymentMethods: viewModel.paymentMethods)
        tableView.dataSource = dataSource

        reload()
    }

    private func showLoadingState(_ enabled: Bool) {
        isProcessing = enabled
        if enabled {
            showSpinner()
            payButton.isEnabled = false
        } else {
            hideSpinner()
            payButton.isEnabled = true
        }
    }

    private func completePayment() {
        AnalyticsService.trackEvent(.successfulPurchase(amount: viewModel.checkoutSummary.totalPrice ?? 0, currency: viewModel.checkoutSummary.currency ?? ""))
        self.delegate?.existingPaymentMethodViewControllerDidCompletePayment(self, amount: self.viewModel.checkoutSummary.totalCo2Generated)
    }

    @objc private func didTapAddMethod() {
        self.delegate?.existingPaymentMethodViewControllerWillAddNewMethod(self, summary: viewModel.checkoutSummary, previousMethod: viewModel.paymentMethods.first!)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }

    @IBAction private func didPressPay(_ sender: Any) {
        guard let paymentMethod = viewModel.paymentMethods.first, let id = paymentMethod.identifier else { return }
        self.showLoadingState(true)

        viewModel.makeOneTimePayment(with: id, type: PaymentMethod.card) { result in
            self.showLoadingState(false)
            switch result {
            case .success(let resp):
                if resp.requiresAction == true {
                    self.viewModel.handleNextAction(resp.clientSecret, authContext: self) { error in
                        if let error = error {
                            self.delegate?.existingPaymentMethodViewControllerDidError(self, error: error)
                        } else {
                            self.completePayment()
                        }
                    }
                } else {
                    self.completePayment()
                }
            case .failure(let error):
                print("[ExistingPaymentMethodViewController] apple pay error: \(error)")
                self.delegate?.existingPaymentMethodViewControllerDidError(self, error: error)
            }
        }
    }

    @objc private func loadTermsConditions(_ gesture: UITapGestureRecognizer) {
        guard let label = gesture.view as? UILabel, let text = label.attributedText?.string else { return }

        guard !isProcessing else { return }

        let rangeTc = text.nsRange(of: termsStr)
        let rangePrivacy = text.nsRange(of: privacyStr)

        if gesture.didTapAttributedTextInLabel(label: label, inRange: rangeTc) {
            self.delegate?.existingPaymentMethodViewControllerWillShowHTMLContent(self, content: .termsOfUse)
        } else if gesture.didTapAttributedTextInLabel(label: label, inRange: rangePrivacy) {
            self.delegate?.existingPaymentMethodViewControllerWillShowHTMLContent(self, content: .privacyPolicy)
        }
    }

    @objc private func applePayTapped() {
        let merchantIdentifier = Configuration.MerchantIdentifier
        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "GB", currency: viewModel.currency)

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "Karai", amount: NSDecimalNumber(value: viewModel.planAmount))
        ]

        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            self.applePayButton.isEnabled = false
            self.showLoadingState(true)
            applePayContext.presentApplePay(completion: nil)
        } else {
            // There is a problem with your Apple Pay configuration
            print("Make sure you've configured Apple Pay correctly, as outlined at https://stripe.com/docs/apple-pay#native")
        }
    }
}

extension ExistingPaymentMethodViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        65.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 24, y: 8, width: 320, height: 20)
        myLabel.font = .K2DBold(16)
        myLabel.textColor = .appColor(.navyBlue)
        myLabel.text = "payments.current.card".localized

        let headerView = UIView()
        headerView.addSubview(myLabel)

        return headerView
    }
}

extension ExistingPaymentMethodViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}

// MARK: - STPApplePayContextDelegate
extension ExistingPaymentMethodViewController: STPApplePayContextDelegate {
    func applePayContext(_: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation _: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        print("Apple PaymentMethod ID - \(paymentMethod.stripeId)")

        viewModel.makeOneTimePayment(with: paymentMethod.stripeId, type: PaymentMethod.wallet) { result in
            switch result {
            case .success(let resp):
                completion(resp.clientSecret, nil)
            case .failure(let error):
                print("[ExistingPaymentMethodViewController] apple pay error: \(error)")
                completion(nil, error)
            }
        }
    }

    func applePayContext(_: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
        self.showLoadingState(false)
        self.applePayButton.isEnabled = true

        switch status {
        case .success:
            // Payment succeeded, show a receipt view
            self.completePayment()
        case .error:
            // Payment failed, show the error
            if let error = error {
                print("Error with Apple Pay - \(error)")
                delegate?.existingPaymentMethodViewControllerDidError(self, error: PaymentError.applePay)
            }
        case .userCancellation:
            // User cancelled the payment
            break
        @unknown default:
            fatalError()
        }
    }
}

private enum PlaceholderKeys {
    static let amount = "{amount}"
}
