//
//  ExistingPaymentMethodViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 28/02/2022.
//

import Foundation
import Stripe

class ExistingPaymentMethodViewModel {
    private let user: DrivekeyUser
    private let api: APIPaymentProtocol
    private let summary: OneTimePaymentSummary

    var paymentMethods: [PaymentMethod] = []

    init(user: DrivekeyUser, api: APIPaymentProtocol, summary: OneTimePaymentSummary) {
        self.user = user
        self.api = api
        self.summary = summary
    }

    var checkoutSummary: OneTimePaymentSummary {
        summary
    }

    var isApplePay: Bool {
        guard let paymentMethod = paymentMethods.first else { return false }
        return paymentMethod.isApplePay
    }

    var planAmount: Double {
        let amount = summary.totalPrice ?? 0
        let divisor = pow(10.0, Double(2))
        return (amount * divisor).rounded() / divisor
    }

    var currency: String {
        summary.currency ?? "GBP"
    }

    var amountString: String {
        guard let currency = summary.currency else { return "" }
        let price = summary.totalPrice ?? 0

        if let formattedAmount = price.formatForCurrency(currency) {
            return "\(formattedAmount)"
        }
        return ""
    }
    
    func fetchPaymentMethods(_ completion: @escaping (Error?) -> Void) {
        guard let customerId = user.customerId else { return }

        api.paymentMethods(customerId) { result in
            switch result {
            case let .success(response):
                let stripePaymentMethods = response.paymentMethods ?? []
                self.paymentMethods = stripePaymentMethods.map({PaymentMethod($0)})
                completion(nil)
            case let .failure(error):
                completion(error)
            }
        }
    }

    func makeOneTimePayment(with paymentMethodId: String, type: String, _ completion: @escaping (Result<OneTimePaymentAPIResponse, Error>) -> Void) {
        AnalyticsService.trackEvent(.makePurchase)
        api.paymentAttach(paymentMethodId: paymentMethodId, type: type) { result in
            switch result {
            case .success:
                self.api.oneTimePayment(data: self.summary) { result in
                    completion(result)
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func handleNextAction(_ paymentIntentSecret: String?, authContext: STPAuthenticationContext, completion: @escaping (Error?) -> Void) {
        guard let secret = paymentIntentSecret else { return }
        let handler = STPPaymentHandler.shared()
        handler.handleNextAction(forPayment: secret, with: authContext, returnURL: nil) { status, paymentIntent, error in
            switch status {
            case .succeeded:
                if let paymentIntent = paymentIntent, paymentIntent.status == STPPaymentIntentStatus.requiresConfirmation {
                    print("Re-confirming PaymentIntent after handling action")
                    self.confirmIntent(paymentIntent, completion: completion)
                } else {
                    completion(nil)
                }
            case .canceled:
                print("Cancelled next action")
            case .failed:
                if let error = error {
                    print("Error handling next action - \(error)")
                    completion(error)
                }
            @unknown default:
                fatalError()
            }
        }
    }

    private func confirmIntent(_ intent: STPPaymentIntent, completion: @escaping (Error?) -> Void) {
        let id = intent.stripeId

        api.confirm(id) { result in
            switch result {
            case .success:
                completion(nil)
            case let .failure(error):
                print("[ExistingPaymentMethodViewModel] Error confirming intent - \(error)")
                completion(error)
            }
        }
    }
}
