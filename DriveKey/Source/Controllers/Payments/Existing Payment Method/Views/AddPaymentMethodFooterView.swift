//
//  AddPaymentMethodFooterView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import Foundation
import UIKit

class AddPaymentMethodFooterView: BaseView {
    private let addImageView = Subviews.addImageView
    private let nameLabel = Subviews.nameLabel

    override func setUpSubviews() {
        super.setUpSubviews()

        addSubview(addImageView)
        addSubview(nameLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()

        NSLayoutConstraint.activate([
            addImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30),
            addImageView.topAnchor.constraint(equalTo: topAnchor, constant: 30),
            addImageView.widthAnchor.constraint(equalToConstant: 17),
            addImageView.heightAnchor.constraint(equalToConstant: 17),

            nameLabel.leadingAnchor.constraint(equalTo: addImageView.trailingAnchor, constant: 20),
            nameLabel.centerYAnchor.constraint(equalTo: addImageView.centerYAnchor)
        ])
    }
}

private struct Subviews {
    static var addImageView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "add"))
        imageView.tintColor = .appColor(.navyBlue)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.font = .K2DRegular(18)
        label.textColor = .appColor(.navyBlue)
        label.text = "payments.add.new.method".localized
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}
