//
//  PaymentMethodListViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import UIKit

protocol PaymentMethodListViewControllerDelegate: AnyObject {
    func paymentMethodsViewController(_ vc: PaymentMethodListViewController, didSelect method: PaymentMethod)
}

class PaymentMethodListViewController: ViewController {
    @IBOutlet weak var paymentMethodTableView: UITableView!

    private(set) var dataSource = PaymentMethodListDataSource(paymentMethods: [])
    var viewModel: PaymentMethodsViewModel!
    weak var delegate: PaymentMethodListViewControllerDelegate?
    var api: APIPaymentProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }

        title = "profile.account.payment".localized

        paymentMethodTableView.dataSource = self.dataSource
        paymentMethodTableView.delegate = self
        paymentMethodTableView.register(PaymentMethodCell.self, forCellReuseIdentifier: PaymentMethodCell.reuseidentifier)
        paymentMethodTableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        fetchPaymentMethods()
    }

    private func fetchPaymentMethods() {
        guard let customerId = viewModel.customerId else { return }
        showSpinner()

        api.paymentMethods(customerId) { result in
            self.hideSpinner()

            switch result {
            case let .success(response):
                self.proceed(response)
            case let .failure(error):
                self.showAlertError(error: error)
            }
        }
    }

    private func proceed(_ methods: PaymentMethodContainer) {
        updatePaymentMethods(methods)
        paymentMethodTableView.reloadData()
    }

    func updatePaymentMethods(_ methods: PaymentMethodContainer) {
        guard let stripePaymentMethods = methods.paymentMethods else { return }

        let paymentMethods = stripePaymentMethods.map({PaymentMethod($0)})
        self.dataSource = PaymentMethodListDataSource(paymentMethods: paymentMethods)
        paymentMethodTableView.dataSource = dataSource
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension PaymentMethodListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let dataSource = tableView.dataSource as? PaymentMethodListDataSource else { return }
        tableView.deselectRow(at: indexPath, animated: true)
        let data = dataSource.paymentMethods[indexPath.row]
        delegate?.paymentMethodsViewController(self, didSelect: data)
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        65.0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 24, y: 8, width: 320, height: 20)
        myLabel.font = .K2DBold(16)
        myLabel.textColor = .appColor(.navyBlue)
        myLabel.text = "payments.current.card".localized

        let headerView = UIView()
        headerView.addSubview(myLabel)

        return headerView
    }
}
