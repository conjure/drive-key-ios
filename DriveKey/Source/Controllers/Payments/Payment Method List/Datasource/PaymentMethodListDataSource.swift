//
//  PaymentMethodListDataSource.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 21/02/2022.
//

import UIKit

class PaymentMethodListDataSource: NSObject, UITableViewDataSource {
    var paymentMethods: [PaymentMethod]

    init(paymentMethods: [PaymentMethod]) {
        self.paymentMethods = paymentMethods
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return paymentMethods.isEmpty ? 0 : 1
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        paymentMethods.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PaymentMethodCell.reuseidentifier, for: indexPath) as? PaymentMethodCell else {
            return UITableViewCell()
        }

        let data = paymentMethods[indexPath.row]
        cell.configure(for: data)

        return cell
    }
}
