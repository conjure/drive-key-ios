//
//  PaymentMethodsViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import Foundation

struct PaymentMethodsViewModel {
    let userData: DrivekeyUser?

    var customerId: String? {
        userData?.customerId
    }
}
