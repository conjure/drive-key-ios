//
//  PaymentMethodDetailViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/09/2021.
//

import UIKit

protocol PaymentMethodDetailViewControllerDelegate: AnyObject {
    func paymentMethodDetailViewControllerWillShowAlert(_ vc: PaymentMethodDetailViewController)
    func paymentMethodDetailViewControllerDidRemove(_ vc: PaymentMethodDetailViewController, method: PaymentMethod)
}

class PaymentMethodDetailViewController: ViewController {

    @IBOutlet private var currentCardHeadingLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet private var expiryDateHeadingLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet private var updatePaymentButton: SmallButton!
    @IBOutlet private var noMethodContainerView: UIView!
    @IBOutlet private var noMethodLabel: UILabel!

    private var cardBrandImageView: UIImageView?
    var viewModel: PaymentMethodDetailViewModel!
    weak var delegate: PaymentMethodDetailViewControllerDelegate?
    var showNoMethodsState = false

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        title = viewModel.network
        currentCardHeadingLabel.text = "payments.current.card".localized

        expiryDateHeadingLabel.isHidden = false
        expiryDateLabel.isHidden = false

        cardNumberLabel.text = viewModel.cardNumber
        expiryDateHeadingLabel.text = "payments.expiry.date".localized
        expiryDateLabel.text = viewModel.expiryDate

        let underlineAtts: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.appColor(.navyBlue),
                                                            .font: UIFont.K2DMedium(16),
                                                            .underlineStyle: NSUnderlineStyle.single.rawValue]
        let buttonTitle = NSMutableAttributedString(string: "payments.remove.method".localized, attributes: underlineAtts)
        updatePaymentButton.setAttributedTitle(buttonTitle, for: .normal)

        noMethodLabel.text = "payments.no.methods".localized

        if showNoMethodsState {
            self.title = "profile.account.payment".localized
            self.noMethodContainerView.isHidden = false
            self.removeCardBrandLogo()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !showNoMethodsState {
            runOnMainThreadAsyncAfter(0.05) {
                self.addCardBrandLogo()
            }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeCardBrandLogo()
    }

    private func addCardBrandLogo() {
        if let navigationBar = navigationController?.navigationBar {
            cardBrandImageView = UIImageView(frame: CGRect.zero)
            cardBrandImageView?.translatesAutoresizingMaskIntoConstraints = false
            cardBrandImageView?.contentMode = .scaleAspectFit
            cardBrandImageView?.image = viewModel.cardBrandImage

            UIView.transition(with: navigationBar, duration: 0.25, options: [.transitionCrossDissolve]) {
                navigationBar.addSubview(self.cardBrandImageView!)
            } completion: { _ in
                //
            }

            let constraints = [
                cardBrandImageView!.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -24),
                cardBrandImageView!.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -9),
                cardBrandImageView!.heightAnchor.constraint(equalToConstant: 37),
                cardBrandImageView!.widthAnchor.constraint(equalToConstant: 37)
            ]
            NSLayoutConstraint.activate(constraints)
        }
    }

    private func removeCardBrandLogo() {
        cardBrandImageView?.removeFromSuperview()
        cardBrandImageView = nil
    }

    private func setLoadingState(_ isLoading: Bool) {
        if isLoading {
            showSpinner()
        } else {
            hideSpinner()
        }

        updatePaymentButton.isEnabled = !isLoading
    }

    @IBAction private func updatePaymentMethod(_: Any) {
        delegate?.paymentMethodDetailViewControllerWillShowAlert(self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

// MARK: - AlertDelegate

extension PaymentMethodDetailViewController: AlertDelegate {
    func alertDidConfirm(_ vc: AlertPopupView, for _: AlertStyle) {
        vc.dismiss(animated: true, completion: nil)

        self.setLoadingState(true)
        self.viewModel.removeMethod { error in
            self.setLoadingState(false)
            if error != nil {
                self.showAlertError(error: error!)
            } else {
                self.delegate?.paymentMethodDetailViewControllerDidRemove(self, method: self.viewModel.paymentMethod)
            }
        }
    }

    func alertDidDismiss(_ vc: AlertPopupView) {
        vc.dismiss(animated: true, completion: nil)
    }
}
