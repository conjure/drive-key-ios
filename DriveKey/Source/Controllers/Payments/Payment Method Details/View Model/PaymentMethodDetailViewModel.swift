//
//  PaymentMethodDetailViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/09/2021.
//

import Foundation
import UIKit

struct PaymentMethodDetailViewModel {
    private(set) var paymentMethod: PaymentMethod
    private let api: APIPaymentProtocol

    init(method: PaymentMethod, api: APIPaymentProtocol) {
        paymentMethod = method
        self.api = api
    }

    var network: String {
        if paymentMethod.isApplePay {
            return "payments.payment.method.apple".localized
        }

        return paymentMethod.brand?.capitalizingFirstLetter() ?? ""
    }

    var cardNumber: String {
        paymentMethod.cardNumber
    }

    var isApplePay: Bool {
        paymentMethod.isApplePay
    }

    var expiryDate: String {
        guard let month = paymentMethod.expMonth, let year = paymentMethod.expYear else { return "" }

        let formattedMonth = String(format: "%02d", month)
        let formattedYear = String(format: "%02d", year % 100)

        return "\(formattedMonth)/\(formattedYear)"
    }

    var cardBrandImage: UIImage? {
        paymentMethod.cardBrandIcon
    }

    func removeMethod(_ completion: @escaping (Error?) -> Void) {
        guard let methodID = paymentMethod.identifier else {
            print("Missing Payment Method Identifier")
            return
        }
        api.removePaymentMethod(methodID) { result in
            switch result {
            case .success:
                completion(nil)
            case .failure(let error):
                print("[PaymentMethodDetailViewModel] Error removing payment method - \(error)")
                completion(error)
            }
        }
    }
}
