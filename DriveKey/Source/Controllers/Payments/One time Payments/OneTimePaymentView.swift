//
//  OneTimePaymentView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/01/2022.
//
// swiftlint:disable weak_delegate

import SwiftUI
import FirebaseAnalytics

protocol OneTimePaymentViewDelegate: AnyObject {
    func oneTimePaymentView(_ view: OneTimePaymentView, willAddCard forSummary: OneTimePaymentSummary)
    func oneTimePaymentView(_ view: OneTimePaymentView, willConfirmCard forSummary: OneTimePaymentSummary)
    func oneTimePaymentView(_ view: OneTimePaymentView, willPresent content: HTMLContentType)
    func oneTimePaymentView(_ view: OneTimePaymentView, willPresent project: CharityProject)
    func oneTimePaymentViewWillDismiss()
}

struct OneTimePaymentView: View {
    @ObservedObject var viewModel: OneTimePaymentViewModel
    @StateObject var projectDelegate = ProjectPickerDelegate()
    @State private var selectedPrice: Double = 0

    let configuration: Configuration

    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                    ScrollView(.vertical, showsIndicators: true) {
                        VStack(spacing: 0) {
                            OneTimePaymentHeaderView(configuration: configuration)
                            
                            switch viewModel.state {
                            case .idle:
                                Color.clear
                                    .onAppear {
                                        viewModel.fetchData()
                                    }
                                
                            case .loading:
                                ProgressView()
                                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
                                            .scaleEffect(2)
                                            .offset(x: 0, y: UIScreen.main.bounds.height * 0.4)
                                                                        Spacer()
                            case .failed:
                                Text("one.time.payment.summary.error".localized)
                                                                        .font(.K2DSemiBold(14))
                                                                        .foregroundColor(.appColor(.pastelBlue))
                                                                        .padding(.top, 10)
                                Spacer()
                                
                            case .loaded:
                                ProjectCarouselView(heading: "", model: .init(charityProject: nil), delegate: projectDelegate)
                                    .padding(.top, 17)

                                AdditionalOffsetView(viewModel: viewModel,
                                                     selectedPrice: $selectedPrice,
                                                     salesPrice: viewModel.pricePerKg,
                                                     currency: viewModel.currency)
                                
                                OneTimePaymentSummaryView(viewModel: viewModel, selectedPrice: $selectedPrice)
                                    .padding(.top, 24)
                                    .padding(.bottom, 10)
                            }
                        }
                }
                
                if viewModel.state == .loaded {
                    ZStack(alignment: .bottom) {
                        OneTimePaymentFooterView(price: viewModel.total, currency: viewModel.currency) {
                            AnalyticsService.trackEvent(.clickedPayNow)
                            
                            let state = self.viewModel.getCheckoutState()
                            if let summary = state.updatedSummary {
                                if state.paymentMethodExists {
                                    self.configuration.delegate?.oneTimePaymentView(self, willConfirmCard: summary)
                                } else {
                                    self.configuration.delegate?.oneTimePaymentView(self, willAddCard: summary)
                                }
                            }
                        } termsPrivacyAction: { contentType in
                            configuration.delegate?.oneTimePaymentView(self, willPresent: contentType)
                        }
                        
                                         LinearGradient(colors: [Color.appColor(.pastelDarkBlue).opacity(0), Color.appColor(.navyBlue)], startPoint: .top, endPoint: .bottom)
                                                .frame( maxWidth: .infinity, maxHeight: 50)
                                                .offset(x: 0, y: -110)
                                                .allowsHitTesting(false)
                    }
                } else if viewModel.state == .failed {
                    Button {
                        self.viewModel.fetchData()
                    } label: {
                        Text(viewModel.buttonText)
                            .font(.K2DMedium(15))
                            .foregroundColor(Color.black)
                            .frame(width: UIScreen.main.bounds.width - 48, height: 50)
                            .background(Color.appColor(.chartGreen))
                            .cornerRadius(7)
                            .padding(.bottom, 20)
                    }
                    .opacity(viewModel.state == .loading ? 0.5 : 1.0)
                    .disabled(viewModel.state == .loading)
                }
            }
            .background(Color.appColor(.navyBlue)
                            .ignoresSafeArea())
            .navigationBarHidden(true)
            .statusBar(style: .lightContent)
            .onReceive(projectDelegate.didChange) { del in
                guard let project = del.project else { return }
                configuration.delegate?.oneTimePaymentView(self, willPresent: project)
            }
            .onAppear() {
                AnalyticsService.trackScreenEvent("OneTimePaymentView")
            }
        }
    }
}

struct OneTimePaymentView_Previews: PreviewProvider {
    static var previews: some View {
        OneTimePaymentView(viewModel: OneTimePaymentViewModel(user: DrivekeyUser(userID: "123", email: "dsa"), api: NetworkManager.shared), configuration: OneTimePaymentView.Configuration())
            .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
            .previewDisplayName("iPhone 13 Pro Max")

        OneTimePaymentView(viewModel: OneTimePaymentViewModel(user: DrivekeyUser(userID: "123", email: "dsa"), api: NetworkManager.shared), configuration: OneTimePaymentView.Configuration())
            .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
            .previewDisplayName("iPhone SE (2nd generation)")
    }
}

extension OneTimePaymentView {
    final class Configuration {
        unowned var delegate: OneTimePaymentViewDelegate?
    }
}
