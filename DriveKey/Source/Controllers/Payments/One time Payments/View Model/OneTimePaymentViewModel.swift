//
//  OneTimePaymentViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/01/2022.
//

import Foundation
import SwiftUI

class OneTimePaymentViewModel: NSObject, ObservableObject {
    enum State {
        case idle
        case loading
        case failed
        case loaded
    }
    @Published private(set) var state = State.idle
 
    @Published var generatedEmissions: Double
    @Published var additionalEmissions: Double
    @Published var offset: Double
    @Published var additionalOffsetCost: Double
    @Published var pricePerKg: Double
    @Published var karaiFeeText: String
    @Published var karaiFee: Double
    @Published var total: Double

    private let user: DrivekeyUser!
    private let api: APICarbonProtocol!
    private(set) var summary: OneTimePaymentSummary?
    private(set) var currency: String

    init(user: DrivekeyUser, api: APICarbonProtocol) {
        self.user = user
        self.api = api

        currency = ""
        generatedEmissions = 0
        additionalEmissions = 0
        additionalOffsetCost = 0
        offset = 0
        pricePerKg = 0
        karaiFeeText = ""
        karaiFee = 0
        total = 0
    }

    func fetchData() {
        self.state = .loading
        api.oneTimePaymentSummary { result in
            switch result {
            case .success(let summary):
                self.summary = summary
                self.generatedEmissions = summary.totalCo2Generated
                self.offset = summary.netCo2Price
                self.pricePerKg = summary.co2UnitPrice
                self.currency = summary.currency ?? ""
                self.update(with: 0)
                self.state = .loaded
            case .failure(let error):
                print("Error fetching checkout summary - \(error)")
                self.state = .failed
            }
        }
    }

    func getCheckoutState() -> (paymentMethodExists: Bool, updatedSummary: OneTimePaymentSummary?) {
        guard let summary = summary else {
            return (false, nil)
        }

        let totalCO2 = generatedEmissions + additionalEmissions
        let updatedSummary = OneTimePaymentSummary(totalCo2Generated: round(totalCO2),
                                                           netCo2Price: summary.netCo2Price,
                                                           co2UnitPrice: summary.co2UnitPrice,
                                                           currency: summary.currency,
                                                           paymentMethodExist: summary.paymentMethodExist,
                                                           dkMargin: summary.dkMargin,
                                                           minTransactionFee: summary.minTransactionFee,
                                                   totalPrice: self.total)
        return (summary.paymentMethodExist, updatedSummary)
    }

    var additionalOffsetDescription: String {
        guard let dashboardSummary = user.dashboardSummary, let co2Generated = dashboardSummary.co2PerKiloGram, let co2Offset = dashboardSummary.co2OffsetPerKiloGram else { return "" }

        if co2Generated == co2Offset {
            if co2Offset == 0 {
                return "one.time.payment.noCarbon.generated".localized
            } else {
                return "one.time.payment.carbon.neutral".localized
            }
        } else if co2Generated > co2Offset {
            return "one.time.payment.carbon.generated".localized
        } else if co2Offset > co2Generated {
            return  "one.time.payment.carbon.positive".localized
        }

        return ""
    }

    var generatedCO2: String {
        let totalCO2 = generatedEmissions + additionalEmissions
        let val = Double(totalCO2).formattedCO2
        return "\(val)kg"
    }

    var totalString: String {
        return total.formatForCurrency(currency) ?? "£0"
    }

    var totalOffset: String {
        return offset.formatForCurrency(currency) ?? "£0"
    }

    var additionalOffsetPrice: String {
        return additionalOffsetCost.formatForCurrency(currency) ?? "£0"
    }

    var driveKeyPrice: String {
        return karaiFee.formatForCurrency(currency) ?? "£0"
    }

    func update(with additionalOffsetPrice: Double) {
        guard let summary = summary else { return }

        self.additionalOffsetCost = additionalOffsetPrice
        self.additionalEmissions = additionalOffsetPrice / pricePerKg
        let newTotal = offset + additionalOffsetPrice
        var driveKeyFee = newTotal * summary.dkMargin

        if offset == 0 && additionalOffsetPrice == 0 {
            karaiFeeText = "karai.fee.none".localized
            driveKeyFee = 0
        } else if driveKeyFee < summary.minTransactionFee {
            karaiFeeText = "karai.fee.minimum".localized
            driveKeyFee = summary.minTransactionFee
        } else {
            let margin = summary.dkMargin * 100
            karaiFeeText = "kari.fee.calculated".localized.replacingOccurrences(of: PlaceholderKeys.margin, with: "\(Int(margin))")
        }

        self.karaiFee = driveKeyFee
        self.total = newTotal + driveKeyFee
    }

    var buttonText: String {
        switch state {
        case .failed:
            return "one.time.payment.retry".localized
        default:
            return "one.time.payment.button.text".localized.replacingOccurrences(of: PlaceholderKeys.amount, with: totalString)
        }
    }

    private enum PlaceholderKeys {
        static let amount = "{amount}"
        static let margin = "{margin}"
    }
}
