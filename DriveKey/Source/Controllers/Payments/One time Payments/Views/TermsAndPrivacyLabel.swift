//
//  TermsAndPrivacyLabel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/02/2022.
//

import SwiftUI
import UIKit

struct TermsAndPrivacyLabel: UIViewRepresentable {
    private let termsConditionLabel = UILabel()
    static let stringTc = "one.time.payment.min.payment.t&c".localized
    static let stringPrivacy = "one.time.payment.min.payment.privacy".localized
    
    var width: CGFloat
    var tappedCallback: ((LinkType) -> Void)
    
    func makeUIView(context: Context) -> UILabel {
        // create attributed string
        let strTerms = "one.time.payment.min.payment".localized
        
        let attrString = NSMutableAttributedString(string: strTerms)
            .underline(TermsAndPrivacyLabel.stringTc)
            .underline(TermsAndPrivacyLabel.stringPrivacy)
        attrString.addAttribute(.font, value: UIFont.K2DSemiBold(getScaledTextSize(for: 12))! as Any, range: strTerms.nsRange(of: strTerms))
        
        termsConditionLabel.textColor = .white.withAlphaComponent(0.5)
        termsConditionLabel.lineBreakMode = .byWordWrapping
        termsConditionLabel.numberOfLines = 2
        termsConditionLabel.isUserInteractionEnabled = true
        termsConditionLabel.textAlignment = .center
        termsConditionLabel.attributedText = attrString
        termsConditionLabel.adjustsFontSizeToFitWidth = true
        termsConditionLabel.minimumScaleFactor = 0.5
        termsConditionLabel.preferredMaxLayoutWidth = width
        
        let tap = UITapGestureRecognizer(target: context.coordinator, action: #selector(context.coordinator.loadTermsConditions))
        termsConditionLabel.addGestureRecognizer(tap)
        
        return termsConditionLabel
    }
    
    func updateUIView(_ uiView: UILabel, context: Context) {
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(label: termsConditionLabel, callback: tappedCallback)
    }

    private func getScaledTextSize(for val: CGFloat) -> CGFloat {
        UIScreen.main.bounds.height <= 900 ? val : val * 1.2
    }
    
    enum LinkType {
        case termsConditions
        case privacyPolicy
    }
    
    class Coordinator: NSObject {
        var label: UILabel
        var tappedCallback: ((LinkType) -> Void)
        
        init(label: UILabel, callback: @escaping ((LinkType) -> Void)) {
            self.label = label
            self.tappedCallback = callback
        }
        
        @objc func loadTermsConditions(_ gesture: UITapGestureRecognizer) {
            guard let label = gesture.view as? UILabel, let text = label.attributedText?.string else { return }
            
            let rangeTc = text.nsRange(of: TermsAndPrivacyLabel.stringTc)
            let rangePrivacy = text.nsRange(of: TermsAndPrivacyLabel.stringPrivacy)
            
            if gesture.didTapAttributedTextInLabel(label: label, inRange: rangeTc) {
                tappedCallback(.termsConditions)
            } else if gesture.didTapAttributedTextInLabel(label: label, inRange: rangePrivacy) {
                tappedCallback(.privacyPolicy)
            }
        }
    }
}
