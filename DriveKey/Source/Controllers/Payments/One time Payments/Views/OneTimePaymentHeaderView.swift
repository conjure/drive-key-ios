//
//  OneTimePaymentHeaderView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/01/2022.
//

import SwiftUI

private struct HeaderTitleView: View {
    var body: some View {
        HStack(spacing: 15) {
            Text("offset.carbon.button.title".localized)
                .font(.K2DBold(24))
                .foregroundColor(.white)
            Spacer()
        }
        .padding(.leading, 24)
    }
}

struct OneTimePaymentHeaderView: View {
    let configuration: OneTimePaymentView.Configuration
    
    var body: some View {
        HStack {
            HeaderTitleView()
            Spacer()
            Button {
                configuration.delegate?.oneTimePaymentViewWillDismiss()
            } label: {
                Image("close-arrow-icon")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 24, height: 24)
                    .padding(EdgeInsets(top: 5, leading: 10, bottom: 5, trailing: 0))
            }
        }
        .padding(.trailing, 20)
        .padding(.top, -10)
    }
}
