//
//  OneTimePaymentFooterView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 22/02/2022.
//

import SwiftUI

struct OneTimePaymentFooterView: View {
    var price: Double
    var currency: String
    var payAction: (() -> Void)
    var termsPrivacyAction: ((HTMLContentType) -> Void)

    var body: some View {
        VStack(spacing: 0) {
            TermsAndPrivacyLabel(width: UIScreen.main.bounds.width - 96) { link in
                if link == .privacyPolicy {
                    termsPrivacyAction(.privacyPolicy)
                } else if link == .termsConditions {
                    termsPrivacyAction(.termsOfUse)
                }
            }
            .frame(height: 40)
            .padding(.horizontal, 46)
            .padding(.bottom, 15)

            Button {
                payAction()
            } label: {
                Text(String(format: "Pay \(price.formatForCurrency(currency) ?? "£0")"))
                    .font(.K2DMedium(15))
                    .foregroundColor(Color.black)
                    .frame(width: UIScreen.main.bounds.width - 48, height: 50)
                    .background(Color.appColor(.chartGreen))
                    .cornerRadius(7)
            }
            .padding(.bottom, 10)
            .opacity(price == 0 ? 0.5 : 1.0)
            .disabled(price == 0)
        }
    }
}

struct OneTimePaymentFooterView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spacer()

            OneTimePaymentFooterView(price: 3.66, currency: "GBP") {
                print("Pay")
            } termsPrivacyAction: { type in
                print(type)
            }
                .frame(maxWidth: .infinity)
        }
        .background(Color.appColor(.navyBlue)
                        .ignoresSafeArea())
            .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
            .previewDisplayName("iPhone 13 Pro Max")

        VStack {
            Spacer()
            
            OneTimePaymentFooterView(price: 3.66, currency: "GBP") {
                print("Pay")
            } termsPrivacyAction: { type in
                print(type)
            }
                .frame(maxWidth: .infinity)
        }
        .background(Color.appColor(.navyBlue)
                        .ignoresSafeArea())
            .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
            .previewDisplayName("iPhone SE (2nd generation)")
    }
}
