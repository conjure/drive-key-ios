//
//  OneTimePaymentSummaryView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/01/2022.
//

import SwiftUI

struct OneTimePaymentSummaryView: View {
    @ObservedObject var viewModel: OneTimePaymentViewModel
    @Binding var selectedPrice: Double

    var body: some View {
        VStack(spacing: 16) {
            HStack {
                Text("one.time.payment.summary.title".localized)
                    .foregroundColor(.white)
                    .font(.K2DBold(16))
                Spacer()
                Text(viewModel.generatedCO2)
                    .foregroundColor(.appColor(.pastelBlue))
                    .font(.K2DSemiBold(16))
            }
            .padding(.horizontal, 24)

            VStack(spacing: 16) {
                OneTimePaymentRowView(attributedHeading: formatCO2String("one.time.payment.offset.heading".localized), value: viewModel.totalOffset)
                OneTimePaymentRowView(heading: "one.time.payment.additional.offset.heading".localized, value: viewModel.additionalOffsetPrice)
                OneTimePaymentRowView(heading: viewModel.karaiFeeText, value: viewModel.driveKeyPrice)
                Divider()
                    .background(Color(0x556C85))
                OneTimePaymentRowView(heading: "one.time.payment.total.heading".localized, value: viewModel.totalString, isTotal: true)
            }
            .padding(16)
            .background(RoundedRectangle(cornerRadius: 12)
                            .fill(Color.appColor(.notificationBlue)))
            .padding(.horizontal, 24)
        }
        .onChange(of: selectedPrice) { newValue in
            viewModel.update(with: Double(newValue))
        }
    }

    private func formatCO2String(_ string: String) -> NSAttributedString {
        let messageAttrs: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.K2DSemiBold(16)!, .foregroundColor: UIColor.appColor(.pastelBlue)]
        let messageStr = NSMutableAttributedString(string: string, attributes: messageAttrs)
        messageStr.updateCO2(for: UIFont.K2DSemiBold(14)!, colour: UIColor.appColor(.pastelBlue))
        return messageStr
    }
}

struct OneTimePaymentSummaryView_Previews: PreviewProvider {
    static var previews: some View {
        OneTimePaymentView(viewModel: OneTimePaymentViewModel(user: DrivekeyUser(userID: "123", email: "dsa"), api: NetworkManager.shared), configuration: OneTimePaymentView.Configuration())
    }
}
