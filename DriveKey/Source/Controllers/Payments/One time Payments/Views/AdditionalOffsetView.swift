//
//  AdditionalOffsetView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 22/02/2022.
//

import SwiftUI

struct AdditionalOffsetView: View {
    @ObservedObject var viewModel: OneTimePaymentViewModel
    @Binding var selectedPrice: Double
    private let offsetPrices: [Double] = [1, 2, 5, 10, 20, 25]
    var salesPrice: Double
    var currency: String

    var body: some View {
        VStack(spacing: 15) {
            Text(viewModel.additionalOffsetDescription)
                .foregroundColor(.white)
                .font(.K2DBold(16))
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal, 24)

            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 12) {
                    ForEach(offsetPrices, id: \.self) { price in
                        AdditionalOffsetTileView(price: price, currency: currency, salesPrice: salesPrice, selectedPrice: $selectedPrice)
                    }

                    Spacer(minLength: 12)
                }
                .padding(.leading, 24)
            }
        }
    }
}

struct AdditionalOffsetView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.appColor(.navyBlue)

            AdditionalOffsetView(viewModel: OneTimePaymentViewModel(user: DrivekeyUser(userID: "123",
                                                                                       email: "dsa"),
                                                                    api: NetworkManager.shared),
                                 selectedPrice: .constant(0), salesPrice: 0.03, currency: "GBP")
        }
    }
}

private struct AdditionalOffsetTileView: View {
    var price: Double
    var currency: String
    var salesPrice: Double
    @Binding var selectedPrice: Double

    private var isSelected: Bool {
        selectedPrice == price
    }

    var body: some View {
        VStack {
            Text(price.formatForCurrency(currency) ?? "")
            .font(.K2DMedium(20))
            .foregroundColor(isSelected ? Color.appColor(.pastelDarkBlue) : .white)
            .frame(width: 66, height: 66)
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color.appColor(.pastelBlue), lineWidth: 1)
                    .background(
                        RoundedRectangle(cornerRadius: 12)
                            .fill(isSelected ? Color.white : Color.appColor(.notificationBlue))
                    )
            )
            .padding(.top, 3)
            .onTapGesture {
                selectedPrice = isSelected ? 0 : price
            }

            Text("\(calculateOffsetAmount())kg")
                .foregroundColor(Color.appColor(.pastelBlue))
                .font(.K2DSemiBold(16))
        }
    }

    private func calculateOffsetAmount() -> String {
        let val = Double(price) / salesPrice
        return val.formattedCO2
    }
}
