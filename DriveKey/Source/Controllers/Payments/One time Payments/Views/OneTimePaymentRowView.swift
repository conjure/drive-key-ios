//
//  OneTimePaymentRowView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/01/2022.
//

import SwiftUI

struct OneTimePaymentRowView: View {
    var heading: String?
    var attributedHeading: NSAttributedString?
    var value: String
    var isTotal: Bool = false

    var body: some View {
        HStack {
            if let attributedHeading = attributedHeading {
                AttributedText(attributedHeading)
            } else if let heading = heading {
                Text(heading)
                    .font(.K2DSemiBold(16))
                    .foregroundColor(.appColor(.pastelBlue))
            }
            Spacer()
            Text(value)
                .font(.K2DSemiBold(16))
                .foregroundColor(isTotal ? .appColor(.chartGreen) : .white)
        }
    }
}
