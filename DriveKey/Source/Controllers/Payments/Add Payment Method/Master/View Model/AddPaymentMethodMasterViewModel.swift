//
//  AddPaymentMethodMasterViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 30/09/2021.
//

import Foundation
import Stripe

class AddPaymentMethodMasterViewModel {
    let userService: UserProvidable
    let paymentMethod: PaymentMethod?
    private let summary: OneTimePaymentSummary?
    private let api: APIPaymentProtocol!

    init(userService: UserProvidable, paymentMethod: PaymentMethod?, summary: OneTimePaymentSummary?, api: APIPaymentProtocol) {
        self.userService = userService
        self.paymentMethod = paymentMethod
        self.summary = summary
        self.api = api
    }

    var customerId: String {
        userService.currentUser?.customerId ?? ""
    }

    var planAmount: Double {
        let amount = (summary?.totalPrice ?? 0)
        let divisor = pow(10.0, Double(2))
        return (amount * divisor).rounded() / divisor
    }

    var co2Offset: Int {
        Int(self.summary?.totalCo2Generated ?? 0) 
    }

    var currency: String {
        summary?.currency ?? "GBP"
    }

    func makeOneTimePayment(with paymentMethodId: String, type: String, _ completion: @escaping (Result<OneTimePaymentAPIResponse, Error>) -> Void) {
        guard let summary = summary else {
            completion(.failure(PaymentMethodError.missingOTPSummary))
            return
        }

        AnalyticsService.trackEvent(.makePurchase)

        attach(paymentMethodId: paymentMethodId, type: type) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                self.api.oneTimePayment(data: summary) { result in
                    completion(result)
                }
            }
        }
    }

    func attach(paymentMethodId: String, type: String, completion: @escaping (Error?) -> Void) {
        api.paymentAttach(paymentMethodId: paymentMethodId, type: type) { result in
            switch result {
            case .success:
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }

    func confirmIntent(_ intentID: String, completion: @escaping (Result<PaymentIntentState, Error>) -> Void) {
        api.confirm(intentID) { result in
            switch result {
            case let .success(resp):
                completion(.success(resp.state))
            case let .failure(error):
                print("[AddPaymentMethodMasterViewModel] Error confirming intent - \(error)")
                completion(.failure(error))
            }
        }
    }

    enum PaymentMethodError: Error {
        case couldNotAttach
        case missingOTPSummary
    }
}
