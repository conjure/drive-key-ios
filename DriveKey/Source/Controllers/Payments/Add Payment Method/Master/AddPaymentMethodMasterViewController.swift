//
//  AddPaymentMethodMasterViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 29/09/2021.
//

import PassKit
import Stripe
import UIKit

protocol AddPaymentMethodMasterViewControllerDelegate: AnyObject {
    func addPaymentMethodMasterViewController(_ vc: AddPaymentMethodMasterViewController, didCompletePaymentFor: Int)
    func addPaymentMethodMasterViewController(_ vc: AddPaymentMethodMasterViewController, didError: ErrorConfirmationType)
}

class AddPaymentMethodMasterViewController: ViewController {
    @IBOutlet private var progressBackgroundView: UIView!
    @IBOutlet private var progressView: UIView!
    @IBOutlet private var containerView: UIView!

    @IBOutlet private var progressWidthConstraint: NSLayoutConstraint!

    private var cardViewController: AddPaymentMethodCardViewController!
    private var addPaymentMethodBillingViewController: AddPaymentMethodBillingViewController!

    var viewModel: AddPaymentMethodMasterViewModel!
    weak var delegate: AddPaymentMethodMasterViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        title = "add.payment.method.title".localized
        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }

        let backImage = UIImage(named: "back_arrow")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(onBack))

        progressBackgroundView.layer.cornerRadius = 2
        progressView.layer.cornerRadius = 2

        // Add Card View Controller
        cardViewController = UIStoryboard.instantiateViewController()
        cardViewController.viewModel = AddPaymentMethodCardViewModel()
        cardViewController.delegate = self
        add(child: cardViewController, to: containerView, animated: false)

        progressWidthConstraint.constant = (UIScreen.main.bounds.width - 46) * 0.5
    }

    private func showBilling(_ card: PaymentMethodCard) {
        view.endEditing(true)

        addPaymentMethodBillingViewController = UIStoryboard.instantiateViewController()
        addPaymentMethodBillingViewController.viewModel = AddPaymentMethodBillingViewModel(card: card,
                                                                                           previousMethod: viewModel.paymentMethod,
                                                                                           currency: viewModel.currency,
                                                                                           amount: viewModel.planAmount)
        addPaymentMethodBillingViewController.delegate = self
        add(child: addPaymentMethodBillingViewController, to: containerView, animated: true)
    }

    private func updateProgress(_ progress: CGFloat) {
        progressWidthConstraint.constant = progressBackgroundView.frame.width * progress
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }

    private func makePayment(_ paymentMethodId: String) {
        self.setLoadingState(true)

        viewModel.makeOneTimePayment(with: paymentMethodId, type: PaymentMethod.card) { result in
            self.setLoadingState(false)
            self.addPaymentMethodBillingViewController.continueButton.isEnabled = true

            switch result {
            case .success(let resp):
                if resp.requiresAction == true {
                    self.handleNextAction(resp.clientSecret)
                } else {
                    self.proceed()
                }
            case .failure(let error):
                print("[AddPaymentMethodMasterViewController] pay error: \(error)")
                self.delegate?.addPaymentMethodMasterViewController(self, didError: .paymentError(message: error.localizedDescription))
            }
        }
    }

    private func confirmIntent(_ intent: STPPaymentIntent) {
        let id = intent.stripeId
        self.setLoadingState(true)

        viewModel.confirmIntent(id) { result in
            self.setLoadingState(false)

            switch result {
            case let .success(state):
                if state == .succeeded {
                    self.proceed()
                } else if state == .requires_action {
                    self.handleNextAction(id)
                }
            case let .failure(error):
                print("Error confirming intent - \(error)")
                self.delegate?.addPaymentMethodMasterViewController(self, didError: .paymentError(message: error.localizedDescription))
            }
        }
    }

    private func payWithApple() {
        let merchantIdentifier = Configuration.MerchantIdentifier
        let paymentRequest = StripeAPI.paymentRequest(withMerchantIdentifier: merchantIdentifier, country: "GB", currency: viewModel.currency)

        // Configure the line items on the payment request
        paymentRequest.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "Karai", amount: NSDecimalNumber(value: viewModel.planAmount))
        ]

        // Initialize an STPApplePayContext instance
        if let applePayContext = STPApplePayContext(paymentRequest: paymentRequest, delegate: self) {
            // Present Apple Pay payment sheet
            cardViewController.applePayButton.isEnabled = false
            self.setLoadingState(true)
            applePayContext.presentApplePay(completion: nil)
        } else {
            // There is a problem with your Apple Pay configuration
            print("Make sure you've configured Apple Pay correctly, as outlined at https://stripe.com/docs/apple-pay#native")
        }
    }

    private func handleNextAction(_ paymentIntentSecret: String?) {
        guard let secret = paymentIntentSecret else { return }
        let handler = STPPaymentHandler.shared()
        handler.handleNextAction(forPayment: secret, with: self, returnURL: nil) { status, paymentIntent, error in
            switch status {
            case .succeeded:
                if let paymentIntent = paymentIntent, paymentIntent.status == STPPaymentIntentStatus.requiresConfirmation {
                    print("Re-confirming PaymentIntent after handling action")
                    self.confirmIntent(paymentIntent)
                } else {
                    self.proceed()
                }
            case .canceled:
                print("Cancelled next action")
            case .failed:
                if let error = error {
                    print("Error handling next action - \(error)")
                    self.delegate?.addPaymentMethodMasterViewController(self, didError: .paymentError(message: error.localizedDescription))
                }
            @unknown default:
                fatalError()
            }
        }
    }

    private func proceed() {
        AnalyticsService.trackEvent(TrackingEvent.successfulPurchase(amount: viewModel.planAmount, currency: viewModel.currency))
        self.delegate?.addPaymentMethodMasterViewController(self, didCompletePaymentFor: self.viewModel.co2Offset)
    }

    private func setLoadingState(_ isLoading: Bool) {
        if isLoading {
            self.showSpinner()
            self.view.isUserInteractionEnabled = false
            self.view.alpha = 0.8
        } else {
            self.hideSpinner()
            self.view.isUserInteractionEnabled = true
            self.view.alpha = 1
        }
    }

    @objc
    private func onBack() {
        if children.count > 1 {
            let billingViewController = children.first(where: { $0 is AddPaymentMethodBillingViewController }) as? AddPaymentMethodBillingViewController
            billingViewController?.remove()
            updateProgress(0.5)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension AddPaymentMethodMasterViewController: AddPaymentMethodCardViewControllerDelegate {
    func addPaymentMethodCardViewControllerWillPayWithApple(_: AddPaymentMethodCardViewController) {
        payWithApple()
    }

    func addPaymentMethodCardViewControllerWillEnterBillingDetails(_: AddPaymentMethodCardViewController, card: PaymentMethodCard) {
        showBilling(card)
        runOnMainThreadAsyncAfter(0.2) {
            self.updateProgress(1)
        }
    }
}

extension AddPaymentMethodMasterViewController: AddPaymentMethodBillingViewControllerDelegate {
    func addPaymentMethodBillingViewControllerDidTapContinue(vc: AddPaymentMethodBillingViewController, paymentMethodId: String) {
        vc.continueButton.isEnabled = false

        makePayment(paymentMethodId)
    }
}

extension AddPaymentMethodMasterViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        self
    }
}

// MARK: - STPApplePayContextDelegate

extension AddPaymentMethodMasterViewController: STPApplePayContextDelegate {
    func applePayContext(_: STPApplePayContext, didCreatePaymentMethod paymentMethod: STPPaymentMethod, paymentInformation _: PKPayment, completion: @escaping STPIntentClientSecretCompletionBlock) {
        print("Apple PaymentMethod ID - \(paymentMethod.stripeId)")

        viewModel.makeOneTimePayment(with: paymentMethod.stripeId, type: PaymentMethod.wallet) { result in
            switch result {
            case .success(let resp):
                completion(resp.clientSecret, nil)
            case .failure(let error):
                print("[AddPaymentMethodMasterViewController] apple pay error: \(error)")
                completion(nil, error)
            }
        }
    }

    func applePayContext(_: STPApplePayContext, didCompleteWith status: STPPaymentStatus, error: Error?) {
        self.setLoadingState(false)
        cardViewController.applePayButton.isEnabled = true

        switch status {
        case .success:
            // Payment succeeded, show a receipt view
            self.proceed()
        case .error:
            // Payment failed, show the error
            if let error = error {
                print("Error with Apple Pay - \(error)")
                self.delegate?.addPaymentMethodMasterViewController(self, didError: .paymentError(message: PaymentError.applePay.localizedDescription))
            }
        case .userCancellation:
            // User cancelled the payment
            break
        @unknown default:
            fatalError()
        }
    }
}
