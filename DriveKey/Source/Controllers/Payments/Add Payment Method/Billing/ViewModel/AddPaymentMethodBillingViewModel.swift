//
//  AddPaymentMethodBillingViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 29/09/2021.
//

import Foundation
import Stripe

struct AddPaymentMethodBillingViewModel {
    private let card: PaymentMethodCard
    private let previousMethod: PaymentMethod?
    private let currency: String?
    private let amount: Double?

    init(card: PaymentMethodCard, previousMethod: PaymentMethod?, currency: String?, amount: Double?) {
        self.card = card
        self.previousMethod = previousMethod
        self.currency = currency
        self.amount = amount
    }

    var amountString: String {
        guard let price = amount else { return "" }

        if let formattedAmount = price.formatForCurrency(currency) {
            return "\(formattedAmount)"
        }
        return ""
    }

    var billingDetails: Address? {
        previousMethod?.billingDetails?.address
    }

    func createPaymentMethod(with addressLine: String, postcode: String, completion: @escaping (Result<String, Error>) -> Void) {
        let address = STPAddress()
        address.line1 = addressLine
        address.postalCode = postcode

        let billingDetails = STPPaymentMethodBillingDetails()
        let billingAddress = STPPaymentMethodAddress(address: address)
        billingDetails.address = billingAddress

        let cardParams = self.card.stripeCardParams
        let paymentMethodParams = STPPaymentMethodParams(card: cardParams, billingDetails: billingDetails, metadata: nil)

        STPAPIClient.shared.createPaymentMethod(with: paymentMethodParams) { paymentMethod, error in
            if error != nil {
                completion(.failure(NetworkManagerError.internalServerError))
                return
            }

            if paymentMethod != nil {
                completion(.success(paymentMethod!.stripeId))
            }
        }
    }
    
    var isCountryUS: Bool {
        guard let currency = currency else { return false }
        
        return currency.lowercased() == "usd"
    }
}
