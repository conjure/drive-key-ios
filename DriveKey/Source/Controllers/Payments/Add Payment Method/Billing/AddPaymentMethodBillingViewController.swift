//
//  AddPaymentMethodBillingViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 29/09/2021.
//

import UIKit

protocol AddPaymentMethodBillingViewControllerDelegate: AnyObject {
    func addPaymentMethodBillingViewControllerDidTapContinue(vc viewController: AddPaymentMethodBillingViewController, paymentMethodId: String)
}

class AddPaymentMethodBillingViewController: ViewController {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var subheaderLabel: UILabel!
    @IBOutlet private var addressLine1TextField: NamedTextField!
    @IBOutlet private var postcodeTextField: NamedTextField!
    @IBOutlet var continueButton: RoundedButton!
    @IBOutlet private var clearDetailsButton: SmallButton!

    var viewModel: AddPaymentMethodBillingViewModel!
    weak var delegate: AddPaymentMethodBillingViewControllerDelegate?

    private let keyboardView = KeyboardAccessoryView(frame: CGRect.zero)
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?

    private enum PlaceholderKeys {
        static let amount = "{amount}"
    }

    deinit {
        stopObservingKeyboardChanges()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.font = UIFont.K2DSemiBold(24)
        headerLabel.textColor = UIColor.appColor(.navyBlue)
        headerLabel.text = "add.payment.method.billing.header".localized

        subheaderLabel.font = UIFont.K2DSemiBold(16)
        subheaderLabel.textColor = .black
        subheaderLabel.text = "add.payment.method.billing.subheader".localized

        let continueAmountStr = viewModel.amountString
        let continueStr = "add.payment.method.billing.keyboard.continue".localized.replacingOccurrences(of: PlaceholderKeys.amount, with: continueAmountStr)
        let continueTitle = continueStr
        continueButton.setTitle(continueTitle, for: .normal)

        keyboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)

        addressLine1TextField.name = "add.payment.method.billing.address.line.1".localized
        addressLine1TextField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        addressLine1TextField.textField.keyboardType = .asciiCapable
        addressLine1TextField.textField.textContentType = .streetAddressLine1
        addressLine1TextField.textField.autocapitalizationType = .words
        addressLine1TextField.textField.tag = 0
        addressLine1TextField.textField.delegate = self
        addressLine1TextField.textField.returnKeyType = .next

        let postcodeName = viewModel.isCountryUS ? "add.payment.method.billing.postcode.us".localized : "add.payment.method.billing.postcode".localized
        let postcodeError = viewModel.isCountryUS ? "invalid.postcode.error.message.us".localized : "invalid.postcode.error.message".localized
        postcodeTextField.name = postcodeName
        postcodeTextField.errorMessage = postcodeError
        postcodeTextField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        postcodeTextField.validator = PostCodeValidator()
        postcodeTextField.textField.tag = 5
        postcodeTextField.textField.keyboardType = .asciiCapable
        postcodeTextField.textField.textContentType = .postalCode
        postcodeTextField.textField.returnKeyType = .done
        postcodeTextField.textField.autocapitalizationType = .allCharacters
        postcodeTextField.textField.textContentType = .postalCode
        postcodeTextField.textField.delegate = self

        startObservingKeyboardChanges()
        setupHideKeyboardOnTap()

        continueButton.isEnabled = false
        populate()
    }

    private func validate() {
        if postcodeTextField.text?.isEmpty == false {
            postcodeTextField.validate()
        }

        if let addressLine1 = addressLine1TextField.text, !addressLine1.isEmpty,
           let postcode = postcodeTextField.text, !postcode.isEmpty {
            continueButton.isEnabled = postcodeTextField.validate()
        } else {
            continueButton.isEnabled = false
        }
        updateClearButton()
    }

    private func populate() {
        guard let address = viewModel.billingDetails else { return }
        addressLine1TextField.value = address.line1
        postcodeTextField.value = address.postal_code

        clearDetailsButton.setTitle("add.payment.method.billing.clear".localized, for: .normal)
        clearDetailsButton.isHidden = false

        validate()
    }

    private func updateClearButton() {
        guard clearDetailsButton.isHidden == false else { return }

        var shouldEnable = false

        for tf in [addressLine1TextField, postcodeTextField] where tf?.text?.isEmpty == false {
            shouldEnable = true
            break
        }

        if shouldEnable {
            clearDetailsButton.isEnabled = true
            clearDetailsButton.alpha = 1.0
        } else {
            clearDetailsButton.isEnabled = false
            clearDetailsButton.alpha = 0.5
        }
    }

    @objc
    func textFieldDidChange() {
        validate()
    }

    private func createPaymentMethod() {
        guard let addressLine = addressLine1TextField.text, let postcode = postcodeTextField.text else { return }
        self.continueButton.isEnabled = false

        self.viewModel.createPaymentMethod(with: addressLine, postcode: postcode) { result in
            switch result {
            case .success(let paymentMethodId):
                self.delegate?.addPaymentMethodBillingViewControllerDidTapContinue(vc: self, paymentMethodId: paymentMethodId)
            case .failure(let error):
                self.continueButton.isEnabled = true
                self.showAlertError(error: error)
            }
        }
    }

    @IBAction
    private func onContinue() {
        createPaymentMethod()
    }

    @IBAction private func clearDetails(_: Any) {
        for tf in [addressLine1TextField, postcodeTextField] {
            tf?.textField.text = ""
        }
        validate()
    }
}

extension AddPaymentMethodBillingViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }

    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height - keyboardView.bounds.height + 80, right: 0)
        scrollView.contentInset = contentInset
    }

    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}

extension AddPaymentMethodBillingViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        switch nextTag {
        case 1:
            postcodeTextField.textField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }

        return true
    }
}
