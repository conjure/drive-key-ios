//
//  AddPaymentMethodCardViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 30/09/2021.
//

import PassKit
import Stripe
import UIKit

protocol AddPaymentMethodCardViewControllerDelegate: AnyObject {
    func addPaymentMethodCardViewControllerWillEnterBillingDetails(_ vc: AddPaymentMethodCardViewController, card: PaymentMethodCard)
    func addPaymentMethodCardViewControllerWillPayWithApple(_ vc: AddPaymentMethodCardViewController)
}

class AddPaymentMethodCardViewController: ViewController {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var stepLabel: UILabel!
    @IBOutlet private var cardNumberLabel: UILabel!
    @IBOutlet private var cardNumberField: NamedTextField!
    @IBOutlet private var expDateField: NamedTextField!
    @IBOutlet private var cvvField: NamedTextField!
    @IBOutlet private var orLabel: UILabel!
    @IBOutlet private var continueButton: RoundedButton!

    var applePayButton: PKPaymentButton!
    private var textFields = [NamedTextField]()
    private let nextKeyboardView = KeyboardAccessoryView(frame: CGRect.zero)
    private let saveKeyboardView = KeyboardAccessoryView(frame: CGRect.zero)
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?

    var viewModel: AddPaymentMethodCardViewModel!
    weak var delegate: AddPaymentMethodCardViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        stepLabel.text = "payments.add.card.heading".localized
        cardNumberLabel.text = "payments.add.card.subheading".localized

        cardNumberField.name = "payments.add.card.number.label".localized
        cardNumberField.textField.tag = 0
        cardNumberField.textField.keyboardType = .numberPad
        cardNumberField.leftIcon = UIImage(named: "card_icon")
        // TODO: Enable when scanner implemented
        // cardNumberField.rightIcon = UIImage(named: "card_scanner")
        cardNumberField.errorMessage = "payments.add.card.number.error.message".localized
        cardNumberField.formatter = CardNumberTextFormatter()
        cardNumberField.validator = CardNumberValidator()
        cardNumberField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textFields.append(cardNumberField)

        expDateField.name = "payments.add.card.exp.label".localized
        expDateField.textField.tag = 1
        expDateField.textField.placeholder = "payments.add.card.exp.label.placeholder".localized
        expDateField.textField.keyboardType = .numberPad
        expDateField.validator = CardExpiryDateValidator()
        expDateField.errorMessage = "payments.add.card.exp.error.message".localized
        expDateField.formatter = CardExpDateFormatter()
        expDateField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textFields.append(expDateField)

        cvvField.name = "payments.add.card.cvv.label".localized
        cvvField.textField.tag = 2
        cvvField.textField.keyboardType = .numberPad
        cvvField.textField.placeholder = "payments.add.card.cvv.label.placeholder".localized
        cvvField.errorMessage = "payments.add.card.cvv.error.message".localized
        cvvField.formatter = CardCVVFormatter()
        cvvField.validator = CardCVVValidator()
        cvvField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        textFields.append(cvvField)

        if viewModel.doesSupportApplePay {
            orLabel.text = "or".localized
            applePayButton = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .whiteOutline)
            applePayButton.cornerRadius = 8.0
            applePayButton.translatesAutoresizingMaskIntoConstraints = false
            applePayButton.addTarget(self, action: #selector(applePayTapped), for: .touchUpInside)
            view.addSubview(applePayButton)

            NSLayoutConstraint.activate([
                applePayButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 24),
                applePayButton.topAnchor.constraint(equalTo: orLabel.bottomAnchor, constant: 15),
                applePayButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -24),
                applePayButton.heightAnchor.constraint(equalToConstant: 50)
            ])
        } else {
            orLabel.isHidden = true
        }

        // next keyboard view
        nextKeyboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        nextKeyboardView.buttonTitle = "next".localized
        nextKeyboardView.onButton = { [weak self] in
            self?.showNextTextfield()
        }
        cardNumberField.textField.inputAccessoryView = nextKeyboardView
        expDateField.textField.inputAccessoryView = nextKeyboardView

        // save keyboard
        saveKeyboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        saveKeyboardView.buttonTitle = "continue".localized
        saveKeyboardView.onButton = { [weak self] in
            self?.keyboardDidTapSave()
        }
        cvvField.textField.inputAccessoryView = saveKeyboardView

        textFields.forEach {
            $0.textField.delegate = self
        }

        startObservingKeyboardChanges()
        setupHideKeyboardOnTap()

        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.isEnabled = false
    }

    private func validate() {
        if let cardNumber = cardNumberField.text, !cardNumber.isEmpty, let expDate = expDateField.text, !expDate.isEmpty,
            let cvv = cvvField.text?.removeAllSpaces(), cvv.count == CardCVVValidator.NumberLenghtMin || cvv.count == CardCVVValidator.NumberLenghtMax {
            if !expDateField.validate() {
                saveKeyboardView.isDisabled = true
                continueButton.isEnabled = false
                return
            }
            saveKeyboardView.isDisabled = false
            continueButton.isEnabled = true
        } else {
            saveKeyboardView.isDisabled = true
            continueButton.isEnabled = false
        }
    }

    private func showNextTextfield() {
        if cardNumberField.textField.isFirstResponder {
            _ = textFieldShouldReturn(cardNumberField.textField)
        } else if expDateField.textField.isFirstResponder {
            _ = textFieldShouldReturn(expDateField.textField)
        }
    }

    private func keyboardDidTapSave() {
        let validCardNumber = cardNumberField.validate()
        let validExp = expDateField.validate()
        let validCVV = cvvField.validate()

        guard validCardNumber, validExp, validCVV else { return }

        let components = expDateField.text!.components(separatedBy: "/")
        guard components.count == 2, let expMonth = components.first, let expYear = components.last else { return }
        guard let expMonthInt = Int(expMonth), let expYearInt = Int(expYear) else { return }

        let card = PaymentMethodCard(number: cardNumberField.text!,
                                     expMonth: expMonthInt,
                                     expYear: expYearInt,
                                     cvv: cvvField.text!)

        delegate?.addPaymentMethodCardViewControllerWillEnterBillingDetails(self, card: card)
    }

    @objc
    private func textFieldDidChange() {
        validate()
    }

    @objc private func applePayTapped() {
        delegate?.addPaymentMethodCardViewControllerWillPayWithApple(self)
    }

    @IBAction private func willContinue(_: Any) {
        keyboardDidTapSave()
    }
}

extension AddPaymentMethodCardViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }

    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height - saveKeyboardView.bounds.height + 80, right: 0)
        scrollView.contentInset = contentInset
    }

    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}

extension AddPaymentMethodCardViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1

        switch nextTag {
        case 1:
            expDateField.textField.becomeFirstResponder()
        case 2:
            cvvField.textField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }

        return true
    }
}
