//
//  AddPaymentMethodCardViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 30/09/2021.
//

import Foundation
import Stripe

class AddPaymentMethodCardViewModel {
    var doesSupportApplePay: Bool {
        StripeAPI.deviceSupportsApplePay()
    }
}
