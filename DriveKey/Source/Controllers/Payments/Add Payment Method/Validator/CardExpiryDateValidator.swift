//
//  CardExpiryDateValidator.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 30/09/2021.
//

import Foundation

class CardExpiryDateValidator: TextValidatable {
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }

    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text, !text.isEmpty else {
                return false
            }

            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/yy"
            if let enteredDate = dateFormatter.date(from: text), let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate) {
                let now = Date()

                return endOfMonth >= now
            }
            return false
        })
    }
}
