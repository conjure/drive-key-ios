//
//  CardCVVValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 03/11/2022.
//

import Foundation

class CardCVVValidator: TextValidatable {
    static let NumberLenghtMin = 3
    static let NumberLenghtMax = 4
    
    var validationClosure: TextValidationClosure
    
    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }
    
    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text?.removeAllSpaces(), text.isNumeric else {
                return false
            }
            
            let count = text.removeAllSpaces().count
            return count == CardCVVValidator.NumberLenghtMin || count == CardCVVValidator.NumberLenghtMax
        })
    }
}
