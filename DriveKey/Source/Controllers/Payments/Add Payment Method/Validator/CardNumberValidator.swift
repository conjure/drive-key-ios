//
//  CardNumberValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/11/2022.
//

import Foundation

class CardNumberValidator: TextValidatable {
    static let NumberLenghtMin = 15
    static let NumberLenghtMax = 16
    
    var validationClosure: TextValidationClosure
    
    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }
    
    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text?.removeAllSpaces(), text.isNumeric else {
                return false
            }
            
            return text.count == CardNumberValidator.NumberLenghtMin || text.count == CardNumberValidator.NumberLenghtMax
        })
    }
}
