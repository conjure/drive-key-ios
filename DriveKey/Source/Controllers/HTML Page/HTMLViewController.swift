//
//  HTMLViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import UIKit
import WebKit

enum HTMLContentType: String {
    case termsOfUse = "terms-of-use"
    case privacyPolicy = "privacy-policy"
}

class HTMLViewController: ViewController {
    @IBOutlet private var webview: WKWebView!
    var contentType: HTMLContentType!

    var didChange = false // Set true when we have to update navigationBar height in viewLayoutMarginsDidChange()

    override func viewLayoutMarginsDidChange() {
        if didChange {
            // set NavigationBar Height here
            navigationController?.navigationBar.sizeToFit()
            didChange.toggle()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        title = contentType == .termsOfUse ? "profile.other.terms.conditions".localized : "profile.other.privacy.policy".localized

        if navigationController?.viewControllers.count == 1 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon"), style: .plain, target: self, action: #selector(tappedDismiss))
            navigationController?.navigationBar.prefersLargeTitles = true
        }

        // Adding webView content
        webview.navigationDelegate = self
        do {
            guard let filePath = Bundle.main.path(forResource: contentType.rawValue, ofType: "html")
            else {
                // File Error
                print("File reading error")
                return
            }

            let contents = try String(contentsOfFile: filePath, encoding: .utf8)
            let baseUrl = URL(fileURLWithPath: filePath)
            webview.loadHTMLString(contents as String, baseURL: baseUrl)
        } catch {
            print("File HTML error")
        }
    }

    @objc private func tappedDismiss() {
        dismiss(animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension HTMLViewController: WKNavigationDelegate {
    func webView(_: WKWebView, didStartProvisionalNavigation _: WKNavigation!) {
        // webView page starts loading
        didChange = true
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.navigationType {
        case .linkActivated:
            UIApplication.shared.open(navigationAction.request.url!)
            decisionHandler(.cancel)
            return
        default:
            break
        }
        decisionHandler(.allow)
    }
}
