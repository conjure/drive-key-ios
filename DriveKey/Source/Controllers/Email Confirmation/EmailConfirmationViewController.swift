//
//  EmailConfirmationViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import UIKit

enum EmailType {
    case resetPassword
    case emailVerification
}

protocol EmailConfirmationViewControllerDelegate: AnyObject {
    func emailConfirmationViewController(_ vc: EmailConfirmationViewController, emailType: EmailType)
}

class EmailConfirmationViewController: ViewController {
    @IBOutlet private var confirmationLabel: UILabel!
    @IBOutlet private var messageLabel: UILabel!
    @IBOutlet private var confirmationButton: RoundedButton!

    var emailType: EmailType!
    weak var delegate: EmailConfirmationViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        confirmationLabel.text = "reset.password.email.label".localized
        messageLabel.text = "reset.password.message.label".localized
        confirmationButton.setTitle("reset.password.email.button".localized, for: .normal)

        navigationItem.hidesBackButton = true
    }

    @IBAction private func showLogin(_: Any) {
        delegate?.emailConfirmationViewController(self, emailType: emailType)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}
