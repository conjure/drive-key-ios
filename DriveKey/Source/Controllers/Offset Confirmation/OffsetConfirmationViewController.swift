//
//  OffsetConfirmationViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import UIKit

protocol OffsetConfirmationViewControllerDelegate: AnyObject {
    func offsetConfirmationViewControllerDidConfirm(_ vc: OffsetConfirmationViewController)
}

class OffsetConfirmationViewController: ViewController {
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var amountLabel: UILabel!
    @IBOutlet private var confirmButton: RoundedButton!

    var offsetAmount: Int = 0
    weak var delegate: OffsetConfirmationViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        descriptionLabel.text = "confirm.offset".localized
        amountLabel.text = "\(offsetAmount)kg"
        confirmButton.setTitle("ok".localized, for: .normal)

        navigationItem.hidesBackButton = true
    }

    @IBAction private func didConfirm(_ sender: Any) {
        self.delegate?.offsetConfirmationViewControllerDidConfirm(self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
}
