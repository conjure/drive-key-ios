//
//  TripHistoryViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/08/2022.
//

import Foundation
import Combine

class TripHistoryViewModel {
    private let userService: UserProvidable
    private let api: APIProtocol
    private(set) var tripsFetcher: TripHistoryFetcher
    private var cancellables = Set<AnyCancellable>()
    private(set) var selectedFilter: TripFilterType?
    
    @Published var trips = [Trip]()
    @Published var dataLoadError: Error?
    @Published var vehicleChanged = false
    @Published var tripRemoved = false
    @Published var tripChanged: Bool = false
    @Published var reportGenerated: Bool = false
    @Published var error: Error?
    
    init(userService: UserProvidable, api: APIProtocol, tripsFetcher: TripHistoryFetcher, filter: TripFilterType?) {
        self.userService = userService
        self.api = api
        self.tripsFetcher = tripsFetcher
        self.selectedFilter = filter
        
        registerSubscriptions()
    }
    
    private func registerSubscriptions() {
        tripsFetcher.data
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                self?.trips = data
            }
            .store(in: &cancellables)
        
        tripsFetcher.error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.dataLoadError = error
            }
            .store(in: &cancellables)
    }
    
    func fetchTrips() {
        tripsFetcher.fetch()
    }
    
    func changeTripVehicle(_ tripId: String, vehicleId: String) {
        api.changeVehicle(tripId, vehicleId: vehicleId) { result in
            switch result {
            case .success:
                self.vehicleChanged = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func removeTrip(_ tripId: String, reson: String) {
        api.removeTrip(tripId, reason: reson) { result in
            switch result {
            case .success:
                self.tripRemoved = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func changeTrip(_ tripId: String, type: TripType) {
        api.changeTrip(tripId, type: type) { result in
            switch result {
            case .success:
                self.tripChanged = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func generateReport(_ trips: [String]) {
        api.generateExpense(trips) { result in
            switch result {
            case .success:
                self.reportGenerated = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    var vehicles: [VehicleData]? {
        guard let vehicles = userService.vehiclesActive else { return nil }
        
        return vehicles
    }
    
    var primaryVehicle: VehicleData? {
        vehicles?.first(where: { $0.primary == true })
    }
    
    func trip(_ tripId: String) -> Trip? {
        tripsFetcher.trips.first(where: { $0.id == tripId })
    }
    
    func filterData(_ filter: TripFilterType) {
        tripsFetcher.filter(filter)
    }
    
    var email: String? {
        userService.currentUser?.email
    }
    
    var unit: UnitDistance {
        guard let user = userService.currentUser else { return .miles }
        
        return user.unitDistance
    }
}
