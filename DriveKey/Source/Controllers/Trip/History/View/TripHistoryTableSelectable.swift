//
//  TripHistoryTableSelectable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/08/2022.
//

import Foundation

protocol TripHistoryTableSelectable: AnyObject {
    func showSelection(_ show: Bool)
}
