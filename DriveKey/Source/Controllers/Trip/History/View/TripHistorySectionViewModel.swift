//
//  TripHistorySectionViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 15/08/2022.
//

import Foundation

class TripHistorySectionViewModel {
    let month: String?
    let name: String
    let date: Date
    var isSelected = false
    var isSelectable = false
    
    init(_ month: String?, name: String, date: Date) {
        self.month = month
        self.name = name
        self.date = date
    }
    
    var monthYearComponents: DateComponents {
        date.get(.month, .year)
    }
    
    var dayMonthYearComponents: DateComponents {
        date.get(.day, .month, .year)
    }
}

extension TripHistorySectionViewModel: Equatable {
    static func == (lhs: TripHistorySectionViewModel, rhs: TripHistorySectionViewModel) -> Bool {
        return lhs.month == rhs.month && lhs.name == rhs.name && lhs.date == rhs.date
    }
}
extension TripHistorySectionViewModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(month)
        hasher.combine(name)
        hasher.combine(date)
    }
}
