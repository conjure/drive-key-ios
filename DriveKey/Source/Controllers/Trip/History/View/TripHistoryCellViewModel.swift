//
//  TripHistoryCellViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/08/2022.
//

import Foundation
import MapKit

class TripHistoryCellViewModel {
    let tripId: String
    let vehicleId: String
    var score: Double?
    let start: String
    let startDate: Date
    let stop: String
    var distance: String
    var co2: Double
    let co2EstPrice: Double
    let startLocation: CLLocationCoordinate2D
    let endLocation: CLLocationCoordinate2D
    let licensePlate: String?
    let name: String?
    let type: TripType?
    var isSelected = false
    var isSelectable = false
    let estimatedRate: Trip.EstimatedRate?
    
    var duration: TimeInterval?
    var emissions: Double?
    var coordinates: [CLLocationCoordinate2D]? {
        didSet {
            handleLocations()
        }
    }
    
    init(_ tripId: String, vehicleId: String, score: Double?, start: String, startDate: Date, stop: String, distance: String, co2: Double, co2EstPrice: Double, startLocation: CLLocationCoordinate2D, stopLocation: CLLocationCoordinate2D, licensePlate: String?, name: String?, type: TripType?, estimatedRate: Trip.EstimatedRate?) {
        self.tripId = tripId
        self.vehicleId = vehicleId
        self.score = score
        self.start = start
        self.startDate = startDate
        self.stop = stop
        self.distance = distance
        self.co2 = co2
        self.co2EstPrice = co2EstPrice
        self.startLocation = startLocation
        self.endLocation = stopLocation
        self.licensePlate = licensePlate
        self.name = name
        self.type = type
        self.estimatedRate = estimatedRate
    }
    
    private func handleLocations() {
        guard let waypoints = coordinates else { return }
        let distance = Waypoint.distanceInMiles(waypoints)
        
        guard let distanceStr = NumberFormatter.distance.string(from: NSNumber(value: distance)) else { return }
        let distanceUnitStr = !distanceStr.isEmpty ? distanceStr + " " + UnitDistance.miles.localizedShort.capitalizingFirstLetter() : ""
        
        if waypoints.count > 1 {
            self.distance = distanceUnitStr
        }
    }
    
    var monthYearComponents: DateComponents {
        startDate.get(.month, .year)
    }
    
    var dayMonthYearComponents: DateComponents {
        startDate.get(.day, .month, .year)
    }
}

extension TripHistoryCellViewModel: Equatable {
    static func == (lhs: TripHistoryCellViewModel, rhs: TripHistoryCellViewModel) -> Bool {
        return lhs.tripId == rhs.tripId && lhs.startDate == rhs.startDate && lhs.licensePlate == rhs.licensePlate && lhs.name == rhs.name && lhs.isSelected == rhs.isSelected
    }
}

extension TripHistoryCellViewModel: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(tripId)
        hasher.combine(startDate)
        hasher.combine(licensePlate)
        hasher.combine(name)
        hasher.combine(isSelected)
    }
}
