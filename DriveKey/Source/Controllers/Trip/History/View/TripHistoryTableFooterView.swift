//
//  TripHistoryTableFooterView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 17/08/2022.
//

import UIKit

class TripHistoryTableFooterView: BaseView {
    private let infoLabel = Subviews.infoLabel
    private let iconView = Subviews.iconView

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(infoLabel)
        addSubview(iconView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            infoLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            infoLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -10),
            infoLabel.heightAnchor.constraint(equalToConstant: 20),
            iconView.leadingAnchor.constraint(equalTo: infoLabel.trailingAnchor, constant: 4),
            iconView.heightAnchor.constraint(equalToConstant: 16),
            iconView.widthAnchor.constraint(equalToConstant: 16),
            iconView.centerYAnchor.constraint(equalTo: infoLabel.centerYAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var title: String? {
        didSet {
            infoLabel.text = title
        }
    }
}

private struct Subviews {
    static var infoLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "trip.history.table.footer".localized

        return label
    }
    
    static var iconView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "trip-table-footer-icon")
        
        return view
    }
}
