//
//  TripHistoryConfirmView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 24/08/2022.
//

import UIKit

protocol TripHistoryConfirmViewDelegate: AnyObject {
    func tripHistoryConfirmViewDidTapCancel(_ confirmView: TripHistoryConfirmView)
    func tripHistoryConfirmViewDidTapConfirm(_ confirmView: TripHistoryConfirmView)
}

class TripHistoryConfirmView: BaseView {
    private let cancelButton = Subviews.cancelButton
    private let confirmButton = Subviews.confirmButton
    
    weak var delegate: TripHistoryConfirmViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(cancelButton)
        addSubview(confirmButton)
        
        cancelButton.addTarget(self, action: #selector(onCancel), for: .touchUpInside)
        confirmButton.addTarget(self, action: #selector(onConfirm), for: .touchUpInside)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            cancelButton.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            cancelButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            cancelButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            cancelButton.bottomAnchor.constraint(equalTo: confirmButton.topAnchor, constant: -12),
            confirmButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            confirmButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            confirmButton.heightAnchor.constraint(equalToConstant: 50)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc
    private func onCancel() {
        delegate?.tripHistoryConfirmViewDidTapCancel(self)
    }
    
    @objc
    private func onConfirm() {
        delegate?.tripHistoryConfirmViewDidTapConfirm(self)
    }
}

private struct Subviews {
    static var cancelButton: RoundedButton {
        let button = RoundedButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.K2DMedium(16)
        button.setTitleColor(.white, for: .normal)
        button.setTitle("cancel".localized, for: .normal)
        button.backgroundColor = .clear
        
        return button
    }
    
    static var confirmButton: RoundedButton {
        let button = RoundedButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.K2DMedium(16)
        button.setTitleColor(.black, for: .normal)
        button.setTitle("trip.history.button.ok".localized, for: .normal)
        
        return button
    }
}
