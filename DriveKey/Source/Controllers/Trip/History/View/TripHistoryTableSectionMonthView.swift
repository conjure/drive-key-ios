//
//  TripHistoryTableSectionMonthView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/08/2022.
//

import UIKit

class TripHistoryTableSectionMonthView: BaseView {
    private let radioImageView = Subviews.radioImageView
    private let contentView = Subviews.contentView
    private let monthLabel = Subviews.monthLabel
    private let titleLabel = Subviews.titleLabel
    
    private let tapRecognizer = UITapGestureRecognizer()
    private weak var viewModel: TripHistorySectionViewModel?

    private var contentViewLeadingConstraint: NSLayoutConstraint!
    
    var onRadio: ((TripHistorySectionViewModel?) -> Void)?
    
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        
        addSubview(radioImageView)
        addSubview(contentView)
        contentView.addSubview(monthLabel)
        contentView.addSubview(titleLabel)
        
        selectRadio(false)
        
        tapRecognizer.addTarget(self, action: #selector(onTap))
        addGestureRecognizer(tapRecognizer)
    }

    override func setUpLayout() {
        super.setUpLayout()
        
        contentViewLeadingConstraint = contentView.leadingAnchor.constraint(equalTo: leadingAnchor)
        
        let constraints = [
            radioImageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            radioImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            radioImageView.widthAnchor.constraint(equalToConstant: 24),
            radioImageView.heightAnchor.constraint(equalToConstant: 24),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentViewLeadingConstraint!,
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            monthLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            monthLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            monthLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            monthLabel.heightAnchor.constraint(equalToConstant: 20),
            titleLabel.topAnchor.constraint(equalTo: monthLabel.bottomAnchor, constant: 12),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 20)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var data: TripHistorySectionViewModel? {
        didSet {
            guard let data = data else { return }
            viewModel = data
            monthLabel.text = data.month ?? ""
            titleLabel.text = data.name
            showSelection(data.isSelectable)
            selectRadio(data.isSelected)
        }
    }
    
    private func selectRadio(_ selected: Bool) {
        let imageName = selected ? "primary-on-icon" : "trip-selected-off"
        radioImageView.image = UIImage(named: imageName)
    }
    
    @objc
    private func onTap() {
        data?.isSelected.toggle()
        if let data = data {
            selectRadio(data.isSelected)
        }
        onRadio?(data)
    }
}

extension TripHistoryTableSectionMonthView: TripHistoryTableSelectable {
    func showSelection(_ show: Bool) {
        radioImageView.isHidden = !show
        let constant = show ? TripHistoryTableViewCell.Constants.radioExpand : TripHistoryTableViewCell.Constants.radioHidden
        contentViewLeadingConstraint.constant = constant
        setNeedsLayout()
    }
}

private struct Subviews {
    static var radioImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
    
    static var contentView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear

        return view
    }
    
    static var monthLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(16)
        label.textColor = .white.withAlphaComponent(0.5)
        label.textAlignment = .left

        return label
    }
    
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white.withAlphaComponent(0.5)
        label.textAlignment = .left

        return label
    }
}
