//
//  TripHistoryTableSectionView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 27/01/2022.
//

import UIKit

class TripHistoryTableSectionView: BaseView {
    private let contentView = Subviews.contentView
    private let titleLabel = Subviews.titleLabel
    
    private var contentViewLeadingConstraint: NSLayoutConstraint!

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        
        addSubview(contentView)
        contentView.addSubview(titleLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()
        
        contentViewLeadingConstraint = contentView.leadingAnchor.constraint(equalTo: leadingAnchor)
        
        let constraints = [
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentViewLeadingConstraint!,
            contentView.widthAnchor.constraint(equalTo: widthAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var data: TripHistorySectionViewModel? {
        didSet {
            guard let data = data else { return }
            titleLabel.text = data.name
            showSelection(data.isSelectable)
        }
    }
}

extension TripHistoryTableSectionView: TripHistoryTableSelectable {
    func showSelection(_ show: Bool) {
        let constant = show ? TripHistoryTableViewCell.Constants.radioExpand : TripHistoryTableViewCell.Constants.radioHidden
        contentViewLeadingConstraint.constant = constant
        setNeedsLayout()
    }
}

private struct Subviews {
    static var contentView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear

        return view
    }
    
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white.withAlphaComponent(0.5)
        label.textAlignment = .left

        return label
    }
}
