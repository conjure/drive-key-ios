//
//  TripHistoryMapTableViewCell.swift
//  DriveKey
//
//  Created by Piotr Wilk on 27/01/2022.
//

import UIKit
import MapKit

class TripHistoryMapTableViewCell: UITableViewCell {
    private let radioImageView = Subviews.radioImageView
    private let roundedView = Subviews.roundedView
    private let nameLabel = Subviews.nameLabel
    private let estimatedRateView = Subviews.estimatedRateView
    private let mapView = Subviews.mapView
    private let statsView = Subviews.statsView
    private let timeStartLabel = Subviews.timeLabel
    private let timeStopLabel = Subviews.timeLabel
    private let toolbarView = Subviews.toolbarView
        
    private var roundedViewLeadingContraint: NSLayoutConstraint!
    private var tripId: String?
    
    var onButtonRemove: ((String?) -> Void)?
    var onButtonVehicle: ((String?) -> Void)?
    var onButtonBusiness: ((String?) -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setUpCell()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("[TripHistoryMapTableViewCell] init(coder:) has not been implemented")
    }

    private func setUpCell() {
        selectionStyle = .none
        contentView.backgroundColor = .clear
        backgroundColor = .clear
        
        contentView.addSubview(radioImageView)
        contentView.addSubview(roundedView)
        roundedView.addSubview(nameLabel)
        roundedView.addSubview(estimatedRateView)
        roundedView.addSubview(statsView)
        roundedView.addSubview(timeStartLabel)
        roundedView.addSubview(timeStopLabel)
        roundedView.addSubview(mapView)
        roundedView.addSubview(toolbarView)
        
        mapView.style = "map-style.json"
        
        selectRadio(false)
        
        toolbarView.delegate = self

        roundedViewLeadingContraint = roundedView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24)
        
        let constraint = [
            radioImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            radioImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            radioImageView.widthAnchor.constraint(equalToConstant: 24),
            radioImageView.heightAnchor.constraint(equalToConstant: 24),
            roundedView.topAnchor.constraint(equalTo: contentView.topAnchor),
            roundedViewLeadingContraint!,
            roundedView.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -48),
            roundedView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 14),
            nameLabel.heightAnchor.constraint(equalToConstant: 20),
            nameLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 16),
            nameLabel.trailingAnchor.constraint(equalTo: estimatedRateView.leadingAnchor, constant: -4),
            estimatedRateView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            estimatedRateView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -16),
            estimatedRateView.heightAnchor.constraint(equalToConstant: 26),
            estimatedRateView.widthAnchor.constraint(greaterThanOrEqualToConstant: 80),
            mapView.topAnchor.constraint(equalTo: estimatedRateView.bottomAnchor, constant: 12),
            mapView.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 16),
            mapView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -16),
            mapView.heightAnchor.constraint(equalToConstant: 218),
            statsView.topAnchor.constraint(equalTo: mapView.bottomAnchor, constant: 14),
            statsView.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 16),
            statsView.heightAnchor.constraint(equalToConstant: 48),
            statsView.trailingAnchor.constraint(equalTo: timeStartLabel.leadingAnchor, constant: -10),
            timeStartLabel.topAnchor.constraint(equalTo: statsView.topAnchor, constant: 1),
            timeStartLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -16),
            timeStartLabel.heightAnchor.constraint(equalToConstant: 20),
            timeStopLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor, constant: -5),
            timeStopLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -16),
            timeStopLabel.heightAnchor.constraint(equalToConstant: 20),
            toolbarView.topAnchor.constraint(equalTo: statsView.bottomAnchor, constant: 16),
            toolbarView.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor),
            toolbarView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor),
            toolbarView.heightAnchor.constraint(equalToConstant: 40),
            toolbarView.bottomAnchor.constraint(equalTo: roundedView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }

    var data: TripHistoryCellViewModel? {
        didSet {
            guard let data = data else { return }
            tripId = data.tripId
            
            nameLabel.text = data.name
            estimatedRateView.updateView(with: data.co2EstPrice, currency: data.estimatedRate?.currencyCode)
            toolbarView.businessHighlighted = data.type == .business
            toolbarView.isUserInteractionEnabled = !data.isSelectable
            updateTime(data.start, stop: data.stop)
            showSelection(data.isSelectable)
            selectRadio(data.isSelected)
            
            statsView.score = data.score
            statsView.plates = data.licensePlate
            statsView.miles = data.distance
            statsView.co2 = data.co2
            
            if let locations = data.coordinates, locations.count > 1 {
                handleLocations(locations)
            } else {
                mapView.updateLocation([data.startLocation, data.endLocation])
            }
        }
    }
    
    private func handleLocations(_ coordinates: [CLLocationCoordinate2D]) {
        mapView.updateLocation(coordinates)
        let distance = Waypoint.distanceInMiles(coordinates)
        guard let distanceStr = NumberFormatter.distance.string(from: NSNumber(value: distance)) else { return }
        let distanceUnitStr = !distanceStr.isEmpty ? distanceStr + " " + UnitDistance.miles.localizedShort.capitalizingFirstLetter() : ""
        statsView.miles = distanceUnitStr
    }

    private func updateTime(_ start: String, stop: String) {
        timeStartLabel.text = start
        timeStopLabel.text = stop
    }
    
    private func selectRadio(_ selected: Bool) {
        let imageName = selected ? "primary-on-icon" : "trip-selected-off"
        radioImageView.image = UIImage(named: imageName)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectRadio(selected)
    }
    
    private func onRemove() {
        onButtonRemove?(tripId)
    }
    
    private func onVehicle() {
        onButtonVehicle?(tripId)
    }
    
    private func onBusiness() {
        onButtonBusiness?(tripId)
    }
}

extension TripHistoryMapTableViewCell: TripHistoryTableSelectable {
    func showSelection(_ show: Bool) {
        radioImageView.isHidden = !show
        let constant = show ? TripHistoryTableViewCell.Constants.radioExpand : TripHistoryTableViewCell.Constants.radioHidden
        roundedViewLeadingContraint.constant = constant
        contentView.setNeedsLayout()
    }
}

private struct Subviews {
    static var radioImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
    
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor.appColor(.pastelDarkBlue)

        return view
    }
    
    static var estimatedRateView: EstimatedTotalView {
        let view = EstimatedTotalView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(16)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }
    
    static var statsView: TripStatsView {
        let view = TripStatsView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var timeLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .right
        
        return label
    }
    
    static var mapView: TripMapView {
        let view = TripMapView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.isUserInteractionEnabled = false
        
        return view
    }
    
    static var toolbarView: TripToolbarView {
        let view = TripToolbarView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
}

extension TripHistoryMapTableViewCell: TripToolbarViewDelegate {
    func tripToolbarViewDidTapRemove(_ toolbarView: TripToolbarView) {
        onRemove()
    }
    
    func tripToolbarViewDidTapVehicle(_ toolbarView: TripToolbarView) {
        onVehicle()
    }
    
    func tripToolbarViewDidTapBusiness(_ toolbarView: TripToolbarView) {
        onBusiness()
    }
}
