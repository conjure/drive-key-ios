//
//  TripHistoryFetcher.swift
//  DriveKey
//
//  Created by Piotr Wilk on 27/01/2022.
//

import Foundation
import Combine
import FirebaseFirestore

enum TripFilterType: Int {
    case all
    case personal
    case business
}

enum TripHistoryError: Error {
    case noUser
    case noTrips
}

class TripHistoryFetcher {
    private let user: DrivekeyUser?
    private let db = Firestore.firestore()
    private var query: Query?
    private(set) var trips = [Trip]()
    private var lastSnapshot: QueryDocumentSnapshot?
    private(set) var isFetching = false
    private(set) var isAllData = false
    private var pageCount: Int = 0
    private var filter: TripFilterType = .all
    private var isFetchingMonth = false
    private var lastItemComponents: DateComponents?
    
    static let itemsPerPage = 10
    
    private(set) var data = PassthroughSubject<[Trip], Never>()
    private(set) var error = PassthroughSubject<Error, Never>()
    
    init(user: DrivekeyUser?) {
        self.user = user
    }
    
    func fetch() {
        fetch(TripHistoryFetcher.itemsPerPage)
    }
    
    func fetchMonth() {
        lastItemComponents = nil
        isFetchingMonth = true
        isFetching = false
        fetch(TripHistoryFetcher.itemsPerPage)
    }
    
    func fetchNextMonth(_ components: DateComponents) {
        lastItemComponents = components
        isFetchingMonth = true
        isFetching = false
        fetch(TripHistoryFetcher.itemsPerPage)
    }
    
    func filter(_ filter: TripFilterType) {
        guard self.filter != filter else { return }
        self.filter = filter
        reset()
        fetch()
    }
    
    private func fetch(_ itemsCount: Int) {
        guard !isFetching else { return }
        
        guard let user = user else {
            self.error.send(TripHistoryError.noUser)
            return
        }
        isFetching = true
        print("[TripHistoryFetcher] fetch itemsCount: \(itemsCount)")
        
        if lastSnapshot == nil {
            query = db.collection("users").document(user.uid).collection("trips")
                .order(by: "startAt", descending: true)
                .whereField("active", isEqualTo: true)
                .limit(to: itemsCount)
        } else {
            query = db.collection("users").document(user.uid).collection("trips")
                .order(by: "startAt", descending: true)
                .whereField("active", isEqualTo: true)
                .start(afterDocument: lastSnapshot!)
                .limit(to: itemsCount)
        }
        
        if filter != .all, let filterName = filter.filterName {
            query = query?.whereField("tripType", isEqualTo: filterName)
        }
        
        query?.getDocuments(completion: { snapshot, error in
            if let error = error {
                self.isFetching = false
                self.error.send(error)
                return
            }
            
            guard let snapshot = snapshot else {
                self.isFetching = false
                self.error.send(TripHistoryError.noTrips)
                return
            }
            
            guard let lastSnapshot = snapshot.documents.last else {
                self.isAllData = true
                self.isFetching = false
                self.data.send(self.trips)
                return
            }
            self.lastSnapshot = lastSnapshot
            self.pageCount += 1
            
            var result = [Trip]()
            let decoder = Firestore.Decoder()
            for snapshot in snapshot.documents {
                do {
                    let trip = try decoder.decode(Trip.self, from: snapshot.data())
                    result.append(trip)
                } catch let error {
                    self.isFetching = false
                    self.error.send(error)
                }
            }
            
            self.trips.append(contentsOf: result)
            if result.count < itemsCount {
                self.isAllData = true
            }
            print("[TripHistoryFetcher] fetch success result: \(result.count) trips: \(self.trips.count) isAllData: \(self.isAllData) isFetchingMonth: \(self.isFetchingMonth) page: \(self.pageCount)")
            
            if self.isFetchingMonth && !self.isAllData && result.count >= 2, let first = result.first, let last = result.last {
                if let firstComponents = first.monthYearComponents, let lastComponents = last.monthYearComponents {
                    let lastFirstComponents = self.lastItemComponents != nil ? self.lastItemComponents : firstComponents
                    if lastFirstComponents == lastComponents {
                        self.isFetching = false
                        self.fetch(TripHistoryFetcher.itemsPerPage)
                        return
                    }
                    self.lastItemComponents = lastComponents
                }
            }
            
            self.isFetching = false
            self.data.send(self.trips)
        })
    }
    
    private func refetchData() {
        let itemPerPageCount = TripHistoryFetcher.itemsPerPage
        let currentCount = pageCount * itemPerPageCount
        
        pageCount = max(pageCount - 1, 0)
        lastSnapshot = nil
        isAllData = false
        trips.removeAll()
        
        print("[TripHistoryFetcher] refetchData currentCount: \(currentCount)")
        
        fetch(currentCount)
    }
    
    private func reset() {
        pageCount = 0
        lastSnapshot = nil
        isAllData = false
        isFetchingMonth = false
        lastItemComponents = nil
        trips.removeAll()
    }
}

extension TripFilterType {
    var filterName: String? {
        switch self {
        case .all:
             return nil
        case .personal:
            return "PERSONAL"
        case .business:
            return "BUSINESS"
        }
    }
}
