//
//  TripHistoryDataSource.swift
//  DriveKey
//
//  Created by Piotr Wilk on 15/08/2022.
//

import UIKit
import MapKit

protocol TripHistoryDataSourceDelegate: AnyObject {
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didScroll scrollView: UIScrollView)
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect tripId: String)
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect month: DateComponents)
}

typealias TripHistorySnapshot = NSDiffableDataSourceSnapshot<TripHistorySectionViewModel, TripHistoryCellViewModel>

class TripHistoryDataSource: UITableViewDiffableDataSource<TripHistorySectionViewModel, TripHistoryCellViewModel> {
    private weak var tableView: UITableView?
    private(set) var sections = [TripHistorySectionViewModel]()
    private var cells = [TripHistoryCellViewModel]()
    private var trips = [Trip]()
    private let timeFormatter = DateFormatter()
    private let dayMonthFormatter = DateFormatter()
    private let monthYearFormatter = DateFormatter()
    private let monthFormatter = DateFormatter()
    private var isExpanded = false
    private var shouldSelectCurrentMonth = false
    private var sectionToSelect: TripHistorySectionViewModel?
    private let unit: UnitDistance
    var currentVehicle: VehicleData?
    
    weak var delegate: TripHistoryDataSourceDelegate?
    
    init(_ tableView: UITableView, unit: UnitDistance, cellProvider: @escaping UITableViewDiffableDataSource<TripHistorySectionViewModel, TripHistoryCellViewModel>.CellProvider) {
        self.unit = unit
        self.tableView = tableView
        
        super.init(tableView: tableView, cellProvider: cellProvider)
        
        timeFormatter.dateFormat = "HH:mm"
        dayMonthFormatter.dateFormat = "EEEE dd MMMM"
        monthYearFormatter.dateFormat = "MMMM yyyy"
        monthFormatter.dateFormat = "MMMM"
    }
    
    func updateTrips(_ data: [Trip]) {
        print("[TripHistoryDiffDataSource] updateTrips count: \(data.count)")
        let models = createCellModels(data)
        updateSnapshot(models)
        trips = data
    }
    
    private func updateSnapshot(_ data: [TripHistoryCellViewModel], animated: Bool = false) {
        let dataSorted = data.sorted { dataA, dataB in
            dataA.startDate > dataB.startDate
        }
        
        var dates = Set<Date>()
        var itemsByDate = [Date: [TripHistoryCellViewModel]]()
        dataSorted.forEach { data in
            if let dateNoTime = data.startDate.removeTime {
                dates.insert(dateNoTime)
                
                var items = itemsByDate[dateNoTime] ?? [TripHistoryCellViewModel]()
                items.append(data)
                itemsByDate[dateNoTime] = items
            }
        }
        
        let datesSorted = dates.sorted { dateA, dateB in
            dateA > dateB
        }
        
        var newSections = [TripHistorySectionViewModel]()
        var lastDateComponents: DateComponents?
        for date in datesSorted {
            var month: String?
            let dateComponents = date.get(.month, .year)
            if lastDateComponents?.month != dateComponents.month || lastDateComponents?.year != dateComponents.year {
                lastDateComponents = dateComponents
                month = monthYearFormatter.string(from: date)
            }
            
            let name = dayMonthFormatter.string(from: date)
            let sectionData = TripHistorySectionViewModel(month, name: name, date: date)
            sectionData.isSelectable = isExpanded
            newSections.append(sectionData)
            
        }
        
        if isExpanded {
            for section in sections {
                if let index = newSections.firstIndex(of: section) {
                    let newSection = newSections[index]
                    newSection.isSelected = section.isSelected
                }
            }
        }
        sections = newSections
        
        cells.removeAll()
        var snapshot = TripHistorySnapshot()
        snapshot.appendSections(newSections)
        for date in datesSorted {
            if let section = sections.first(where: { $0.date == date }), let sectionCells = itemsByDate[date] {
                snapshot.appendItems(sectionCells, toSection: section)
                cells.append(contentsOf: sectionCells)
            }
        }
        applySnapshotPreservingSelection(snapshot, animated: animated)
        
        if sectionToSelect != nil, let index = sections.firstIndex(of: sectionToSelect!) {
            let sectionSelect = sections[index]
            selectCells(sectionSelect)
        }
    }
    
    private func update(_ cells: [TripHistoryCellViewModel], in section: TripHistorySectionViewModel, animated: Bool = false) {
        var selectedIndexes = [IndexPath]()
        for cellData in cells {
            if let indexPath = indexPath(for: cellData) {
                selectedIndexes.append(indexPath)
            }
        }
        
        var snapshot = snapshot()
        snapshot.reloadItems(cells)
        applySnapshotPreservingSelection(snapshot, animated: false)
        
        if section.isSelected {
            selectedIndexes.forEach({ tableView?.selectRow(at: $0, animated: false, scrollPosition: .none) })
        } else {
            selectedIndexes.forEach({ tableView?.deselectRow(at: $0, animated: false) })
        }
    }
    
    private func update(_ section: TripHistorySectionViewModel, animated: Bool = false) {
        var snapshot = snapshot()
        snapshot.reloadSections([section])
        applySnapshotPreservingSelection(snapshot, animated: animated)
    }
    
    private func applySnapshotPreservingSelection(_ snapshot: TripHistorySnapshot, animated: Bool) {
        let selectedRows = tableView?.indexPathsForSelectedRows
        apply(snapshot, animatingDifferences: animated)
        if let indexes = selectedRows, isExpanded {
            for indexPath in indexes {
                let data = itemIdentifier(for: indexPath)
                data?.isSelected = true
                tableView?.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }
    
    func scrollToTop() {
        if !sections.isEmpty {
            let indexPath = IndexPath(row: 0, section: 0)
            tableView?.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func showSelection(_ show: Bool) {
        if let indexes = tableView?.indexPathsForSelectedRows {
            indexes.forEach({ tableView?.deselectRow(at: $0, animated: false) })
        }
        
        isExpanded = show
        let models = createCellModels(trips)
        updateSnapshot(models)
        
        let todayComponents = Date().get(.month, .year)
        let currentMontSection = sections.filter({ $0.monthYearComponents == todayComponents }).first
        currentMontSection?.isSelected = true
        sectionToSelect = currentMontSection
    }
    
    private func selectCells(_ section: TripHistorySectionViewModel) {
        let sectionCells = cells.filter({ $0.monthYearComponents == section.monthYearComponents })
        sectionCells.forEach({ $0.isSelected = section.isSelected })
        update(sectionCells, in: section)
    }
    
    private func onSectionMonth(_ section: TripHistorySectionViewModel) {
        guard isExpanded else { return }
        
        sectionToSelect = section
        delegate?.tripsHistoryDataSource(self, didSelect: section.monthYearComponents)
        
        selectCells(section)
    }
    
    private func checkMonthSelection(_ cell: TripHistoryCellViewModel) {
        guard let sectionMonth = monthSection(cell) else { return }
        let sectionCells = sectionCells(sectionMonth)
        let cellsDiff = sectionCells.filter({ $0.isSelected != cell.isSelected })
        
        if !cellsDiff.isEmpty && sectionMonth.isSelected {
            sectionMonth.isSelected.toggle()
            update(sectionMonth)
        } else if cellsDiff.isEmpty && sectionMonth.isSelected != cell.isSelected {
            sectionMonth.isSelected.toggle()
            update(sectionMonth)
        }
    }
    
    private func monthSection(_ cell: TripHistoryCellViewModel) -> TripHistorySectionViewModel? {
        sections.filter({ $0.monthYearComponents == cell.monthYearComponents }).first
    }
    
    private func sectionCells(_ section: TripHistorySectionViewModel) -> [TripHistoryCellViewModel] {
        cells.filter({ $0.monthYearComponents == section.monthYearComponents })
    }
    
    private func createCellModels(_ data: [Trip]) -> [TripHistoryCellViewModel] {
        var result = [TripHistoryCellViewModel]()
        
        for i in 0..<data.count {
            let trip = data[i]
            
            if let start = trip.startDate,
               let tripId = trip.id,
               let vehicleId = trip.vehicleId,
               let end = trip.endDate,
               let score = trip.ecoDriverScore,
               let distance = trip.distance(unit),
               let co2 = trip.co2Value,
               let co2Price = trip.co2EstPrice,
               let locationStart = trip.startCoordinate,
               let locationEnd = trip.endCoordinate {
                let startStr = timeFormatter.string(from: start)
                let endStr = timeFormatter.string(from: end)
                let distanceStr = NumberFormatter.distance.string(from: NSNumber(value: distance)) ?? ""
                
                let distanceUnitStr = !distanceStr.isEmpty ? distanceStr + " " + unit.localizedShort.capitalizingFirstLetter() : ""
                let cellData = TripHistoryCellViewModel(tripId,
                                                        vehicleId: vehicleId,
                                                        score: score,
                                                        start: startStr,
                                                        startDate: start,
                                                        stop: endStr,
                                                        distance: distanceUnitStr,
                                                        co2: co2, co2EstPrice: co2Price,
                                                        startLocation: locationStart,
                                                        stopLocation: locationEnd,
                                                        licensePlate: trip.licensePlateValue,
                                                        name: trip.name,
                                                        type: trip.tripType,
                                                        estimatedRate: trip.estimatedRate)
                cellData.isSelectable = isExpanded
                cellData.coordinates = trip.coordinates
                result.append(cellData)
            }
        }
        
        if isExpanded {
            for cell in cells {
                if let index = result.firstIndex(of: cell) {
                    let newCell = result[index]
                    newCell.isSelected = cell.isSelected
                }
            }
        }
        
        return result
    }
    
    var report: Report? {
        let selectedRows = tableView?.indexPathsForSelectedRows
        var selectedItems = [TripHistoryCellViewModel]()
        selectedRows?.forEach({ indexPath in
            if let item = itemIdentifier(for: indexPath) {
                selectedItems.append(item)
            }
        })
        
        if selectedItems.isEmpty {
            return nil
        }
        
        let monthSections = sections.filter({ $0.month != nil })
        let selectedMonthIndexes = monthSections
            .enumerated()
            .filter({ $0.element.month != nil && $0.element.isSelected == true })
            .map({ $0.0 })
        
        var monthCaption = ""
        if selectedMonthIndexes.count > 1 {
            var itemsCount = 0
            selectedMonthIndexes.forEach({
                itemsCount += sectionCells(monthSections[$0]).count
            })
            
            // https://stackoverflow.com/a/45471134
            if itemsCount == selectedItems.count, (selectedMonthIndexes.map({ $0 - 1 }).dropFirst() == selectedMonthIndexes.dropLast()) {
                if let endDateIndex = selectedMonthIndexes.last {
                    let startDateIndex = selectedMonthIndexes[0]
                    let startDate = monthSections[startDateIndex].date
                    let endDate = monthSections[endDateIndex].date
                    monthCaption += monthFormatter.string(from: startDate)
                    monthCaption += " " + "tray.report.info.to".localized + " "
                    monthCaption += monthYearFormatter.string(from: endDate)
                }
            }
        }
        
        if selectedMonthIndexes.count == 1 {
            let section = monthSections[selectedMonthIndexes.first!]
            let monthCells = sectionCells(section)
            
            if monthCells.count == selectedItems.count {
                let date = monthSections[selectedMonthIndexes.first!].date
                monthCaption += monthYearFormatter.string(from: date)
            }
        }
        
        let currencyCode = selectedItems.first(where: { $0.estimatedRate?.currencyCode != nil })?.estimatedRate?.currencyCode
        let potential = selectedItems.reduce(0) { $0 + ($1.estimatedRate?.total ?? 0)}
        let potentialStr = potential.formatForCurrency(currencyCode) ?? ""
        let co2 = selectedItems.reduce(0) { $0 + $1.co2 }
        let co2Str = "\(co2.formattedCO2)" + "tray.report.tile.co2.kg".localized
        let distanceStr = " " + unit.localizedShort.capitalizingFirstLetter()
        let distance = selectedItems.reduce(0) { $0 + (Double($1.distance.replacingOccurrences(of: distanceStr, with: "")) ?? 0) }
        
        let result = Report(month: monthCaption,
                            potential: potentialStr,
                            co2: co2Str,
                            trips: selectedItems.count,
                            distance: distance,
                            unit: unit)
        
        return result
    }
    
    var selectedTrips: [String]? {
        let selectedRows = tableView?.indexPathsForSelectedRows
        var selectedItems = [TripHistoryCellViewModel]()
        selectedRows?.forEach({ indexPath in
            if let item = itemIdentifier(for: indexPath) {
                selectedItems.append(item)
            }
        })
        let result = selectedItems.map({ $0.tripId })
        
        return result.isEmpty ? nil : result
    }
}

extension TripHistoryDataSource: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && indexPath.section == 0 {
            return 390
        }
        
        return 162
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let data = sections[section]
        
        if data.month != nil {
            let sectionView = TripHistoryTableSectionMonthView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 60))
            sectionView.data = data
            sectionView.onRadio = { [weak self] data in
                guard let data = data else { return }
                self?.onSectionMonth(data)
            }
            return sectionView
        }
        
        let sectionView = TripHistoryTableSectionView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        sectionView.data = data
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let data = sections[section]
        return data.month != nil ? 64 : 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let data = itemIdentifier(for: indexPath) else { return }
        data.isSelected = true
        checkMonthSelection(data)
        
        guard !isExpanded else { return }
        delegate?.tripsHistoryDataSource(self, didSelect: data.tripId)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let data = itemIdentifier(for: indexPath) else { return }
        data.isSelected = false
        checkMonthSelection(data)
    }
}

extension TripHistoryDataSource: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.tripsHistoryDataSource(self, didScroll: scrollView)
    }
}
