//
//  TripHistoryViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/01/2022.
//

import UIKit
import Combine

protocol TripHistoryViewControllerDelegate: AnyObject {
    func tripHistoryViewControllerDidFailLoading(_ viewController: TripHistoryViewController)
    func tripHistoryViewControllerDidTapAddVehicle(_ viewController: TripHistoryViewController)
    func tripHistoryViewController(_ viewController: TripHistoryViewController, didSelect trip: Trip)
}

class TripHistoryViewController: ViewController, TrayPresentable {
    @IBOutlet weak var filterControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmView: TripHistoryConfirmView!
    private var reportButton: UIButton!
    
    @IBOutlet private var tableViewConfirmConstraint: NSLayoutConstraint!
    @IBOutlet private var tableViewBottomConstraint: NSLayoutConstraint!
    
    var viewModel: TripHistoryViewModel!
    weak var delegate: TripHistoryViewControllerDelegate?
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // settings
        view.backgroundColor = UIColor.appColor(.navyBlue)
        title = "trip.history.title".localized
        
        reportButton = UIButton(type: .custom)
        reportButton.addTarget(self, action: #selector(onReport), for: .touchUpInside)
        reportButton.setImage(UIImage(named: "trip-report-icon"), for: .normal)
        reportButton.setImage(UIImage(named: "trip-report-icon-selected"), for: .selected)
        let reportItem = UIBarButtonItem(customView: reportButton)
        navigationItem.leftBarButtonItem = reportItem
        
        let closeButton = UIBarButtonItem(image: UIImage(named: "close-arrow-icon"),
                                          style: .plain,
                                          target: self,
                                          action: #selector(onClose))
        navigationItem.rightBarButtonItem = closeButton
        reportButton.alpha = 0
        
        filterControl.layer.cornerRadius = 22
        filterControl.backgroundColor = UIColor.appColor(.navyBlue)
        filterControl.selectedSegmentTintColor = .appColor(.notificationBlue)
        let textAttributes = [
            NSAttributedString.Key.font: UIFont.K2DBold(16)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        filterControl.setTitleTextAttributes(textAttributes, for: .normal)
        filterControl.setTitleTextAttributes(textAttributes, for: .selected)
        filterControl.removeAllSegments()
        filterControl.insertSegment(withTitle: "trip.history.filter.all".localized, at: TripFilterType.all.rawValue, animated: false)
        filterControl.insertSegment(withTitle: "trip.history.filter.personal".localized, at: TripFilterType.personal.rawValue, animated: false)
        filterControl.insertSegment(withTitle: "trip.history.filter.business".localized, at: TripFilterType.business.rawValue, animated: false)
        let selectedFilter = viewModel.selectedFilter != nil ? viewModel.selectedFilter!: .all
        filterControl.selectedSegmentIndex = selectedFilter.rawValue

        // table view
        tableView.allowsMultipleSelection = true
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.showsVerticalScrollIndicator = false
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.register(TripHistoryTableViewCell.self, forCellReuseIdentifier: TripHistoryTableViewCell.className)
        tableView.register(TripHistoryMapTableViewCell.self, forCellReuseIdentifier: TripHistoryMapTableViewCell.className)
        
        let footerView = TripHistoryTableFooterView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 50))
        tableView.tableFooterView = footerView
        
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        dataSource.currentVehicle = viewModel.primaryVehicle
        dataSource.delegate = self
        
        confirmView.delegate = self
        confirmView.alpha = 0
        
        // data
        registerSubscriptions()
        viewModel.fetchTrips()
        
        if selectedFilter != .all {
            runOnMainThreadAsyncAfter(0.3) {
                self.onFilter()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (navigationController as? NavigationController)?.setDefaultTheme()
        logDetectionStatus()
    }
    
    lazy var dataSource: TripHistoryDataSource = {
        let result = TripHistoryDataSource(tableView, unit: viewModel.unit, cellProvider: { [weak self] (tableView, indexPath, model) -> UITableViewCell? in
            if indexPath.section == 0 && indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: TripHistoryMapTableViewCell.className, for: indexPath) as! TripHistoryMapTableViewCell
                cell.data = model
                cell.onButtonRemove = { [weak self] tripId in
                    self?.handleOnButtonRemove(tripId)
                }
                cell.onButtonVehicle = { [weak self] tripId in
                    self?.handleOnButtonVehicle(tripId)
                }
                cell.onButtonBusiness = { [weak self] tripId in
                    self?.handleOnButtonBusiness(tripId)
                }
                
                return cell
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: TripHistoryTableViewCell.className, for: indexPath) as! TripHistoryTableViewCell
            cell.data = model
            cell.onButtonRemove = { [weak self] tripId in
                self?.handleOnButtonRemove(tripId)
            }
            cell.onButtonVehicle = { [weak self] tripId in
                self?.handleOnButtonVehicle(tripId)
            }
            cell.onButtonBusiness = { [weak self] tripId in
                self?.handleOnButtonBusiness(tripId)
            }
            
            return cell
        })
        
        return result
    }()
    
    private func registerSubscriptions() {
        viewModel.$trips
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                self?.hideSpinner()
                self?.dataSource.updateTrips(data)
            }
            .store(in: &cancellables)
        
        viewModel.$dataLoadError
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    guard let self = self else { return }
                    self.showAlertError("trip.history.error.loading".localized, andCheckConnection: error!) { _ in
                        self.delegate?.tripHistoryViewControllerDidFailLoading(self)
                    }
                }
            }
            .store(in: &cancellables)
        
        viewModel.$vehicleChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
            }
            .store(in: &cancellables)
        
        viewModel.$tripRemoved
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.showRemoveTripConfirmation()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$tripChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.hideSpinner()
            }
            .store(in: &cancellables)
        
        viewModel.$reportGenerated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.showReportConfirmation()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &cancellables)
    }
    
    private func performChangeVehicle(_ tripId: String, vehicleId: String?) {
        guard let vehicleId = vehicleId else { return }
        
        showSpinner()
        viewModel.changeTripVehicle(tripId, vehicleId: vehicleId)
    }
    
    private func performRemoveTrip(_ tripId: String, reason: String) {
        showSpinner()
        viewModel.removeTrip(tripId, reson: reason)
    }
    
    private func performChangeTripType(_ tripId: String, type: TripType) {
        showSpinner()
        viewModel.changeTrip(tripId, type: type)
    }
    
    private func performGenerateReport(_ trips: [String]) {
        showSpinner()
        viewModel.generateReport(trips)
    }
    
    private func showPrimaryChange(_ tripId: String, vehicleId: String?) {
        guard let vehicles = viewModel.vehicles else { return }
        
        var tripVehicle: VehicleData?
        if vehicleId != nil {
            tripVehicle = vehicles.first(where: { $0.vehicleId == vehicleId! })
        }
        
        showTrayPrimaryChange(tripVehicle, vehicles: vehicles) { vehicle in
            self.performChangeVehicle(tripId, vehicleId: vehicle.vehicleId)
        } addVehicle: {
            self.delegate?.tripHistoryViewControllerDidTapAddVehicle(self)
        }
    }
    
    private func showRemoveTrip(_ tripId: String) {
        showTrayRemoveTrip { reason in
            self.performRemoveTrip(tripId, reason: reason)
        }
    }
    
    private func showRemoveTripConfirmation() {
        showTrayRemoveTripConfirmation(nil)
    }
    
    private func showReport() {
        guard let report = dataSource.report else { return }
        showTrayReport(report) {
            guard let trips = self.dataSource.selectedTrips else { return }
            self.performGenerateReport(trips)
        }
    }
    
    private func showReportConfirmation() {
        showTrayReportConfirmation(viewModel.email, completion: nil)
    }
    
    func handleOnButtonRemove(_ tripId: String?) {
        guard let tripId = tripId else { return }
        showRemoveTrip(tripId)
    }
    
    func handleOnButtonVehicle(_ tripId: String?) {
        guard let tripId = tripId, let trip = viewModel.trip(tripId), let vehicleId = trip.vehicleId else { return }
        showPrimaryChange(tripId, vehicleId: vehicleId)
    }
    
    func handleOnButtonBusiness(_ tripId: String?) {
        guard let tripId = tripId, let trip = viewModel.trip(tripId) else { return }
        let newType: TripType = trip.tripType == .personal ? .business : .personal
        performChangeTripType(tripId, type: newType)
    }
    
    private func cancelReport() {
        if reportButton.isSelected {
            reportButton.isSelected.toggle()
            dataSource.showSelection(reportButton.isSelected)
        }
    }
    
    private func showConfirmView(_ show: Bool) {
        let alpha: CGFloat = show ? 1 : 0
        UIView.animate(withDuration: 0.25) {
            self.confirmView.alpha = alpha
        }
        
        if show {
            tableViewBottomConstraint.isActive = false
            tableViewConfirmConstraint.isActive = true
        } else {
            tableViewConfirmConstraint.isActive = false
            tableViewBottomConstraint.isActive = true
        }
        view.setNeedsLayout()
    }
    
    private func showReportView(_ show: Bool) {
        let alpha: CGFloat = show ? 1 : 0
        UIView.animate(withDuration: 0.25) {
            self.reportButton.alpha = alpha
        }
    }

    @objc
    func onReport() {
        reportButton.isSelected.toggle()
        dataSource.showSelection(reportButton.isSelected)
        viewModel.tripsFetcher.fetchMonth()
        showConfirmView(reportButton.isSelected)
    }
    
    @objc
    func onClose() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction
    func onFilter() {
        cancelReport()
        
        if let filter = TripFilterType(rawValue: filterControl.selectedSegmentIndex) {
            let isBusiness = filter == .business
            showReportView(isBusiness)
            if !isBusiness {
                showConfirmView(false)
            }
            viewModel.filterData(filter)
            dataSource.scrollToTop()
        }
    }
    
    private func logDetectionStatus() {
        let sceneDelegate = UIApplication.appWindow?.windowScene?.delegate as? SceneDelegate
        sceneDelegate?.logDetectionStatus()
    }
}

extension TripHistoryViewController: TripHistoryDataSourceDelegate {
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didScroll scrollView: UIScrollView) {
        let bottomY = scrollView.contentOffset.y + scrollView.frame.size.height
        if !viewModel.tripsFetcher.isAllData, !viewModel.tripsFetcher.isFetching {
            if bottomY >= scrollView.contentSize.height {
                viewModel.fetchTrips()
            }
        }
    }
    
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect tripId: String) {
        guard let trip = viewModel.trip(tripId) else { return }
        delegate?.tripHistoryViewController(self, didSelect: trip)
    }
    
    func tripsHistoryDataSource(_ dataSource: TripHistoryDataSource, didSelect month: DateComponents) {
        viewModel.tripsFetcher.fetchNextMonth(month)
    }
}

extension TripHistoryViewController: TripHistoryConfirmViewDelegate {
    func tripHistoryConfirmViewDidTapCancel(_ confirmView: TripHistoryConfirmView) {
        showConfirmView(false)
        cancelReport()
    }
    
    func tripHistoryConfirmViewDidTapConfirm(_ confirmView: TripHistoryConfirmView) {
        showReport()
    }
}
