//
//  TripDetailsLegendView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2022.
//

import UIKit

class TripDetailsLegendView: BaseView {
    private let startImageView = Subviews.imageView
    private let startLabel = Subviews.infoLabel
    private let separatorImageView = Subviews.imageView
    private let endImageView = Subviews.imageView
    private let endLabel = Subviews.infoLabel
    private let timeStartLabel = Subviews.timeLabel
    private let timeEndLabel = Subviews.timeLabel
    private let timeFormatter = DateFormatter()
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(separatorImageView)
        addSubview(startImageView)
        addSubview(startLabel)
        addSubview(timeStartLabel)
        addSubview(endImageView)
        addSubview(endLabel)
        addSubview(timeEndLabel)
        
        startImageView.image = UIImage(named: "map-start-icon")
        separatorImageView.image = UIImage(named: "trip-legend-separator")
        endImageView.image = UIImage(named: "map-end-icon")
        
        startLabel.text = "trip.details.label.start".localized
        endLabel.text = "trip.details.label.destination".localized
        
        timeFormatter.dateFormat = "HH:mm"
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            startImageView.topAnchor.constraint(equalTo: topAnchor, constant: 2),
            startImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            startImageView.widthAnchor.constraint(equalToConstant: 11),
            startImageView.heightAnchor.constraint(equalToConstant: 11),
            startLabel.centerYAnchor.constraint(equalTo: startImageView.centerYAnchor),
            startLabel.leadingAnchor.constraint(equalTo: startImageView.trailingAnchor, constant: 8),
            startLabel.heightAnchor.constraint(equalToConstant: 18),
            startLabel.trailingAnchor.constraint(equalTo: timeStartLabel.leadingAnchor, constant: -10),
            timeStartLabel.centerYAnchor.constraint(equalTo: startImageView.centerYAnchor),
            timeStartLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            timeStartLabel.heightAnchor.constraint(equalToConstant: 18),
            
            separatorImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            separatorImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5),
            separatorImageView.widthAnchor.constraint(equalToConstant: 1),
            separatorImageView.heightAnchor.constraint(equalToConstant: 28),
            
            endImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -2),
            endImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            endImageView.widthAnchor.constraint(equalToConstant: 11),
            endImageView.heightAnchor.constraint(equalToConstant: 11),
            endLabel.centerYAnchor.constraint(equalTo: endImageView.centerYAnchor),
            endLabel.leadingAnchor.constraint(equalTo: endImageView.trailingAnchor, constant: 8),
            endLabel.heightAnchor.constraint(equalToConstant: 18),
            endLabel.trailingAnchor.constraint(equalTo: timeEndLabel.leadingAnchor, constant: -10),
            timeEndLabel.centerYAnchor.constraint(equalTo: endImageView.centerYAnchor),
            timeEndLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            timeEndLabel.heightAnchor.constraint(equalToConstant: 18)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: Trip? {
        didSet {
            guard let start = data?.startDate, let end = data?.endDate else { return }
            timeStartLabel.text = timeFormatter.string(from: start)
            timeEndLabel.text = timeFormatter.string(from: end)
        }
    }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit

        return view
    }
    
    static var infoLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        
        return label
    }
    
    static var timeLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .right
        
        return label
    }
}
