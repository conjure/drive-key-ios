//
//  TripDetailsMapView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2022.
//

import UIKit
import MapKit
import GoogleMaps
import Polyline

class TripDetailsMapView: BaseView {
    private let mapView = Subviews.mapView
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(mapView)
        
        mapView.style = "map-style.json"
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: Trip? {
        didSet {
            guard let data = data else { return }
            if data.coordinates.count > 1 {
                mapView.updateLocation(data.coordinates)
            } else if let start = data.startCoordinate, let end = data.endCoordinate {
                mapView.updateLocation([start, end])
            }
        }
    }
}

private struct Subviews {
    static var mapView: TripMapView {
        let view = TripMapView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.isUserInteractionEnabled = false
        
        return view
    }
}
