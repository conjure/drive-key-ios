//
//  TripDetailsBusinessView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 14/09/2022.
//

import UIKit

protocol TripDetailsBusinessViewDelegate: AnyObject {
    func tripDetailsBusinessViewDidTapReason(_ businessView: TripDetailsBusinessView)
    func tripDetailsBusinessViewDidTapEdit(_ businessView: TripDetailsBusinessView)
}

class TripDetailsBusinessView: BaseView {
    private let businessLabel = Subviews.businessLabel
    private let editButton = Subviews.editButton
    private let reasonField = Subviews.reasonField
    private let priceField = Subviews.priceField
    private let notesField = Subviews.notesField
    
    weak var delegate: TripDetailsBusinessViewDelegate?
    
    private enum PlaceholderKeys {
        static let unit = "{unit}"
    }
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(businessLabel)
        addSubview(editButton)
        addSubview(reasonField)
        addSubview(priceField)
        addSubview(notesField)
        
        editButton.addTarget(self, action: #selector(onEdit), for: .touchUpInside)
        
        reasonField.onRightButton = { [weak self] in
            self?.onReason()
        }
        
        notesField.textField.delegate = self
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            businessLabel.topAnchor.constraint(equalTo: topAnchor),
            businessLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            businessLabel.heightAnchor.constraint(equalToConstant: 32),
            businessLabel.widthAnchor.constraint(equalToConstant: 121),
            editButton.centerYAnchor.constraint(equalTo: businessLabel.centerYAnchor),
            editButton.trailingAnchor.constraint(equalTo: trailingAnchor),
            editButton.heightAnchor.constraint(equalToConstant: 80),
            reasonField.topAnchor.constraint(equalTo: businessLabel.bottomAnchor, constant: 24),
            reasonField.leadingAnchor.constraint(equalTo: leadingAnchor),
            reasonField.trailingAnchor.constraint(equalTo: trailingAnchor),
            reasonField.heightAnchor.constraint(equalToConstant: 80),
            priceField.topAnchor.constraint(equalTo: reasonField.bottomAnchor, constant: -8),
            priceField.leadingAnchor.constraint(equalTo: leadingAnchor),
            priceField.trailingAnchor.constraint(equalTo: trailingAnchor),
            priceField.heightAnchor.constraint(equalToConstant: 80),
            notesField.topAnchor.constraint(equalTo: priceField.bottomAnchor, constant: -8),
            notesField.leadingAnchor.constraint(equalTo: leadingAnchor),
            notesField.trailingAnchor.constraint(equalTo: trailingAnchor),
            notesField.heightAnchor.constraint(equalToConstant: 80)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var currencySymbol: String? {
        didSet {
            guard let currency = currencySymbol else { return }
            priceField.formatter = PricePerMileFormatter(currency)
        }
    }
    
    var reason: String? {
        didSet {
            guard let reason = reason else { return }
            reasonField.textField.text = reason
        }
    }
    
    var notes: String? {
        get {
            notesField.text
        }
        set {
            notesField.textField.text = newValue
        }
    }
    
    var pricePerMile: String? {
        didSet {
            priceField.value = pricePerMile
        }
    }
    
    var unit: UnitDistance? {
        didSet {
            guard let unit = unit else { return }
            let priceStr = "trip.details.field.price.name".localized
            priceField.name = priceStr.replacingOccurrences(of: PlaceholderKeys.unit, with: unit.localized)
        }
    }
    
    var pricePerMileNumber: Double? {
        let formatter = priceField.formatter as? PricePerMileFormatter
        let currencySymbol = formatter != nil ? formatter!.currencySymbol : ""
        let rateNoSymbol = priceField.text?.replacingOccurrences(of: currencySymbol, with: "")
        let rateDouble: Double? = rateNoSymbol != nil ?  Double(rateNoSymbol!) : nil
        
        return rateDouble
    }
    
    func enableEditing(_ enable: Bool) {
        let editText = enable ? "save".localized : "edit".localized
        editButton.setTitle(editText, for: .normal)
        priceField.isEnabled = enable
        notesField.isEnabled = enable
        reasonField.isEnabled = enable
        let highlightColor: UIColor = enable ? .appColor(.notificationBlue) : .appColor(.navyBlue)
        reasonField.updateHighlight(highlightColor)
    }
    
    @objc
    func onEdit() {
        delegate?.tripDetailsBusinessViewDidTapEdit(self)
    }
    
    private func onReason() {
        delegate?.tripDetailsBusinessViewDidTapReason(self)
    }
}

private struct Subviews {
    static var businessLabel: TripDetailsBusinessLabel {
        let view = TripDetailsBusinessLabel(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var editButton: SmallButton {
        let button = SmallButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("edit".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .highlighted)
        button.setTitleColor(UIColor.white.withAlphaComponent(0.5), for: .selected)
        button.titleLabel?.font = UIFont.K2DMedium(16)
        button.contentHorizontalAlignment = .right
        
        return button
    }
    
    static var reasonField: NamedPickerField {
        let field = NamedPickerField(frame: .zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        field.name = "trip.details.field.reason.name".localized
        field.textField.placeholder = "trip.details.field.reason.placeholder".localized
        field.highlightColor = .appColor(.notificationBlue)
        field.nameColor = .white.withAlphaComponent(0.7)
        field.textColor = .white
        field.placeholderColor = .white.withAlphaComponent(0.5)
        field.separatorColor = .white
        field.addRightButton(UIImage(named: "picker-arrow-down-white")!, size: CGSize(width: 24, height: 24))

        return field
    }
    
    static var priceField: NamedTextField {
        let field = NamedTextField(frame: .zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        field.theme = NamedFieldTheme.light
        field.textField.keyboardType = .decimalPad
        
        return field
    }
    
    static var notesField: NamedTextField {
        let field = NamedTextField(frame: .zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        field.theme = NamedFieldTheme.light
        field.name = "trip.details.field.note.name".localized
        
        return field
    }
}

extension TripDetailsBusinessView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
}
