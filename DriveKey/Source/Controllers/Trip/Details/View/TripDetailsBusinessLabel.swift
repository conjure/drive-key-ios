//
//  TripDetailsBusinessLabel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 14/09/2022.
//

import UIKit

class TripDetailsBusinessLabel: BaseView {
    private let iconImageView = Subviews.iconImageView
    private let textLabel = Subviews.textLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .appColor(.chartBlue)
        layer.cornerRadius = 12
        
        addSubview(iconImageView)
        addSubview(textLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            iconImageView.heightAnchor.constraint(equalToConstant: 24),
            iconImageView.widthAnchor.constraint(equalToConstant: 24),
            textLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            textLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8),
            textLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            textLabel.heightAnchor.constraint(equalToConstant: 20)
        ]
        NSLayoutConstraint.activate(constraint)
    }
}

private struct Subviews {
    static var iconImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "trip-suitcase-black-icon")
        
        return view
    }
    
    static var textLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .K2DBold(16)
        label.textColor = .appColor(.navyBlue)
        label.text = "trip.details.label.business".localized
        
        return label
    }
}
