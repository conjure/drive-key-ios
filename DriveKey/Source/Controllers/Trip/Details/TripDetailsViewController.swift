//
//  TripDetailsViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2022.
//

import UIKit
import Combine

protocol TripDetailsViewControllerDelegate: AnyObject {
    func tripDetailsViewControllerDidTapAddVehicle(_ viewController: TripDetailsViewController)
    func tripDetailsViewControllerDidRemoveTrip(_ viewController: TripDetailsViewController)
}

class TripDetailsViewController: ViewController, PickerPresentable, TrayPresentable {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var statsView: TripStatsView!
    @IBOutlet private var estimatedRateView: EstimatedTotalView!
    @IBOutlet private var mapView: TripDetailsMapView!
    @IBOutlet private var legendView: TripDetailsLegendView!
    @IBOutlet private var businessView: TripDetailsBusinessView!
    @IBOutlet private var legendViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var businessViewBottomConstraint: NSLayoutConstraint!
    
    var viewModel: TripDetailsViewModel!
    weak var delegate: TripDetailsViewControllerDelegate?
    private var cancellables = Set<AnyCancellable>()
    private var isEditMode = false
    
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?
    
    deinit {
        viewModel.removeListeners()
        stopObservingKeyboardChanges()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.appColor(.navyBlue)
        title = viewModel.title
        scrollView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        let item = UIBarButtonItem(image: UIImage(named: "trip-three-dots-icon"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(onMore))
        navigationItem.rightBarButtonItem = item
        
        statsView.update(viewModel.trip, unit: viewModel.unit)
        statsView.scoreBackgroundColor = .appColor(.notificationBlue)
        statsView.score = viewModel.score
        mapView.data = viewModel.trip
        legendView.data = viewModel.trip
        estimatedRateView.updateView(with: viewModel.trip.co2EstPrice, currency: viewModel.trip.estimatedRate?.currencyCode)
        businessView.isHidden = true
        businessView.unit = viewModel.unit
        
        if viewModel.isBusiness {
            showBusinessData()
            updateBusinessData()
            businessView.enableEditing(false)
            businessView.delegate = self
            
            startObservingKeyboardChanges()
            setupHideKeyboardOnTap()
        }
        
        registerSubscriptions()
        viewModel.listenForDataUpdates()
    }
    
    private func registerSubscriptions() {
        viewModel.$tripUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                if success {
                    self?.updateBusinessData()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$businessDataUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.enableEditing(false)
                }
            }
            .store(in: &cancellables)
        
        viewModel.$vehicleChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
            }
            .store(in: &cancellables)
        
        viewModel.$tripRemoved
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.showRemoveTripConfirmation()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &cancellables)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (navigationController as? NavigationController)?.setDefaultTheme()
    }
    
    private func showBusinessData() {
        businessView.isHidden = false
        legendViewBottomConstraint.isActive = false
        businessViewBottomConstraint.isActive = true
        view.setNeedsLayout()
    }
    
    private func updateBusinessData() {
        statsView.update(viewModel.trip, unit: viewModel.unit)
        businessView.currencySymbol = viewModel.currencySymbol
        businessView.reason = viewModel.reason
        businessView.pricePerMile = viewModel.pricePerMile
        businessView.notes = viewModel.notes
    }
    
    private func performSaveData() {
        guard let rate = businessView.pricePerMileNumber else { return }
        guard let reason = viewModel.reason else { return }
        guard let notes = businessView.notes else { return }
        
        showSpinner()
        view.endEditing(true)
        viewModel.update(rate, notes: notes, reason: reason)
    }
    
    private func performRemoveTrip(_ reason: String) {
        showSpinner()
        viewModel.removeTrip(reason)
    }
    
    private func performChangeVehicle(_ vehicleId: String?) {
        guard let vehicleId = vehicleId else { return }
        
        showSpinner()
        viewModel.changeTripVehicle(vehicleId)
    }
    
    private func presentPicker() {
        showPicker(viewModel.reasons) { [weak self] index in
            self?.viewModel.reasonIndex = index
            self?.businessView.reason = self?.viewModel.reason
        }
    }
    
    private func showMore() {
        showTrayMore { option in
            switch option {
            case .removeTrip:
                self.showRemoveTrip()
            case .changeVehicle:
                self.showPrimaryChange()
            }
        }
    }
    
    private func showRemoveTrip() {
        showTrayRemoveTrip { reason in
            self.performRemoveTrip(reason)
        }
    }
    
    private func showPrimaryChange() {
        guard let vehicles = viewModel.vehicles else { return }
        
        showTrayPrimaryChange(viewModel.tripVehicle, vehicles: vehicles) { vehicle in
            self.performChangeVehicle(vehicle.vehicleId)
        } addVehicle: {
            self.delegate?.tripDetailsViewControllerDidTapAddVehicle(self)
        }
    }
    
    private func showRemoveTripConfirmation() {
        showTrayRemoveTripConfirmation {
            self.delegate?.tripDetailsViewControllerDidRemoveTrip(self)
        }
    }
    
    private func enableEditing(_ enable: Bool) {
        isEditMode = enable
        businessView.enableEditing(enable)
    }
    
    @objc
    func onMore() {
        showMore()
    }
    
    private func onEdit() {
        if isEditMode {
            performSaveData()
        } else {
            enableEditing(true)
        }
    }
}

extension TripDetailsViewController: TripDetailsBusinessViewDelegate {
    func tripDetailsBusinessViewDidTapReason(_ businessView: TripDetailsBusinessView) {
        presentPicker()
    }
    
    func tripDetailsBusinessViewDidTapEdit(_ businessView: TripDetailsBusinessView) {
        onEdit()
    }
}

extension TripDetailsViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }
    
    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height + 80, right: 0)
        scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}
