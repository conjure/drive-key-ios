//
//  TripDetailsViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2022.
//

import Foundation
import FirebaseFirestore

class TripDetailsViewModel {
    private let userService: UserProvidable
    private let api: APIProtocol
    private var db: Firestore?
    private var tripListener: ListenerRegistration?
    private(set) var trip: Trip
    private let dayFormatter = DateFormatter()
    var reasonIndex: Int?
    
    @Published var tripUpdated: Bool = false
    @Published var businessDataUpdated: Bool = false
    @Published var vehicleChanged = false
    @Published var tripRemoved = false
    @Published var error: Error?
    
    deinit {
        tripListener?.remove()
        tripListener = nil
    }
    
    init(_ userService: UserProvidable, api: APIProtocol, trip: Trip) {
        self.userService = userService
        self.api = api
        self.trip = trip
        
        dayFormatter.dateFormat = "EEEE dd MMMM"
    }
    
    func listenForDataUpdates() {
        guard let userId = userService.currentUser?.uid, let tripId = trip.id else { return }
        
        db = Firestore.firestore()
        setupListener(userId, tripId: tripId)
    }
    
    private func setupListener(_ userId: String, tripId: String) {
        tripListener = db?.collection("users").document(userId).collection("trips").document(tripId)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("[TripDetailsViewModel] Error fetching trip document: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("[TripDetailsViewModel] Trip document data was empty.")
                    return
                }
                
                let decoder = Firestore.Decoder()
                do {
                    let data = try decoder.decode(Trip.self, from: data)
                    self.trip = data
                    self.tripUpdated = true
                } catch {
                    print("[TripDetailsViewModel] Could not parse trip - \(error)")
                }
            }
    }
    
    func update(_ rate: Double, notes: String, reason: String) {
        guard let tripId = trip.id else { return }
        api.updateBusiness(tripId, rate: rate, notes: notes, reason: reason) { result in
            switch result {
            case .success:
                self.businessDataUpdated = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func changeTripVehicle(_ vehicleId: String) {
        guard let tripId = trip.id else { return }
        api.changeVehicle(tripId, vehicleId: vehicleId) { result in
            switch result {
            case .success:
                self.vehicleChanged = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func removeTrip(_ reson: String) {
        guard let tripId = trip.id else { return }
        api.removeTrip(tripId, reason: reson) { result in
            switch result {
            case .success:
                self.tripRemoved = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    var score: Double {
        trip.ecoDriverScore ?? 0
    }
    
    var pricePerMile: String? {
        if let rateData = trip.estimatedRate, let rate = rateData.rate {
            return String(rate)
        }
        
        return nil
    }
    
    var title: String {
        guard let date = trip.startDate else { return "" }
        
        return dayFormatter.string(from: date)
    }
    
    var isBusiness: Bool {
        trip.tripType == .business
    }
    
    var currency: String? {
        trip.estimatedRate?.currencyCode
    }
    
    var currencySymbol: String {
        if let currency = currency {
            return String.symbolForCurrencyCode(code: currency.uppercased())
        }
        
        return ""
    }
    
    var reasons: [String] {
        [
            "trip.details.reason.1".localized,
            "trip.details.reason.2".localized,
            "trip.details.reason.3".localized,
            "trip.details.reason.4".localized,
            "trip.details.reason.5".localized
        ]
    }
    
    var reason: String? {
        if let index = reasonIndex, index < reasons.count {
            return reasons[index]
        } else {
            return trip.businessReason
        }
    }
    
    var notes: String? {
        trip.businessNotes
    }
    
    var vehicles: [VehicleData]? {
        guard let vehicles = userService.vehiclesActive else { return nil }
        
        return vehicles
    }
    
    var tripVehicle: VehicleData? {
        guard let vehicles = vehicles else { return nil }
        return vehicles.first
    }
    
    var unit: UnitDistance {
        guard let user = userService.currentUser else { return .miles }
        
        return user.unitDistance
    }
    
    func removeListeners() {
        tripListener?.remove()
        tripListener = nil
    }
}
