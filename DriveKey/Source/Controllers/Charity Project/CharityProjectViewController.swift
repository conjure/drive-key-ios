//
//  CharityProjectViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/03/2022.
//
// swiftlint:disable weak_delegate

import UIKit
import SwiftUI
import Combine

protocol CharityProjectViewControllerDelegate: AnyObject {
    func charityProjectViewController(_ vc: CharityProjectViewController, didTap url: URL)
}

class CharityProjectViewController: ViewController {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var infoView: CharityProjectInfoView!
    @IBOutlet private var linkView: CharityProjectLinkView!
    @IBOutlet private var charitiesContainerView: UIView!

    private var hostingController: HostingController?
    private var tasks = Set<AnyCancellable>()
    private var model: ProjectCarouselViewModel = .init(charityProject: nil)

    var viewModel: CharityProjectViewModel!
    var pickerDelegate: ProjectPickerDelegate!
    weak var delegate: CharityProjectViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.appColor(.navyBlue)
        scrollView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        navigationController?.navigationBar.prefersLargeTitles = true
        if let navigationController = navigationController as? NavigationController {
            navigationController.setDefaultTheme()
        }
        
        if viewModel.screenSource != .offsetCarbon {
            let closeButton = UIBarButtonItem(image: UIImage(named: "close-arrow-icon"),
                                              style: .plain,
                                              target: self,
                                              action: #selector(onClose))
            navigationItem.rightBarButtonItem = closeButton
        }
        
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
        if let project = viewModel.charityProject {
            updateContent(project)
            setupProjectCarousel(for: project)
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTapUrl))
        linkView.addGestureRecognizer(tapGesture)

        pickerDelegate.didChange.sink { [weak self] del in
            guard let project = del.project, let self = self else { return }
            self.updateContent(project)

            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.scrollView.setContentOffset(.zero, animated: true)
            }
        }.store(in: &tasks)
    }
    
    private func updateContent(_ project: CharityProject) {
        title = project.screenTitle
        imageView.image = UIImage(named: project.imageName)
        infoView.updateContent(charityProject: project)
        model.charity = project

        trackProject(project)
    }

    private func setupProjectCarousel(for project: CharityProject) {
        // Then, remove the child from its parent
        hostingController?.removeFromParent()
        // Finally, remove the child’s view from the parent’s
        hostingController?.view.removeFromSuperview()

        let carousel = ProjectCarouselView(heading: "projects.carousel.heading.dashboard".localized, model: model, delegate: self.pickerDelegate)
        self.hostingController = HostingController(wrappedView: carousel, hideNavigationBar: false, prefersLargeTitles: true)

        guard let hostingController = hostingController else { return }
        // Add as a child of the current view controller.
        addChild(hostingController)

        // Add the SwiftUI view to the view controller view hierarchy.
        charitiesContainerView.addSubview(hostingController.view)

        // Setup the constraints to update the SwiftUI view boundaries.
        hostingController.view.backgroundColor = .clear
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            hostingController.view.topAnchor.constraint(equalTo: charitiesContainerView.topAnchor),
            hostingController.view.leftAnchor.constraint(equalTo: charitiesContainerView.leftAnchor),
            charitiesContainerView.bottomAnchor.constraint(equalTo: hostingController.view.bottomAnchor),
            charitiesContainerView.rightAnchor.constraint(equalTo: hostingController.view.rightAnchor)
        ]

        NSLayoutConstraint.activate(constraints)

        // Notify the hosting controller that it has been moved to the current view controller.
        hostingController.didMove(toParent: self)
    }
    
    @objc
    private func onClose() {
        dismiss(animated: true, completion: nil)
    }

    private func trackProject(_ project: CharityProject) {
        switch project {
        case .brazilianAmazon:
            AnalyticsService.trackEvent(.viewProjectBrazilian)
        case .talas:
            AnalyticsService.trackEvent(.viewProjectTalas)
        case .midilli:
            AnalyticsService.trackEvent(.viewProjectMidilli)
        }
    }

    @objc private func didTapUrl() {
        guard let url = viewModel.ctaURL else { return }
        self.delegate?.charityProjectViewController(self, didTap: url)
    }

}
