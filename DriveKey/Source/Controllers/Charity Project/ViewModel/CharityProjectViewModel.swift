//
//  CharityProjectViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/03/2022.
//

import Foundation

struct CharityProjectViewModel {
    let charityProject: CharityProject?
    let screenSource: ProjectScreenSource
    
    var ctaURL: URL? {
        return URL(string: "https://www.kar.ai/")
    }
}
