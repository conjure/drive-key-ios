//
//  CharityProjectInfoView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/03/2022.
//

import UIKit
import SwiftUI

class CharityProjectInfoView: BaseView {
    private let itemsStackView = Subviews.itemsStackView
    private let aboutLabel = Subviews.aboutLabel
    private let infoLabel = Subviews.infoLabel
    private let projectsContainerView = Subviews.projectsContainerView

    private var galleryView: CharityProjectGalleryView!
    private var model: CharityGalleryViewModel = .init(charityProject: .brazilianAmazon)
    private var fullText: NSAttributedString!
    private var containerHeightContraint: NSLayoutConstraint!

    private var isExpanded: Bool = false {
        didSet {
            if isExpanded {
                self.infoLabel.numberOfLines = 0
            } else {
                self.infoLabel.numberOfLines = 7
            }
            self.infoLabel.attributedText = fullText
            self.infoLabel.sizeToFit()
            self.projectsContainerView.isHidden = !isExpanded
        }
    }
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear

        itemsStackView.addArrangedSubview(infoLabel)
        itemsStackView.addArrangedSubview(projectsContainerView)
        projectsContainerView.isHidden = true
        
        addSubview(aboutLabel)
        addSubview(itemsStackView)

        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapLabel))
        infoLabel.addGestureRecognizer(tap)
    }
    
    override func setUpLayout() {
        super.setUpLayout()

        self.containerHeightContraint = projectsContainerView.heightAnchor.constraint(equalToConstant: 410)
        
        let constraint = [
            aboutLabel.topAnchor.constraint(equalTo: topAnchor),
            aboutLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            aboutLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            aboutLabel.heightAnchor.constraint(equalToConstant: 30),
            itemsStackView.topAnchor.constraint(equalTo: aboutLabel.bottomAnchor, constant: 12),
            itemsStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            itemsStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            itemsStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerHeightContraint!
        ]
        NSLayoutConstraint.activate(constraint)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if galleryView == nil {
            if let parentVC = self.findViewController() {
                self.galleryView = CharityProjectGalleryView(model: model)
                parentVC.addSwiftUIView(galleryView, to: projectsContainerView)
            } else {
                print("[DashboardContentView] Can't find view controller")
            }
        }
    }

    func updateContent(charityProject: CharityProject) {
        self.isExpanded = false
        let infoAttrStr = NSMutableAttributedString(string: charityProject.info,
                                                    attributes: [.font: UIFont.K2DBold(16)!,
                                                                 .foregroundColor: UIColor.appColor(.pastelBlue)])
        infoAttrStr.append(.init(string: "\n\r\(charityProject.infoDescription)",
                                 attributes: [.font: UIFont.K2DRegular(16)!,
                                              .foregroundColor: UIColor.appColor(.pastelBlue)]))
        self.fullText = infoAttrStr
        infoLabel.attributedText = infoAttrStr

        let firstSectionFont = ( UIFont.K2DBold(16)!, UIColor.appColor(.pastelBlue))
        let secondSectionFont = ( UIFont.K2DRegular(16)!, UIColor.appColor(.pastelBlue))
        self.infoLabel.addTrailingWithAttrSections(firstSectionFont: firstSectionFont,
                                                   secondSectionFont: secondSectionFont,
                                                   trailingText: "..",
                                                   moreText: "charity.project.info.read.more".localized,
                                                   trailingFont: (UIFont.K2DBold(16)!,
                                                                  UIColor.white.withAlphaComponent(0.7)),
                                                   labelFrame: .init(x: 0, y: 0,
                                                                     width: UIScreen.main.bounds.width - 48,
                                                                     height: 148))

        self.model.imageNames = charityProject.projectImages
        self.containerHeightContraint.constant = charityProject.projectImages.count == 4 ? 410 : 240
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 200)
    }

    @objc private func didTapLabel() {
        guard !isExpanded else { return }

        self.isExpanded = true
    }
}

private struct Subviews {
    static var aboutLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(20)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = "charity.project.about.project".localized
        
        return label
    }

    static var itemsStackView: UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.spacing = 24

        return stackView
    }
    
    static var infoLabel: VerticalAlignLabel {
        let label = VerticalAlignLabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(16)
        label.textColor = .appColor(.pastelBlue)
        label.textAlignment = .left
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 7
        label.isUserInteractionEnabled = true

        return label
    }

    static var projectsContainerView: UIView {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = .clear
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }
}
