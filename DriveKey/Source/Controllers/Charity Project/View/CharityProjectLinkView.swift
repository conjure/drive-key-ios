//
//  CharityProjectLinkView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/03/2022.
//

import UIKit

class CharityProjectLinkView: BaseView {
    private let topSeparatorView = Subviews.separatorView
    private let infoLabel = Subviews.infoLabel
    private let bottomSeparatorView = Subviews.separatorView
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(topSeparatorView)
        addSubview(infoLabel)
        addSubview(bottomSeparatorView)
        
        updateText()
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            topSeparatorView.topAnchor.constraint(equalTo: topAnchor),
            topSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            topSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            topSeparatorView.heightAnchor.constraint(equalToConstant: 1),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            bottomSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomSeparatorView.heightAnchor.constraint(equalToConstant: 1),
            bottomSeparatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    private func updateText() {
        let attrDescriptionStr = NSMutableAttributedString(string: "charity.project.link.description".localized + " ",
                                                           attributes: [.font: UIFont.K2DRegular(16)!,
                                                                            .foregroundColor: UIColor.white,
                                                                            .kern: 0.09])
        attrDescriptionStr.append(.init(string: "charity.project.link".localized,
                                        attributes: [.font: UIFont.K2DSemiBold(16)!,
                                                        .foregroundColor: UIColor.appColor(.lightBlue),
                                                        .kern: 0.09]))
        infoLabel.attributedText = attrDescriptionStr
    }
}

private struct Subviews {
    static var infoLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DSemiBold(16)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 0

        return label
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .appColor(.pastelBlue)
        view.alpha = 0.5

        return view
    }
}
