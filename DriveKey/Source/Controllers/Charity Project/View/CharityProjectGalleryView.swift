//
//  CharityProjectGalleryView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 07/03/2022.
//

import SwiftUI

class CharityGalleryViewModel: ObservableObject {
    @Published var imageNames: [String]

    init(charityProject: CharityProject) {
        self.imageNames = charityProject.projectImages
    }
}

struct CharityProjectGalleryView: View {
    @ObservedObject var model: CharityGalleryViewModel

    init(model: CharityGalleryViewModel) {
        self.model = model
    }

    var body: some View {
        VStack(spacing: 14) {
            HStack(spacing: 14) {
                Image(decorative: model.imageNames[0])
                    .resizable()
                    .frame( height: 240)
                    .cornerRadius(12)

                VStack(spacing: 14) {
                    Image(decorative: model.imageNames[1])
                              .resizable()
                              .frame(height: 112)
                              .cornerRadius(12)

                    Image(decorative: model.imageNames[2])
                                         .resizable()
                                         .frame(height: 112)
                                         .cornerRadius(12)
                }
            }

            if model.imageNames.count == 4 {
                Image(decorative: model.imageNames[3])
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(height: 156)
                    .cornerRadius(12)

            }
        }
    }
}

struct CharityProjectGalleryView_Previews: PreviewProvider {
    static var previews: some View {
        ForEach([CharityProject.brazilianAmazon, CharityProject.talas, CharityProject.midilli]) { project in
            CharityProjectGalleryView(model: CharityGalleryViewModel(charityProject: project))
                .padding(.horizontal, 24)
                .previewDevice(PreviewDevice(rawValue: "iPhone 13 Pro Max"))
                .previewDisplayName("iPhone 13 Pro Max")

            CharityProjectGalleryView(model: CharityGalleryViewModel(charityProject: project))
                .padding(.horizontal, 24)
                .previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
                .previewDisplayName("iPhone SE (2nd generation)")
        }
    }
}
