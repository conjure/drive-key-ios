//
//  RedeemVoucherViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import Foundation

struct RedeemVoucherViewModel {
    private let api: APICarbonProtocol

    init(api: APICarbonProtocol) {
        self.api = api
    }

    func redeemVoucher(_ voucherCode: String, completion: @escaping (Result<VoucherAPIResponse, Error>) -> Void) {
        api.redeemVoucher(voucherCode) { result in
            switch result {
            case .success(let resp):
                completion(.success(resp))
            case .failure(let error):
                print("[RedeemVoucherViewModel] Error redeeming voucher - \(error)")
                completion(.failure(error))
            }
        }
    }
}

enum VoucherRedeemError {
    case expired
    case invalid
    case unknown
}
