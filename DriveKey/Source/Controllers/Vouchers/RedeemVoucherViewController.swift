//
//  RedeemVoucherViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import UIKit
import Combine

protocol RedeemVoucherViewControllerDelegate: AnyObject {
    func redeemVoucherViewController(_ vc: RedeemVoucherViewController, didRedeem amount: Int)
}

class RedeemVoucherViewController: ViewController {
    @IBOutlet var voucherTextField: NamedTextField!
    @IBOutlet var redeemButton: RoundedButton!

    var viewModel: RedeemVoucherViewModel!
    weak var delegate: RedeemVoucherViewControllerDelegate?
    
    private var tasks = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
    
        title = "voucher.redeem.title".localized
        voucherTextField.name = "voucher.textfield.title".localized
        voucherTextField.errorMessage = "voucher.redeem.error".localized
        redeemButton.setTitle("voucher.redeem.button".localized, for: .normal)
        redeemButton.isEnabled = false

        voucherTextField.textField.autocorrectionType = .no
        voucherTextField.textField.delegate = self
        voucherTextField.textField.returnKeyType = .done
        voucherTextField.textField.becomeFirstResponder()

        voucherTextField.textField.textPublisher()
            .map { text in
                return !text.isEmpty
            }
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { enabled in
                self.voucherTextField.setError(false)
                self.redeemButton.isEnabled = enabled
            })
            .store(in: &tasks)

        hideKeyboardWhenTappedAround()
    }

    @IBAction func redeemVoucher(_ sender: Any) {
        guard let code = voucherTextField.text, !code.isEmpty else { return }
        setLoading(true)

        self.viewModel.redeemVoucher(code) { result in
            self.setLoading(false)

            switch result {
            case .success(let resp):
                let amount = Int(resp.amount ?? 0)
                self.delegate?.redeemVoucherViewController(self, didRedeem: amount)
            case .failure:
                self.voucherTextField.setError(true)
            }
        }
    }

    private func setLoading(_ loading: Bool) {
        voucherTextField.isEnabled = !loading
        redeemButton.isEnabled = !loading
        if loading {
            showSpinner()
        } else {
            hideSpinner()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension RedeemVoucherViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()

        return true
    }
}
