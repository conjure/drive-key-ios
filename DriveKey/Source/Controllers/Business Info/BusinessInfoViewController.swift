//
//  BusinessInfoViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 01/09/2022.
//

import UIKit
import Combine

class BusinessInfoViewController: ViewController, TrayPresentable {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var priceField: NamedTextField!
    @IBOutlet private var businessField: NamedSwitchField!
    private var editButton: UIButton?
    
    var viewModel: BusinessInfoViewModel!
    private var cancellables = Set<AnyCancellable>()
    
    private var isEditMode = false
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?
    
    private enum PlaceholderKeys {
        static let unit = "{unit}"
    }
    
    deinit {
        stopObservingKeyboardChanges()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "business.info.title".localized
        
        editButton = SmallButton(type: .custom)
        editButton?.translatesAutoresizingMaskIntoConstraints = false
        editButton?.setTitle("edit".localized, for: .normal)
        editButton?.addTarget(self, action: #selector(onEdit), for: .touchUpInside)
        editButton?.setTitleColor(.black, for: .normal)
        editButton?.setTitleColor(UIColor.black.withAlphaComponent(0.5), for: .highlighted)
        editButton?.titleLabel?.font = UIFont.K2DMedium(16)
        editButton?.adjustsImageWhenHighlighted = true
        let editItem = UIBarButtonItem(customView: editButton!)
        navigationItem.rightBarButtonItem = editItem
        
        let priceStr = "business.info.price.label".localized
        priceField.name = priceStr.replacingOccurrences(of: PlaceholderKeys.unit, with: viewModel.unit.localized)
        priceField.value = viewModel.rate
        priceField.textField.keyboardType = .decimalPad
        priceField.formatter = PricePerMileFormatter(viewModel.currencySymbol)
        
        businessField.name = "business.info.businnes.name".localized
        businessField.label = "business.info.businnes.label".localized
        businessField.value = viewModel.isDefaultBusiness
        
        enableEditing(false)
        
        startObservingKeyboardChanges()
        setupHideKeyboardOnTap()
        
        registerSubscriptions()
    }
    
    private func registerSubscriptions() {
        viewModel.$dataUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.enableEditing(false)
                }
            }
            .store(in: &cancellables)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &cancellables)
    }
    
    private func enableEditing(_ enable: Bool) {
        isEditMode = enable
        let editText = enable ? "save".localized : "edit".localized
        editButton?.setTitle(editText, for: .normal)
        priceField.isEnabled = enable
        businessField.isEnabled = enable
    }
    
    private func performSaveData() {
        guard let text = priceField.text else { return }
        let textNumber = text.replacingOccurrences(of: viewModel.currencySymbol, with: "")
        guard let value = Double(textNumber) else { return }
        
        showSpinner()
        view.endEditing(true)
        viewModel.update(value, isBusiness: businessField.isOn)
    }
    
    private func proceed() {
        enableEditing(false)
    }
    
    @objc
    func onEdit() {
        if isEditMode {
            performSaveData()
        } else {
            enableEditing(true)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension BusinessInfoViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }
    
    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height + 80, right: 0)
        scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}
