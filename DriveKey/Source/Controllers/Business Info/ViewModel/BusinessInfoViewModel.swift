//
//  BusinessInfoViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 01/09/2022.
//

import Foundation
import Combine

class BusinessInfoViewModel {
    private let api: APIProtocol
    private let userService: UserProvidable
    
    @Published var dataUpdated: Bool = false
    @Published var error: Error?
    
    init(_ api: APIProtocol, userService: UserProvidable) {
        self.api = api
        self.userService = userService
    }
    
    func update(_ price: Double, isBusiness: Bool) {
        let tripType: TripType = isBusiness ? .business : .personal
        let data = UpdateUserPayload(standardRate: price, defaultTripType: tripType)
        api.userData(data) { result in
            switch result {
            case .success:
                self.dataUpdated = true
            case .failure(let error):
                self.error = error
                
            }
        }
    }
    
    var rate: String? {
        if let user = userService.currentUser, let standardRate = user.standardRate {
            return String(standardRate)
        }
        
        return nil
    }
    
    var currency: String? {
        if let user = userService.currentUser, let currency = user.currencyCode {
            return currency
        }
        
        return nil
    }
    
    var currencySymbol: String {
        if let currency = currency {
            return String.symbolForCurrencyCode(code: currency.uppercased())
        }
        
        return ""
    }
    
    var unit: UnitDistance {
        guard let user = userService.currentUser else { return .miles }
        
        return user.unitDistance
    }
    
    var isDefaultBusiness: Bool {
        guard let tripType = userService.currentUser?.defaultTripType else { return false }
        
        return tripType == .business
    }
}
