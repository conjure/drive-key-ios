//
//  DashboardViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 09/08/2021.
//

import Foundation
import Combine

class DashboardViewModel {
    let userService: UserProvidable
    private let notificationService: NotificationService
    private let api: APIProtocol
    private let tripMonitor: TripsMonitor
    let bubbleProvider: BubbleProvidable

    private var tasks = Set<AnyCancellable>()

    @Published var vehicleChanged: Bool = false
    @Published var tripRemoved: Bool = false
    @Published var tripChanged: Bool = false
    @Published var error: Error?
    
    private enum PlaceholderKeys {
        static let daysLeft = "{days-left}"
        static let dayDays = "{day-days}"
    }
    
    init(userService: UserProvidable, notificationService: NotificationService, 
         api: APIProtocol,
         bubbleProvider: BubbleProvidable, tripsMonitor: TripsMonitor) {
        self.userService = userService
        self.notificationService = notificationService
        self.api = api
        self.tripMonitor = tripsMonitor
        self.bubbleProvider = bubbleProvider

        userService.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] usr in
                if usr.activeVehicle != nil { self?.tripMonitor.startMonitoring() }
            }
            .store(in: &tasks)
    }

    func welcomeMessage(for date: Date = Date()) -> String {
        let hour = Calendar.current.component(.hour, from: date)
        
        switch hour {
        case 0..<12 : return "welcome.message.morning".localized
        case 12..<17 : return "welcome.message.afternoon".localized
        case 17..<24 : return "welcome.message.evening".localized
        default: return ""
        }
    }

    var subHeading: String {
        guard let user = userService.currentUser, let vehicle = user.activeVehicle else { return "" }

        return vehicle.platesValue ?? ""
    }

    var ecoScore: Double? {
        userService.currentUser?.dashboardSummary?.overallEcoDrivingScore
    }
    
    var co2VTotalValue: Double {
        guard let user = userService.currentUser, let value = user.dashboardSummary?.co2PerKiloGram else {
            return 0
        }
        
        return round(value)
    }
    
    var co2OffsetValue: Double {
        guard let user = userService.currentUser, let value = user.dashboardSummary?.co2OffsetPerKiloGram else {
            return 0
        }
        
        return round(value)
    }
    
    var co2NetValue: Double {
        max(co2VTotalValue - co2OffsetValue, 0)
    }
    
    var co2Progress: Double {
        co2OffsetValue / co2VTotalValue
    }
    
    func updateFCMToken() {
        let token = notificationService.fcmToken
        guard token != nil else { return }

        let payload = UpdateUserPayload(fcmToken: token)
        api.userData(payload) { result in
            switch result {
            case .success:
                print("[DashboardViewModel] updateFCMToken success")
            case let .failure(error):
                print("[DashboardViewModel] updateFCMToken error: \(error)")
            }
        }
    }
    
    var lastTrip: Trip? {
        userService.lastTrip
    }
    
    var summary: MileageSummary? {
        userService.mileageSummary
    }

    var trips: Int {
        guard let user = userService.currentUser else {
            return 0
        }

        return user.dashboardSummary?.trips ?? 0
    }
    
    var unit: UnitDistance {
        guard let user = userService.currentUser else { return .miles }
        
        return user.unitDistance
    }

    var distance: Int {
        guard let summary = userService.currentUser?.dashboardSummary, let distance = summary.distance(unit) else {
            return 0
        }

        return Int(distance)
    }

    var scores: [Double] {
        // [Eco - Braking - Accleration - Cornering]
        guard let user = userService.currentUser,
                let eco = user.dashboardSummary?.overallEcoDrivingScore, eco > 0,
              let accel = user.dashboardSummary?.hardAcceleration,
              let braking = user.dashboardSummary?.hardBrake,
              let cornering = user.dashboardSummary?.hardTurn else {
            return []
        }

        return [eco, braking, accel, cornering]
    }

    var hasTrayBubbles: Bool {
        return !bubbleProvider.bubbles.filter({!$0.shownOnDashboard}).isEmpty
    }
    
    var vehicles: [VehicleData] {
        guard let data = userService.vehicles else { return [VehicleData]() }
        
        return data.filter({ $0.status == .active })
    }
    
    var primaryVehicle: VehicleData? {
        vehicles.first(where: { $0.primary == true })
    }
    
    func changeLastTripVehicle(_ vehicleId: String) {
        guard let tripId: String = lastTrip?.id else { return }
        
        api.changeVehicle(tripId, vehicleId: vehicleId) { result in
            switch result {
            case .success:
                self.vehicleChanged = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func removeTrip(_ tripId: String, reson: String) {
        api.removeTrip(tripId, reason: reson) { result in
            switch result {
            case .success:
                self.tripRemoved = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func changeTrip(_ tripId: String, type: TripType) {
        api.changeTrip(tripId, type: type) { result in
            switch result {
            case .success:
                self.tripChanged = true
            case .failure(let error):
                self.error = error
            }
        }
    }

    var isDebugEnabled: Bool {
        if let user = userService.currentUser, let isDebug = user.isDebug {
            return isDebug
        }

        return false
    }
}
