//
//  DashboardDrawerViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/08/2021.
//

import Combine
import Foundation

protocol DashboardDrawerViewModelDelegate: AnyObject {
    func dashboardDrawerViewModelDidSyncBubbles(_ viewModel: DashboardDrawerViewModel)
}

class DashboardDrawerViewModel {
    private let bubbleService: BubbleProvidable
    private let userService: UserProvidable
    private let userDefaults: UserDefaults
    private var isListeningForBubbles = false
    private var tasks = Set<AnyCancellable>()

    weak var delegate: DashboardDrawerViewModelDelegate?

    init(bubbleService: BubbleProvidable, userService: UserProvidable, userDefaults: UserDefaults = .standard) {
        self.bubbleService = bubbleService
        self.userService = userService
        self.userDefaults = userDefaults

        if let user = self.userService.currentUser {
            bubbleService.sync(for: user)
            isListeningForBubbles = true
        } else {
            userService.syncedSubject
                .receive(on: DispatchQueue.main)
                .sink { [weak self] user in
                    guard let strongSelf = self else { return }

                    if !strongSelf.isListeningForBubbles {
                        strongSelf.bubbleService.sync(for: user)
                        strongSelf.isListeningForBubbles = true
                    }
                }
                .store(in: &tasks)
        }

        bubbleService.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.delegate?.dashboardDrawerViewModelDidSyncBubbles(strongSelf)
            }
            .store(in: &tasks)
    }

    func refreshBubbles() {
        bubbleService.refresh()
    }

    func getCurrentBubbles() -> [Bubble] {
        let trayBubbles = bubbleService.bubbles.filter({!$0.shownOnDashboard})
        return trayBubbles.sorted()
    }
}
