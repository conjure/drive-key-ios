//
//  BubbleCell.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/08/2021.
//

import UIKit

class BubbleCell: UICollectionViewCell {
    static let reuseIdentifer = "bubble-cell"
    private let cornerRadius: CGFloat = 12.0

    @IBOutlet private var containerView: UIView!
    @IBOutlet private var iconView: UIImageView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var messageLabel: UILabel!

    var bubble: Bubble! {
        didSet {
            self.update()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        configure()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        // Improve scrolling performance with an explicit shadowPath
        layer.shadowPath = UIBezierPath(
            roundedRect: bounds,
            cornerRadius: cornerRadius
        ).cgPath
    }
}

extension BubbleCell {
    func configure() {
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.masksToBounds = true

        // Set masks to bounds to false to avoid the shadow
        // from being clipped to the corner radius
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = false

        // Apply a shadow
        layer.shadowRadius = 2.0
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor(red: 0.408, green: 0.408, blue: 0.408, alpha: 0.22).cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0.85)
    }

    private func update() {
        titleLabel.text = bubble.title
        iconView.image = UIImage(named: bubble.imageName)
        let messageAttrs: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: messageLabel.font ?? UIFont.systemFont(ofSize: 16)]
        let messageStr = NSMutableAttributedString(string: bubble.message, attributes: messageAttrs)
        messageStr.updateCO2(for: messageLabel.font, colour: messageLabel.textColor)
        messageLabel.attributedText = messageStr
    }
}
