//
//  DashboardDrawerViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/08/2021.
//

import Pulley
import UIKit

protocol DashboardDrawerViewControllerDelegate: AnyObject {
    func dashboardDrawerViewController(_ vc: DashboardDrawerViewController, willPresent bubble: Bubble)
}

class DashboardDrawerViewController: ViewController {
    enum Section {
        case bubbleContent
    }

    @IBOutlet private var draggerImageView: UIImageView!
    @IBOutlet private var bubbleCollectionView: UICollectionView!

    private var bubbles: [Bubble] = [] {
        didSet {
            bubbleCollectionView.backgroundView?.alpha = bubbles.isEmpty ? 1.0 : 0.0
        }
    }

    var viewModel: DashboardDrawerViewModel!
    weak var delegate: DashboardDrawerViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCollectionView()

        viewModel.delegate = self

        NotificationCenter.default.addObserver(self, selector: #selector(refreshBubbles), name: UIApplication.didBecomeActiveNotification, object: nil)

        bubbleCollectionView.contentInset = .init(top: 5, left: 0, bottom: 50, right: 0)
        bubbleCollectionView.setEmptyMessage("dashboard.drawer.no.bubbles".localized)

        self.view.backgroundColor = UIColor.appColor(.pastelDarkBlue)
        self.bubbleCollectionView.backgroundColor = UIColor.appColor(.pastelDarkBlue)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshBubbles()
    }

    private func configureCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 16

        bubbleCollectionView.collectionViewLayout = layout

        bubbleCollectionView.backgroundColor = .systemBackground
        bubbleCollectionView.register(UINib(nibName: "BubbleCell", bundle: nil), forCellWithReuseIdentifier: BubbleCell.reuseIdentifer)
    }

    @objc private func refreshBubbles() {
        viewModel.refreshBubbles()
    }
}

extension DashboardDrawerViewController: UICollectionViewDataSource {
    func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        bubbles.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let bubble = bubbles[indexPath.row]

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BubbleCell.reuseIdentifer, for: indexPath) as? BubbleCell else { fatalError("Could not create bubble cell") }
        cell.bubble = bubble
        return cell
    }
}

extension DashboardDrawerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_: UICollectionView, layout _: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width - 48, height: 105)
    }
}

extension DashboardDrawerViewController: UICollectionViewDelegate {
    func collectionView(_: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let bubble = bubbles[indexPath.row]
        delegate?.dashboardDrawerViewController(self, willPresent: bubble)
    }
}

extension DashboardDrawerViewController: PulleyDrawerViewControllerDelegate {
    func partialRevealDrawerHeight(bottomSafeArea _: CGFloat) -> CGFloat {
        // For devices with a bottom safe area, we want to make our drawer taller. Your implementation may not want to do that. In that case, disregard the bottomSafeArea value.
        0.9 * UIScreen.main.bounds.height
    }

    func supportedDrawerPositions() -> [PulleyPosition] {
        [.partiallyRevealed, .collapsed, .closed]
    }

    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea _: CGFloat) {
        bubbleCollectionView.isScrollEnabled = drawer.drawerPosition == .partiallyRevealed || drawer.currentDisplayMode == .panel

        if drawer.drawerPosition == .closed || drawer.drawerPosition == .collapsed {
            NotificationCenter.default.post(name: .closedDrawerNotification, object: nil)
        }
    }

    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return 0
    }
}

extension DashboardDrawerViewController: DashboardDrawerViewModelDelegate {
    func dashboardDrawerViewModelDidSyncBubbles(_: DashboardDrawerViewModel) {
        bubbles = viewModel.getCurrentBubbles()
        bubbleCollectionView.reloadData()
    }
}
