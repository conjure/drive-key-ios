//
//  DashboardCarbonStateProvider.swift
//  DriveKey
//
//  Created by Piotr Wilk on 28/02/2022.
//

import Foundation

enum KaraiCarbonState: Equatable {
    case noData
    case negative(_ progress: Double)
    case neutral
    case positiveSingle(_ progress: Double, value: Double)
    case positiveMulti(_ multiplier: Int, value: Double)
}

protocol KaraiCarbonStateUpdatable: AnyObject {
    func update(_ state: KaraiCarbonState)
}

class DashboardCarbonStateProvider {
    private var components = [KaraiCarbonStateUpdatable]()
    private(set) var state: KaraiCarbonState = .noData
    
    func updateCarbonData(_ total: Double?, offset: Double?) {
        let totalValue = total != nil ? total! : 0
        let offsetValue = offset != nil ? offset! : 0
        
        var carbonState = KaraiCarbonState.noData
        if totalValue > 0, totalValue == offsetValue {
            carbonState = .neutral
        } else if totalValue > offsetValue {
            var progress: Double = min(offsetValue / totalValue, 1)
            if offsetValue == 0 {
                progress = 1
            }
            carbonState = .negative(progress)
        } else if offsetValue > totalValue {
            if totalValue > 0 {
                let isMulti = (offsetValue - totalValue * 2) >= 0
                if isMulti == false {
                    let progress: Double = min(((offsetValue - totalValue) / totalValue), 1)
                    carbonState = .positiveSingle(progress, value: (offsetValue - totalValue))
                } else {
                    let multiplier = Int((offsetValue/totalValue).rounded() - 1)
                    carbonState = .positiveMulti(multiplier, value: (offsetValue - totalValue))
                }
            } else {
                carbonState = .positiveMulti(1, value: offsetValue)
            }
        }
        
        state = carbonState
        components.forEach({ $0.update(state) })
    }
    
    func register(_ component: KaraiCarbonStateUpdatable) {
        components.append(component)
    }
}
