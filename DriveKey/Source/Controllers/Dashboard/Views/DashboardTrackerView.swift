//
//  DashboardTrackerView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/01/2022.
//

import UIKit

protocol DashboardTrackerViewDelegate: AnyObject {
    func dashboardTrackerViewDidFinishAnimation(_ trackerView: DashboardTrackerView, for state: KaraiCarbonState)
}

class DashboardTrackerView: BaseView {
    private let baseLayer = Subviews.circleLayer
    private let circleLayer = Subviews.circleLayer
    private let progressLayer = Subviews.progressLayer
    private var progressValue: CGFloat = 0
    private var carbonState = KaraiCarbonState.noData
    
    weak var delegate: DashboardTrackerViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        baseLayer.strokeColor = UIColor.appColor(.navyBlue).cgColor
        
        layer.addSublayer(baseLayer)
        layer.addSublayer(circleLayer)
        layer.addSublayer(progressLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let widthRatio: CGFloat = UIScreen.main.bounds.width <= 320 ? 0.285 : 0.292
        let radius = bounds.width * widthRatio
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: bounds.width * 0.5, y: bounds.height * 0.5),
                                      radius: radius,
                                      startAngle: CGFloat(-Double.pi/2),
                                      endAngle: CGFloat(3*Double.pi/2),
                                      clockwise: true)
        
        baseLayer.path = circlePath.cgPath
        circleLayer.path = circlePath.cgPath
        progressLayer.path = circlePath.cgPath
    }
    
    func progress(_ progress: CGFloat, animated: Bool) {
        guard progressValue != progress else { return }
        let progressOld = progressValue
        progressValue = progress
        
        let duration: TimeInterval = 2
        var durationAnimation = animated == true ? duration * progress : 0
        if  animated == true, progressValue == 0 {
            durationAnimation = duration * progressOld
        }
        
        CATransaction.begin()
        CATransaction.setCompletionBlock({
            self.delegate?.dashboardTrackerViewDidFinishAnimation(self, for: self.carbonState)
        })
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = durationAnimation
        animation.fromValue = progressOld
        animation.toValue = progressValue
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = progressValue == 0 ? true : false
        progressLayer.add(animation, forKey: "progress-animation")
        CATransaction.commit()
    }
}

extension DashboardTrackerView: KaraiCarbonStateUpdatable {
    func update(_ state: KaraiCarbonState) {
        carbonState = state
        
        switch state {
        case .noData:
            circleLayer.strokeColor = UIColor.appColor(.lightWhite).cgColor
            circleLayer.opacity = 0.2
            progress(0, animated: false)
            progressLayer.strokeColor = UIColor.clear.cgColor
        case .negative(let value):
            
            circleLayer.strokeColor = UIColor.white.cgColor
            circleLayer.opacity = 1
            let progressColor = value == 1 ? UIColor.white : UIColor.appColor(.green)
            progressLayer.strokeColor = progressColor.cgColor
            progress(CGFloat(value), animated: true)
        case .neutral:
            circleLayer.strokeColor = UIColor.white.cgColor
            circleLayer.opacity = 1
            progressLayer.strokeColor = UIColor.appColor(.green).cgColor
            progress(1, animated: true)
        case .positiveSingle(let value, _):
            circleLayer.strokeColor = UIColor.appColor(.green).cgColor
            circleLayer.opacity = 0.5
            progressLayer.strokeColor = UIColor.appColor(.lightBlue).cgColor
            progress(CGFloat(value), animated: true)
        case .positiveMulti:
            circleLayer.strokeColor = UIColor.appColor(.green).cgColor
            circleLayer.opacity = 0.5
            progressLayer.strokeColor = UIColor.appColor(.lightBlue).cgColor
            progress(1, animated: true)
        }
    }
}

private struct Subviews {
    static var circleLayer: CAShapeLayer {
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = .round
        layer.lineWidth = 8
        layer.strokeEnd = 1
        layer.strokeColor = UIColor.white.withAlphaComponent(0.9).cgColor
        
        return layer
    }
    
    static var progressLayer: CAShapeLayer {
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        layer.lineCap = .square
        layer.lineWidth = 8
        layer.strokeEnd = 0
        layer.strokeColor = UIColor.appColor(.green).cgColor
        
        return layer
    }
}
