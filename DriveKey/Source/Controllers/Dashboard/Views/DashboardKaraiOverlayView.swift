//
//  DashboardKaraiOverlayView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 31/01/2022.
//

import UIKit

protocol DashboardKaraiOverlayViewDelegate: AnyObject {
    func dashboardKaraiOverlayViewDidTapInfo(_ view: DashboardKaraiOverlayView)
}

class DashboardKaraiOverlayView: BaseView {
    private let scoreView = Subviews.scoreView
    private let infoButton = Subviews.infoButton
    private let offsetView = Subviews.offsetView
    private let totalView = Subviews.totalView
    
    weak var delegate: DashboardKaraiOverlayViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(scoreView)
        addSubview(infoButton)
        addSubview(offsetView)
        addSubview(totalView)
        
        infoButton.addTarget(self, action: #selector(onInfo), for: .touchUpInside)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let bottomOffset: CGFloat = UIScreen.main.bounds.width > 320 ? -60 : -40
        let constraints = [
            scoreView.topAnchor.constraint(equalTo: topAnchor),
            scoreView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            scoreView.widthAnchor.constraint(equalToConstant: 190),
            infoButton.topAnchor.constraint(equalTo: topAnchor, constant: -10),
            infoButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            infoButton.widthAnchor.constraint(equalToConstant: 40),
            infoButton.heightAnchor.constraint(equalToConstant: 40),
            offsetView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            offsetView.widthAnchor.constraint(equalToConstant: 70),
            offsetView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomOffset),
            totalView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            totalView.widthAnchor.constraint(equalToConstant: 70),
            totalView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomOffset)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var ecoScore: Double? {
        didSet {
            scoreView.value = ecoScore
        }
    }

    var co2Offset: Double? {
        didSet {
            offsetView.value = co2Offset
        }
    }
    
    var co2Total: Double? {
        didSet {
            totalView.value = co2Total
        }
    }
    
    @objc
    private func onInfo() {
        delegate?.dashboardKaraiOverlayViewDidTapInfo(self)
    }
}

extension DashboardKaraiOverlayView: KaraiCarbonStateUpdatable {
    func update(_ state: KaraiCarbonState) {
        offsetView.update(state)
    }
}

private struct Subviews {
    static var scoreView: DashboardScoreView {
        let view = DashboardScoreView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var infoButton: UIButton {
        let button = UIButton(frame: CGRect.zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "information"), for: .normal)

        return button
    }
    
    static var offsetView: DashboardCo2OffsetView {
        let view = DashboardCo2OffsetView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var totalView: DashboardCo2TotalView {
        let view = DashboardCo2TotalView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
}
