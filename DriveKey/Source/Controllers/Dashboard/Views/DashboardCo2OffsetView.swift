//
//  DashboardCo2OffsetView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 31/01/2022.
//

import UIKit

class DashboardCo2OffsetView: BaseView {
    private let squareView = Subviews.squareView
    private let valueLabel = Subviews.valueLabel
    private let infoLabel = Subviews.infoLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(squareView)
        addSubview(valueLabel)
        addSubview(infoLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            squareView.topAnchor.constraint(equalTo: topAnchor),
            squareView.trailingAnchor.constraint(equalTo: trailingAnchor),
            squareView.widthAnchor.constraint(equalToConstant: 8),
            squareView.heightAnchor.constraint(equalToConstant: 8),
            valueLabel.topAnchor.constraint(equalTo: squareView.bottomAnchor, constant: 5),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: valueLabel.bottomAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var value: Double? {
        didSet {
            let valueDouble = value != nil ? value! : 0
            let valueStr = valueDouble.formattedCO2 + "dashboard.co2.value.unit".localized
            valueLabel.text = valueStr
        }
    }
    
    func update(_ state: KaraiCarbonState) {
       switch state {
       case .noData:
           valueLabel.textColor = .white
           infoLabel.textColor = .white
           squareView.backgroundColor = .white
       case .negative(let progress):
           let color = progress == 1 ? UIColor.white : UIColor.appColor(.green)
           valueLabel.textColor = color
           infoLabel.textColor = color
           squareView.backgroundColor = color
       case .neutral:
           valueLabel.textColor = UIColor.appColor(.green)
           infoLabel.textColor = UIColor.appColor(.green)
           squareView.backgroundColor = UIColor.appColor(.green)
       case .positiveSingle, .positiveMulti:
           valueLabel.textColor = UIColor.appColor(.lightBlue)
           infoLabel.textColor = UIColor.appColor(.lightBlue)
           squareView.backgroundColor = UIColor.appColor(.lightBlue)
       }
   }
}

private struct Subviews {
    static var squareView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 1.6
        view.backgroundColor = .appColor(.green)

        return view
    }
    
    static var valueLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(16)
        label.textColor = .appColor(.green)
        label.textAlignment = .right
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5

        return label
    }
    
    static var infoLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .appColor(.green)
        label.textAlignment = .right
        label.numberOfLines = 1
        label.text = "dashboard.co2.offset.info.label".localized

        return label
    }
}
