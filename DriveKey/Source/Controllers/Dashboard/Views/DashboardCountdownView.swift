//
//  DashboardCountdownView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/08/2021.
//

import UIKit

class DashboardCountdownView: BaseView {
    private let infoLabel = Subviews.infoLabel

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(infoLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            infoLabel.topAnchor.constraint(equalTo: topAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var info: String? {
        didSet {
            infoLabel.text = info
        }
    }
}

private struct Subviews {
    static var infoLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        let fontSize: CGFloat = UIScreen.main.bounds.width > 320 ? 16 : 14
        label.font = UIFont.K2DBold(fontSize)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0

        return label
    }
}
