//
//  DashboardHeaderView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 10/08/2021.
//

import UIKit

protocol DashboardHeaderViewDelegate: AnyObject {
    func dashboardViewWillPresentProfile(_ view: DashboardHeaderView)
    func dashboardViewWillPresentNofifications(_ view: DashboardHeaderView)
}

class DashboardHeaderView: XibView {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var subHeadingLabel: UILabel!
    @IBOutlet private var profileButton: SmallButton!
    @IBOutlet private var notificationButton: SmallButton!

    weak var delegate: DashboardHeaderViewDelegate?

    override func setup() {
        super.setup()

        let fontSize: CGFloat = UIScreen.main.bounds.width > 320 ? 25 : 20
        headingLabel.font = UIFont.K2DBold(fontSize)
    }

    func update(heading: String, subheading: String, hasBubbles: Bool) {
        headingLabel.text = heading
        subHeadingLabel.text = subheading
        notificationButton.setImage(hasBubbles ? UIImage(named: "notifications") : UIImage(named: "no_notifcations"), for: .normal)
    }

    @IBAction private func didPressProfile(_: Any) {
        delegate?.dashboardViewWillPresentProfile(self)
    }

    @IBAction private func didPressNotifications(_ sender: Any) {
        delegate?.dashboardViewWillPresentNofifications(self)
    }
    
}
