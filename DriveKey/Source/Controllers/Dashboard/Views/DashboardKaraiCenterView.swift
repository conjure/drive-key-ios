//
//  DashboardKaraiCenterView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/02/2022.
//

import UIKit

class DashboardKaraiCenterView: BaseView {
    private let treeView = Subviews.treeView
    private let valueLabel = Subviews.valueLabel
    private var carbonState: KaraiCarbonState = .noData
    
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(treeView)
        addSubview(valueLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()
        
        let screenWidth = UIScreen.main.bounds.width
        let offsetYTree: CGFloat = screenWidth > 320 ? 16 : 20
        let offsetYValue: CGFloat = screenWidth > 320 ? 18 : 14
        let treeHeight = screenWidth * 0.11
        
        let constraints = [
            treeView.topAnchor.constraint(equalTo: topAnchor, constant: offsetYTree),
            treeView.leadingAnchor.constraint(equalTo: leadingAnchor),
            treeView.trailingAnchor.constraint(equalTo: trailingAnchor),
            treeView.heightAnchor.constraint(equalToConstant: treeHeight),
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: offsetYValue),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            valueLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            valueLabel.heightAnchor.constraint(equalToConstant: 60)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var value: Double? {
        didSet {
            var strValue = value != nil ? value!.formattedCO2 : "0"

            let strKg = "karai.message.subscript.kg".localized
            var strBottom = ""
            var str2 = ""
            
            switch carbonState {
            case .neutral:
                strBottom = "karai.message.co2.neutral".localized
                str2 = ""
            case .positiveSingle(_, let offsetValue):
                strBottom = "karai.message.co2.positive".localized
                str2 = ""
                strValue = "+" + offsetValue.formattedCO2
            case .positiveMulti(_, let offsetValue):
                strBottom = "karai.message.co2.positive".localized
                str2 = ""
                strValue = "+" + offsetValue.formattedCO2
            default:
                strBottom = "karai.message.net.co2".localized
                str2 = "karai.message.subscript.2".localized
            }
            
            let descriptionStr = strValue + strKg + "\n" + strBottom

            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1
            paragraphStyle.lineHeightMultiple = 0.7
            paragraphStyle.alignment = .center
            
            let screenWidth = UIScreen.main.bounds.width
            let fontSizeTop: CGFloat = screenWidth > 320 ? 36 : 28
            let fontSizeTopSub: CGFloat = screenWidth > 320 ? 18 : 16
            let fontSizeBottom: CGFloat = screenWidth > 320 ? 22 : 17
            let fontSizeBottomSub: CGFloat = screenWidth > 320 ? 10 : 8
            let fontTop = UIFont.K2DBold(fontSizeTop) ?? UIFont.systemFont(ofSize: fontSizeTop, weight: .bold)
            let fontTopSubscript = UIFont.K2DBold(fontSizeTopSub) ?? UIFont.systemFont(ofSize: fontSizeTopSub, weight: .bold)
            let fontBottom = UIFont.K2DLight(fontSizeBottom)
            let fontBottomSubscript = UIFont.K2DLightItalic(fontSizeBottomSub) ?? UIFont.systemFont(ofSize: fontSizeBottomSub, weight: .bold)

            let rangeValue = descriptionStr.nsRange(of: strValue)
            let rangeBottom = descriptionStr.nsRange(of: strBottom)
            let rangeKg = descriptionStr.nsRange(of: strKg)
            let descriptionFullStr = descriptionStr + str2
            let rangeParagraph = NSRange(location: 0, length: descriptionFullStr.count)

            let descriptionAttStr = NSMutableAttributedString(string: descriptionFullStr)
            descriptionAttStr.addAttribute(.font, value: fontTop, range: rangeValue)
            descriptionAttStr.addAttribute(.font, value: fontBottom, range: rangeBottom)
            descriptionAttStr.addAttribute(.font, value: fontTopSubscript, range: rangeKg)
            descriptionAttStr.addAttribute(.paragraphStyle, value: paragraphStyle, range: rangeParagraph)
            
            if !str2.isEmpty {
                let range2 = NSRange(location: descriptionFullStr.count - 1, length: 1)
                descriptionAttStr.addAttribute(.font, value: fontBottomSubscript, range: range2)
            }

            valueLabel.attributedText = descriptionAttStr
        }
    }
}

extension DashboardKaraiCenterView: KaraiCarbonStateUpdatable {
    func update(_ state: KaraiCarbonState) {
        carbonState = state
        treeView.update(state)
   }
}

private struct Subviews {
    static var treeView: DashboardKaraiTreeView {
        let view = DashboardKaraiTreeView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }
    
    static var valueLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 2

        return label
    }
}
