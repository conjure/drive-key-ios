//
//  DashboardKaraiTreeView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/02/2022.
//

import UIKit

class DashboardKaraiTreeView: BaseView {
    private(set) var imageView = Subviews.imageView
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(imageView)
        
        update(.noData)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
}

extension DashboardKaraiTreeView: KaraiCarbonStateUpdatable {
    func update(_ state: KaraiCarbonState) {
       switch state {
       case .noData, .negative:
           imageView.image = UIImage(named: "tracker-tree-green")
           imageView.alpha = 0.3
       case .neutral:
           imageView.image = UIImage(named: "tracker-tree-green")
           imageView.alpha = 1
       case .positiveSingle:
           imageView.image = UIImage(named: "tracker-tree-green")
           imageView.alpha = 1
       case .positiveMulti(let multiplier, _):
           let treeMultiplier = min(multiplier, 7)
           let imageName = "tracker-tree-blue-\(treeMultiplier)"
           imageView.image = UIImage(named: imageName)
           imageView.alpha = 1
       }
   }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
}
