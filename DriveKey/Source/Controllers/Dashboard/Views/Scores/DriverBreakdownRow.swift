//
//  DriverBreakdownRow.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 02/09/2021.
//

import UIKit

enum ScoreBreakdownType: CaseIterable {
    case cornering
    case braking
    case acceleration
}

class ScoreBreakdownRow: BaseView {
    private let headingLabel = Subviews.headingLabel
    private let valueLabel = Subviews.valueLabel
    private let progressBackground = Subviews.progressBackgroundView
    private let progressView = Subviews.progressView

    private var progressWidthConstraint: NSLayoutConstraint!

    var value: Double = 0 {
        didSet {
            self.update(value)
        }
    }

    convenience init(type: ScoreBreakdownType) {
        self.init()

        switch type {
        case .cornering:
            headingLabel.text =  "dashboard.scores.cornering.heading".localized
            progressView.backgroundColor = UIColor.appColor(.pastelBlue)
        case .braking:
            headingLabel.text = "dashboard.scores.braking.heading".localized
            progressView.backgroundColor = UIColor.appColor(.pastelBlue)
        case .acceleration:
            headingLabel.text = "dashboard.scores.acceleration.heading".localized
            progressView.backgroundColor = UIColor.appColor(.pastelBlue)
        }

        valueLabel.text = "0/100"
    }

    override func setUpLayout() {
        super.setUpLayout()

        backgroundColor = .clear

        progressWidthConstraint = progressView.widthAnchor.constraint(equalToConstant: 0)

        let constraints = [
            headingLabel.topAnchor.constraint(equalTo: topAnchor),
            headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor),

            valueLabel.centerYAnchor.constraint(equalTo: headingLabel.centerYAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),

            progressBackground.leadingAnchor.constraint(equalTo: leadingAnchor),
            progressBackground.trailingAnchor.constraint(equalTo: trailingAnchor),
            progressBackground.bottomAnchor.constraint(equalTo: bottomAnchor),
            progressBackground.heightAnchor.constraint(equalToConstant: 10),

            progressView.leadingAnchor.constraint(equalTo: progressBackground.leadingAnchor),
            progressView.topAnchor.constraint(equalTo: progressBackground.topAnchor),
            progressView.bottomAnchor.constraint(equalTo: progressBackground.bottomAnchor),
            progressWidthConstraint!
        ]

        NSLayoutConstraint.activate(constraints)
    }

    override func setUpSubviews() {
        super.setUpSubviews()

        addSubview(headingLabel)
        addSubview(valueLabel)
        addSubview(progressBackground)
        progressBackground.addSubview(progressView)
    }

    private func update(_ score: Double) {
        progressWidthConstraint.constant = (progressBackground.frame.width * score)
        let val = Int((score * 100).rounded(0))
        valueLabel.text = "\(val)/100"
    }
}

private struct Subviews {
    static var headingLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(11)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1

        return label
    }

    static var valueLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(11)
        label.textColor = .white.withAlphaComponent(0.5)
        label.textAlignment = .right
        label.numberOfLines = 1

        return label
    }

    static var progressBackgroundView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(hex: 0xE2E2E2).withAlphaComponent(0.2)
        view.layer.cornerRadius = 3.0
        view.clipsToBounds = true
        return view
    }

    static var progressView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 3.0
        view.clipsToBounds = true
        return view
    }
}
