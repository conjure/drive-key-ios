//
//  DashboardNoScoreView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/01/2022.
//

import UIKit

class DashboardNoScoreView: BaseView {
    private let headingLabel = Subviews.ecoHeadingLabel
    private let infoImageView = Subviews.infoImageView
    private let messageLabel = Subviews.messageLabel

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.pastelDarkBlue)

        addSubview(headingLabel)
        addSubview(infoImageView)
        addSubview(messageLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            headingLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            infoImageView.heightAnchor.constraint(equalToConstant: 16),
            infoImageView.widthAnchor.constraint(equalToConstant: 16),
            infoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            infoImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            messageLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 7),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 80)
    }
}

private struct Subviews {
    static var ecoHeadingLabel: UILabel {
        let label = UILabel()
        label.text = "dashboard.scores.eco.heading".localized
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white

        return label
    }

    static var infoImageView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "information"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var messageLabel: UILabel {
        let label = UILabel()
        label.text = "dashboard.scores.no.score".localized
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(12)
        label.textColor = .appColor(.pastelBlue)

        return label
    }
}
