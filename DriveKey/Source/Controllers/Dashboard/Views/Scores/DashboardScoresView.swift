//
//  DashboardScoresView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/01/2022.
//

import UIKit

class DashboardScoresView: BaseView {
    private let headingLabel = Subviews.ecoHeadingLabel
    private let infoImageView = Subviews.infoImageView
    private let ecoScoreLabel = Subviews.ecoScoreLabel
    private let brakingRow = Subviews.brakingRow
    private let accelerationRow = Subviews.accelerationRow
    private let corneringRow = Subviews.corneringRow

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.pastelDarkBlue)

        addSubview(headingLabel)
        addSubview(infoImageView)
        addSubview(ecoScoreLabel)
        addSubview(brakingRow)
        addSubview(accelerationRow)
        addSubview(corneringRow)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            headingLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            headingLabel.heightAnchor.constraint(equalToConstant: 16),
            infoImageView.heightAnchor.constraint(equalToConstant: 16),
            infoImageView.widthAnchor.constraint(equalToConstant: 16),
            infoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            infoImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            ecoScoreLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 15),
            ecoScoreLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            ecoScoreLabel.heightAnchor.constraint(equalToConstant: 25),

            brakingRow.topAnchor.constraint(equalTo: ecoScoreLabel.bottomAnchor, constant: 15),
            brakingRow.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            brakingRow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            brakingRow.heightAnchor.constraint(equalToConstant: 31),

            accelerationRow.topAnchor.constraint(equalTo: brakingRow.bottomAnchor, constant: 10),
            accelerationRow.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            accelerationRow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            accelerationRow.heightAnchor.constraint(equalToConstant: 31),

            corneringRow.topAnchor.constraint(equalTo: accelerationRow.bottomAnchor, constant: 10),
            corneringRow.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            corneringRow.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            corneringRow.heightAnchor.constraint(equalToConstant: 31),
            corneringRow.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func updateContent(ecoScore: Double, braking: Double, acceleration: Double, cornering: Double) {
        let attrValueString = NSMutableAttributedString(string: "\(Int((ecoScore * 100).rounded(0)))", attributes: [.font: UIFont.K2DBold(28)!, .foregroundColor: UIColor.appColor(.chartGreen)])
        let attrFractionString = NSMutableAttributedString(string: "/", attributes: [.font: UIFont.K2DBold(12)!, .foregroundColor: UIColor.white])
        let attrUnitString = NSMutableAttributedString(string: "100", attributes: [.font: UIFont.K2DLight(12), .foregroundColor: UIColor.white])

        attrValueString.append(attrFractionString)
        attrValueString.append(attrUnitString)

        self.ecoScoreLabel.attributedText = attrValueString

        brakingRow.value = braking
        accelerationRow.value = acceleration
        corneringRow.value = cornering
    }

}

private struct Subviews {
    static var ecoHeadingLabel: UILabel {
        let label = UILabel()
        label.text = "dashboard.scores.eco.heading".localized
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white

        return label
    }

    static var infoImageView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "information"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var ecoScoreLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(28)
        label.textColor = .white

        return label
    }

    static var brakingRow: ScoreBreakdownRow {
        let row = ScoreBreakdownRow(type: .braking)
        row.translatesAutoresizingMaskIntoConstraints = false
        return row
    }

    static var accelerationRow: ScoreBreakdownRow {
        let row = ScoreBreakdownRow(type: .acceleration)
        row.translatesAutoresizingMaskIntoConstraints = false
        return row
    }

    static var corneringRow: ScoreBreakdownRow {
        let row = ScoreBreakdownRow(type: .cornering)
        row.translatesAutoresizingMaskIntoConstraints = false
        return row
    }
}
