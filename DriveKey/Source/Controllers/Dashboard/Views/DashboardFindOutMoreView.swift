//
//  DashboardFindOutMoreView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 31/01/2022.
//

import UIKit

class DashboardFindOutMoreView: BaseView {
    private let headingLabel = Subviews.headingLabel
    private let infoImageView = Subviews.infoImageView
    private let descriptionLabel = Subviews.descriptionLabel

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.pastelDarkBlue)

        addSubview(headingLabel)
        addSubview(infoImageView)
        addSubview(descriptionLabel)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            headingLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            infoImageView.heightAnchor.constraint(equalToConstant: 16),
            infoImageView.widthAnchor.constraint(equalToConstant: 16),
            infoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 15),
            infoImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            descriptionLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 5),
            descriptionLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 90)
    }
}

private struct Subviews {
    static var headingLabel: UILabel {
        let label = UILabel()
        label.text = "dashboard.find.out.more.heading".localized
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white

        return label
    }

    static var infoImageView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "information"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var descriptionLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.text = "dashboard.find.out.more.description".localized
        label.textColor = .appColor(.pastelBlue)
        label.numberOfLines = 0

        return label
    }
}
