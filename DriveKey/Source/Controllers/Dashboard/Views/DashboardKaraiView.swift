//
//  DashboardKaraiView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 12/08/2021.
//

import Lottie
import UIKit

class DashboardKaraiView: BaseView {
    private let pulseAnimationView = Subviews.pulseAnimationView
    private let circleView = Subviews.circleView
    private let trackerView = Subviews.trackerView
    private let centerView = Subviews.centerView
    private var trackerProgress: Double = -1
    private var shouldShowPulse = false

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear

        addSubview(pulseAnimationView)
        addSubview(circleView)
        addSubview(trackerView)
        addSubview(centerView)

        setupInitialState()
        
        trackerView.delegate = self
    }

    override func setUpLayout() {
        super.setUpLayout()

        let isBigScreen = UIScreen.main.bounds.width > 320
        let screenWidth = UIScreen.main.bounds.width
        let innerSpaceSize = isBigScreen ? screenWidth * 0.48 : screenWidth * 0.5

        circleView.padding = screenWidth * 0.2
        circleView.topOffset = 0
        
        let circleOffsetY: CGFloat = isBigScreen ? 6 : 4

        let constraints = [
            circleView.topAnchor.constraint(equalTo: topAnchor, constant: -circleOffsetY),
            circleView.leadingAnchor.constraint(equalTo: leadingAnchor),
            circleView.trailingAnchor.constraint(equalTo: trailingAnchor),
            circleView.bottomAnchor.constraint(equalTo: bottomAnchor),
            trackerView.topAnchor.constraint(equalTo: topAnchor, constant: -12),
            trackerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            trackerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            trackerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            pulseAnimationView.topAnchor.constraint(equalTo: topAnchor),
            pulseAnimationView.leadingAnchor.constraint(equalTo: leadingAnchor),
            pulseAnimationView.trailingAnchor.constraint(equalTo: trailingAnchor),
            pulseAnimationView.bottomAnchor.constraint(equalTo: bottomAnchor),
            centerView.widthAnchor.constraint(equalToConstant: innerSpaceSize),
            centerView.heightAnchor.constraint(equalToConstant: innerSpaceSize),
            centerView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -4),
            centerView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func start() {
        playTrackerPulseAnimation()
        updateProgress()
    }

    private func resetAnimation() {
        playTrackerPulseAnimation()
    }

    private func playTrackerPulseAnimation() {
        pulseAnimationView.stop()
        pulseAnimationView.play(fromProgress: 0,
                                toProgress: 1.0,
                                loopMode: .loop,
                                completion: { _ in
            
        }
        )
    }
    
    func updateProgress() {
        runOnMainThreadAsyncAfter(1) {
            if self.trackerProgress >= 0 {
                self.trackerView.progress(self.trackerProgress, animated: true)
            }
        }
    }

    private func setupInitialState() {
        pulseAnimationView.stop()
        pulseAnimationView.transform = .identity
        pulseAnimationView.alpha = 1.0
        
        trackerView.transform = .identity
        trackerView.alpha = 1.0
        
        centerView.transform = .identity
        centerView.alpha = 1.0
    }

    private func performTransitionToTracker() {
        pulseAnimationView.play(fromProgress: 0, toProgress: 1, loopMode: .loop, completion: nil)

        UIView.animate(withDuration: 1.0, delay: 0, options: []) {
            self.pulseAnimationView.transform = .identity
            self.pulseAnimationView.alpha = 1.0
        } completion: { _ in }
        
        UIView.animate(withDuration: 0.8, delay: 0.2, options: []) {
            self.trackerView.transform = .identity
            self.trackerView.alpha = 1.0
        } completion: { _ in }
        
        animatePopAppear(centerView, delay: 0.4)
    }
    
    func progress(_ progress: Double?, animated: Bool) {
        guard let progress = progress else { return }
        trackerView.progress(progress, animated: animated)
    }
    
    private func updatePulseColor(_ state: KaraiCarbonState) {
        var r: CGFloat = 0, g: CGFloat = 0, b: CGFloat = 0
        
        switch state {
        case .noData, .negative:
            shouldShowPulse = false
            updatePulse()
        case .neutral:
            shouldShowPulse = true
            r = 38
            g = 183
            b = 135
        case .positiveSingle, .positiveMulti:
            shouldShowPulse = true
            r = 0
            g = 190
            b = 240
        }
        
        guard g > 0 else { return }
        let color1 = LottieColor(r: (r/255), g: (g/255), b: (b/255), a: 0.7)
        let color2 = LottieColor(r: (r/255), g: (g/255), b: (b/255), a: 1)
        let colorValueProvider1 = ColorValueProvider(color1)
        let colorValueProvider2 = ColorValueProvider(color2)
        
        let colorKeyPath1 = AnimationKeypath(keypath: "Layor_01.Ellipse 1.Fill 1.Color")
        pulseAnimationView.setValueProvider(colorValueProvider1, keypath: colorKeyPath1)
        
        let colorKeyPath2 = AnimationKeypath(keypath: "Layor_02.Ellipse 1.Fill 1.Color")
        pulseAnimationView.setValueProvider(colorValueProvider2, keypath: colorKeyPath2)
        
        let colorKeyPath3 = AnimationKeypath(keypath: "Layor_03.Ellipse 1.Fill 1.Color")
        pulseAnimationView.setValueProvider(colorValueProvider2, keypath: colorKeyPath3)
    }
    
    private func updatePulse() {
        let alpha: CGFloat = shouldShowPulse ? 1 : 0
        if alpha == 1 {
            pulseAnimationView.play()
        }
        
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       options: []) {
            self.pulseAnimationView.alpha = alpha
        } completion: { _ in
            if alpha == 0 {
                self.pulseAnimationView.pause()
            }
        }

    }
    
    var co2Net: Double? {
        didSet {
            guard let value = co2Net else { return }
            centerView.value = value
        }
    }
    
    var progress: Double? {
        didSet {
            guard let value = progress else { return }
            trackerProgress = min(value, 1)
            self.updateProgress()
        }
    }
    
    private func animatePopAppear(_ view: UIView, delay: TimeInterval = 0, durationExpand: TimeInterval = 0.2, durationPop: TimeInterval = 0.3) {
        runOnMainThreadAsyncAfter(delay) {
            UIView.animate(withDuration: durationExpand) {
                view.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                view.alpha = 0
            } completion: { _ in
                view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)

                UIView.animate(withDuration: durationPop) {
                    view.transform = .identity
                    view.alpha = 1
                }
            }
        }
    }
}

extension DashboardKaraiView: KaraiCarbonStateUpdatable {
    func update(_ state: KaraiCarbonState) {
        updatePulseColor(state)
        centerView.update(state)
        trackerView.update(state)
    }
}

extension DashboardKaraiView: DashboardTrackerViewDelegate {
    func dashboardTrackerViewDidFinishAnimation(_ trackerView: DashboardTrackerView, for state: KaraiCarbonState) {
        updatePulse()
    }
}

private struct Subviews {
    static var pulseAnimationView: LottieAnimationView {
        let view = LottieAnimationView(name: "karai-sleep")
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.backgroundBehavior = .pauseAndRestore

        return view
    }

    static var circleView: CircleView {
        let view = CircleView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }
    
    static var trackerView: DashboardTrackerView {
        let view = DashboardTrackerView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }
    
    static var centerView: DashboardKaraiCenterView {
        let view = DashboardKaraiCenterView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }

    static var messageBoundingView: UIView {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .appColor(.navyBlue)
        view.clipsToBounds = true

        return view
    }
}
