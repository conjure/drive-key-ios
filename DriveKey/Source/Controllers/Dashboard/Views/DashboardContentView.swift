//
//  DashboardContentView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 25/01/2022.
//

// swiftlint:disable weak_delegate

import UIKit
import SwiftUI
import Combine

protocol DashboardContentViewDelegate: AnyObject {
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView)
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, vehicle vehicleId: String?)
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, remove tripId: String?)
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, business tripId: String?, newType: TripType?)
    func dashboardContentViewDidTapScores(_ contentView: DashboardContentView)
    func dashboardContentViewDidTapFindOutMore(_ contentView: DashboardContentView)
    func dashboardContentViewDidTapBubble(_ contentView: DashboardContentView, bubbleType: BubbleType)
    func dashboardContentViewDidTap(_ contentView: DashboardContentView, charityProject: CharityProject)
    func dashboardContentViewDidTapMileageSummary(_ contentView: DashboardContentView)
}

class DashboardContentView: BaseView {
    private let itemsStackView = Subviews.itemsStackView
    private let statsView = Subviews.statsView
    private let vehicleRegBubble = Subviews.vehicleRegBubbleView
    private let locationBubble = Subviews.locationPermissionBubbleView
    private let noScoresView = Subviews.noScoresView
    private let scoresView = Subviews.scoresView
    private let findOutMoreView = Subviews.findOutMoreView
    private let mileageView = Subviews.mileageView
    private let lastTripView = Subviews.lastTripView
    private let projectsContainerView = Subviews.projectsContainerView
    private var bottomConstraint: NSLayoutConstraint!

    private var projectCarousel: ProjectCarouselView!
    private let pickerDelegate = ProjectPickerDelegate()
    private var tasks = Set<AnyCancellable>()
    
    weak var delegate: DashboardContentViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear

        pickerDelegate.didChange.sink { [weak self] del in
            guard let project = del.project, let self = self else { return }
            self.delegate?.dashboardContentViewDidTap(self, charityProject: project)
        }.store(in: &tasks)

        itemsStackView.addArrangedSubview(statsView)
        itemsStackView.addArrangedSubview(vehicleRegBubble)
        itemsStackView.addArrangedSubview(locationBubble)
//        itemsStackView.addArrangedSubview(noScoresView)
//        itemsStackView.addArrangedSubview(scoresView)
        itemsStackView.addArrangedSubview(mileageView)
        itemsStackView.addArrangedSubview(lastTripView)
        itemsStackView.addArrangedSubview(projectsContainerView)
        itemsStackView.addArrangedSubview(findOutMoreView)

        vehicleRegBubble.isHidden = true
        locationBubble.isHidden = true
        noScoresView.isHidden = true
        scoresView.isHidden = true
        mileageView.isHidden = true
        lastTripView.isHidden = true
        
        mileageView.delegate = self

        addSubview(itemsStackView)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapRecognizer))
        addGestureRecognizer(tapGestureRecognizer)
 
        lastTripView.onButtonRemove = { [weak self] tripId in
            guard let self = self else { return }
            self.delegate?.dashboardContentViewDidTapLastTrip(self, remove: tripId)
        }
        lastTripView.onButtonVehicle = { [weak self] vehicleId in
            guard let self = self else { return }
            self.delegate?.dashboardContentViewDidTapLastTrip(self, vehicle: vehicleId)
        }
        lastTripView.onButtonBusiness = { [weak self] tripId, newType in
            guard let self = self else { return }
            self.delegate?.dashboardContentViewDidTapLastTrip(self, business: tripId, newType: newType)
        }
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        // space for the "Offset your carbon" button
        bottomConstraint = itemsStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -110)
        
        let constraints = [
            itemsStackView.topAnchor.constraint(equalTo: topAnchor),
            itemsStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            itemsStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            bottomConstraint!,
            projectsContainerView.heightAnchor.constraint(equalToConstant: 220),
            statsView.heightAnchor.constraint(equalToConstant: 50.0)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let bottomOffset: CGFloat = 110 - UIApplication.safeAreaHeightBottom
        bottomConstraint.constant = -bottomOffset
    }

    func updateBubbles(_ bubbles: [Bubble]) {
        let vehicleRegBubbles = bubbles.filter({$0.bubbleType == .vehicleReg})
        let locationBubbles = bubbles.filter({$0.bubbleType == .locationPermission})

        if let bubble = vehicleRegBubbles.first {
            vehicleRegBubble.configure(bubble)
        }
        if let bubble = locationBubbles.first {
            locationBubble.configure(bubble)
        }
        vehicleRegBubble.isHidden = vehicleRegBubbles.isEmpty
        locationBubble.isHidden = locationBubbles.isEmpty
    }

    func updateStats(trips: Int, distance: Int, unit: UnitDistance, delegate: DashboardStatsContainerViewDelegate) {
        self.statsView.updateStats(trips: trips, distance: distance, unit: unit)
        self.statsView.delegate = delegate
    }

    func updateScores(_ scores: [Double]) {
        guard scores.count == 4 else {
            scoresView.isHidden = true
            noScoresView.isHidden = false
            return
        }

        let ecoScore = scores[0]
        let braking = scores[1]
        let acceleration = scores[2]
        let cornering = scores[3]

        scoresView.updateContent(ecoScore: ecoScore, braking: braking, acceleration: acceleration, cornering: cornering)
        noScoresView.isHidden = true
        scoresView.isHidden = false
    }
    
    func updateMileageSummary(_ data: MileageSummary?, unit: UnitDistance) {
        guard let summary = data, summary.months?.isEmpty == false else {
            mileageView.isHidden = true
            return
        }

        mileageView.update(summary, unit: unit)
        mileageView.isHidden = false
    }
    
    func updateLastTrip(_ trip: Trip?, unit: UnitDistance) {
        guard let trip = trip else {
            lastTripView.isHidden = true
            return
        }

        lastTripView.update(trip, unit: unit)
        lastTripView.isHidden = false
    }

    func updateProjects() {
        if projectCarousel == nil {
            if let parentVC = self.findViewController() {
                projectCarousel = ProjectCarouselView(heading: "projects.carousel.heading.dashboard".localized,
                                                      model: .init(charityProject: nil),
                                                      delegate: pickerDelegate)
                parentVC.addSwiftUIView(projectCarousel, to: projectsContainerView, hideNavBar: true)
            } else {
                print("[DashboardContentView] Can't find view controller")
            }
        }
    }
    
    @objc
    func onTapRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        let location = gestureRecognizer.location(in: self)
        
        if !lastTripView.isHidden, lastTripView.frame.contains(location) {
            delegate?.dashboardContentViewDidTapLastTrip(self)
        } else if !noScoresView.isHidden, noScoresView.frame.contains(location) {
            delegate?.dashboardContentViewDidTapScores(self)
        } else if !scoresView.isHidden, scoresView.frame.contains(location) {
            delegate?.dashboardContentViewDidTapScores(self)
        } else if findOutMoreView.frame.contains(location) {
            delegate?.dashboardContentViewDidTapFindOutMore(self)
        } else if !vehicleRegBubble.isHidden, vehicleRegBubble.frame.contains(location) {
            delegate?.dashboardContentViewDidTapBubble(self, bubbleType: .vehicleReg)
        } else if !locationBubble.isHidden, locationBubble.frame.contains(location) {
            delegate?.dashboardContentViewDidTapBubble(self, bubbleType: .locationPermission)
        }
    }
}

private struct Subviews {
    static var itemsStackView: UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.spacing = 15
        
        return stackView
    }

    static var vehicleRegBubbleView: DashboardBubbleView {
        let view = DashboardBubbleView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    static var locationPermissionBubbleView: DashboardBubbleView {
        let view = DashboardBubbleView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    static var statsView: DashboardStatsContainerView {
        let view = DashboardStatsContainerView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    static var scoresView: DashboardScoresView {
        let view = DashboardScoresView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }

    static var noScoresView: DashboardNoScoreView {
        let view = DashboardNoScoreView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }

    static var mileageView: DashboardMileageView {
        let view = DashboardMileageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var lastTripView: DashboardLastTripView {
        let lastTripView = DashboardLastTripView(frame: CGRect.zero)
        lastTripView.translatesAutoresizingMaskIntoConstraints = false
        return lastTripView
    }

    static var projectsContainerView: UIView {
        let container = UIView(frame: CGRect.zero)
        container.backgroundColor = .clear
        container.translatesAutoresizingMaskIntoConstraints = false
        return container
    }

    static var findOutMoreView: DashboardFindOutMoreView {
        let view = DashboardFindOutMoreView(frame: .zero)
        view.layer.cornerRadius = 10
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}

extension DashboardContentView: DashboardMileageViewDelegate {
    func dashboardMileageViewDidTap(_ mileageView: DashboardMileageView) {
        delegate?.dashboardContentViewDidTapMileageSummary(self)
    }
}
