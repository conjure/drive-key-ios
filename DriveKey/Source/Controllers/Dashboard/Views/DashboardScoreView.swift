//
//  DashboardScoreView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 28/01/2022.
//

import UIKit

class DashboardScoreView: BaseView {
    private let roundedView = Subviews.roundedView
    private let scoreLabel = Subviews.scoreLabel
    private let infoLabel = Subviews.infoLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(roundedView)
        addSubview(scoreLabel)
        addSubview(infoLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            roundedView.topAnchor.constraint(equalTo: topAnchor),
            roundedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundedView.widthAnchor.constraint(equalToConstant: 90),
            roundedView.bottomAnchor.constraint(equalTo: bottomAnchor),
            scoreLabel.topAnchor.constraint(equalTo: roundedView.topAnchor),
            scoreLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor),
            scoreLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor),
            scoreLabel.bottomAnchor.constraint(equalTo: roundedView.bottomAnchor),
            infoLabel.topAnchor.constraint(equalTo: topAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: 10),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var value: Double? {
        didSet {
            guard let score = value, score > 0 else {
                scoreLabel.attributedText = nil
                scoreLabel.text = "dashboard.eco.score.no.score".localized
                return
            }
            
            let attrValueString = NSMutableAttributedString(string: "\(Int((score * 100).rounded(0)))", attributes: [.font: UIFont.K2DBold(28)!, .foregroundColor: UIColor.white])
            let attrFractionString = NSMutableAttributedString(string: "/", attributes: [.font: UIFont.K2DBold(12)!, .foregroundColor: UIColor.white])
            let attrUnitString = NSMutableAttributedString(string: "100", attributes: [.font: UIFont.K2DLight(12), .foregroundColor: UIColor.white])
            attrValueString.append(attrFractionString)
            attrValueString.append(attrUnitString)
            
            scoreLabel.attributedText = attrValueString
        }
    }
}

private struct Subviews {
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        view.backgroundColor = .appColor(.pastelDarkBlue)

        return view
    }
    
    static var scoreLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(28)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 1

        return label
    }
    
    static var infoLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(12)
        label.textColor = .appColor(.pastelBlue)
        label.textAlignment = .left
        label.numberOfLines = 2
        label.text = "dashboard.eco.score.info.label".localized

        return label
    }
}
