//
//  DashboardBubbleView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 08/02/2022.
//

import UIKit

class DashboardBubbleView: BaseView {
    private let labelStackView = Subviews.itemsStackView
    private let headingLabel = Subviews.headingLabel
    private let descriptionLabel = Subviews.descriptionLabel
    private let imageView = Subviews.iconView

    private var imageSize: CGFloat {
        UIScreen.main.bounds.height <= 667 ? 48 : 56
    }

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.pastelBlue).withAlphaComponent(0.25)
        addSubview(imageView)

        labelStackView.addArrangedSubview(headingLabel)
        labelStackView.addArrangedSubview(descriptionLabel)

        addSubview(labelStackView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: imageSize),
            imageView.widthAnchor.constraint(equalToConstant: imageSize),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 14),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20),

            labelStackView.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 8),
            labelStackView.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
            labelStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -14)
        ])
    }

    func configure(_ bubble: Bubble) {
        self.headingLabel.text = bubble.title
        self.descriptionLabel.attributedText = bubble.message.subscriptCO2(.K2DRegular(textSize * 0.5)!)
        self.imageView.image = UIImage(named: bubble.imageName)
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 90)
    }
}

private struct Subviews {
    static var itemsStackView: UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing

        return stackView
    }

    static var headingLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DBold(textSize)
        label.textColor = .white
        label.textAlignment = .left

        return label
    }

    static var descriptionLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DRegular(textSize)
        label.textColor = .appColor(.pastelBlue)
        label.numberOfLines = 2

        return label
    }

    static var iconView: UIImageView {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}

private var textSize: CGFloat {
    UIScreen.main.bounds.height <= 667 ? 14 : 16
}
