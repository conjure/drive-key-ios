//
//  DashboardStatsContainerView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 26/01/2022.
//

import UIKit

enum DashboardStat {
    case trips
    case miles
}

protocol DashboardStatsContainerViewDelegate: AnyObject {
    func dashboardStatsContainerView(_ view: DashboardStatsContainerView, didTap stat: DashboardStat)
}

class DashboardStatsContainerView: BaseView {
    private let itemsStackView = Subviews.itemsStackView
    private let tripsView = Subviews.tripsView
    private let milesView = Subviews.distanceView

    weak var delegate: DashboardStatsContainerViewDelegate?

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        itemsStackView.addArrangedSubview(tripsView)
        itemsStackView.addArrangedSubview(milesView)

        addSubview(itemsStackView)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapView))
        tripsView.addGestureRecognizer(tapGestureRecognizer)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            itemsStackView.topAnchor.constraint(equalTo: topAnchor),
            itemsStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            itemsStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            itemsStackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func updateStats(trips: Int, distance: Int, unit: UnitDistance) {
        self.tripsView.updateContent(value: "\(trips)", unit: trips == 1 ? "dashboard.stats.trip".localized : "dashboard.stats.trips".localized, shouldHideInfoIcon: trips < 1)
        self.milesView.updateContent(value: "\(distance)", unit: distance == 1 ? unit.localized.capitalizingFirstLetter() : unit.localizedPlural.capitalizingFirstLetter(), shouldHideInfoIcon: true)
    }

    @objc private func didTapView(_ sender: UITapGestureRecognizer) {
        if sender.view == tripsView {
            self.delegate?.dashboardStatsContainerView(self, didTap: .trips)
        } else if sender.view == milesView {
            self.delegate?.dashboardStatsContainerView(self, didTap: .trips)
        }
    }
}

private struct Subviews {
    static var itemsStackView: UIStackView {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 12

        return stackView
    }

    static var tripsView: DashboardStatsView {
        let view = DashboardStatsView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }

    static var distanceView: DashboardStatsView {
        let view = DashboardStatsView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 10
        return view
    }
}
