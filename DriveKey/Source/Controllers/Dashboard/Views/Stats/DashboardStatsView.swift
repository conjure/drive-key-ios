//
//  DashboardStatsView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 26/01/2022.
//

import UIKit

class DashboardStatsView: BaseView {
    private let valueLabel = Subviews.valueLabel
    private let imageView = Subviews.infoImageView

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.pastelDarkBlue)

        addSubview(valueLabel)
        addSubview(imageView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            valueLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            valueLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.heightAnchor.constraint(equalToConstant: 16),
            imageView.widthAnchor.constraint(equalToConstant: 16),
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func updateContent(value: String, unit: String, shouldHideInfoIcon: Bool) {
        let attrValueString = NSMutableAttributedString(string: value, attributes: [.font: UIFont.K2DMedium(22), .foregroundColor: UIColor.white])
        let attrUnitString = NSMutableAttributedString(string: "  " + unit, attributes: [.font: UIFont.K2DMedium(12), .foregroundColor: UIColor.white.withAlphaComponent(0.5)])

        attrValueString.append(attrUnitString)

        self.valueLabel.attributedText = attrValueString
        self.imageView.isHidden = shouldHideInfoIcon
    }
}

private struct Subviews {
    static var valueLabel: UILabel {
        let label = UILabel()
        label.font = .K2DMedium(22)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    static var infoImageView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "information"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}
