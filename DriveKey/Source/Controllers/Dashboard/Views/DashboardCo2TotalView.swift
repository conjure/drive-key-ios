//
//  DashboardCo2TotalView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 31/01/2022.
//

import UIKit

class DashboardCo2TotalView: BaseView {
    private let squareView = Subviews.squareView
    private let valueLabel = Subviews.valueLabel
    private let infoLabel = Subviews.infoLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(squareView)
        addSubview(valueLabel)
        addSubview(infoLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            squareView.topAnchor.constraint(equalTo: topAnchor),
            squareView.leadingAnchor.constraint(equalTo: leadingAnchor),
            squareView.widthAnchor.constraint(equalToConstant: 8),
            squareView.heightAnchor.constraint(equalToConstant: 8),
            valueLabel.topAnchor.constraint(equalTo: squareView.bottomAnchor, constant: 5),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.topAnchor.constraint(equalTo: valueLabel.bottomAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var value: Double? {
        didSet {
            let valueDouble = value != nil ? value! : 0
            let valueStr = valueDouble.formattedCO2 + "dashboard.co2.value.unit".localized
            valueLabel.text = valueStr
        }
    }
}

private struct Subviews {
    static var squareView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 1.6
        view.backgroundColor = .white

        return view
    }
    
    static var valueLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(16)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5

        return label
    }
    
    static var infoLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 1
        label.text = "dashboard.co2.total.info.label".localized

        return label
    }
}
