//
//  DashboardMileageView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/09/2022.
//

import UIKit
import DGCharts

protocol DashboardMileageViewDelegate: AnyObject {
    func dashboardMileageViewDidTap(_ mileageView: DashboardMileageView)
}

class DashboardMileageView: BaseView {
    private let roundedView = Subviews.roundedView
    private let iconImageView = Subviews.iconImageView
    private let titleLabel = Subviews.titleLabel
    private let chartView = Subviews.chartView
    
    weak var delegate: DashboardMileageViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(roundedView)
        addSubview(iconImageView)
        addSubview(titleLabel)
        addSubview(chartView)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTap))
        addGestureRecognizer(tapRecognizer)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            roundedView.topAnchor.constraint(equalTo: topAnchor),
            roundedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            roundedView.heightAnchor.constraint(equalToConstant: 296),
            roundedView.bottomAnchor.constraint(equalTo: bottomAnchor),
            iconImageView.topAnchor.constraint(equalTo: topAnchor, constant: 14),
            iconImageView.widthAnchor.constraint(equalToConstant: 24),
            iconImageView.heightAnchor.constraint(equalToConstant: 24),
            iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12),
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            chartView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 50),
            chartView.leadingAnchor.constraint(equalTo: leadingAnchor),
            chartView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chartView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -50)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    func update(_ summary: MileageSummary, unit: UnitDistance) {
        chartView.update(summary, unit: unit)
    }
    
    @objc
    func onTap() {
        delegate?.dashboardMileageViewDidTap(self)
    }
}

private struct Subviews {
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor.appColor(.pastelDarkBlue)
        
        return view
    }
    
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DBold(16)
        label.textColor = .white
        label.textAlignment = .left
        label.text = "dashboard.mileage.title".localized
        
        return label
    }
    
    static var chartView: DashboardMileageChartView {
        let view = DashboardMileageChartView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var iconImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "trip-suitcase-icon")
        
        return view
    }
}
