//
//  DashboardMileageChartView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/09/2022.
//

import UIKit
import DGCharts

class DashboardMileageChartView: BaseView {
    private let chartView = Subviews.chartView
    private let separatorView = Subviews.separatorView
    private let noDataLabel = Subviews.noDataLabel

    private(set) var labelsTop = [DashboardMileageTotalView]()
    private(set) var lines = [CAShapeLayer]()
    private(set) var labelsBottom = [UILabel]()
    private let monthsRange: Int = 3
    private let monthFormatter = DateFormatter()
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(chartView)
        addSubview(separatorView)
        addSubview(noDataLabel)
        
        monthFormatter.dateFormat = "MMMM"
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            chartView.topAnchor.constraint(equalTo: topAnchor),
            chartView.leadingAnchor.constraint(equalTo: leadingAnchor),
            chartView.trailingAnchor.constraint(equalTo: trailingAnchor),
            chartView.bottomAnchor.constraint(equalTo: bottomAnchor),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            noDataLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            noDataLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -40)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    func update(_ summary: MileageSummary, unit: UnitDistance) {
        let monthsDate = summary.months?.filter({ $0.startAt != nil })
        
        let today = Date()
        let calendar = Calendar.current
        var entries = [ChartDataEntry]()
        var months = [MonthValueSummary]()
        for i in 0..<monthsRange {
            let date = calendar.date(byAdding: .month, value: -(monthsRange - i - 1), to: today) ?? Date()
            let dateComponents = date.get(.month, .year)
            let monthData = monthsDate?.first(where: { $0.startDate!.get(.month, .year) == dateComponents })
            
            let caption = monthFormatter.string(from: date)
            var miles: Double = 0
            if let data = monthData {
                miles = data.miles ?? 0
            }
            let entry = ChartDataEntry(x: Double(i) + 0.5, y: miles)
            entry.data = caption
            entries.append(entry)
            
            if let data = monthData, let total = data.total, let code = data.currencyCode, let distance = data.distance(unit) {
                let distanceValueStr = NumberFormatter.distance.string(from: NSNumber(value: distance)) ?? ""
                let distanceStr = distanceValueStr + unit.localizedShort
                let monthSummary = MonthValueSummary(currencyCode: code, distance: distanceStr, total: total)
                months.append(monthSummary)
            } else {
                let monthSummary = MonthValueSummary(currencyCode: nil, distance: nil, total: nil)
                months.append(monthSummary)
            }
        }
        
        var entriesSides = [ChartDataEntry]()
        let entryLeft = ChartDataEntry(x: 0, y: entries[0].y)
        let entryRight = ChartDataEntry(x: 3, y: entries[2].y)
        entriesSides.append(entryLeft)
        entriesSides.append(contentsOf: entries)
        entriesSides.append(entryRight)

        let dataSetPoints = LineChartDataSet(entries: entriesSides)
        chartView.leftAxis.axisMaximum = dataSetPoints.yMax + 5
        dataSetPoints.fillAlpha = 1
        dataSetPoints.isDrawLineWithGradientEnabled = false
        dataSetPoints.lineDashLengths = nil
        dataSetPoints.highlightLineDashLengths = nil
        dataSetPoints.setColors(.appColor(.chartFlashBlue))
        dataSetPoints.drawCirclesEnabled = false
        dataSetPoints.lineWidth = 2
        dataSetPoints.drawCircleHoleEnabled = false
        dataSetPoints.drawValuesEnabled = false
        dataSetPoints.mode = .horizontalBezier

        // Check for actual data in past 3 months
        let past3MonthsTotals = months.compactMap({$0.total})

        if past3MonthsTotals.isEmpty {
            // Show no data
            noDataLabel.isHidden = false
        } else {
            noDataLabel.isHidden = true
            let gradientColors = [
                UIColor.appColor(.chartBlue).withAlphaComponent(0.17).cgColor,
                UIColor.appColor(.chartBlue).cgColor
            ]
            let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
            dataSetPoints.fill = LinearGradientFill(gradient: gradient, angle: 90)
            dataSetPoints.drawFilledEnabled = true
        }
        let data = LineChartData(dataSet: dataSetPoints)
        chartView.data = data
        chartView.setVisibleXRangeMaximum(4)
        addTopLabels(entries, rates: months)
        addBottomLabels(entries)
    }
    
    private func addTopLabels(_ data: [ChartDataEntry], rates: [MonthValueSummary]) {
        guard data.count == rates.count else { return }
        labelsTop.forEach({ $0.removeFromSuperview() })
        labelsTop.removeAll()
        lines.forEach({ $0.removeFromSuperlayer() })
        lines.removeAll()
        
        for i in 0..<data.count {
            let entry = data[i]
            let monthSummary = rates[i]
            
            let pos = chartView.pixelForValues(x: entry.x, y: entry.y, axis: .left)
            let labelWidth: CGFloat = 80
            let label = DashboardMileageTotalView(frame: CGRect(x: pos.x - labelWidth * 0.5, y: pos.y - 55, width: labelWidth, height: 40))
            label.data = monthSummary
            addSubview(label)
            labelsTop.append(label)
            
            if monthSummary.total != nil {
                addLine(pos)
            }
        }
    }
    
    private func addLine(_ end: CGPoint) {
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = UIColor.white.withAlphaComponent(0.6).cgColor
        lineLayer.lineWidth = 1
        lineLayer.lineDashPattern = [2, 2]
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: end.x, y: bounds.height), CGPoint(x: end.x, y: end.y - 1)])
        lineLayer.path = path
        
        layer.addSublayer(lineLayer)
        lines.append(lineLayer)
    }
    
    private func addBottomLabels(_ data: [ChartDataEntry]) {
        labelsBottom.forEach({ $0.removeFromSuperview() })
        labelsBottom.removeAll()

        var caption = ""
        for entry in data {
            let pos = chartView.pixelForValues(x: entry.x, y: entry.y, axis: .left)
            if let labelName = entry.data as? String {
                caption = labelName
            }
            addLabel(pos.x, text: caption)
        }
    }
    
    private func addLabel(_ posX: CGFloat, text: String) {
        guard !posX.isNaN else { return }
        
        let labelWidth: CGFloat = 80
        let label = UILabel(frame: CGRect(x: posX - labelWidth * 0.5, y: chartView.bounds.height + 12, width: labelWidth, height: 20))
        label.backgroundColor = .clear
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textAlignment = .center
        label.alpha = 1
        label.text = text
        label.textColor = .white
        
        addSubview(label)
        labelsBottom.append(label)
    }
}

private struct Subviews {    
    static var chartView: LineChartView {
        let chartView = LineChartView(frame: .zero)
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.isUserInteractionEnabled = false
        
        chartView.legend.enabled = false
        chartView.chartDescription.enabled = false
        chartView.dragEnabled = false
        chartView.maxVisibleCount = 5
        chartView.xAxis.enabled = false
        chartView.xAxis.drawAxisLineEnabled = true
        chartView.xAxis.drawGridLinesBehindDataEnabled = false
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawLimitLinesBehindDataEnabled = false
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.leftAxis.axisMinimum = 0
        chartView.leftAxis.axisMaximum = 100
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = false
        chartView.rightAxis.enabled = false
        chartView.noDataText = ""
        chartView.minOffset = 0
        
        return chartView
    }
    
    static var separatorView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .appColor(.chartBlue).withAlphaComponent(0.2)

        return view
    }

    static var noDataLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.text = "dashboard.mileage.no.trips".localized
        label.font = .K2DRegular(14)!
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
}
