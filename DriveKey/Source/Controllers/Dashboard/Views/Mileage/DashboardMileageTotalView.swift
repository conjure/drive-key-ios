//
//  DashboardMileageTotalView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/09/2022.
//

import UIKit

class DashboardMileageTotalView: BaseView {
    private let totalLabel = Subviews.totalLabel
    private let distanceLabel = Subviews.distanceLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(totalLabel)
        addSubview(distanceLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            totalLabel.topAnchor.constraint(equalTo: topAnchor),
            totalLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            totalLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            totalLabel.heightAnchor.constraint(equalToConstant: 18),
            distanceLabel.topAnchor.constraint(equalTo: totalLabel.bottomAnchor),
            distanceLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            distanceLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            distanceLabel.heightAnchor.constraint(equalToConstant: 18)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: MonthValueSummary? {
        didSet {
            guard let data = data else { return }
            
            if let code = data.currencyCode, let total = data.total {
                totalLabel.text = total.formatForCurrency(code)
            }
            
            if let distance = data.distance {
                distanceLabel.text = distance
            } else {
                distanceLabel.text = "--"
            }
        }
    }
}

private struct Subviews {
    static var totalLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 1
        
        return label
    }
    
    static var distanceLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 1
        
        return label
    }
}
