//
//  DashboardLastTripView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 25/01/2022.
//

import UIKit
import MapKit

class DashboardLastTripView: BaseView {
    private let roundedView = Subviews.roundedView
    private let nameLabel = Subviews.nameLabel
    private let estimatedRateView = Subviews.estimatedRateView
    private let mapView = Subviews.mapView
    private let statsView = Subviews.statsView
    private let timeStartLabel = Subviews.timeLabel
    private let timeStopLabel = Subviews.timeLabel
    private let toolbarView = Subviews.toolbarView
    private let timeFormatter = DateFormatter()
    
    private var vehicleId: String?
    private var tripId: String?
    private var tripType: TripType?
    
    var onButtonRemove: ((String?) -> Void)?
    var onButtonVehicle: ((String?) -> Void)?
    var onButtonBusiness: ((String?, TripType?) -> Void)?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(roundedView)
        addSubview(nameLabel)
        addSubview(statsView)
        addSubview(timeStartLabel)
        addSubview(timeStopLabel)
        addSubview(mapView)
        addSubview(toolbarView)
        roundedView.addSubview(estimatedRateView)
        
        timeFormatter.dateFormat = "HH:mm"
        
        mapView.style = "map-style.json"
        
        toolbarView.delegate = self
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            roundedView.topAnchor.constraint(equalTo: topAnchor),
            roundedView.leadingAnchor.constraint(equalTo: leadingAnchor),
            roundedView.trailingAnchor.constraint(equalTo: trailingAnchor),
            roundedView.bottomAnchor.constraint(equalTo: bottomAnchor),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 14),
            nameLabel.heightAnchor.constraint(equalToConstant: 20),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            nameLabel.trailingAnchor.constraint(equalTo: estimatedRateView.leadingAnchor, constant: -1),
            estimatedRateView.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            estimatedRateView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -16),
            estimatedRateView.heightAnchor.constraint(equalToConstant: 26),
            estimatedRateView.widthAnchor.constraint(greaterThanOrEqualToConstant: 80),
            mapView.topAnchor.constraint(equalTo: estimatedRateView.bottomAnchor, constant: 14),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            mapView.heightAnchor.constraint(equalToConstant: 218),
            statsView.topAnchor.constraint(equalTo: mapView.bottomAnchor, constant: 14),
            statsView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            statsView.heightAnchor.constraint(equalToConstant: 48),
            statsView.trailingAnchor.constraint(equalTo: timeStartLabel.leadingAnchor, constant: -10),
            timeStartLabel.topAnchor.constraint(equalTo: statsView.topAnchor, constant: 1),
            timeStartLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            timeStartLabel.heightAnchor.constraint(equalToConstant: 20),
            timeStopLabel.bottomAnchor.constraint(equalTo: statsView.bottomAnchor, constant: -5),
            timeStopLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            timeStopLabel.heightAnchor.constraint(equalToConstant: 20),
            toolbarView.topAnchor.constraint(equalTo: statsView.bottomAnchor, constant: 16),
            toolbarView.leadingAnchor.constraint(equalTo: leadingAnchor),
            toolbarView.trailingAnchor.constraint(equalTo: trailingAnchor),
            toolbarView.heightAnchor.constraint(equalToConstant: 40),
            toolbarView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    func update(_ trip: Trip, unit: UnitDistance) {
        vehicleId = trip.vehicleId
        tripId = trip.id
        tripType = trip.tripType
        
        nameLabel.text = trip.name
        estimatedRateView.updateView(with: trip.co2EstPrice, currency: trip.estimatedRate?.currencyCode)
        statsView.update(trip, unit: unit)
        toolbarView.businessHighlighted = trip.tripType == .business
        
        if trip.coordinates.count > 1 {
            mapView.updateLocation(trip.coordinates)
        } else if let start = trip.startCoordinate, let end = trip.endCoordinate {
            mapView.updateLocation([start, end])
        }
        
        if let start = trip.startDate, let end = trip.endDate {
            let start = timeFormatter.string(from: start)
            let end = timeFormatter.string(from: end)
            updateTime(start, stop: end)
        }
    }
    
    func updateTime(_ start: String, stop: String) {
        timeStartLabel.text = start
        timeStopLabel.text = stop
    }
    
    private func onRemove() {
        onButtonRemove?(tripId)
    }
    
    private func onVehicle() {
        onButtonVehicle?(vehicleId)
    }
    
    private func onBusiness() {
        let newType: TripType = tripType == .personal ? .business : .personal
        onButtonBusiness?(tripId, newType)
    }
}

private struct Subviews {
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor.appColor(.pastelDarkBlue)

        return view
    }
    
    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(16)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }
    
    static var estimatedRateView: EstimatedTotalView {
        let view = EstimatedTotalView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
    static var statsView: TripStatsView {
        let view = TripStatsView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
    
    static var timeLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .right
        
        return label
    }
    
    static var mapView: TripMapView {
        let view = TripMapView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.isUserInteractionEnabled = false
        
        return view
    }
    
    static var toolbarView: TripToolbarView {
        let view = TripToolbarView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }
}

extension DashboardLastTripView: TripToolbarViewDelegate {
    func tripToolbarViewDidTapRemove(_ toolbarView: TripToolbarView) {
        onRemove()
    }
    
    func tripToolbarViewDidTapVehicle(_ toolbarView: TripToolbarView) {
        onVehicle()
    }
    
    func tripToolbarViewDidTapBusiness(_ toolbarView: TripToolbarView) {
        onBusiness()
    }
}
