//
//  DashboardStatusViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/12/2023.
//

import UIKit

class DashboardStatusViewController: ViewController {
    @IBOutlet private var textView: UITextView!
    
    var api: APIProtocol!
    var location: LocationProvidable!
    var motion: MotionActivityProvidable!
    var user: UserProvidable!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.isEditable = false
        textView.isUserInteractionEnabled = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showData()
    }

    private func showData() {
        textView.text = statusDescription
    }

    var statusDescription: String {
        var result = "Location"
        result += "\n"
        result += "Always: \(location.isAuthorizedAlways)"
        result += "\n"
        result += "When In Use: \(location.isAuthorizedWhenInUse)"
        result += "\n"
        result += "Precise: \(location.isPreciseLocation)"
        result += "\n"
        result += "\n"
        result += "Motion Activity"
        result += "\n"
        result += "Authorized: \(motion.isAuthorized)"
        result += "\n"
        result += "\n"
        result += "Trip Detection"
        result += "\n"
        result += "Modes: \(TripsMonitor.Settings.modes.map({ $0.rawValue }))"
        result += "\n"
        result += "Activity Count: \(TripsMonitor.Settings.activityCount)"
        result += "\n"
        result += "\n"
        result += "\n"
        result += "\n"
        result += "App"
        result += "\n"
        result += "User ID: \(user.currentUser?.uid ?? "NIL")"
        result += "\n"
        result += "Version: \(Bundle.appVersion)"
        result += "\n"
        return result
    }
}
