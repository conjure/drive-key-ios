//
//  DashboardLogsViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/12/2023.
//

import UIKit

class DashboardLogsViewController: ViewController {
    @IBOutlet private var textView: UITextView!
    @IBOutlet private var shareButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView.isEditable = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textView.text = Logger.shared.logs.joined(separator: "\n")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.scrollToBottom()
    }
    
    @IBAction
    func onShare() {
        if let parent = navigationController?.viewControllers.first(where: { $0 is DashboardDebugViewController }) {
            (parent as! DashboardDebugViewController).onLogsShare()
        }
    }
}

extension UITextView {
    func scrollToBottom() {
        guard !text.isEmpty else { return }
        let bottom = contentSize.height - bounds.size.height
        guard bottom > 0 else { return }
        setContentOffset(CGPoint(x: 0, y: bottom), animated: true)
    }
}
