//
//  DashboardDebugViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/08/2021.
//

import UIKit

protocol DashboardDebugViewControllerDelegate: AnyObject {
    func dashboardDebugViewController(_ viewController: DashboardDebugViewController, didTapShare item: URL)
}

class DashboardDebugViewController: ViewController {
    @IBOutlet private var contentView: UIView!
    weak var delegate: DashboardDebugViewControllerDelegate?
    private var toggleButton: UIBarButtonItem!
    
    private let toggleLogs = "Logs"
    private let toggleStatus = "Status"
    
    var api: APIProtocol!
    var location: LocationProvidable!
    var motion: MotionActivityProvidable!
    var user: UserProvidable!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Debug"
        
        let motionButton = UIBarButtonItem(title: "Motion Data", style: .done, target: self, action: #selector(onMotionShare))
        navigationItem.rightBarButtonItem  = motionButton
        
        toggleButton = UIBarButtonItem(title: toggleLogs, style: .done, target: self, action: #selector(onToggle))
        navigationItem.leftBarButtonItem  = toggleButton

        #if DEBUG
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapRecognizer))
        tapRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapRecognizer)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressRecognizer))
        longPressRecognizer.minimumPressDuration = 2
        view.addGestureRecognizer(longPressRecognizer)
        #endif
        
        showStatus()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logDetectionStatus()
    }
    
    private func showStatus() {
        let statusViewController: DashboardStatusViewController = UIStoryboard.instantiateViewController()
        statusViewController.api = api
        statusViewController.location = location
        statusViewController.motion = motion
        statusViewController.user = user
        add(child: statusViewController, to: contentView, animated: false)
    }
    
    private func showLogs() {
        let logsViewController: DashboardLogsViewController = UIStoryboard.instantiateViewController()
        add(child: logsViewController, to: contentView, animated: false)
    }
    
    @objc
    func onToggle() {
        if toggleButton.title == toggleLogs {
            toggleButton.title = toggleStatus
            showLogs()
        } else {
            toggleButton.title = toggleLogs
            showStatus()
        }
    }
    
    @objc
    func onMotionShare() {
        let itemURL = MotionDataLogger.shared.generateCSV()
        delegate?.dashboardDebugViewController(self, didTapShare: itemURL)
    }
    
    func onLogsShare() {
        let itemURL = Logger.shared.generateTXT()
        Logger.shared.reset()
        delegate?.dashboardDebugViewController(self, didTapShare: itemURL)
    }
    
    private func logDetectionStatus() {
        let sceneDelegate = UIApplication.appWindow?.windowScene?.delegate as? SceneDelegate
        sceneDelegate?.logDetectionStatus()
    }

    @objc
    func onTapRecognizer() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc
    func onLongPressRecognizer(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .ended {
            let sceneDelegate = UIApplication.appWindow?.windowScene?.delegate as? SceneDelegate
            let tripsMonitor = sceneDelegate?.appCoordinator?.dependencies.tripsMonitor
            //tripsMonitor?.dummyCreateTrip(.cycling)
            tripsMonitor?.dummyUploadTrip(.cycling)
        }
    }
}

extension UIBackgroundRefreshStatus: CustomStringConvertible {
    public var description: String {
        switch self {
        case .restricted:
            return "Restricted"
        case .denied:
            return "Denied"
        case .available:
            return "Available"
        @unknown default:
            return "Unknown"
        }
    }
}
