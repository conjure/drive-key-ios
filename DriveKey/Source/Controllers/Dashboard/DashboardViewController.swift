//
//  DashboardViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 09/08/2021.
//

import Combine
import UIKit
import Pulley

protocol DashboardViewControllerDelegate: AnyObject {
    func dashboardViewControllerWillPresentProfile(_ vc: DashboardViewController)
    func dashboardViewControllerDidDidTapInfo(_ viewController: DashboardViewController)
    func dashboardViewControllerDidTripleTap(_ viewController: DashboardViewController)
    func dashboardViewControllerWillPresentTrips(_ vc: DashboardViewController)
    func dashboardViewControllerWillPresent(_ vc: DashboardViewController, info: InfoSection?)
    func dashboardViewControllerWillPresentPayment(_ vc: DashboardViewController)
    func dashboardViewControllerWillBubble(_ vc: DashboardViewController, bubbleType: BubbleType)
    func dashboardViewControllerDidTap(_ viewController: DashboardViewController, charityProject: CharityProject)
    func dashboardViewControllerDidDidTapAddVehicle(_ viewController: DashboardViewController)
    func dashboardViewControllerDidDidTapMileageSummary(_ viewController: DashboardViewController)
}

class DashboardViewController: ViewController, TrayPresentable {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var headerView: DashboardHeaderView!
    @IBOutlet private var karaiView: DashboardKaraiView!
    @IBOutlet private var karaiOverlayView: DashboardKaraiOverlayView!
    @IBOutlet private var dashboardContentView: DashboardContentView!
    @IBOutlet private var offsetCarbonButton: RoundedButton!
    
    @IBOutlet private var karaiViewEqualWidth: NSLayoutConstraint!
    @IBOutlet private var karaiOverlayViewHeightProportion: NSLayoutConstraint!
    @IBOutlet private var karaiOverlayViewSmallHeightProportion: NSLayoutConstraint!
    
    var viewModel: DashboardViewModel!
    var carbonStateProvider: DashboardCarbonStateProvider!
    weak var delegate: DashboardViewControllerDelegate?
    private var tasks = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.delegate = self
        
        scrollView.backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        dashboardContentView.delegate = self
        
        if UIScreen.main.bounds.width <= 320 {
            karaiViewEqualWidth.constant = -32 // 10% smaller
            karaiOverlayViewHeightProportion.isActive = false
            karaiOverlayViewSmallHeightProportion.isActive = true
        }
        
        karaiOverlayView.delegate = self
        
        carbonStateProvider.register(karaiOverlayView)
        carbonStateProvider.register(karaiView)
        
        registerSubscriptions()
        
        runOnMainThreadAsyncAfter(0.1) {
            self.karaiView.start()
        }
        
        runOnMainThreadAsyncAfter(1) {
            self.viewModel.updateFCMToken()
        }
        
        offsetCarbonButton.setTitle("offset.carbon.button.title".localized, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willUpdateHeader),
                                               name: .closedDrawerNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willUpdateHeader),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapRecognizer))
        tapRecognizer.numberOfTapsRequired = 3
        view.addGestureRecognizer(tapRecognizer)
    }
    
    private func registerSubscriptions() {
        viewModel.userService.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateDashboard()
            }
            .store(in: &tasks)
        
        viewModel.bubbleProvider.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateDashboard()
            }
            .store(in: &tasks)
        
        viewModel.$vehicleChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
            }
            .store(in: &tasks)
        
        viewModel.$tripRemoved
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.showRemoveTripConfirmation()
                }
            }
            .store(in: &tasks)
        
        viewModel.$tripChanged
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.hideSpinner()
            }
            .store(in: &tasks)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &tasks)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateDashboard()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func updateDashboard() {
        headerView.update(heading: viewModel.welcomeMessage(), subheading: viewModel.subHeading, hasBubbles: viewModel.hasTrayBubbles)
        karaiView.co2Net = viewModel.co2NetValue
        carbonStateProvider.updateCarbonData(viewModel.co2VTotalValue, offset: viewModel.co2OffsetValue)
        karaiOverlayView.ecoScore = viewModel.ecoScore
        karaiOverlayView.co2Offset = viewModel.co2OffsetValue
        karaiOverlayView.co2Total = viewModel.co2VTotalValue
        dashboardContentView.updateStats(trips: viewModel.trips, distance: viewModel.distance, unit: viewModel.unit, delegate: self)
        dashboardContentView.updateBubbles(viewModel.bubbleProvider.bubbles)
        dashboardContentView.updateScores(viewModel.scores)
        dashboardContentView.updateMileageSummary(viewModel.summary, unit: viewModel.unit)
        dashboardContentView.updateLastTrip(viewModel.lastTrip, unit: viewModel.unit)
        dashboardContentView.updateProjects()
    }
    
    private func presentProfile() {
        delegate?.dashboardViewControllerWillPresentProfile(self)
    }
    
    private func performChangeVehicle(_ vehicleId: String?) {
        guard let vehicleId = vehicleId else { return }

        showSpinner()
        viewModel.changeLastTripVehicle(vehicleId)
    }
    
    private func performRemoveTrip(_ tripId: String, reason: String) {
        showSpinner()
        viewModel.removeTrip(tripId, reson: reason)
    }
    
    private func performChangeTripType(_ tripId: String, type: TripType) {
        showSpinner()
        viewModel.changeTrip(tripId, type: type)
    }
    
    private func showPrimary(_ vehicleId: String?) {
        let vehicles = viewModel.vehicles
        var tripVehicle: VehicleData?
        
        if vehicleId != nil {
            tripVehicle = vehicles.first(where: { $0.vehicleId == vehicleId! })
        }
        
        showTrayPrimaryChange(tripVehicle, vehicles: vehicles) { vehicle in
            self.performChangeVehicle(vehicle.vehicleId)
        } addVehicle: {
            self.delegate?.dashboardViewControllerDidDidTapAddVehicle(self)
        }
    }
    
    private func showRemoveTrip(_ tripId: String) {
        showTrayRemoveTrip { reason in
            self.performRemoveTrip(tripId, reason: reason)
        }
    }
    
    private func showRemoveTripConfirmation() {
        showTrayRemoveTripConfirmation(nil)
    }
    
    @objc
    func onTapRecognizer() {
        #if DEBUG
        delegate?.dashboardViewControllerDidTripleTap(self)
        #else
        if viewModel.isDebugEnabled {
            delegate?.dashboardViewControllerDidTripleTap(self)
        }
        #endif
    }
    
    @objc private func willUpdateHeader(notification: Notification) {
        self.updateDashboard()
    }
    
    @IBAction private func didTapOffsetCarbon(_ sender: Any) {
        AnalyticsService.trackEvent(.clickedOffsetCarbon)        
        self.delegate?.dashboardViewControllerWillPresentPayment(self)
    }
}

extension DashboardViewController: DashboardHeaderViewDelegate {
    func dashboardViewWillPresentProfile(_: DashboardHeaderView) {
        presentProfile()
    }
    
    func dashboardViewWillPresentNofifications(_ view: DashboardHeaderView) {
        if let drawer = self.parent as? PulleyViewController {
            drawer.setDrawerPosition(position: .partiallyRevealed, animated: true, completion: nil)
        }
    }
}

extension DashboardViewController: DashboardStatsContainerViewDelegate {
    func dashboardStatsContainerView(_ view: DashboardStatsContainerView, didTap stat: DashboardStat) {
        switch stat {
        case .trips:
            if viewModel.trips > 0 {
                self.delegate?.dashboardViewControllerWillPresentTrips(self)
            }
        default: return
        }
    }
}

extension DashboardViewController: DashboardKaraiOverlayViewDelegate {
    func dashboardKaraiOverlayViewDidTapInfo(_ view: DashboardKaraiOverlayView) {
        delegate?.dashboardViewControllerDidDidTapInfo(self)
    }
}

extension DashboardViewController: DashboardContentViewDelegate {
    func dashboardContentViewDidTapFindOutMore(_ contentView: DashboardContentView) {
        AnalyticsService.trackEvent(.clickFindOutMoreCarbon)
        delegate?.dashboardViewControllerWillPresent(self, info: .whatIsCO2)
    }
    
    func dashboardContentViewDidTapScores(_ contentView: DashboardContentView) {
        AnalyticsService.trackEvent(.clickFindOutMoreEcoDriving)
        delegate?.dashboardViewControllerWillPresent(self, info: .whatEcoDrivingScore)
    }
    
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView) {
        delegate?.dashboardViewControllerWillPresentTrips(self)
    }
    
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, remove tripId: String?) {
        guard let tripId = tripId else { return }
        showRemoveTrip(tripId)
    }
    
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, vehicle vehicleId: String?) {
        showPrimary(vehicleId)
    }
    
    func dashboardContentViewDidTapLastTrip(_ contentView: DashboardContentView, business tripId: String?, newType: TripType?) {
        guard let tripId = tripId, let newType = newType else { return }
        performChangeTripType(tripId, type: newType)
    }
    
    func dashboardContentViewDidTapBubble(_ contentView: DashboardContentView, bubbleType: BubbleType) {
        delegate?.dashboardViewControllerWillBubble(self, bubbleType: bubbleType)
    }
    
    func dashboardContentViewDidTap(_ contentView: DashboardContentView, charityProject: CharityProject) {
        delegate?.dashboardViewControllerDidTap(self, charityProject: charityProject)
    }
    
    func dashboardContentViewDidTapMileageSummary(_ contentView: DashboardContentView) {
        delegate?.dashboardViewControllerDidDidTapMileageSummary(self)
    }
}
