//
//  ProfileViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/08/2021.
//

import Combine
import Foundation

class ProfileViewModel: ObservableObject {
    let authService: AuthService
    private(set) var userService: UserProvidable
    private let biometricService: BiometricService
    private let bubbleService: BubbleProvidable
    private let api: APIProtocol
    private let tripsMonitor: TripsMonitor
    private var profileSections: [ProfileSection]!

    private var tasks = Set<AnyCancellable>()

    @Published var loggedOut: Bool = false
    @Published var authError: Error?
    
    enum PlaceholderKeys {
        static let versionNumber = "{version_number}"
    }

    init(authService: AuthService, userService: UserProvidable, biometricService: BiometricService, bubbleService: BubbleProvidable, api: APIProtocol, tripMonitor: TripsMonitor) {
        self.authService = authService
        self.userService = userService
        self.biometricService = biometricService
        self.bubbleService = bubbleService
        self.tripsMonitor = tripMonitor
        self.api = api

        generateContent()
    }

    var sections: [ProfileSection] {
        profileSections
    }

    var userName: String {
        guard let user = userService.currentUser, let userName = user.firstName, !userName.isEmpty else {
            return "profile.no.name".localized
        }

        return userName
    }

    var shouldUpdateName: Bool {
        if let user = userService.currentUser, let userName = user.firstName, !userName.isEmpty {
            return false
        }

        return true
    }

    var userEmail: String {
        guard let user = userService.currentUser else { return "" }

        return user.email
    }

    var isUserVerified: Bool {
        authService.isUserEmailVerified
    }

    var versionString: String {
        "profile.other.version".localized.replacingOccurrences(of: PlaceholderKeys.versionNumber, with: Bundle.appVersion)
    }

    func logout() {
        let payload = UpdateUserPayload(fcmToken: "")
        api.userData(payload) { result in
            switch result {
            case .success:
                self.authService.logout()
                    .receive(on: DispatchQueue.main)
                    .sink { [weak self] completion in
                        switch completion {
                        case let .failure(err):
                            self?.authError = err
                        default:
                            break
                        }
                    } receiveValue: { [weak self] _ in
                        self?.bubbleService.reset()
                        self?.userService.reset()
                        self?.tripsMonitor.stopMonitoring()
                        self?.loggedOut = true
                    }
                    .store(in: &self.tasks)
            case let .failure(error):
                print("[ProfileViewModel] logout error removing fcm token: \(error)")
                self.authError = error
            }
        }
    }

    func updatecontent() {
        generateContent()
    }

    private func generateContent() {
        var tmpSections: [ProfileSection] = [.myProfile, .payments, .rewards, .about]
        if biometricService.canEvaluate() { tmpSections.insert(.security, at: 3) }

        profileSections = tmpSections
    }
}

enum ProfileSection: String {
    case myProfile = "profile.section.profile"
    case payments = "profile.section.payments"
    case rewards = "profile.section.rewards"
    case security = "profile.section.security"
    case about = "profile.section.general"
}

extension ProfileSection {
    var iconName: String {
        switch self {
        case .myProfile:
            return "profile_account"
        case .payments:
            return "profile_payment"
        case .rewards:
            return "profile_rewards"
        case .security:
            return "profile_security"
        case .about:
            return "profile_general"
        }
    }
}
