//
//  ProfileViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/08/2021.
//

import Combine
import UIKit

protocol ProfileViewControllerDelegate: AnyObject {
    func profileViewController(_ vc: ProfileViewController, didSelect row: ProfileSection)
    func profileViewControllerDidLogout(_ vc: ProfileViewController)
    func profileViewControllerWillVerify(_ vc: ProfileViewController)
    func profileViewControllerDidTapAddName(_ viewController: ProfileViewController)
}

class ProfileViewController: ViewController, TrayPresentable {
    @IBOutlet weak var headerView: ProfileHeaderView!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var verifyHeaderView: ProfileVerificationView!
    @IBOutlet private var versionLabel: UILabel!
    @IBOutlet private var logoutButton: UIButton!

    private var tasks = Set<AnyCancellable>()
    var viewModel: ProfileViewModel!
    weak var delegate: ProfileViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true

        // header view
        headerView.delegate = self
        headerView.name = viewModel.userName
        headerView.email = viewModel.userEmail

        // verification view
        verifyHeaderView.isVerified = viewModel.isUserVerified
        verifyHeaderView.delegate = self

        // tableview
        tableView.register(ProfileCell.self, forCellReuseIdentifier: ProfileCell.reuseidentifier)
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        let topPadding = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 15))
        tableView.tableHeaderView = topPadding

        // logout button
        logoutButton.setTitle("profile.logout.heading".localized, for: .normal)
        logoutButton.contentEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 8)
        logoutButton.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: -8)

        registerSubscriptions()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        updateContent()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func updateUserData() {
        headerView.name = viewModel.userName
        headerView.email = viewModel.userEmail
        verifyHeaderView.isVerified = viewModel.isUserVerified
    }

    func updateContent() {
        versionLabel.text = viewModel.versionString
        viewModel.updatecontent()
        tableView.reloadData()
    }

    private func registerSubscriptions() {
        // Publishers
        viewModel.$loggedOut
            .receive(on: DispatchQueue.main)
            .sink { [weak self] loggedOut in
                self?.hideSpinner()
                if loggedOut, let self = self {
                    self.delegate?.profileViewControllerDidLogout(self)
                }
            }.store(in: &tasks)

        viewModel.$authError
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if let error = error {
                    self?.showAlertError(error: error)
                }
            }.store(in: &tasks)

        viewModel.userService.syncedSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateUserData()
            }
            .store(in: &tasks)

        viewModel.authService.verificationSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateUserData()
            }
            .store(in: &tasks)
    }

    private func removeSubscriptions() {
        tasks.removeAll()
    }

    @IBAction private func didPressLogout(_ sender: Any) {
        showTrayLogout { [weak self] in
            self?.showSpinner()
            self?.viewModel.logout()
        }
    }
}

extension ProfileViewController: UITableViewDataSource {
    func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.sections.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.reuseidentifier, for: indexPath) as? ProfileCell else {
            return UITableViewCell()
        }

        let data = viewModel.sections[indexPath.row]
        cell.configure(data)

        return cell
    }
}

extension ProfileViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = viewModel.sections[indexPath.row]
        delegate?.profileViewController(self, didSelect: row)
    }

    func tableView(_: UITableView, heightForHeaderInSection _: Int) -> CGFloat {
        0
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        60.0
    }
}

extension ProfileViewController: ProfileHeaderViewDelegate {
    func profileHeaderViewWillUpdateName(_: ProfileHeaderView) {
        guard viewModel.shouldUpdateName else { return }
        delegate?.profileViewControllerDidTapAddName(self)
    }

    func profileHeaderViewWillDismiss(_: ProfileHeaderView) {
        dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController: ProfileVerificationViewDelegate {
    func profileVerificationViewWillVerify(_: ProfileVerificationView) {
        delegate?.profileViewControllerWillVerify(self)
    }
}
