//
//  ProfileHeaderView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 13/09/2021.
//

import UIKit

protocol ProfileHeaderViewDelegate: AnyObject {
    func profileHeaderViewWillUpdateName(_ view: ProfileHeaderView)
    func profileHeaderViewWillDismiss(_ view: ProfileHeaderView)
}

class ProfileHeaderView: BaseView {
    private let closeButton = Subviews.closeButton
    private let nameLabel = Subviews.nameLabel
    private let emailLabel = Subviews.emailLabel

    weak var delegate: ProfileHeaderViewDelegate?

    var name: String! {
        didSet {
            nameLabel.text = name
        }
    }

    var email: String! {
        didSet {
            self.emailLabel.text = email
        }
    }

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = UIColor.appColor(.navyBlue)

        addSubview(closeButton)
        addSubview(nameLabel)
        addSubview(emailLabel)

        setupLabelTap()
        closeButton.addTarget(self, action: #selector(closeScreen), for: .touchUpInside)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            closeButton.widthAnchor.constraint(equalToConstant: 17),
            closeButton.heightAnchor.constraint(equalToConstant: 17),
            closeButton.topAnchor.constraint(equalTo: nameLabel.topAnchor, constant: 7),
            closeButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),

            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: UIDevice.current.hasNotch ? 70 : 30),
            nameLabel.trailingAnchor.constraint(equalTo: closeButton.leadingAnchor, constant: -20),

            emailLabel.heightAnchor.constraint(equalToConstant: 20),
            emailLabel.leadingAnchor.constraint(equalTo: nameLabel.leadingAnchor),
            emailLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor),
            emailLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -40),
            emailLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20)
        ]

        NSLayoutConstraint.activate(constraints)
    }

    func setupLabelTap() {
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(labelTapped(_:)))
        nameLabel.isUserInteractionEnabled = true
        nameLabel.addGestureRecognizer(labelTap)
    }

    @objc private func labelTapped(_: UITapGestureRecognizer) {
        delegate?.profileHeaderViewWillUpdateName(self)
    }

    @objc private func closeScreen() {
        delegate?.profileHeaderViewWillDismiss(self)
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 80)
    }
}

private struct Subviews {
    static var closeButton: SmallButton {
        let button = SmallButton(type: .system)
        let image = UIImage(named: "close-icon")?.withRenderingMode(.alwaysTemplate)
        button.setImage(image, for: .normal)
        button.tintColor = UIColor(hex: 0x87919F)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }

    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = UIFont.K2DBold(28)
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }

    static var emailLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = UIFont.K2DRegular(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5

        return label
    }
}
