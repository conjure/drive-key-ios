//
//  ProfileVerificationView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 14/09/2021.
//

import UIKit

protocol ProfileVerificationViewDelegate: AnyObject {
    func profileVerificationViewWillVerify(_ view: ProfileVerificationView)
}

class ProfileVerificationView: BaseView {
    private let stackView = Subviews.stackView
    private let verifiedImageView = Subviews.verifiedIcon
    private let statusLabel = Subviews.statusLabel

    weak var delegate: ProfileVerificationViewDelegate?

    override func setUpSubviews() {
        super.setUpSubviews()

        stackView.addArrangedSubview(verifiedImageView)
        stackView.addArrangedSubview(statusLabel)
        addSubview(stackView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            stackView.heightAnchor.constraint(equalToConstant: 16.0),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }

    var isVerified: Bool! {
        didSet {
            backgroundColor = isVerified ? UIColor(hex: 0x26B787) : UIColor(hex: 0xE8505B)
            verifiedImageView.isHidden = !isVerified

            if isVerified {
                statusLabel.text = "profile.verified".localized
            } else {
                // create attributed string
                let firstString = "profile.unverified".localized + " "
                let atts = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.paragraph1]
                let attrString = NSMutableAttributedString(string: firstString, attributes: atts)
                let secondString = "here".localized
                let underlineAtts: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white,
                                                                    .font: UIFont.paragraph1,
                                                                    .underlineStyle: NSUnderlineStyle.single.rawValue]
                attrString.append(NSMutableAttributedString(string: secondString, attributes: underlineAtts))
                statusLabel.attributedText = attrString
                setupLabelTap()
            }
        }
    }

    @objc func labelTapped(_: UITapGestureRecognizer) {
        guard !isVerified else { return }
        delegate?.profileVerificationViewWillVerify(self)
    }

    func setupLabelTap() {
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(labelTapped(_:)))
        statusLabel.isUserInteractionEnabled = true
        statusLabel.addGestureRecognizer(labelTap)
    }
}

private struct Subviews {
    static var verifiedIcon: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "verified"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var statusLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = .K2DRegular(16)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    static var stackView: UIStackView {
        let stackView = UIStackView()
        stackView.axis = NSLayoutConstraint.Axis.horizontal
        stackView.distribution = UIStackView.Distribution.equalSpacing
        stackView.alignment = .center
        stackView.spacing = 9.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
}
