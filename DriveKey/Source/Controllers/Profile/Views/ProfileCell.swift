//
//  ProfileCell.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 14/09/2021.
//

import UIKit

class ProfileCell: UITableViewCell {
    static let reuseidentifier = "profileCell"

    private let iconView = Subviews.iconView
    private let nameLabel = Subviews.nameLabel
    private let separator = Subviews.separatorView
    private let disclosureIcon = Subviews.disclosureIndicator

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white

        addSubview(iconView)
        addSubview(nameLabel)
        addSubview(separator)
        addSubview(disclosureIcon)

        NSLayoutConstraint.activate([
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            iconView.heightAnchor.constraint(equalToConstant: 16),
            iconView.widthAnchor.constraint(equalToConstant: 16),
            iconView.centerYAnchor.constraint(equalTo: centerYAnchor),

            nameLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 8),

            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            separator.heightAnchor.constraint(equalToConstant: 1.0),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),

            disclosureIcon.heightAnchor.constraint(equalToConstant: 16),
            disclosureIcon.widthAnchor.constraint(equalToConstant: 8),
            disclosureIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
            disclosureIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),

            nameLabel.trailingAnchor.constraint(equalTo: disclosureIcon.leadingAnchor, constant: -21)
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        selectionStyle = .default
        nameLabel.font = UIFont.K2DRegular(16)
    }

    func configure(_ row: ProfileSection) {
        iconView.image = UIImage(named: row.iconName)
        nameLabel.text = row.rawValue.localized
    }
}

private struct Subviews {
    static var iconView: UIImageView {
        let imgView = UIImageView()
        imgView.translatesAutoresizingMaskIntoConstraints = false
        return imgView
    }

    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.font = UIFont.K2DRegular(16)
        label.textColor = UIColor.appColor(.navyBlue)
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }

    static var separatorView: UIView {
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor(hex: 0xB8B8B8)
        separator.translatesAutoresizingMaskIntoConstraints = false
        return separator
    }

    static var disclosureIndicator: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "cell_disclosure"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}
