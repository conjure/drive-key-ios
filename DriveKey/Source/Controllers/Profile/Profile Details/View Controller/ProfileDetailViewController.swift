//
//  ProfileDetailViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/06/2022.
//

import UIKit
import Combine
import AuthenticationServices

protocol ProfileDetailViewControllerDelegate: AnyObject {
    func profileDetailViewController(_ vc: ProfileDetailViewController, didTapRow: ProfileContentRow)
    func profileDetailViewControllerDidRemoveAccount(_ vc: ProfileDetailViewController)
}

class ProfileDetailViewController: ViewController, TrayPresentable {
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var removeAccountButton: UIButton!
    
    var viewModel: ProfileDetailViewModel!
    weak var delegate: ProfileDetailViewControllerDelegate?
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        
        title = viewModel.screenTitle
        
        // tableview
        tableView.register(ProfileDetailCell.self, forCellReuseIdentifier: ProfileDetailCell.reuseidentifier)
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tableView.delegate = self
        tableView.dataSource = self
        
        // remove account button
        removeAccountButton.setTitle("profile.remove.account".localized, for: .normal)
        removeAccountButton.isHidden = viewModel.shouldHideDeleteAccount
        
        registerSubscriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        (navigationController as? NavigationController)?.setProfileBlackTheme()
        
        viewModel.updatecontent()
        tableView.reloadData()
    }
    
    private func registerSubscriptions() {
        viewModel.$deleted
            .receive(on: DispatchQueue.main)
            .sink { [weak self] deleted in
                self?.hideSpinner()
                if deleted {
                    self?.showRemoveConfirmation()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$deleteError
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    if let err = error as? ASAuthorizationError {
                        switch err.code {
                        case .canceled, .unknown:
                            return
                        default:
                            break
                        }
                    }
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &cancellables)
    }
    
    private func performRemove() {
        showSpinner()
        viewModel.disableAccount(self)
    }
    
    private func showRemoveInfo() {
        showTrayDeleteInfo {
            self.performRemove()
        }
    }
    
    private func showRemoveConfirmation() {
        showTrayDeleteConfirmation {
            self.delegate?.profileDetailViewControllerDidRemoveAccount(self)
        }
    }
    
    @IBAction
    private func onRemove() {
        showTrayDelete {
            self.showRemoveInfo()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension ProfileDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileDetailCell.reuseidentifier, for: indexPath) as? ProfileDetailCell else {
            return UITableViewCell()
        }
        
        let data = viewModel.rows[indexPath.row]
        cell.viewModel = viewModel.cellViewModel
        cell.configure(data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = viewModel.rows[indexPath.row]
        delegate?.profileDetailViewController(self, didTapRow: row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

extension ProfileDetailViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for _: ASAuthorizationController)
        -> ASPresentationAnchor {
        view.window!
    }
}
