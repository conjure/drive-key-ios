//
//  ProfileDetailViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/06/2022.
//

import Foundation
import Combine
import AuthenticationServices

class ProfileDetailViewModel {
    private let userService: UserProvidable
    private let biometricService: BiometricService
    private let api: APIUserProtocol
    private let authService: AuthService
    private let bubbleService: BubbleProvidable
    private let section: ProfileSection
    private(set) var rows: [ProfileContentRow]!
    private var cancellables = Set<AnyCancellable>()
    
    @Published var deleted: Bool = false
    @Published var deleteError: Error?
    
    init(_ section: ProfileSection, userService: UserProvidable, biometricService: BiometricService, api: APIUserProtocol, authService: AuthService, bubbleService: BubbleProvidable) {
        self.section = section
        self.userService = userService
        self.biometricService = biometricService
        self.api = api
        self.authService = authService
        self.bubbleService = bubbleService
        
        registerSubscription()
    }
    
    private func registerSubscription() {
        authService.authCodeSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] code in
                print("[ProfileDetailViewModel] authCodeSubject")
                self?.performDisable(code)
            }
            .store(in: &cancellables)
        
        authService.errorSubject
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.deleteError = error
            }
            .store(in: &cancellables)
    }
    
    var screenTitle: String {
        return section.rawValue.localized
    }
    
    var shouldHideDeleteAccount: Bool {
        return section != .myProfile
    }
    
    var cellViewModel: ProfileDetailCellViewModel {
        ProfileDetailCellViewModel(biometricService: biometricService, userService: userService, api: api)
    }
    
    func updatecontent() {
        guard let user = userService.currentUser else { return }
        
        switch section {
        case .myProfile:
            let plate = user.activeVehicle?.platesValue ?? ""
            let units = user.unitDistance
            let currency = user.currencyCode
            
            var rate = user.standardRate?.formatForCurrency(currency) ?? ""
            if !rate.isEmpty {
                rate += "/" + units.localized
            }
            
            self.rows = [
                .personalInfo,
                .myVehicles(plate: plate),
                .businessInfo(rate: rate),
                .units(unit: units.localizedPlural.capitalizingFirstLetter())
            ]
        case .payments:
            var tmpRows: [ProfileContentRow] = []
            if let pm = user.paymentMethodId, !pm.isEmpty {
                tmpRows.append(.payment)
            }
            let currency = user.currencyCode ?? ""
            tmpRows.append(.currency(currency))
            self.rows = tmpRows
        case .rewards:
            self.rows = [.rewards, .vouchers]
        case .security:
            self.rows = biometricService.sourceType == .faceID ? [.privacyFaceID] : [.privacyTouchID]
        case .about:
            self.rows = [.about, .privacyPolicy, .termsConditions]
        }
    }
    
    func disableAccount(_ host: ASAuthorizationControllerPresentationContextProviding) {
        if authService.isAppleSignIn {
            requestAuthCode(host)
        } else {
            print("[ProfileDetailViewModel] disableAccount")
            performDisable(nil)
        }
    }
    
    private func requestAuthCode(_ host: ASAuthorizationControllerPresentationContextProviding) {
        let request = authService.generateAppleIDRequest()
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = authService
        authorizationController.presentationContextProvider = host
        authorizationController.performRequests()
    }
    
    private func performDisable(_ authCode: String?) {
        print("[ProfileDetailViewModel] performDisable")
        api.disableAccount(authCode) { result in
            print("[ProfileDetailViewModel] performDisable callback")
            switch result {
            case .success:
                self.logout()
            case .failure(let error):
                self.deleteError = error
            }
        }
    }
    
    private func logout() {
        authService.logout()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    self?.deleteError = error
                default:
                    break
                }
            } receiveValue: { [weak self] _ in
                self?.bubbleService.reset()
                self?.userService.reset()
                self?.deleted = true
            }
            .store(in: &self.cancellables)
    }
}
