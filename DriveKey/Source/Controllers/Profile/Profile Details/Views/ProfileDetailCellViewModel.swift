//
//  ProfileDetailCellViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 28/06/2022.
//

import Foundation

class ProfileDetailCellViewModel {
    private let userService: UserProvidable
    private let biometricService: BiometricService
    private let api: APIUserProtocol
    private let userDefaults: UserDefaults

    init(biometricService: BiometricService, userService: UserProvidable, api: APIUserProtocol, userDefaults: UserDefaults = .standard) {
        self.biometricService = biometricService
        self.userDefaults = userDefaults
        self.userService = userService
        self.api = api
    }

    var optInStatus: Bool {
        userService.currentUser?.notifications ?? false
    }

    func toggleBiometricPermission(_ enable: Bool, completion: @escaping (Bool) -> Void) {
        let currentBiometricStatus = userDefaults.integer(forKey: UserDefaultKeys.biometricStatus)

        if currentBiometricStatus == BiometricStatus.granted.rawValue || currentBiometricStatus == BiometricStatus.dismissedBubble.rawValue, enable {
            biometricService.evaluate { _, error in
                if let error = error {
                    switch error {
                    case .userFallback:
                        self.userDefaults.set(BiometricStatus.unknown.rawValue, forKey: UserDefaultKeys.biometricStatus)
                    default:
                        self.userDefaults.set(BiometricStatus.denied.rawValue, forKey: UserDefaultKeys.biometricStatus)
                    }
                    completion(true)
                    return
                } else {
                    self.userDefaults.set(BiometricStatus.granted.rawValue, forKey: UserDefaultKeys.biometricStatus)
                }
            }
        } else {
            let status: BiometricStatus = enable ? .granted : .disabled
            userDefaults.set(status.rawValue, forKey: UserDefaultKeys.biometricStatus)
        }

        completion(false)
    }

    func updateNotifications(willOptIn: Bool) {
        let payload = UpdateUserPayload(notifications: willOptIn)
        api.userData(payload) { result in
            switch result {
            case .success:
                print("[ProfileViewModel] updateOptInSetting")
            case let .failure(error):
                print("[ProfileViewModel] updateOptInSetting error: \(error)")
            }
        }
    }
}
