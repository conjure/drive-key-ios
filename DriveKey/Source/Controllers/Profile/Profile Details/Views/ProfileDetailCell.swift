//
//  ProfileDetailCell.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/06/2022.
//

import UIKit

class ProfileDetailCell: UITableViewCell {
    static let reuseidentifier = "profileDetailCell"
    
    private let detailLabel = Subviews.subDetailLabel
    private let nameLabel = Subviews.nameLabel
    private let separator = Subviews.separatorView
    private let disclosureIcon = Subviews.disclosureIndicator
    private let toggleSwitch = Subviews.toggleSwitch
    
    var viewModel: ProfileDetailCellViewModel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        
        addSubview(nameLabel)
        addSubview(detailLabel)
        addSubview(separator)
        addSubview(disclosureIcon)
        contentView.addSubview(toggleSwitch)
        
        disclosureIcon.isHidden = true
        toggleSwitch.isHidden = true
        
        NSLayoutConstraint.activate([
            nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            
            detailLabel.trailingAnchor.constraint(equalTo: disclosureIcon.leadingAnchor, constant: -12),
            detailLabel.centerYAnchor.constraint(equalTo: nameLabel.centerYAnchor),
            
            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            separator.heightAnchor.constraint(equalToConstant: 1.0),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            disclosureIcon.heightAnchor.constraint(equalToConstant: 16),
            disclosureIcon.widthAnchor.constraint(equalToConstant: 8),
            disclosureIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
            disclosureIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            
            toggleSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            toggleSwitch.centerYAnchor.constraint(equalTo: centerYAnchor),
            toggleSwitch.widthAnchor.constraint(equalToConstant: 51),
            nameLabel.trailingAnchor.constraint(equalTo: toggleSwitch.leadingAnchor, constant: -16)
        ])
    }
    
    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        selectionStyle = .default
        nameLabel.font = UIFont.K2DRegular(16)
        detailLabel.text = ""
        disclosureIcon.isHidden = true
        toggleSwitch.isHidden = true
    }
    
    func configure(_ content: ProfileContentRow) {
        switch content {
        case .privacyFaceID, .privacyTouchID:
            nameLabel.text = content.title
            toggleSwitch.isHidden = false
            let enabled = (UserDefaults.standard.integer(forKey: UserDefaultKeys.biometricStatus) == BiometricStatus.granted.rawValue)
            toggleSwitch.isOn = enabled
            toggleSwitch.addTarget(self, action: #selector(toggleBiometric), for: .valueChanged)
            selectionStyle = .none
        case .rewards:
            nameLabel.text = content.title
            toggleSwitch.isHidden = false
            toggleSwitch.isOn = viewModel.optInStatus
            toggleSwitch.addTarget(self, action: #selector(toggleRewards), for: .valueChanged)
            selectionStyle = .none
        default:
            nameLabel.text = content.title
            detailLabel.text = content.detail
            disclosureIcon.isHidden = false
        }
    }
    
    @objc private func toggleBiometric() {
        let currentState = toggleSwitch.isOn
        viewModel.toggleBiometricPermission(currentState) { shouldReset in
            if shouldReset {
                self.toggleSwitch.setOn(false, animated: true)
            }
        }
    }
    
    @objc private func toggleRewards() {
        let currentState = toggleSwitch.isOn
        viewModel.updateNotifications(willOptIn: currentState)
    }
}

private struct Subviews {
    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.font = UIFont.K2DRegular(16)
        label.textColor = UIColor.appColor(.navyBlue)
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }
    
    static var subDetailLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textAlignment = .right
        label.font = UIFont.K2DRegular(16)
        label.textColor = UIColor.appColor(.darkGray)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    static var separatorView: UIView {
        let separator = UIView(frame: .zero)
        separator.backgroundColor = UIColor(hex: 0xB8B8B8)
        separator.translatesAutoresizingMaskIntoConstraints = false
        return separator
    }
    
    static var disclosureIndicator: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "cell_disclosure"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
    
    static var toggleSwitch: UISwitch {
        let toggle = UISwitch(frame: .zero)
        toggle.onTintColor = UIColor(hex: 0x26B787)
        toggle.translatesAutoresizingMaskIntoConstraints = false
        return toggle
    }
}

enum ProfileContentRow {
    case personalInfo
    case myVehicles(plate: String)
    case businessInfo(rate: String)
    case units(unit: String)
    case payment
    case currency(String)
    case privacyFaceID
    case privacyTouchID
    case rewards
    case vouchers
    case about
    case termsConditions
    case privacyPolicy
    
    var title: String {
        switch self {
        case .personalInfo:
            return "profile.myProfile.personal".localized
        case .myVehicles:
            return "profile.myProfile.vehicle".localized
        case .businessInfo:
            return "profile.myProfile.business".localized
        case .units:
            return "profile.myProfile.units".localized
        case .payment:
            return "profile.account.payment".localized
        case .currency:
            return "profile.account.currency".localized
        case .privacyFaceID:
            return "profile.other.privacy.face".localized
        case .privacyTouchID:
            return "profile.other.privacy.touch".localized
        case .rewards:
            return "profile.rewards.row".localized
        case .vouchers:
            return "profile.rewards.voucher".localized
        case .about:
            return "profile.other.about".localized
        case .termsConditions:
            return "profile.other.terms.conditions".localized
        case .privacyPolicy:
            return "profile.other.privacy.policy".localized
        }
    }
    
    var detail: String {
        switch self {
        case .myVehicles(let plate):
            return plate
        case .businessInfo(let rate):
            return rate
        case .units(let unit):
            return unit
        case .currency(let code):
            return code
        default:
            return ""
        }
    }
}
