//
//  UnitsViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2022.
//

import Foundation

class UnitsViewModel {
    private let userService: UserProvidable
    
    var selectedUnit: String?
    
    init(_ userService: UserProvidable) {
        self.userService = userService
        
        if let user = userService.currentUser, let unit = user.units, let distance = unit.distance {
            if let index = units.firstIndex(of: distance.localizedPlural.capitalizingFirstLetter()) {
                self.selectedUnit = units[index]
            }
        }
    }
    
    var units: [String] {
        [
            UnitDistance.miles.localizedPlural.capitalizingFirstLetter(),
            UnitDistance.kilometers.localizedPlural.capitalizingFirstLetter()
        ]
    }
}
