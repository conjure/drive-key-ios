//
//  UnitsViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2022.
//

import UIKit
import Combine

class UnitsViewController: ViewController {
    @IBOutlet var unitsField: NamedTextField!
    
    var viewModel: UnitsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "units.settings.title".localized
        
        unitsField.name = "units.settings.field.units.name".localized
        unitsField.isDisabled = true
        
        if let value = viewModel.selectedUnit {
            unitsField.value = value
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
