//
//  ErrorTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/07/2022.
//

import UIKit

class ErrorTrayViewController: TrayViewController {
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var label: UILabel!
    @IBOutlet private var tryAgainButton: RoundedButton!
    
    var message: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.setImage(UIImage(named: "close-tray-icon"), for: .normal)
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "alert-triangle-icon")

        label.font = .K2DBold(24)
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.text = "tray.error.label".localized
        
        tryAgainButton.setTitle("ok".localized, for: .normal)
        tryAgainButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let message = message else { return }
        label.text = message
    }
    
    override var contentHeight: CGFloat {
        310
    }
    
    @IBAction
    func onClose() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onTryAgain() {
        dismiss(animated: true)
        onContinue?()
    }
}
