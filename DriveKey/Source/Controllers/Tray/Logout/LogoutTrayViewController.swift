//
//  LogoutTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 12/07/2022.
//

import UIKit

class LogoutTrayViewController: TrayViewController {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var label: UILabel!
    @IBOutlet private var cancelButton: RoundedButton!
    @IBOutlet private var logoutButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "alert-triangle-icon")

        label.font = .K2DBold(24)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.text = "tray.logout.label".localized
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = .K2DMedium(15)
        
        logoutButton.setTitle("tray.logout.button.ok".localized, for: .normal)
        logoutButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        364
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onLogout() {
        logoutButton.isEnabled = false
        onContinue?()
    }
}
