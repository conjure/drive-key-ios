//
//  ReportTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 30/08/2022.
//

import UIKit

class ReportTrayViewController: TrayViewController {
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var tilesView: ReportTilesView!
    @IBOutlet private var generateButton: RoundedButton!
    
    var report: Report?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.setImage(UIImage(named: "close-tray-icon"), for: .normal)

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.textAlignment = .left
        headerLabel.text = "tray.report.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.adjustsFontSizeToFitWidth = true
        infoLabel.textAlignment = .left
        infoLabel.text = report?.summary
        
        tilesView.data = report
        
        generateButton.setTitle("tray.report.button.ok".localized, for: .normal)
        generateButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        285
    }
    
    @IBAction
    func onClose() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onTryAgain() {
        dismiss(animated: true)
        onContinue?()
    }
}
