//
//  ReportConfirmationTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 01/09/2022.
//

import UIKit

class ReportConfirmationTrayViewController: TrayViewController {
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var continueButton: RoundedButton!
    
    var email: String?
    
    private enum PlaceholderKeys {
        static let email = "{email}"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .center
        headerLabel.text = "tray.report.confirmation.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 2
        infoLabel.textAlignment = .center
        
        let infoLocalizedStr = "tray.report.confirmation.label.info".localized
        let infoStr = infoLocalizedStr.replacingOccurrences(of: PlaceholderKeys.email, with: email ?? "")
        infoLabel.text = infoStr
        
        continueButton.setTitle("ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        259
    }
    
    @IBAction
    func onContinue() {
        dismiss(animated: true)
        onContinue?()
    }
}
