//
//  ReportTilesView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 30/08/2022.
//

import UIKit

class ReportTilesView: BaseView {
    private let stackView = Subviews.stackView
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(stackView)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: Report? {
        didSet {
            guard let data = data else { return }
            addTile("tray.report.tile.potential".localized, value: data.potential)
            addTile("tray.report.tile.co2".localized, value: data.co2)
            
            let tripsStr = data.trips > 1 ? "tray.report.text.trips".localized : "tray.report.text.trip".localized
            addTile(tripsStr, value: String(data.trips))
            
            let unitSingular = data.unit.localized.capitalizingFirstLetter()
            let unitPlural = data.unit.localizedPlural.capitalizingFirstLetter()
            let milesStr = data.distance <= 1 ? unitSingular : unitPlural
            let milesValueStr = NumberFormatter.distance.string(from: NSNumber(value: data.distance)) ?? ""
            addTile(milesStr, value: milesValueStr)
        }
    }
    
    private func addTile(_ type: String, value: String) {
        let tileView = ReportTileView(frame: .zero)
        tileView.translatesAutoresizingMaskIntoConstraints = false
        tileView.value = value
        tileView.type = type.subscriptCO2(.K2DRegular(8) ?? .systemFont(ofSize: 8))
        stackView.addArrangedSubview(tileView)
    }
}

private struct Subviews {
    static var stackView: UIStackView {
        let view = UIStackView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.alignment = .fill
        view.spacing = 8
        
        return view
    }
}
