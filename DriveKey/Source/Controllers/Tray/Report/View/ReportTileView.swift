//
//  ReportTileView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 30/08/2022.
//

import UIKit

class ReportTileView: BaseView {
    private let valueLabel = Subviews.valueLabel
    private let typeLabel = Subviews.typeLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.withAlphaComponent(0.05).cgColor
        
        addSubview(valueLabel)
        addSubview(typeLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            valueLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
            valueLabel.heightAnchor.constraint(equalToConstant: 26),
            typeLabel.topAnchor.constraint(equalTo: valueLabel.bottomAnchor, constant: 6),
            typeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
            typeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
            typeLabel.heightAnchor.constraint(equalToConstant: 14)
            
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var value: String? {
        didSet {
            guard let value = value else { return }
            valueLabel.text = value
        }
    }
    
    var type: NSAttributedString? {
        didSet {
            guard let type = type else { return }
            typeLabel.attributedText = type
        }
    }
}

private struct Subviews {
    static var valueLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(20)
        label.numberOfLines = 1
        label.textColor = .black
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true

        return label
    }
    
    static var typeLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(12)
        label.numberOfLines = 1
        label.textColor = .appColor(.separatorDarkGray)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true

        return label
    }
}
