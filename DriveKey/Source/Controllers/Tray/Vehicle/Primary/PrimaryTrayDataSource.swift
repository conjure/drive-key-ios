//
//  PrimaryTrayDataSource.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/07/2022.
//

import UIKit

protocol PrimaryTrayDataSourceDelegate: AnyObject {
    func primaryTrayDataSourceDidSelectRow(_ dataSource: PrimaryTrayDataSource)
}

class PrimaryTrayDataSource: NSObject {
    private let tableView: UITableView
    private let preselectedVehicle: VehicleData?
    private var items = [PrimaryTrayCellViewModel]()
    private(set) var selectedVehicle: VehicleData?
    
    weak var delegate: PrimaryTrayDataSourceDelegate?
    
    struct Constants {
        static let cellHeight: CGFloat = 50
    }
        
    init(_ tableView: UITableView, preselectedVehicle: VehicleData?) {
        self.tableView = tableView
        self.preselectedVehicle = preselectedVehicle
    }
    
    func loadData(_ data: [VehicleData]) {
        items.removeAll()
        data.forEach { data in
            let viewModel = PrimaryTrayCellViewModel(data)
            items.append(viewModel)
        }
        
        if preselectedVehicle != nil {
            items.forEach({ $0.isSelected = false })
        }
        
        tableView.reloadData()
        
        var preselectedIndex: Int = -1
        if let preselected = preselectedVehicle, let index = items.firstIndex(where: { $0.data.vehicleId == preselected.vehicleId }) {
            preselectedIndex = index
        } else if let index = items.firstIndex(where: { $0.isSelected == true }) {
            preselectedIndex = index
        }
        
        if preselectedIndex >= 0 {
            let viewModel = items[preselectedIndex]
            viewModel.isSelected = true
            selectedVehicle = viewModel.data
            tableView.selectRow(at: IndexPath(row: preselectedIndex, section: 0), animated: false, scrollPosition: .none)
        }
    }
}

extension PrimaryTrayDataSource: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
}

extension PrimaryTrayDataSource: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: PrimaryTrayTableViewCell.className, for: indexPath) as! PrimaryTrayTableViewCell
        
        cell.data = data
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = items[indexPath.row]
        selectedVehicle = data.data
        delegate?.primaryTrayDataSourceDidSelectRow(self)
    }
}
