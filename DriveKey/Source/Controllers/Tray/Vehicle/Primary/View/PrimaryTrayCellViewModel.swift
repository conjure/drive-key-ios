//
//  PrimaryTrayCellViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/07/2022.
//

import Foundation

class PrimaryTrayCellViewModel {
    private(set) var data: VehicleData
    var isSelected = false
    
    init(_ data: VehicleData) {
        self.data = data
        
        if let primary = data.primary {
            isSelected = primary
        }
    }
    
    var plates: String {
        data.platesValue ?? ""
    }
}
