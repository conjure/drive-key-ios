//
//  PrimaryTrayTableViewCell.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/07/2022.
//

import UIKit

class PrimaryTrayTableViewCell: UITableViewCell {
    private let radioImageView = Subviews.radioImageView
    private let platesLabel = Subviews.platesLabel
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setUpCell()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("[PrimaryTrayTableViewCell] init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        platesLabel.text = nil
    }

    private func setUpCell() {
        selectionStyle = .none
        contentView.backgroundColor = .clear
        backgroundColor = .clear

        contentView.addSubview(radioImageView)
        contentView.addSubview(platesLabel)
                
        let constraint = [
            radioImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            radioImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            radioImageView.widthAnchor.constraint(equalToConstant: 24),
            radioImageView.heightAnchor.constraint(equalToConstant: 24),
            platesLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            platesLabel.leadingAnchor.constraint(equalTo: radioImageView.trailingAnchor, constant: 8),
            platesLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            platesLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: PrimaryTrayCellViewModel? {
        didSet {
            guard let data = data else { return }
            platesLabel.text = data.plates
            selectRadio(data.isSelected)
        }
    }
    
    private func selectRadio(_ selected: Bool) {
        let imageName = selected ? "primary-on-icon" : "primary-off-icon"
        radioImageView.image = UIImage(named: imageName)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectRadio(selected)
    }
}

private struct Subviews {
    static var radioImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }

    static var platesLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(18)
        label.numberOfLines = 1
        label.textColor = .black
        label.textAlignment = .left

        return label
    }
}
