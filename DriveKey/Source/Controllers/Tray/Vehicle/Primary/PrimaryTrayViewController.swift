//
//  PrimaryTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 26/07/2022.
//

import UIKit

class PrimaryTrayViewController: TrayViewController {
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var addButton: RoundedButton!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var tableViewHeightConstraint: NSLayoutConstraint!
    
    var vehicles: [VehicleData]!
    private var dataSource: PrimaryTrayDataSource!
    var shouldDismiss: Bool = true
    var isUpdate: Bool = true
    var isChange: Bool = false
    var preselectedVehicle: VehicleData?
    
    var onSelect: ((VehicleData) -> Void)?
    var onAddVehicle: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeButton.setImage(UIImage(named: "close-tray-icon"), for: .normal)
        closeButton.isHidden = !isUpdate
        
        headerLabel.font = .K2DBold(18)
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .left
        headerLabel.adjustsFontSizeToFitWidth = true
        let text = isUpdate ? "tray.primary.info.label.update".localized : "tray.primary.info.label.choose".localized
        headerLabel.text = text
        if isChange {
            headerLabel.text = "tray.primary.info.label.change".localized
        }
        
        tableView.backgroundColor = .clear
        tableView.register(PrimaryTrayTableViewCell.self, forCellReuseIdentifier: PrimaryTrayTableViewCell.className)
        tableView.separatorStyle = .none
        tableView.sectionHeaderHeight = 0
        tableView.showsVerticalScrollIndicator = false
        
        addButton.setTitle("tray.primary.button.add".localized, for: .normal)
        addButton.backgroundColor = .clear
        addButton.titleLabel?.font = .K2DMedium(15)
        
        continueButton.setTitle("tray.primary.button.ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
        
        guard let vehicles = vehicles else { return }
        dataSource = PrimaryTrayDataSource(tableView, preselectedVehicle: preselectedVehicle)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        dataSource.loadData(vehicles)
        dataSource.delegate = self
        
        continueButton.isEnabled = dataSource.selectedVehicle != nil
    }
    
    private func enableScroll(_ enabled: Bool) {
        tableView.isScrollEnabled = enabled
        tableView.alwaysBounceVertical = enabled
    }
    
    override var contentHeight: CGFloat {
        let cellsHeight = CGFloat(vehicles.count * Int(PrimaryTrayDataSource.Constants.cellHeight))
        var totalHeight = 265 + cellsHeight
        
        var maxHeight: CGFloat = UIScreen.main.bounds.height
        if let window = UIApplication.shared.windows.first {
            maxHeight -= window.safeAreaInsets.top
        }
        
        if totalHeight > maxHeight {
            totalHeight = maxHeight
            enableScroll(true)
            tableViewHeightConstraint.constant = totalHeight - 265
        } else {
            enableScroll(false)
            tableViewHeightConstraint.constant = cellsHeight
        }
        view.setNeedsLayout()
        
        return totalHeight
    }
    
    override var shouldDismissOnTap: Bool {
        shouldDismiss
    }
    
    override var shouldDismissOnDrag: Bool {
        shouldDismiss
    }
    
    @IBAction
    func onClose() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onAdd() {
        dismiss(animated: true)
        onAddVehicle?()
    }
    
    @IBAction
    func onContinue() {
        guard let vehicle = dataSource.selectedVehicle else { return }
        dismiss(animated: true)
        onSelect?(vehicle)
    }
}

extension PrimaryTrayViewController: PrimaryTrayDataSourceDelegate {
    func primaryTrayDataSourceDidSelectRow(_ dataSource: PrimaryTrayDataSource) {
        continueButton.isEnabled = true
    }
}
