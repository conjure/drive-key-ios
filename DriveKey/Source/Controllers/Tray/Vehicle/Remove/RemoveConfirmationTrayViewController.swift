//
//  RemoveConfirmationTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 22/07/2022.
//

import UIKit

enum RemoveConfirmationTrayType: Error {
    case vehicle
    case trip
}

class RemoveConfirmationTrayViewController: TrayViewController {
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var continueButton: RoundedButton!
    var type: RemoveConfirmationTrayType = .vehicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.textAlignment = .center
        headerLabel.text = type.title
        
        continueButton.setTitle("ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        168
    }
    
    override func willDismissOnTap() {
        onContinue?()
    }
    
    override func willDismissOnSwipe() {
        onContinue?()
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onContinue() {
        dismiss(animated: true)
        onContinue?()
    }
}

extension RemoveConfirmationTrayType {
    var title: String {
        switch self {
        case .vehicle:
            return "tray.remove.confirmation.label.header".localized
        case .trip:
            return "tray.remove.trip.confirmation.label.header".localized
        }
    }
}
