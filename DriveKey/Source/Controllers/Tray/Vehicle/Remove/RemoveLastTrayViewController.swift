//
//  RemoveLastTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 25/07/2022.
//

import UIKit

class RemoveLastTrayViewController: TrayViewController {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var cancelButton: RoundedButton!
    @IBOutlet private var continueButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "alert-triangle-icon")

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.textAlignment = .center
        headerLabel.text = "tray.remove.last.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center
        let infoText = "tray.remove.last.label.info".localized.subscriptCO2(.K2DRegular(10) ?? .systemFont(ofSize: 10))
        infoLabel.attributedText = infoText
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = .K2DMedium(15)
        
        continueButton.setTitle("tray.remove.last.button.ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        397
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onContinue() {
        dismiss(animated: true)
        onContinue?()
    }
}
