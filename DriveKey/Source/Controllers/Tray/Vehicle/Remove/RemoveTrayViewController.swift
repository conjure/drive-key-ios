//
//  RemoveTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 22/07/2022.
//

import UIKit

class RemoveTrayViewController: TrayViewController {
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var cancelButton: RoundedButton!
    @IBOutlet private var continueButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.textAlignment = .center
        headerLabel.text = "tray.remove.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center
        infoLabel.text = "tray.remove.label.info".localized
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = .K2DMedium(15)
        
        continueButton.setTitle("tray.remove.button.ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        294
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onContinue() {
        dismiss(animated: true)
        onContinue?()
    }
}
