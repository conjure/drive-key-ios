//
//  DeleteConfirmationTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/07/2022.
//

import UIKit

class DeleteConfirmationTrayViewController: TrayViewController {
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var continueButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 0
        headerLabel.textAlignment = .center
        headerLabel.text = "tray.delete.confirmation.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center
        infoLabel.text = "tray.delete.confirmation.label.info".localized
        
        continueButton.setTitle("tray.delete.confirmation.button.ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        312
    }
    
    override func willDismissOnTap() {
        onContinue?()
    }
    
    override func willDismissOnSwipe() {
        onContinue?()
    }
    
    @IBAction
    func onContinue() {
        onContinue?()
    }
}
