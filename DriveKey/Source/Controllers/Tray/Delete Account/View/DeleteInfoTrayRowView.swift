//
//  DeleteInfoTrayRowView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/07/2022.
//

import UIKit

class DeleteInfoTrayRowView: BaseView {
    private let imageView = Subviews.imageView
    private let infoLabel = Subviews.infoLabel
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(imageView)
        addSubview(infoLabel)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 20),
            imageView.heightAnchor.constraint(equalToConstant: 20),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor),
            infoLabel.topAnchor.constraint(equalTo: topAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 18),
            infoLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var text: String? {
        didSet {
            infoLabel.text = text
        }
    }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "tray-x-circle-icon")
        
        return view
    }
    
    static var infoLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DRegular(16)
        label.textColor = .black
        label.textAlignment = .left
        
        return label
    }
}
