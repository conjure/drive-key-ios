//
//  DeleteInfoTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/07/2022.
//

import UIKit

class DeleteInfoTrayViewController: TrayViewController {
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var stackView: UIStackView!
    @IBOutlet private var cancelButton: RoundedButton!
    @IBOutlet private var deleteButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.font = .K2DBold(18)
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .left
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.text = "tray.delete.info.label".localized
        
        addInfo("tray.delete.info.1".localized)
        addInfo("tray.delete.info.2".localized)
        addInfo("tray.delete.info.3".localized)
        addInfo("tray.delete.info.4".localized)
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = .K2DMedium(15)
        
        deleteButton.setTitle("tray.delete.info.button.ok".localized, for: .normal)
        deleteButton.titleLabel?.font = .K2DMedium(15)
        
        view.layoutIfNeeded()
    }
    
    private func addInfo(_ text: String) {
        let view = DeleteInfoTrayRowView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = text
        stackView.addArrangedSubview(view)
    }
    
    override var contentHeight: CGFloat {
        return 422
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onDelete() {
        dismiss(animated: true)
        onContinue?()
    }
}
