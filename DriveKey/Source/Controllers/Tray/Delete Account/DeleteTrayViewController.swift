//
//  DeleteTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/07/2022.
//

import UIKit

class DeleteTrayViewController: TrayViewController {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var cancelButton: RoundedButton!
    @IBOutlet private var continueButton: RoundedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "alert-triangle-icon")

        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.textAlignment = .center
        headerLabel.text = "tray.delete.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center
        infoLabel.text = "tray.delete.label.info".localized
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.backgroundColor = .clear
        cancelButton.titleLabel?.font = .K2DMedium(15)
        
        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
    }
    
    override var contentHeight: CGFloat {
        376
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onContinue() {
        dismiss(animated: true)
        onContinue?()
    }
}
