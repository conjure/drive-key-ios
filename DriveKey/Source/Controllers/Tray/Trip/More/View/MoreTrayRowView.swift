//
//  MoreTrayRowView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 16/09/2022.
//

import UIKit

class MoreTrayRowView: BaseView {
    private let imageView = Subviews.imageView
    private let infoLabel = Subviews.infoLabel
    
    var onTap: ((Int) -> Void)?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .white
        layer.cornerRadius = 12
        
        addSubview(imageView)
        addSubview(infoLabel)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTapRecognizer))
        addGestureRecognizer(tapRecognizer)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            infoLabel.topAnchor.constraint(equalTo: topAnchor),
            infoLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            infoLabel.trailingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 10),
            infoLabel.heightAnchor.constraint(equalToConstant: 50),
            infoLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            imageView.centerYAnchor.constraint(equalTo: infoLabel.centerYAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            imageView.widthAnchor.constraint(equalToConstant: 24),
            imageView.heightAnchor.constraint(equalToConstant: 24)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var text: String? {
        didSet {
            infoLabel.text = text
        }
    }
    
    var icon: UIImage? {
        didSet {
            imageView.image = icon
        }
    }
    
    @objc
    func onTapRecognizer() {
        onTap?(tag)
    }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "tray-x-circle-icon")
        
        return view
    }
    
    static var infoLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DRegular(16)
        label.textColor = .black
        label.textAlignment = .left
        
        return label
    }
}
