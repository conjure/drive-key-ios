//
//  MoreTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 16/09/2022.
//

import UIKit

enum MoreTrayOption: Int {
    case removeTrip
    case changeVehicle
}

class MoreTrayViewController: TrayViewController {
    @IBOutlet private var closeButton: UIButton!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var stackView: UIStackView!
    
    var onSelect: ((MoreTrayOption) -> Void)?
    var onAddVehicle: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .appColor(.trayLightGray)
        
        closeButton.setImage(UIImage(named: "close-tray-icon"), for: .normal)
        
        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 2
        headerLabel.textAlignment = .left
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.text = "tray.more.label.header".localized
        
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.spacing = 16
        
        addRow("tray.more.option.remove".localized,
               icon: UIImage(named: "trip-bin-icon")!,
               index: MoreTrayOption.removeTrip.rawValue)
        addRow("tray.more.option.change".localized,
               icon: UIImage(named: "trip-car-icon")!,
               index: MoreTrayOption.changeVehicle.rawValue)
    }
    
    private func addRow(_ text: String, icon: UIImage, index: Int) {
        let view = MoreTrayRowView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = text
        view.icon = icon
        view.tag = index
        view.onTap = { [weak self] index in
            self?.onRow(index)
        }
        
        stackView.addArrangedSubview(view)
    }
    
    private func onRow(_ index: Int) {
        dismiss(animated: true)
        guard let option = MoreTrayOption(rawValue: index) else { return }
        onSelect?(option)
    }
    
    override var contentHeight: CGFloat {
        221
    }
    
    @IBAction
    func onClose() {
        dismiss(animated: true)
    }
}
