//
//  RemoveTripTrayTableViewSection.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2022.
//

import UIKit

class RemoveTripTrayTableViewSection: BaseView {
    private let titleLabel = Subviews.titleLabel
    private let leftSeparatorView = Subviews.separatorView
    private let rightSeparatorView = Subviews.separatorView

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        
        addSubview(leftSeparatorView)
        addSubview(titleLabel)
        addSubview(rightSeparatorView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            leftSeparatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            leftSeparatorView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            leftSeparatorView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            leftSeparatorView.heightAnchor.constraint(equalToConstant: 2),
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            titleLabel.widthAnchor.constraint(equalToConstant: 44),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            rightSeparatorView.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            rightSeparatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            rightSeparatorView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            rightSeparatorView.heightAnchor.constraint(equalToConstant: 2)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
}

private struct Subviews {
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(16)
        label.textColor = .appColor(.navyBlue).withAlphaComponent(0.5)
        label.textAlignment = .center
        label.text = "tray.remove.trip.option.separator.text".localized

        return label
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black.withAlphaComponent(0.1)

        return view
    }
}
