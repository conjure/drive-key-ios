//
//  RemoveTripTrayTableViewCell.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2022.
//

import UIKit

class RemoveTripTrayTableViewCell: UITableViewCell {
    private let radioImageView = Subviews.radioImageView
    private let iconImageView = Subviews.iconImageView
    private let reasonLabel = Subviews.reasonLabel
    private var iconImageViewWidthContraint: NSLayoutConstraint!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setUpCell()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("[RemoveTripTrayTableViewCell] init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        reasonLabel.text = nil
        iconImageView.image = nil
    }

    private func setUpCell() {
        selectionStyle = .none
        contentView.backgroundColor = .clear
        backgroundColor = .clear

        contentView.addSubview(radioImageView)
        contentView.addSubview(iconImageView)
        contentView.addSubview(reasonLabel)
        
        iconImageViewWidthContraint = iconImageView.widthAnchor.constraint(equalToConstant: 24)
        
        let constraint = [
            radioImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            radioImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            radioImageView.widthAnchor.constraint(equalToConstant: 24),
            radioImageView.heightAnchor.constraint(equalToConstant: 24),
            iconImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            iconImageView.leadingAnchor.constraint(equalTo: radioImageView.trailingAnchor, constant: 8),
            iconImageViewWidthContraint!,
            iconImageView.heightAnchor.constraint(equalToConstant: 24),
            reasonLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            reasonLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 12),
            reasonLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            reasonLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: RemoveTripTrayCellViewModel? {
        didSet {
            guard let data = data else { return }
            reasonLabel.text = data.reason
            
            if data.image != nil {
                iconImageView.image = UIImage(named: data.image!)
                iconImageViewWidthContraint.constant = 24
            } else {
                iconImageViewWidthContraint.constant = 0
            }
            setNeedsLayout()
        }
    }
    
    private func selectRadio(_ selected: Bool) {
        let imageName = selected ? "primary-on-icon" : "primary-off-icon"
        radioImageView.image = UIImage(named: imageName)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectRadio(selected)
    }
}

private struct Subviews {
    static var radioImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }
    
    static var iconImageView: UIImageView {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        
        return view
    }

    static var reasonLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(18)
        label.numberOfLines = 1
        label.textColor = .black
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true

        return label
    }
}
