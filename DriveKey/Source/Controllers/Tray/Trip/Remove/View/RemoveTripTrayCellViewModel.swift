//
//  RemoveTripTrayCellViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2022.
//

import Foundation

class RemoveTripTrayCellViewModel {
    private(set) var reason: String
    private(set) var image: String?
    var isSelected = false
    
    init(_ reason: String, image: String?) {
        self.reason = reason
        self.image = image
    }
}
