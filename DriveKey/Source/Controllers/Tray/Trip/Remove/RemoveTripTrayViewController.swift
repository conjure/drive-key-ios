//
//  RemoveTripTrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2022.
//

import UIKit

class RemoveTripTrayViewController: TrayViewController {
    @IBOutlet private var cancelButton: UIButton!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var tableLabel: UILabel!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var tableViewHeightConstraint: NSLayoutConstraint!
    
    private var dataSource: RemoveTripTrayDataSource!
    
    var onSelect: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cancelButton.setTitle("cancel".localized, for: .normal)
        cancelButton.titleLabel?.font = .K2DSemiBold(14)
        cancelButton.setTitleColor(.black, for: .normal)
        cancelButton.contentHorizontalAlignment = .right
        
        headerLabel.font = .K2DBold(24)
        headerLabel.numberOfLines = 1
        headerLabel.textAlignment = .center
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.text = "tray.remove.trip.label.header".localized
        
        infoLabel.font = .K2DRegular(16)
        infoLabel.numberOfLines = 0
        infoLabel.textAlignment = .center
        infoLabel.text = "tray.remove.trip.label.info".localized
        
        tableLabel.font = .K2DBold(16)
        tableLabel.numberOfLines = 1
        tableLabel.textAlignment = .left
        tableLabel.text = "tray.remove.trip.label.table".localized
        
        tableView.backgroundColor = .clear
        tableView.register(RemoveTripTrayTableViewCell.self, forCellReuseIdentifier: RemoveTripTrayTableViewCell.className)
        tableView.separatorStyle = .none
        tableView.sectionHeaderHeight = 0
        tableView.showsVerticalScrollIndicator = false
        if #available(iOS 15.0, *) {
            tableView.sectionHeaderTopPadding = 0
        }
        
        continueButton.setTitle("tray.remove.trip.button.ok".localized, for: .normal)
        continueButton.titleLabel?.font = .K2DMedium(15)
        
        dataSource = RemoveTripTrayDataSource(tableView)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        dataSource.loadData()
        dataSource.delegate = self
        
        continueButton.isEnabled = false
    }
    
    private func enableScroll(_ enabled: Bool) {
        tableView.isScrollEnabled = enabled
        tableView.alwaysBounceVertical = enabled
    }
    
    override var contentHeight: CGFloat {
        let cellsHeight = CGFloat(dataSource.items.count * Int(RemoveTripTrayDataSource.Constants.cellHeight)) + RemoveTripTrayDataSource.Constants.sectionHeight
        let baseHeight: CGFloat = 301
        var totalHeight = baseHeight + cellsHeight
        
        var maxHeight: CGFloat = UIScreen.main.bounds.height
        if let window = UIApplication.shared.windows.first {
            maxHeight -= window.safeAreaInsets.top
        }
        
        if totalHeight > maxHeight {
            totalHeight = maxHeight
            enableScroll(true)
            tableViewHeightConstraint.constant = totalHeight - baseHeight
        } else {
            enableScroll(false)
            tableViewHeightConstraint.constant = cellsHeight
        }
        view.setNeedsLayout()
        
        return totalHeight
    }
    
    @IBAction
    func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction
    func onContinue() {
        guard let reason = dataSource.selectedReason else { return }
        dismiss(animated: true)
        onSelect?(reason)
    }
}

extension RemoveTripTrayViewController: RemoveTripTrayDataSourceDelegate {
    func removeTripTrayDataSourceDidSelectRow(_ dataSource: RemoveTripTrayDataSource) {
        continueButton.isEnabled = true
    }
}
