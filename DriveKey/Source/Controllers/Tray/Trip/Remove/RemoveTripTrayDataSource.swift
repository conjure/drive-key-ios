//
//  RemoveTripTrayDataSource.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2022.
//

import UIKit

protocol RemoveTripTrayDataSourceDelegate: AnyObject {
    func removeTripTrayDataSourceDidSelectRow(_ dataSource: RemoveTripTrayDataSource)
}

class RemoveTripTrayDataSource: NSObject {
    private let tableView: UITableView
    private(set) var items = [RemoveTripTrayCellViewModel]()
    private(set) var selectedReason: String?
    
    weak var delegate: RemoveTripTrayDataSourceDelegate?
    
    struct Constants {
        static let cellHeight: CGFloat = 50
        static let sectionHeight: CGFloat = 66
    }
    
    init(_ tableView: UITableView) {
        self.tableView = tableView
    }
    
    func loadData() {
        items.removeAll()
        
        for i in 0..<options.count {
            let reason = options[i]
            let image = images[i]
            let imageStr = image.isEmpty ? nil : image
            let cellModel = RemoveTripTrayCellViewModel(reason, image: imageStr)
            items.append(cellModel)
        }
        
        tableView.reloadData()
    }
    
    var options: [String] {
        [
            "tray.remove.trip.option.pasenger".localized,
            "tray.remove.trip.option.taxi".localized,
            "tray.remove.trip.option.bus".localized,
            "tray.remove.trip.option.motorcycle".localized,
            "tray.remove.trip.option.bike".localized,
            "tray.remove.trip.option.other".localized
        ]
    }
    
    var images: [String] {
        [
            "tray-passenger-icon",
            "tray-taxi-icon",
            "tray-bus-icon",
            "tray-motorcycle-icon",
            "tray-bike-icon",
            ""
        ]
    }
    
    private func data(_ indexPath: IndexPath) -> RemoveTripTrayCellViewModel {
        if indexPath.section == 0 {
            return items[indexPath.row]
        }
        
        return items[options.count - 1]
    }
}

extension RemoveTripTrayDataSource: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constants.cellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return Constants.sectionHeight
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let sectionView = RemoveTripTrayTableViewSection()
            sectionView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: Constants.sectionHeight)
            return sectionView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        CGFloat.leastNonzeroMagnitude
    }
}

extension RemoveTripTrayDataSource: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return items.count - 1
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = data(indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RemoveTripTrayTableViewCell.className, for: indexPath) as! RemoveTripTrayTableViewCell
        
        cell.data = data
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = data(indexPath)
        selectedReason = data.reason
        delegate?.removeTripTrayDataSourceDidSelectRow(self)
    }
}
