//
//  BiometricViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import Foundation

class BiometricViewModel {
    private let biometricService: BiometricService
    private let userDefaults: UserDefaults
    private let fromBubble: Bool

    init(biometricService: BiometricService, userDefaults: UserDefaults = .standard, fromBubble: Bool) {
        self.biometricService = biometricService
        self.userDefaults = userDefaults
        self.fromBubble = fromBubble
    }

    var isBubbleTriggered: Bool {
        fromBubble
    }

    var biometricType: BiometricType {
        biometricService.sourceType
    }

    func enableBiometrics() -> AnyPublisher<Bool, Error> {
        Future<Bool, Error> { promise in
            self.biometricService.evaluate { _, error in
                if let error = error {
                    switch error {
                    case .userFallback:
                        self.userDefaults.set(BiometricStatus.unknown.rawValue, forKey: UserDefaultKeys.biometricStatus)
                        return promise(.success(false))
                    default:
                        self.userDefaults.set(BiometricStatus.denied.rawValue, forKey: UserDefaultKeys.biometricStatus)
                    }

                    return promise(.failure(error))
                } else {
                    self.userDefaults.set(BiometricStatus.granted.rawValue, forKey: UserDefaultKeys.biometricStatus)
                    AnalyticsService.trackEvent(.enableBiometrics)
                    return promise(.success(true))
                }
            }
        }.eraseToAnyPublisher()
    }

    func delayPermission() {
        if isBubbleTriggered {
            userDefaults.set(BiometricStatus.dismissedBubble.rawValue, forKey: UserDefaultKeys.biometricStatus)
        } else {
            userDefaults.set(BiometricStatus.unknown.rawValue, forKey: UserDefaultKeys.biometricStatus)
        }
    }
}
