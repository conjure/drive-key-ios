//
//  BiometricViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Combine
import UIKit

protocol BiometricViewControllerDelegate: AnyObject {
    func biometricViewControllerWillActionLater(_ vc: BiometricViewController)
    func biometricViewControllerDidEnable(_ vc: BiometricViewController)
    func biometricViewContollerDidActionBubble(_ vc: BiometricViewController)
}

class BiometricViewController: ViewController {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet private var iconView: UIImageView!
    @IBOutlet private var detailLabel: UILabel!
    @IBOutlet private var declineButton: SmallButton!
    @IBOutlet private var enableButton: RoundedButton!

    private var tasks = Set<AnyCancellable>()
    private var alertTitle: String = ""
    var viewModel: BiometricViewModel!
    weak var delegate: BiometricViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        switch viewModel.biometricType {
        case .faceID:
            alertTitle = "Face ID"
            headingLabel.text = "biometric.face.heading.label".localized
            detailLabel.text = "biometric.face.detail.label".localized
            iconView.image = UIImage(named: "faceid")
        case .touchID:
            alertTitle = "Touch ID"
            headingLabel.text = "biometric.touch.heading.label".localized
            detailLabel.text = "biometric.touch.detail.label".localized
            iconView.image = UIImage(named: "touch_id")
        default:
            print("Unsupported biometric type")
            headingLabel.text = "".localized
            detailLabel.text = "".localized
        }

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.37
        let buttonTitle = viewModel.isBubbleTriggered ? "bubble.dismiss.button".localized : "biometric.decline.button".localized
        let attributes = [.underlineStyle: NSUnderlineStyle.single.rawValue,
                          .kern: 0.11,
                          .paragraphStyle: paragraphStyle,
                          .font: UIFont.link,
                          .foregroundColor: UIColor.black] as [NSAttributedString.Key: Any]
        let attrButtonTitle = NSMutableAttributedString(string: buttonTitle,
                                                        attributes: attributes)
        declineButton.setAttributedTitle(attrButtonTitle, for: .normal)

        enableButton.setTitle("biometric.accept.button".localized, for: .normal)

        if viewModel.isBubbleTriggered {
            let button = UIButton(type: .system)
            button.setImage(UIImage(named: "close-icon"), for: .normal)
            button.tintColor = .white
            button.translatesAutoresizingMaskIntoConstraints = false
            button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
            view.addSubview(button)

            let constraints = [
                button.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
                button.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -26),
                button.heightAnchor.constraint(equalToConstant: 19),
                button.widthAnchor.constraint(equalToConstant: 19)
            ]
            NSLayoutConstraint.activate(constraints)
        }
    }

    @objc private func dismissScreen() {
        dismiss(animated: true, completion: nil)
    }

    @IBAction private func declineBiometric(_: Any) {
        viewModel.delayPermission()
        deferAction()
    }

    @IBAction private func enableBiometric(_: Any) {
        viewModel.enableBiometrics()
            .sink { completion in
                switch completion {
                case let .failure(error):
                    if case BiometricError.userCancel = error {
                        return
                    } else if case BiometricError.biometryNotAvailable = error {
                        self.deferAction()
                        return
                    }
                    self.showAlert(self.alertTitle, message: error.localizedDescription) { _ in
                        self.deferAction()
                    }
                default:
                    break
                }
            } receiveValue: { enabled in
                if self.viewModel.isBubbleTriggered {
                    self.delegate?.biometricViewContollerDidActionBubble(self)
                } else if enabled {
                    self.delegate?.biometricViewControllerDidEnable(self)
                } else {
                    self.delegate?.biometricViewControllerWillActionLater(self)
                }
            }
            .store(in: &tasks)
    }

    private func deferAction() {
        if viewModel.isBubbleTriggered {
            delegate?.biometricViewContollerDidActionBubble(self)
        } else {
            delegate?.biometricViewControllerWillActionLater(self)
        }
    }
}
