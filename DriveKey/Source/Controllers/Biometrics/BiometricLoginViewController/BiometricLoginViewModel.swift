//
//  BiometricLoginViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/08/2021.
//

import Combine
import Foundation

class BiometricLoginViewModel: ObservableObject {
    private let biometricService: BiometricService

    init(biometricService: BiometricService) {
        self.biometricService = biometricService
    }

    func authenticate() -> AnyPublisher<Bool, Error> {
        Future<Bool, Error> { promise in
            self.biometricService.authenticateWithPasscode { _, error in
                if let error = error {
                    switch error {
                    case .userFallback:
                        return promise(.success(false))
                    default:
                        break
                    }

                    return promise(.failure(error))
                } else {
                    return promise(.success(true))
                }
            }
        }.eraseToAnyPublisher()
    }
}
