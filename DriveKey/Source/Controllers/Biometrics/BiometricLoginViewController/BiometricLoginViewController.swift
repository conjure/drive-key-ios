//
//  BiometricLoginViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/08/2021.
//

import Combine
import UIKit

protocol BiometricLoginViewControllerDelegate: AnyObject {
    func biometricLoginViewControllerWillLoginWithEmail(_ vc: BiometricLoginViewController)
    func biometricLoginViewControllerDidLogin(_ vc: BiometricLoginViewController)
}

class BiometricLoginViewController: ViewController {
    @IBOutlet private var emailLoginButton: RoundedButton!

    private var tasks = Set<AnyCancellable>()
    var viewModel: BiometricLoginViewModel!
    weak var delegate: BiometricLoginViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        emailLoginButton.setTitle("biometric.login.email.button".localized, for: .normal)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        viewModel.authenticate()
            .sink { completion in
                switch completion {
                case .failure:
                    return
                default:
                    break
                }
            } receiveValue: { enabled in
                if enabled {
                    self.delegate?.biometricLoginViewControllerDidLogin(self)
                }
            }
            .store(in: &tasks)
    }

    @IBAction private func loginWithEmail(_: Any) {
        delegate?.biometricLoginViewControllerWillLoginWithEmail(self)
    }
}
