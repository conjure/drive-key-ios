//
//  VerifyEmailViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 15/09/2021.
//

import Foundation

struct VerifyEmailViewModel {
    private let userService: UserProvidable
    private let authService: AuthService

    init(authService: AuthService, userService: UserProvidable) {
        self.authService = authService
        self.userService = userService
    }

    var emailAddress: String {
        userService.currentUser?.email ?? ""
    }

    func sendVerifyEmail(_ completion: @escaping (Error?) -> Void) {
        authService.sendEmailVerification { error in
            completion(error)
        }
    }
}
