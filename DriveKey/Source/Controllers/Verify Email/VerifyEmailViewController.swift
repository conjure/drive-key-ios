//
//  VerifyEmailViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 15/09/2021.
//

import UIKit

protocol VerifyEmailViewControllerDelegate: AnyObject {
    func verifyEmailViewControllerDidSendEmail(_ vc: VerifyEmailViewController)
    func verifyEmailViewControllerDidError(_ vc: VerifyEmailViewController)
}

class VerifyEmailViewController: ViewController {
    @IBOutlet private var confirmButton: RoundedButton!
    @IBOutlet private var emailField: NamedLabel!

    var viewModel: VerifyEmailViewModel!
    weak var delegate: VerifyEmailViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.prefersLargeTitles = true
        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        title = "verify.email.title".localized

        emailField.name = "verify.email.textfield.title".localized
        emailField.value = viewModel.emailAddress
        confirmButton.setTitle("verify.email.button".localized, for: .normal)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if let navigationController = navigationController as? NavigationController {
            navigationController.setDefaultTheme()
        }
    }

    @IBAction private func didConfirmEmail(_: Any) {
        showSpinner()
        viewModel.sendVerifyEmail { error in
            self.hideSpinner()
            if let error = error {
                print("Error sending verification email - \(error)")
                self.delegate?.verifyEmailViewControllerDidError(self)
            } else {
                self.delegate?.verifyEmailViewControllerDidSendEmail(self)
            }
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
