//
//  CurrencyViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import Foundation
import Combine

class CurrencyViewModel {
    private let userService: UserProvidable
    let currencies: [Currency]
    
    var selectedCurrency: Currency?
    
    init(_ userService: UserProvidable, currencies: [Currency]) {
        self.userService = userService
        self.currencies = currencies
        
        if let user = userService.currentUser, let currencyCode = user.currencyCode {
            self.selectedCurrency = currencies.first(where: { $0.code.lowercased() == currencyCode.lowercased() })
        }
    }
}
