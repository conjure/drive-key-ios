//
//  CurrencyViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import UIKit

class CurrencyViewController: ViewController {
    @IBOutlet var currencyField: NamedTextField!
    
    var viewModel: CurrencyViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        title = "currency.title".localized
        
        currencyField.name = "currency.textfield.name".localized
        currencyField.isDisabled = true

        if let currency = viewModel.selectedCurrency {
            currencyField.leftIcon = UIImage(named: currency.iconName)
            currencyField.value = currency.code
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
