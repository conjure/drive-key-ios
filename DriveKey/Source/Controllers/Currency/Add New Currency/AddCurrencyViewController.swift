//
//  AddCurrencyViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/08/2022.
//

import UIKit
import Combine

protocol AddCurrencyViewControllerDelegate: AnyObject {
    func addCurrencyViewControllerDidUpdateCurrency(_ viewController: AddCurrencyViewController)
}

class AddCurrencyViewController: ViewController, PickerPresentable, TrayPresentable {
    @IBOutlet private var headingLabel: UILabel!
    @IBOutlet var pickerField: NamedPickerField!
    @IBOutlet var continueButton: RoundedButton!

    var viewModel: AddCurrencyViewModel!
    weak var delegate: AddCurrencyViewControllerDelegate?
    private var cancellables = Set<AnyCancellable>()

    override func viewDidLoad() {
        super.viewDidLoad()
        print("[AddCurrencyViewController] AppDataService viewDidLoad")
        navigationItem.hidesBackButton = true

        headingLabel.text = "currency.title.add".localized

        pickerField.name = "currency.textfield.name".localized
        pickerField.textField.placeholder = "currency.textfield.placeholder".localized
        pickerField.addRightButton(UIImage(named: "picker-arrow-down")!, size: CGSize(width: 8, height: 16))
        pickerField.onRightButton = { [weak self] in
            self?.onArrow()
        }

        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.isEnabled = false

        updateSelectedCurrency()
        if viewModel.currencies.isEmpty {
            showSpinner()
            registerSubscriptions()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presentedViewController?.dismiss(animated: false)
    }
    
    private func registerSubscriptions() {
        viewModel.$shouldReload
            .receive(on: DispatchQueue.main)
            .dropFirst()
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.updateSelectedCurrency()
                }
            }
            .store(in: &cancellables)
    }
    
    private func updateSelectedCurrency() {
        if let index = viewModel.selectedCurrencyIndex {
            updateData(index)
        }
    }

    private func updateData(_ index: Int) {
        let currencies = viewModel.currencies
        guard currencies.count > index else { return }
        let currency = currencies[index]
        updatePicker(currency)
        viewModel.selectedCurrency = currency
        continueButton.isEnabled = true
    }

    private func updatePicker(_ data: Currency?) {
        guard let currency = data else { return }
        pickerField.leftIcon = UIImage(named: currency.iconName)
        pickerField.value = currency.code
    }

    private func updateUser() {
        showSpinner()
        viewModel.updateUser { error in
            self.hideSpinner()

            if let error = error {
                self.showAlertError(error: error)
            } else {
                self.delegate?.addCurrencyViewControllerDidUpdateCurrency(self)
            }
        }
    }

    private func presentPicker(_ currencies: [Currency]) {
        showPicker(currencies.map({ $0.code })) { [weak self] index in
            self?.updateData(index)
        }
    }

    private func onArrow() {
        let currencies = viewModel.currencies
        guard !currencies.isEmpty else { return }
        presentPicker(currencies)
    }

    @IBAction
    func onContinue() {
        showTrayConfirmCurrency {
            self.updateUser()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

}
