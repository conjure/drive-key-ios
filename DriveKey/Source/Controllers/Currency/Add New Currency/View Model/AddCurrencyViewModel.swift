//
//  AddCurrencyViewModel.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/08/2022.
//

import Foundation
import Combine

class AddCurrencyViewModel {
    private let api: APIProtocol
    private let userService: UserProvidable
    private let appService: AppDataProvidable
    private var cancellables = Set<AnyCancellable>()
    
    private(set) var currencies = [Currency]()
    var selectedCurrency: Currency?
    
    @Published var shouldReload = false

    init(_ api: APIProtocol, userService: UserProvidable, appService: AppDataProvidable) {
        self.api = api
        self.userService = userService
        self.appService = appService

        // Select default based on locale
        if !appService.currencies.isEmpty {
            updateDefaultCurrency()
        } else {
            registerSubscriptions()
        }
    }
    
    private func registerSubscriptions() {
        appService.dataUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                if success {
                    self?.updateDefaultCurrency()
                    self?.shouldReload = true
                }
            }
            .store(in: &cancellables)
    }
    
    private func updateDefaultCurrency() {
        currencies = appService.currencies
        
        if let code = Locale.current.currencyCode, let currency = currencies.first(where: { $0.code.lowercased().contains(code.lowercased())}) {
            selectedCurrency = currency
        }
    }

    func updateUser(_ completion: @escaping (Error?) -> Void) {
        guard let currency = selectedCurrency else {
            completion(AddCurrencyError.noSelectedCurrency)
            return
        }

        let update = UpdateUserPayload(currency: currency.name, currencyCode: currency.code)
        api.userData(update) { result in
            switch result {
            case .success:
                completion(nil)
            case let .failure(error):
                completion(error)
            }
        }
    }

    var selectedCurrencyIndex: Int? {
        if let currency = selectedCurrency {
            return currencies.firstIndex(where: { $0.code == currency.code })
        }

        return nil
    }

    enum AddCurrencyError: Error, LocalizedError {
        case noSelectedCurrency

        var errorDescription: String? {
            switch self {
            case .noSelectedCurrency:
                return "error.auth.internal".localized
            }
        }
    }
}
