//
//  AboutKaraiViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/08/2021.
//

import UIKit

class AboutKaraiViewController: ViewController {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView1: AboutKaraiContentView!
    @IBOutlet private var contentView2: AboutKaraiContentView!
    @IBOutlet private var contentView3: AboutKaraiContentView!

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "about.title".localized
        contentView1.update(iconName: "welcome_icon_1", content: "about.message.1".localized)
        contentView2.update(iconName: "welcome_icon_2", content: "about.message.2".localized)
        contentView3.update(iconName: "welcome_icon_3", content: "about.message.3".localized, hideSeparator: true)

        if let navigationController = navigationController as? NavigationController {
            navigationController.setDefaultTheme()
        }

        scrollView.indicatorStyle = .white
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.flashScrollIndicators()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
    }
}
