//
//  AboutKaraiContentView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/08/2021.
//

import UIKit

class AboutKaraiContentView: XibView {
    @IBOutlet private var iconView: UIImageView!
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var separatorLine: UIView!

    override func setup() {
        super.setup()

        if UIScreen.main.bounds.width <= 375 {
            contentLabel.font = .K2DRegular(14)
        }
    }

    func update(iconName: String, content: String, hideSeparator: Bool = false) {
        iconView.image = UIImage(named: iconName)
        contentLabel.text = content
        separatorLine.isHidden = hideSeparator
    }
}
