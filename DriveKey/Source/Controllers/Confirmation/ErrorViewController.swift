//
//  ErrorViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 15/09/2021.
//

import UIKit

enum ErrorConfirmationType {
    case errEmailVerification
    case paymentError(message: String)
}

protocol ErrorViewControllerDelegate: AnyObject {
    func errorViewController(_ vc: ErrorViewController, didConfirm type: ErrorConfirmationType)
}

class ErrorViewController: ViewController {
    @IBOutlet private var iconView: UIImageView!
    @IBOutlet private var messageLabel: UILabel!
    @IBOutlet private var confirmButton: RoundedButton!

    var confirmationType: ErrorConfirmationType!
    weak var delegate: ErrorViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        messageLabel.font = .K2DBold(18)

        switch confirmationType {
        case .paymentError(let message):
            iconView.image = UIImage(named: "error_cross")
            messageLabel.textColor = .appColor(.red)
            messageLabel.text = message
            confirmButton.setTitle("one.time.payment.retry".localized, for: .normal)
        case .errEmailVerification:
            iconView.image = UIImage(named: "error_cross")
            messageLabel.textColor = .appColor(.red)
            messageLabel.text = "error.screen.email.verify".localized
            confirmButton.setTitle("ok".localized, for: .normal)
        case .none:
            break
        }

        navigationItem.hidesBackButton = true
    }

    @IBAction func didConfirm(_: Any) {
        delegate?.errorViewController(self, didConfirm: confirmationType)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
