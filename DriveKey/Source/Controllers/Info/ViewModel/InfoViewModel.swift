//
//  InfoViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/01/2022.
//

import Foundation
import SwiftUI

enum InfoSection: Int {
    case whatIsCO2
    case whyCO2Matters
    case whyFocusOnDriving
    case howHelpReduceCO2
    case whereMyMoneyGoes
    case offsetCO2Costs
    case whatEcoDrivingScore
    case ecoDrivingTips
}

struct InfoViewModel {
    let section: InfoSection?
    
    var sectionIndex: Int? {
        section?.rawValue
    }
}
