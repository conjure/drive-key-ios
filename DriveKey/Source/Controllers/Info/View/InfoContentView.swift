//
//  InfoContentView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

final class InfoContentView: BaseView {
    private let titleLabel = Subviews.titleLabel
    private let separatorView = Subviews.separatorView
    
    override func setUpSubviews() {
       super.setUpSubviews()
      
        backgroundColor = .clear
        addSubview(titleLabel)
        addSubview(separatorView)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 9),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var title: String? {
        didSet {
            let attStr = title?.subscriptCO2(.K2DRegular(8) ?? .systemFont(ofSize: 8))
            titleLabel.attributedText = attStr
        }
    }
}

private struct Subviews {
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DRegular(16)
        label.textColor = .appColor(.pastelBlue)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(hex: 0xB8B8B8)
        view.alpha = 0.2

        return view
    }
}
