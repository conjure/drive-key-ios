//
//  InfoSectionView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

class InfoSectionView: BaseView {
    private let titleLabel = Subviews.titleLabel
    private let arrowImageView = Subviews.arrowImageView
    private let separatorView = Subviews.separatorView
    
    var sectionIndex: Int = 0
    var isSelected = false
    
    override func setUpSubviews() {
       super.setUpSubviews()
      
        backgroundColor = .clear
        addSubview(titleLabel)
        addSubview(arrowImageView)
        addSubview(separatorView)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 14),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            titleLabel.trailingAnchor.constraint(equalTo: arrowImageView.leadingAnchor, constant: -10),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -14),
            arrowImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            arrowImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            arrowImageView.widthAnchor.constraint(equalToConstant: 9),
            arrowImageView.heightAnchor.constraint(equalToConstant: 7),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var title: String? {
        didSet {
            let attStr = title?.subscriptCO2(.K2DRegular(8) ?? .systemFont(ofSize: 8))
            titleLabel.attributedText = attStr
        }
    }
}

// MARK: - AccordionSectionable
extension InfoSectionView: AccordionSectionable {
    func toggle() {
        isSelected.toggle()
        
        let rotation: Double = isSelected ? 180 : 0
        arrowImageView.rotate(rotation)
        
        let alpha: CGFloat = isSelected ? 0 : 0.2
        UIView.animate(withDuration: 0.15) {
            self.separatorView.alpha = alpha
        }
    }
}

private struct Subviews {
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .K2DRegular(16)
        label.textColor = .white
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }
    
    static var arrowImageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "info-expand-arrow")
        
        return view
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(hex: 0xB8B8B8)
        view.alpha = 0.2

        return view
    }
}
