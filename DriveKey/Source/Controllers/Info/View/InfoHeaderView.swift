//
//  InfoHeaderView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

class InfoHeaderView: BaseView {
    private let imageView = Subviews.imageView
    
    override func setUpSubviews() {
       super.setUpSubviews()
      
        backgroundColor = .clear
        addSubview(imageView)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let width = UIScreen.main.bounds.width - 2 * 25
        let height = width * 0.68 // 280 to 190 ratio
        let constraints = [
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25),
            imageView.heightAnchor.constraint(equalToConstant: height.rounded(.down)),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -40)
        ]
        NSLayoutConstraint.activate(constraints)
    }
}

private struct Subviews {
    static var imageView: UIImageView {
        let view = UIImageView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "info-header-dashboard")
        
        return view
    }
}
