//
//  InfoViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

class InfoViewController: ViewController {
    @IBOutlet weak var accordionView: AccordionView!
    
    var viewModel: InfoViewModel!
    private var dataSource: InfoDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // settings
        view.backgroundColor = UIColor.appColor(.navyBlue)
        
        // title
        navigationController?.navigationBar.prefersLargeTitles = true
        if let navigationController = navigationController as? NavigationController {
            navigationController.setDefaultTheme()
        }
        title = "info.title".localized
        
        // close
        let closeButton = UIBarButtonItem(image: UIImage(named: "close-arrow-icon"),
                                          style: .plain,
                                          target: self,
                                          action: #selector(onClose))
        navigationItem.rightBarButtonItem = closeButton
        
        // accordion
        let headerView = InfoHeaderView(frame: CGRect.zero)
        accordionView.setHeaderView(headerView)
        accordionView.tableView.backgroundColor = .clear
        accordionView.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        
        // data source
        dataSource = InfoDataSource(accordionView)
        accordionView.dataSource = dataSource
        dataSource.reloadData()
        
        // section
        if let section = self.viewModel.sectionIndex, section < self.dataSource.items.count {
            runOnMainThreadAsyncAfter(0.5) {
                self.accordionView.open(section, scrollToVisible: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = .white
    }
    
    @objc
    func onClose() {
        dismiss(animated: true, completion: nil)
    }
}
