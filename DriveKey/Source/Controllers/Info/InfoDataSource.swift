//
//  InfoDataSource.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

class InfoDataSource {
    typealias InfoItemData = (title: String, description: String)
    private(set) var items = [InfoItemData]()
    private let accordionView: AccordionView
    
    static let sectionCount: Int = 8
    
    init(_ tableView: AccordionView) {
        self.accordionView = tableView
    }
    
    func reloadData() {
        loadData()
        accordionView.reloadData()
    }
    
    private func loadData() {
        items.removeAll()
        
        for i in 0..<InfoDataSource.sectionCount {
            let item = InfoItemData(getSectionTitle(i), getSectionDescription(i))
            items.append(item)
        }
    }
    
    private func getSectionTitle(_ index: Int) -> String {
        let result = "info.section.name." + String(index + 1)
        return result.localized
    }
    
    private func getSectionDescription(_ index: Int) -> String {
        let result = "info.section.description." + String(index + 1)
        return result.localized
    }
}

// MARK: - AccordionViewDataSource
extension InfoDataSource: AccordionViewDataSource {
    func numberOfSections(_ accordionView: AccordionView) -> Int {
        return items.count
    }
    
    func accordionView(_ accordionView: AccordionView, viewForSection index: Int) -> AccordionSectionable {
        let data = items[index]
        
        // section
        let sectionView = InfoSectionView(frame: CGRect.zero)
        sectionView.title = data.title
        
        return sectionView
    }
    
    func accordionView(_ accordionView: AccordionView, viewForSectionContent index: Int) -> UIView {
        // data
        let data = items[index]
        
        let contentView = InfoContentView(frame: CGRect.zero)
        contentView.title = data.description
        
        // fallback
        return contentView
    }
}
