//
//  VehicleLookupEditViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2021.
//

import Foundation

struct VehicleLookupEditViewModel {
    let vehicleData: VehicleData

    var fuelType: String {
        ""
    }

    var fuelConsumption: String {
        ""
    }

    var bhp: String {
        ""
    }

    var engineCapacity: String {
        ""
    }
}
