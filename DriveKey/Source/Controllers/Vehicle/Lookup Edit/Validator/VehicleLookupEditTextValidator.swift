//
//  VehicleLookupEditTextValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2021.
//

import Foundation

class VehicleLookupEditTextValidator: TextValidatable {
    static let TextMinimumLenght = 1
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }

    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text else {
                return true
            }

            return text.removeAllSpaces().count >= VehicleLookupEditTextValidator.TextMinimumLenght
        })
    }
}
