//
//  VehicleLookupEditViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2021.
//

import UIKit

protocol VehicleLookupEditViewControllerDelegate: AnyObject {
    func vehicleLookupEditViewControllerDidTapSave(_ viewController: VehicleLookupEditViewController, vehicleData: VehicleData)
}

class VehicleLookupEditViewController: ViewController {
    @IBOutlet private var karaiImageView: UIImageView!
    @IBOutlet private var cardView: CardContainerView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var modelTextField: NamedTextField!
    @IBOutlet private var makeTextField: NamedTextField!
    @IBOutlet private var editionTextField: NamedTextField!
    @IBOutlet private var yearTextField: NamedTextField!
    @IBOutlet private var co2TextField: NamedTextField!
    @IBOutlet private var saveButton: RoundedButton!
    @IBOutlet private var headerTopSpaceConstraint: NSLayoutConstraint!

    var viewModel: VehicleLookupEditViewModel!
    weak var delegate: VehicleLookupEditViewControllerDelegate?
    var keyboardObservables: [Any]? = [Any]()
    var keyboardConstraint: NSLayoutConstraint?
    var keyboardConstraintOffset: CGFloat?

    deinit {
        stopObservingKeyboardChanges()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.appColor(.navyBlue)
        view.sendSubviewToBack(cardView)
        view.sendSubviewToBack(karaiImageView)
        scrollView.backgroundColor = .clear
        contentView.backgroundColor = .clear

        startObservingKeyboardChanges()
        setupHideKeyboardOnTap()

        if UIScreen.main.bounds.height < 812 {
            headerTopSpaceConstraint.constant = 150
        }

        headerLabel.font = UIFont.K2DBold(28)
        headerLabel.textColor = .white
        headerLabel.textAlignment = .center
        headerLabel.text = "vehicle.lookup.edit.header.label".localized

        modelTextField.name = "vehicle.lookup.edit.model.label".localized
        modelTextField.textField.delegate = self
        modelTextField.validator = VehicleLookupEditTextValidator()
        modelTextField.errorMessage = "vehicle.lookup.edit.model.error.message".localized

        makeTextField.name = "vehicle.lookup.edit.make.label".localized
        makeTextField.textField.delegate = self
        makeTextField.validator = VehicleLookupEditTextValidator()
        makeTextField.errorMessage = "vehicle.lookup.edit.make.error.message".localized

        editionTextField.name = "vehicle.lookup.edit.edition.label".localized
        editionTextField.textField.delegate = self

        yearTextField.name = "vehicle.lookup.edit.year.label".localized
        yearTextField.textField.keyboardType = .numberPad
        yearTextField.textField.delegate = self
        yearTextField.validator = VehicleLookupEditTextValidator()
        yearTextField.errorMessage = "vehicle.lookup.edit.year.error.message".localized

        co2TextField.name = "vehicle.lookup.edit.co2.label".localized
        co2TextField.textField.keyboardType = .numberPad
        co2TextField.textField.delegate = self
        co2TextField.validator = VehicleLookupEditTextValidator()
        co2TextField.errorMessage = "vehicle.lookup.edit.co2.error.message".localized

        saveButton.setTitle("save".localized, for: .normal)
    }

    @IBAction
    func onSave() {
        let isMakeValid = modelTextField.validate()
        let isModelValid = makeTextField.validate()
        let isYearValid = yearTextField.validate()
        let isCo2Valid = co2TextField.validate()
        guard isMakeValid, isModelValid, isYearValid, isCo2Valid else { return }

//        var vehicleData = viewModel.vehicleData
//        vehicleData.updateModelData(modelTextField.text!,
//                                    model: makeTextField.text!,
//                                    edition: editionTextField.text,
//                                    year: yearTextField.text!,
//                                    co2Emissions: co2TextField.text!)
//        delegate?.vehicleLookupEditViewControllerDidTapSave(self, vehicleData: vehicleData)
    }

    var activeTextField: UITextField? {
        let allTextFields = [
            modelTextField.textField,
            makeTextField.textField,
            editionTextField.textField,
            yearTextField.textField,
            co2TextField.textField
        ]

        for textField in allTextFields where textField.isFirstResponder {
            return textField
        }

        return nil
    }
}

extension VehicleLookupEditViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension VehicleLookupEditViewController: KeyboardHandler {
    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return 0 }
        return value.cgRectValue.height
    }

    func keyboardWillShow(_ notification: Notification) {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        var keyboardFrame: CGRect = value.cgRectValue
        keyboardFrame = view.convert(keyboardFrame, from: nil)
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardFrame.height - 80, right: 0)
        scrollView.contentInset = contentInset
    }

    func keyboardWillHide(_: Notification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}
