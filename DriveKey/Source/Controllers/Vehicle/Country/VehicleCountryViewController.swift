//
//  VehicleCountryViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 24/05/2022.
//

import UIKit

protocol VehicleCountryViewControllerDelegate: AnyObject {
    func vehicleCountryViewControllerDidTapSkip(_ viewController: VehicleCountryViewController)
    func vehicleCountryViewControllerDidTapContinue(_ viewController: VehicleCountryViewController, country: Country, state: CountryState?)
}

class VehicleCountryViewController: ViewController, PickerPresentable {
    @IBOutlet private var karaiImageView: UIImageView!
    @IBOutlet private var cardView: CardContainerView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var countryField: NamedPickerField!
    @IBOutlet private var stateField: NamedPickerField!
    @IBOutlet private var skipButton: PlainButton!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var headerLabelBottomConstraint: NSLayoutConstraint!
    
    var viewModel: VehicleCountryViewModel!
    weak var delegate: VehicleCountryViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.appColor(.navyBlue)
        view.sendSubviewToBack(cardView)
        view.sendSubviewToBack(karaiImageView)
        
        if UIScreen.main.bounds.height < 812 {
            headerLabelBottomConstraint.constant = 20
        }
        
        headerLabel.font = UIFont.K2DBold(28)
        headerLabel.textColor = .white
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 2
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.minimumScaleFactor = 0.5
        headerLabel.text = "vehicle.country.header.label".localized
        
        countryField.name = "vehicle.country.field.name".localized
        countryField.textField.placeholder = "vehicle.country.field.placeholder".localized
        countryField.highlightColor = UIColor(hex: 0xE6E8EA)
        countryField.nameColor = .appColor(.navyBlue).withAlphaComponent(0.7)
        countryField.textColor = .appColor(.navyBlue)
        countryField.addRightButton(UIImage(named: "picker-arrow-down")!, size: CGSize(width: 8, height: 16))
        countryField.onRightButton = { [weak self] in
            self?.onCountry()
        }
        
        stateField.name = "vehicle.country.state.field.name".localized
        stateField.textField.placeholder = "vehicle.country.state.field.placeholder".localized
        stateField.highlightColor = UIColor(hex: 0xE6E8EA)
        stateField.nameColor = .appColor(.navyBlue).withAlphaComponent(0.7)
        stateField.textColor = .appColor(.navyBlue)
        stateField.addRightButton(UIImage(named: "picker-arrow-down")!, size: CGSize(width: 8, height: 16))
        stateField.onRightButton = { [weak self] in
            self?.onState()
        }
        stateField.alpha = 0
        
        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.isEnabled = false
        let skipTitle = NSMutableAttributedString(attributedString: "skip".localized.underline())
        skipTitle.addAttributes([.font: UIFont.K2DMedium(16), .foregroundColor: UIColor.black], range: "skip".localized.nsRange(of: "skip".localized))
        skipButton.setAttributedTitle(skipTitle, for: .normal)
        
        if viewModel.screenSource == .bubble || viewModel.screenSource == .profile {
            karaiImageView.alpha = 0.2
            skipButton.isHidden = true
        }
        
        if let defaultIndex = viewModel.defaultCountryIndex {
            updateDataCountry(defaultIndex)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presentedViewController?.dismiss(animated: false)
    }
    
    private func presentPicker(_ data: [Country]) {
        showPicker(data.map({ $0.name })) { [weak self] index in
            self?.updateDataCountry(index)
        }
    }
    
    private func presentPicker(_ data: [CountryState]) {
        showPicker(data.map({ $0.name })) { [weak self] index in
            self?.updateDataState(index)
        }
    }
    
    private func showPickerField(_ pickerField: NamedPickerField, show: Bool) {
        let alpha: CGFloat = show ? 1 : 0
        UIView.transition(with: pickerField,
                          duration: 0.15,
                          options: .transitionCrossDissolve,
                          animations: {
            pickerField.alpha = alpha
        })
    }
    
    private func updateDataCountry(_ index: Int) {
        let countries = viewModel.countries
        guard countries.count > index else { return }
        let country = countries[index]
        countryField.value = country.name
        viewModel.selectedCountry = country
        
        let showState = viewModel.shouldShowStates(country)
        showPickerField(stateField, show: showState)
        if !showState {
            stateField.value = nil
            viewModel.selectedState = nil
        }
        
        validate()
    }
    
    private func updateDataState(_ index: Int) {
        guard let country = viewModel.selectedCountry, country.states.count > index else { return }
        let state = country.states[index]
        stateField.value = state.name
        viewModel.selectedState = state
        validate()
    }
    
    private func validate() {
        continueButton.isEnabled = viewModel.validate()
    }
    
    private func onCountry() {
        let countries = viewModel.countries
        guard !countries.isEmpty else { return }
        presentPicker(countries)
    }
    
    private func onState() {
        guard let country = viewModel.selectedCountry, !country.states.isEmpty else { return }
        presentPicker(country.states)
    }
    
    @IBAction
    func onSkip() {
        delegate?.vehicleCountryViewControllerDidTapSkip(self)
    }
    
    @IBAction
    func onContinue() {
        guard let country = viewModel.selectedCountry else { return }
        delegate?.vehicleCountryViewControllerDidTapContinue(self, country: country, state: viewModel.selectedState)
    }
}
