//
//  VehicleCountryViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 24/05/2022.
//

import Foundation
import Combine

class VehicleCountryViewModel {
    let countries: [Country]
    let screenSource: VehicleScreenSource
    private let statePickerCountry = "us"
    
    var selectedCountry: Country?
    var selectedState: CountryState?
        
    init(_ countries: [Country], screenSource: VehicleScreenSource) {
        self.countries = countries
        self.screenSource = screenSource
    }
    
    func shouldShowStates(_ country: Country) -> Bool {
        country.code.lowercased() == statePickerCountry
    }
    
    func validate() -> Bool {
        guard let country = selectedCountry else { return false }
        if shouldShowStates(country) {
            return selectedState != nil
        }
        
        return true
    }
    
    var otherCountry: Country {
        Country.other
    }
    
    var defaultCountryIndex: Int? {
        if let regionCode = Locale.current.regionCode {
            if let country = countries.first(where: { $0.code.lowercased().contains(regionCode.lowercased())}) {
                return countries.firstIndex(where: { $0.code == country.code })
            }
        }
        
        return countries.firstIndex(where: { $0.code == otherCountry.code })
    }
}
