//
//  VehicleListViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/07/2022.
//

import Foundation
import Combine
import FirebaseFirestore

class VehicleListViewModel {
    private let db = Firestore.firestore()
    private var dataListener: ListenerRegistration?
    private let user: DrivekeyUser?
    private let api: APIProtocol
    private let vehicleFetcher: VehicleProvidable
    
    var primaryUpdated = PassthroughSubject<Bool, Never>()
    var error = PassthroughSubject<Error, Never>()
    private var cancellables = Set<AnyCancellable>()
    
    init(_ user: DrivekeyUser?, api: APIProtocol, vehicleFetcher: VehicleProvidable) {
        self.user = user
        self.api = api
        self.vehicleFetcher = vehicleFetcher
        
        registerSubscriptions()
    }
    
    var vehicles: AnyPublisher<[VehicleData], Never> {
        vehicleFetcher.data.eraseToAnyPublisher()
    }
    
    private func registerSubscriptions() {
        vehicleFetcher.error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.error.send(error)
            }
            .store(in: &cancellables)
    }
    
    func updatePrimary(_ vehicleId: String) {
        api.primaryVehicle(vehicleId) { result in
            switch result {
            case .success:
                self.primaryUpdated.send(true)
            case .failure(let error):
                self.error.send(error)
            }
        }
    }
    
    func fetchData() {
        guard let user = user else { return }
        vehicleFetcher.registerListener(user.uid)
    }
    
    func unregisterListener() {
        dataListener?.remove()
        dataListener = nil
    }
}
