//
//  VehicleListViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/07/2022.
//

import UIKit
import Combine

protocol VehicleListViewControllerDelegate: AnyObject {
    func vehicleListViewControllerDidTapAddVehicle(_ viewController: VehicleListViewController)
    func vehicleListViewController(_ viewController: VehicleListViewController, didSelect vehicle: VehicleData, activeVehicles: [VehicleData])
}

class VehicleListViewController: ViewController, TrayPresentable {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: VehicleListViewModel!
    weak var delegate: VehicleListViewControllerDelegate?
    private var dataSource: VehicleListDataSource!
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "vehicle.list.title".localized
        
        navigationController?.navigationBar.prefersLargeTitles = true
        let item = UIBarButtonItem(image: UIImage(named: "add-vehicle-icon"),
                                   style: .plain,
                                   target: self,
                                   action: #selector(onAddVehicle))
        navigationItem.rightBarButtonItem = item
        
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tableView.register(VehicleListActiveTableViewCell.self, forCellReuseIdentifier: VehicleListActiveTableViewCell.className)
        tableView.register(VehicleListInactiveTableViewCell.self, forCellReuseIdentifier: VehicleListInactiveTableViewCell.className)
        
        dataSource = VehicleListDataSource(tableView)
        tableView.dataSource = dataSource
        tableView.delegate = dataSource
        tableView.separatorStyle = .none
        dataSource.delegate = self
        
        registerSubscriptions()
        
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (navigationController as? NavigationController)?.setProfileBlackTheme()
        
        if !dataSource.vehiclesActive.isEmpty, dataSource.isPrimary == false {
            showPrimaryChoose()
        }
    }
    
    private func registerSubscriptions() {
        viewModel?.vehicles
            .receive(on: DispatchQueue.main)
            .sink { [weak self] data in
                self?.hideSpinner()
                self?.dataSource.updateData(data)
            }
            .store(in: &cancellables)
        
        viewModel?.primaryUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.hideSpinner()
            }
            .store(in: &cancellables)
        
        viewModel?.error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                self?.showAlertError(error: error)
            }
            .store(in: &cancellables)
    }
    
    private func fetchData() {
        showSpinner()
        viewModel.fetchData()
    }
    
    private func updatePrimary(_ vehicleId: String) {
        showSpinner()
        viewModel.updatePrimary(vehicleId)
    }
    
    private func showPrimaryUpdate() {
        showTrayPrimaryUpdate(dataSource.vehiclesActive) { vehicle in
            if let vehicleId = vehicle.vehicleId {
                self.updatePrimary(vehicleId)
            }
        } addVehicle: {
            self.delegate?.vehicleListViewControllerDidTapAddVehicle(self)
        }
    }
    
    private func showPrimaryChoose() {
        showTrayPrimarySet(dataSource.vehiclesActive) { vehicle in
            if let vehicleId = vehicle.vehicleId {
                self.updatePrimary(vehicleId)
            }
        } addVehicle: {
            self.delegate?.vehicleListViewControllerDidTapAddVehicle(self)
        }
    }
    
    @objc
    func onAddVehicle() {
        delegate?.vehicleListViewControllerDidTapAddVehicle(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}

extension VehicleListViewController: VehicleListDataSourceDelegate {
    func vehicleListDataSource(_ dataSource: VehicleListDataSource, didSelect vehicle: VehicleData) {
        delegate?.vehicleListViewController(self, didSelect: vehicle, activeVehicles: dataSource.vehiclesActive)
    }
    
    func vehicleListDataSourceDidTapPrimary(_ dataSource: VehicleListDataSource) {
        showPrimaryUpdate()
    }
}
