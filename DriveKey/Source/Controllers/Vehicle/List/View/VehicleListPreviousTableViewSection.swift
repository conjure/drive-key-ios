//
//  VehicleListPreviousTableViewSection.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/07/2022.
//

import UIKit

class VehicleListPreviousTableViewSection: BaseView {
    private let titleLabel = Subviews.titleLabel
    private let toggleSwitch = Subviews.toggleSwitch

    var onToggle: (() -> Void)?
    
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(titleLabel)
        addSubview(toggleSwitch)
        
        toggleSwitch.addTarget(self, action: #selector(onSwitch), for: .valueChanged)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 26),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -26),
            toggleSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -26),
            toggleSwitch.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor)            
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var isOn: Bool? {
        didSet {
            guard let isOn = isOn else { return }
            toggleSwitch.isOn = isOn
        }
    }
    
    @objc
    private func onSwitch() {
        onToggle?()
    }
}

private struct Subviews {
    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(18)
        label.textColor = .black
        label.textAlignment = .left
        label.text = "vehicle.list.toggle.label".localized

        return label
    }
    
    static var toggleSwitch: UISwitch {
        let view = UISwitch(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        return view
    }
}
