//
//  VehicleListActiveTableViewCell.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/07/2022.
//

import UIKit

class VehicleListActiveTableViewCell: UITableViewCell {
    private let roundedView = Subviews.roundedView
    private let platesLabel = Subviews.platesLabel
    private let makeLabel = Subviews.makeLabel
    private let primaryLabel = Subviews.primaryLabel
    private let separatorView = Subviews.separatorView
    private var platesLabelHeightContraint: NSLayoutConstraint!

    var onPrimary: (() -> Void)?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        setUpCell()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("[VehicleListActiveTableViewCell] init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        platesLabel.text = nil
        makeLabel.text = nil
        primaryLabel.text = nil
    }

    private func setUpCell() {
        selectionStyle = .none
        contentView.backgroundColor = .clear
        backgroundColor = .clear

        contentView.addSubview(roundedView)
        roundedView.addSubview(platesLabel)
        roundedView.addSubview(makeLabel)
        roundedView.addSubview(primaryLabel)
        roundedView.addSubview(separatorView)
        
        platesLabelHeightContraint = platesLabel.heightAnchor.constraint(equalToConstant: 22)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTap))
        primaryLabel.addGestureRecognizer(tapRecognizer)
        primaryLabel.isUserInteractionEnabled = true

        let constraint = [
            roundedView.topAnchor.constraint(equalTo: contentView.topAnchor),
            roundedView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            roundedView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            roundedView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -24),
            platesLabel.topAnchor.constraint(equalTo: roundedView.topAnchor, constant: 12),
            platesLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 15),
            platesLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -15),
            platesLabelHeightContraint!,
            makeLabel.topAnchor.constraint(equalTo: platesLabel.bottomAnchor, constant: 4),
            makeLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 15),
            makeLabel.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor, constant: -15),
            makeLabel.heightAnchor.constraint(equalToConstant: 20),
            separatorView.topAnchor.constraint(equalTo: roundedView.topAnchor, constant: 66),
            separatorView.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: roundedView.trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            primaryLabel.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 0),
            primaryLabel.leadingAnchor.constraint(equalTo: roundedView.leadingAnchor, constant: 15),
            primaryLabel.widthAnchor.constraint(equalTo: platesLabel.widthAnchor, multiplier: 0.5),
            primaryLabel.bottomAnchor.constraint(equalTo: roundedView.bottomAnchor, constant: -2)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var data: VehicleData? {
        didSet {
            guard let data = data else { return }
            platesLabel.text = data.platesValue
            makeLabel.text = data.makeValue
            
            if let make = data.makeValue, !make.isEmpty {
                platesLabelHeightContraint.constant = 22
            } else {
                platesLabelHeightContraint.constant = 40
            }
            setNeedsLayout()
            
            guard let isPrimary = data.primary else { return }
            if isPrimary {
                primaryLabel.textColor = .appColor(.green)
                primaryLabel.text = "vehicle.list.vehicle.primary".localized
            } else {
                primaryLabel.textColor = .appColor(.darkGray)
                primaryLabel.text = "vehicle.list.vehicle.set.primary".localized
            }
        }
    }
    
    @objc
    private func onTap() {
        onPrimary?()
    }
}

private struct Subviews {
    static var roundedView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = .white
        
        view.layer.cornerRadius = 16
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 00)
        view.layer.shadowRadius = 5
        view.layer.shadowOpacity = 0.1

        return view
    }

    static var platesLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(18)
        label.numberOfLines = 1
        label.textColor = .black
        label.textAlignment = .left

        return label
    }

    static var makeLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DSemiBold(14)
        label.numberOfLines = 1
        label.textColor = .black
        label.textAlignment = .left

        return label
    }

    static var primaryLabel: UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textAlignment = .left

        return label
    }

    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.appColor(.separatorDarkGray).withAlphaComponent(0.1)

        return view
    }
}
