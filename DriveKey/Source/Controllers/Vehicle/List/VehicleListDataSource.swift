//
//  VehicleListDataSource.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/07/2022.
//

import UIKit

protocol VehicleListDataSourceDelegate: AnyObject {
    func vehicleListDataSource(_ dataSource: VehicleListDataSource, didSelect vehicle: VehicleData)
    func vehicleListDataSourceDidTapPrimary(_ dataSource: VehicleListDataSource)
}

class VehicleListDataSource: NSObject {
    private let tableView: UITableView
    private(set) var vehiclesActive = [VehicleData]()
    private(set) var vehiclesInactive = [VehicleData]()
    private var isPreviousSection = false
    
    weak var delegate: VehicleListDataSourceDelegate?
    
    init(_ tableView: UITableView) {
        self.tableView = tableView
    }
    
    func updateData(_ data: [VehicleData]) {
        let active = data.filter({ $0.status == .active })
        vehiclesActive.removeAll()
        vehiclesActive.append(contentsOf: active)
        vehiclesActive.sort { v1, v2 in
            let v1Primary = v1.primary ?? false
            let v2Primary = v2.primary ?? false
            return v1Primary && !v2Primary
        }
        
        let inactive = data.filter({ $0.status == .inactive })
        vehiclesInactive.removeAll()
        vehiclesInactive.append(contentsOf: inactive)
        
        tableView.reloadData()
    }
    
    private func onPreviousToggle() {
        isPreviousSection.toggle()
        tableView.reloadData()
    }
    
    private func dataCount(_ section: Int) -> Int {
        if section == 0 {
            return vehiclesActive.count
        }
        
        if section == 1 {
            return isPreviousSection == true ? vehiclesInactive.count : 0
        }
        
        return 0
    }
    
    private func data(_ indexPath: IndexPath) -> VehicleData {
        if indexPath.section == 0 {
            return vehiclesActive[indexPath.row]
        }
        
        return vehiclesInactive[indexPath.row]
    }
    
    private func onPrimary() {
        delegate?.vehicleListDataSourceDidTapPrimary(self)
    }
    
    var isPrimary: Bool {
        vehiclesActive.filter({ $0.primary == true }).first != nil
    }
    
    var isLastActive: Bool {
        vehiclesActive.count <= 1
    }
}

extension VehicleListDataSource: UITableViewDelegate {
    func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 129
        }
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 60
        }
        
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if !vehiclesInactive.isEmpty {
            return 2
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let sectionView = VehicleListPreviousTableViewSection()
            sectionView.frame = CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40)
            sectionView.isOn = isPreviousSection
            sectionView.onToggle = { [weak self] in
                self?.onPreviousToggle()
            }
            
            return sectionView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        CGFloat.leastNonzeroMagnitude
    }
}

extension VehicleListDataSource: UITableViewDataSource {
    func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataCount(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = data(indexPath)
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: VehicleListActiveTableViewCell.className, for: indexPath) as! VehicleListActiveTableViewCell
            cell.onPrimary = { [weak self] in
                self?.onPrimary()
            }
            cell.data = data
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: VehicleListInactiveTableViewCell.className, for: indexPath) as! VehicleListInactiveTableViewCell
            cell.data = data
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = data(indexPath)
        delegate?.vehicleListDataSource(self, didSelect: data)
    }
}
