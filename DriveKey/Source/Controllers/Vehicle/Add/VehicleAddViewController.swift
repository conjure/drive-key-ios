//
//  VehicleAddViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 16/09/2021.
//

import UIKit

protocol VehicleAddViewControllerDelegate: AnyObject {
    func vehicleAddViewControllerDidTapClose(_ viewController: VehicleAddViewController)
    func vehicleAddViewControllerDidTapAdd(_ viewController: VehicleAddViewController, source: VehicleScreenSource)
}

class VehicleAddViewController: ViewController {
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var addButton: RoundedButton!

    var viewModel: VehicleAddViewModel!
    weak var delegate: VehicleAddViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setDefaultTheme()
        }
        
        view.backgroundColor = UIColor.appColor(.navyBlue)

        imageView.image = UIImage(named: "vehicle-add-car")

        headerLabel.font = UIFont.K2DBold(28)
        headerLabel.textColor = .white
        headerLabel.text = "vehicle.add.header.label".localized
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 1

        infoLabel.font = UIFont.K2DRegular(16)
        infoLabel.textColor = .white
        let infoAttStr = "vehicle.add.info.label".localized.subscriptCO2(UIFont.K2DRegular(10)!)
        infoLabel.attributedText = infoAttStr
        infoLabel.textAlignment = .center
        
        addButton.setTitle("vehicle.add.add.button".localized, for: .normal)
        
        if navigationController?.viewControllers.count == 1 {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon"), style: .plain, target: self, action: #selector(onClose))
        }
    }

    @objc private func onClose() {
        delegate?.vehicleAddViewControllerDidTapClose(self)
    }

    @IBAction
    func onAdd() {
        delegate?.vehicleAddViewControllerDidTapAdd(self, source: viewModel.source)
    }
}
