//
//  VehicleAddViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 16/09/2021.
//

import Foundation

struct VehicleAddViewModel {
    private let screenSource: VehicleScreenSource

    init(_ screenSource: VehicleScreenSource) {
        self.screenSource = screenSource
    }

    var source: VehicleScreenSource { screenSource }
}
