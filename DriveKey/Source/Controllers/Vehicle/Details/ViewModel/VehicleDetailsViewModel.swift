//
//  VehicleDetailsViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2021.
//

import Foundation
import Combine

class VehicleDetailsViewModel {
    private(set) var selectedVehicle: VehicleData?
    private(set) var activeVehicles: [VehicleData]?
    private let countries: [Country]
    private let api: APIProtocol
    let numberFormatter = NumberFormatter()
    
    @Published var removed: Bool = false
    @Published var primaryUpdated: Bool = false
    @Published var error: Error?
    
    init(_ selectedVehicle: VehicleData?, activeVehicles: [VehicleData]?, countries: [Country], api: APIProtocol) {
        self.selectedVehicle = selectedVehicle
        self.activeVehicles = activeVehicles
        self.countries = countries
        self.api = api
        
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 0
    }
    
    func removeVehicle(_ vehicleId: String) {
        api.removeVehicle(vehicleId) { result in
            switch result {
            case .success:
                self.removed = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    func updatePrimary(_ vehicleId: String) {
        api.primaryVehicle(vehicleId) { result in
            switch result {
            case .success:
                self.primaryUpdated = true
            case .failure(let error):
                self.error = error
            }
        }
    }
    
    var vehicleId: String? {
        selectedVehicle?.vehicleId
    }
    
    var isActive: Bool {
        guard let data = selectedVehicle, let status = data.status else { return false }
        
        return status == .active
    }
    
    var isPrimary: Bool {
        guard let primary = selectedVehicle?.primary else { return false }
        
        return primary
    }
    
    var isLastActive: Bool {
        guard let vehicles = activeVehicles else { return false }
        
        return vehicles.count <= 1
    }
    
    var country: String? {
        selectedVehicle?.country
    }
    
    var isCountryUS: Bool {
        guard let countryCode = selectedVehicle?.countryCode else { return false }
        
        return countryCode.lowercased() == "us"
    }
    
    var state: String? {
        guard let countryCode = selectedVehicle?.countryCode else { return nil }
        guard let stateCode = selectedVehicle?.state else { return nil }
        guard let country = countries.first(where: { $0.code.lowercased() == countryCode.lowercased() }) else { return nil }
        
        return country.states.first(where: { $0.code.lowercased() == stateCode.lowercased() })?.name
    }
    
    var licensePlate: String? {
        selectedVehicle?.platesValue
    }
    
    var co2Emissions: String? {
        guard let valueInGrams = selectedVehicle?.co2Emissions else { return nil }
        let valueInKg = Double(valueInGrams) / 1000
        return String(valueInKg.formattedCO2) + " " + "vehicle.details.emissions.unit".localized
    }
    
    var model: String? {
        let make = selectedVehicle?.make ?? ""
        let model = selectedVehicle?.modelValue ?? ""
        if make.isEmpty {
            return model
        }
        
        return make + " " + model
    }
    
    var year: String? {
        selectedVehicle?.yearValue
    }
    
    var weight: String? {
        guard let weight = selectedVehicle?.weightValue, let formatted = numberFormatter.string(from: NSNumber(value: weight)) else { return nil }
        return formatted + " " + "vehicle.details.weight.unit".localized
    }
    
    var shouldHideCO2CalculationHeading: Bool {
        (fuelType == nil && fuelConsumption == nil) && (bhp == nil && engineCapacity == nil)
    }
    
    var fuelType: String? {
        selectedVehicle?.fuelValue
    }
    
    var fuelConsumption: String? {
        guard let min = selectedVehicle?.fuelConsumptionMin, let max = selectedVehicle?.fuelConsumptionMax else {
            return nil
        }
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        guard let minFuel = formatter.string(from: NSNumber(value: min)), let maxFuel = formatter.string(from: NSNumber(value: max)) else { return nil }
        return "\(minFuel)-\(maxFuel) \("vehicle.lookup.fuel.consumption.unit".localized)"
    }
    
    var bhp: String? {
        guard let bhp = selectedVehicle?.bhpValue else { return nil }
        return numberFormatter.string(from: NSNumber(value: bhp)) ?? nil
    }
    
    var engineCapacity: String? {
        guard let valueInt = selectedVehicle?.engineCapacityValue else { return nil }
        return String(valueInt) + " " + "vehicle.details.engine.capacity.unit".localized
    }
}
