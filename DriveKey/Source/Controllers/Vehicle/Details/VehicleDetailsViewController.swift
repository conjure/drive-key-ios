//
//  VehicleDetailsViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 21/09/2021.
//

import UIKit
import Combine

protocol VehicleDetailsViewControllerDelegate: AnyObject {
    func vehicleDetailsViewControllerDidRemoveVehicle(_ viewController: VehicleDetailsViewController)
    func vehicleDetailsViewControllerDidTapAddVehicle(_ viewController: VehicleDetailsViewController)
}

class VehicleDetailsViewController: ViewController, TrayPresentable {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var countryLabel: NamedLabel!
    @IBOutlet private var stateLabel: NamedLabel!
    @IBOutlet private var plateLabel: NamedLabel!
    @IBOutlet private var emissionsLabel: NamedLabel!
    @IBOutlet private var vehicleLabel: NamedLabel!
    @IBOutlet private var yearLabel: NamedLabel!
    @IBOutlet private var weightLabel: NamedLabel!
    @IBOutlet private var calculationLabel: UILabel!
    @IBOutlet private var fuelTypeLabel: NamedLabel!
    @IBOutlet private var fuelConsumptionLabel: NamedLabel!
    @IBOutlet private var bhpLabel: NamedLabel!
    @IBOutlet private var engineCapacityLabel: NamedLabel!
    @IBOutlet private var primaryButton: RoundedButton!
    @IBOutlet private var stackViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var primaryButtonBottomConstraint: NSLayoutConstraint!
    
    var viewModel: VehicleDetailsViewModel!
    weak var delegate: VehicleDetailsViewControllerDelegate?
    private var cancellables = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "vehicle.details.title".localized
        
        navigationController?.navigationBar.prefersLargeTitles = true
        if viewModel.isActive {
            let item = UIBarButtonItem(image: UIImage(named: "bin-icon"),
                                       style: .plain,
                                       target: self,
                                       action: #selector(onRemove))
            navigationItem.rightBarButtonItem = item
        }
        
        if let country = viewModel.country {
            countryLabel.name = "vehicle.details.country".localized
            countryLabel.value = country
        } else {
            countryLabel.isHidden = true
        }
        
        if let state = viewModel.state {
            stateLabel.name = "vehicle.details.state".localized
            stateLabel.value = state
        } else {
            stateLabel.isHidden = true
        }
        
        if let plate = viewModel.licensePlate {
            let labelName = viewModel.isCountryUS ? "vehicle.details.license.plate.us".localized : "vehicle.details.license.plate".localized
            plateLabel.name = labelName
            plateLabel.value = plate
        } else {
            plateLabel.isHidden = true
        }
        
        if let emissions = viewModel.co2Emissions {
            emissionsLabel.attributedName = "vehicle.details.emissions".localized.subscriptCO2(.K2DMedium(9))
            emissionsLabel.value = emissions
        } else {
            emissionsLabel.isHidden = true
        }
        
        if let vehicle = viewModel.model {
            vehicleLabel.name = "vehicle.details.vehicle.label".localized
            vehicleLabel.value = vehicle
        } else {
            vehicleLabel.isHidden = true
        }
        
        if let year = viewModel.year {
            yearLabel.name = "vehicle.details.year.label".localized
            yearLabel.value = year
        } else {
            yearLabel.isHidden = true
        }
        
        if let weight = viewModel.weight {
            weightLabel.name = "vehicle.details.weight.label".localized
            weightLabel.value = weight
        } else {
            weightLabel.isHidden = true
        }
        
        calculationLabel.font = UIFont.K2DMedium(16)
        calculationLabel.textColor = UIColor.black
        calculationLabel.attributedText = "vehicle.details.calculation.label".localized.subscriptCO2(UIFont.K2DMedium(10))
        calculationLabel.isHidden = viewModel.shouldHideCO2CalculationHeading
        
        if let fuelType = viewModel.fuelType {
            fuelTypeLabel.name = "vehicle.details.fuel.type.label".localized
            fuelTypeLabel.value = fuelType
        } else {
            fuelTypeLabel.isHidden = true
        }
        
        if let consumption = viewModel.fuelConsumption {
            fuelConsumptionLabel.name = "vehicle.details.fuel.consumption.label".localized
            fuelConsumptionLabel.value = consumption
        } else {
            fuelConsumptionLabel.isHidden = true
        }
        
        if let bhp = viewModel.bhp {
            bhpLabel.name = "vehicle.details.bhp.label".localized
            bhpLabel.value = bhp
        } else {
            bhpLabel.isHidden = true
        }
        
        if let engine = viewModel.engineCapacity {
            engineCapacityLabel.name = "vehicle.details.engine.capacity.label".localized
            engineCapacityLabel.value = engine
        } else {
            engineCapacityLabel.isHidden = true
        }
        
        primaryButton.setTitle("vehicle.details.button.primary".localized, for: .normal)
        if viewModel.isPrimary || !viewModel.isActive {
            hidePrimaryButton()
        }
        
        registerSubscriptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        (navigationController as? NavigationController)?.setProfileBlackTheme()
    }
    
    private func hidePrimaryButton() {
        primaryButton.isHidden = true
        primaryButtonBottomConstraint.isActive = false
        stackViewBottomConstraint.isActive = true
        view.setNeedsLayout()
    }
    
    private func registerSubscriptions() {
        viewModel.$removed
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.showRemoveConfirmation()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$primaryUpdated
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.hideSpinner()
                if success {
                    self?.hidePrimaryButton()
                }
            }
            .store(in: &cancellables)
        
        viewModel.$error
            .receive(on: DispatchQueue.main)
            .sink { [weak self] error in
                self?.hideSpinner()
                if error != nil {
                    self?.showTrayError(error!.localizedDescription, completion: nil)
                }
            }
            .store(in: &cancellables)
    }
    
    private func performRemove() {
        guard let vehicleId = viewModel.vehicleId else { return }
        
        showSpinner()
        viewModel.removeVehicle(vehicleId)
    }
    
    private func performUpdatePrimary(_ vehicle: VehicleData) {
        guard let vehicleId = vehicle.vehicleId else { return }
        
        showSpinner()
        viewModel.updatePrimary(vehicleId)
    }
    
    private func showRemove() {
        showTrayRemove {
            self.performRemove()
        }
    }
    
    private func showRemoveLast() {
        showTrayRemoveLast {
            self.performRemove()
        }
    }
    
    private func showRemoveConfirmation() {
        showTrayRemoveVehicleConfirmation {
            self.delegate?.vehicleDetailsViewControllerDidRemoveVehicle(self)
        }
    }
    
    private func showPrimary() {
        guard let vehicle = viewModel.selectedVehicle, let vehicles = viewModel.activeVehicles else { return }
        showTrayPrimaryUpdate(vehicle, vehicles: vehicles) { selectedVehicle in
            self.performUpdatePrimary(selectedVehicle)
        } addVehicle: {
            self.delegate?.vehicleDetailsViewControllerDidTapAddVehicle(self)
        }
    }
    
    @objc
    func onRemove() {
        if viewModel.isLastActive {
            showRemoveLast()
        } else {
            showRemove()
        }
    }
    
    @IBAction
    func onPrimary() {
        showPrimary()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
