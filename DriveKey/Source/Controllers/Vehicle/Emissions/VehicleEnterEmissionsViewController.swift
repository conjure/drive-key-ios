//
//  VehicleEnterEmissionsViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/11/2021.
//

import UIKit

protocol VehicleEnterEmissionsViewControllerDelegate: AnyObject {
    func vehicleEnterEmissionsViewControllerDidTapSkip(_ viewController: VehicleEnterEmissionsViewController)
    func vehicleEnterEmissionsViewControllerDidTapContinue(_ viewController: VehicleEnterEmissionsViewController)
}

class VehicleEnterEmissionsViewController: ViewController {
    @IBOutlet private var karaiImageView: UIImageView!
    @IBOutlet private var cardView: CardContainerView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet private var platesTextField: NamedTextField!
    @IBOutlet private var emissionsTextField: NamedTextField!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var skipButton: PlainButton!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var headerLabelBottomConstraint: NSLayoutConstraint!

    var viewModel: VehicleEnterEmissionsViewModel!
    weak var delegate: VehicleEnterEmissionsViewControllerDelegate?
    var api: APIProtocol!

    private let keyboardView = KeyboardAccessoryView(frame: CGRect.zero)

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.appColor(.navyBlue)
        view.sendSubviewToBack(cardView)
        view.sendSubviewToBack(karaiImageView)

        if UIScreen.main.bounds.height < 812 {
            headerLabelBottomConstraint.constant = 20
        }

        headerLabel.font = UIFont.K2DBold(28)
        headerLabel.textColor = .white
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 2
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.minimumScaleFactor = 0.5
        headerLabel.text = "vehicle.enter.emissions.header.label".localized

        platesTextField.name = "vehicle.enter.emissions.plates.label.name".localized
        platesTextField.textField.delegate = self
        platesTextField.textField.keyboardType = .asciiCapable
        platesTextField.textField.returnKeyType = .next
        platesTextField.textField.autocapitalizationType = .allCharacters
        platesTextField.textField.autocorrectionType = .no
        platesTextField.textField.text = viewModel.vehicleData?.licencePlate?.uppercased()
        let platesStr = viewModel.vehicleData?.licencePlate != nil ? viewModel.vehicleData?.licencePlate : viewModel.vrm
        platesTextField.textField.text = platesStr
        
        let emissionsStr = "vehicle.enter.emissions.co2.label.name".localized
        let emissionsAttStr = emissionsStr.subscriptCO2(UIFont.K2DMedium(10))
        emissionsTextField.attributedName = emissionsAttStr
        emissionsTextField.rightIcon = UIImage(named: "emissions-unit-icon")
        emissionsTextField.textField.delegate = self
        emissionsTextField.textField.keyboardType = .numberPad
        emissionsTextField.textField.returnKeyType = .done

        keyboardView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 55)
        keyboardView.buttonTitle = "continue".localized
        keyboardView.onButton = { [weak self] in
            self?.view.endEditing(true)
            self?.performSaveData()
        }
        emissionsTextField.textField.inputAccessoryView = keyboardView
        platesTextField.textField.inputAccessoryView = keyboardView

        let isBigScreen = UIScreen.main.bounds.width > 320
        let infoFont = isBigScreen ? UIFont.K2DRegular(16) : UIFont.K2DRegular(13)
        infoLabel.font = infoFont
        infoLabel.textColor = .appColor(.navyBlue)
        infoLabel.textAlignment = .center
        infoLabel.numberOfLines = 0

        let infoStrLinkCo2 = "vehicle.enter.emissions.info.label.link.co2".localized
        let infoStrLinkValue = "vehicle.enter.emissions.info.label.link.value".localized
        var infoAttStr = "vehicle.enter.emissions.info.label".localized.subscriptCO2(UIFont.K2DRegular(9)!) as? NSMutableAttributedString
        infoAttStr = infoAttStr?.underline(infoStrLinkCo2)
        infoAttStr = infoAttStr?.underline(infoStrLinkValue)
        infoLabel.attributedText = infoAttStr
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onTap))
        infoLabel.isUserInteractionEnabled = true
        infoLabel.addGestureRecognizer(tapRecognizer)

        skipButton.titleLabel?.font = .K2DMedium(16)
        skipButton.setTitleColor(.black, for: .normal)
        skipButton.setAttributedTitle("skip".localized.underline(), for: .normal)
        skipButton.isHidden = viewModel.screenSource != .onboarding

        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.isEnabled = false

        hideKeyboardWhenTappedAround()
        
        platesTextField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        emissionsTextField.textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        platesTextField.textField.sendActions(for: .editingChanged)
        emissionsTextField.textField.sendActions(for: .editingChanged)
    }

    private func performSaveData() {
        guard let emissionsStr = emissionsTextField.text, let emissionsValue = Double(emissionsStr) else { return }
        guard let licencePlateStr = platesTextField.text, !licencePlateStr.isEmpty else { return }

        showSpinner()
        continueButton.isEnabled = false

        let dataVehicle = viewModel.vehicleData
        let vehicleCountryName = dataVehicle?.country == nil ? viewModel.country?.name : dataVehicle?.country
        let vehicleCountryCode = dataVehicle?.countryCode == nil ? viewModel.country?.code : dataVehicle?.countryCode
        let vehicleState = dataVehicle?.state == nil ? viewModel.state?.code : dataVehicle?.state
        let data = VehicleData(vehicleId: dataVehicle?.vehicleId,
                               licencePlate: licencePlateStr,
                               vrm: nil,
                               co2Emissions: emissionsValue,
                               model: dataVehicle?.model,
                               year: dataVehicle?.year,
                               weight: dataVehicle?.weight,
                               fuelType: dataVehicle?.fuelType,
                               fuelConsumptionMin: dataVehicle?.fuelConsumptionMin,
                               fuelConsumptionMax: dataVehicle?.fuelConsumptionMax,
                               bhp: dataVehicle?.bhp,
                               engineCapacity: dataVehicle?.engineCapacity,
                               isElectricVehicle: dataVehicle?.isElectricVehicle,
                               make: dataVehicle?.make,
                               country: vehicleCountryName,
                               countryCode: vehicleCountryCode,
                               state: vehicleState,
                               status: nil,
                               primary: nil,
                               vehicleRegistration: nil)

        api.registerVehicle(data) { result in
            self.hideSpinner()
            self.continueButton.isEnabled = true
            switch result {
            case .success:
                AnalyticsService.trackEvent(.manualCO2)
                self.delegate?.vehicleEnterEmissionsViewControllerDidTapContinue(self)
            case let .failure(error):
                self.showAlertError(error: error)
            }
        }
    }
    
    private func performTypicalValue() {
        view.endEditing(true)
        emissionsTextField.value = "150"
        emissionsTextField.textField.sendActions(for: .editingChanged)
    }
    
    private func openCo2Url() {
        if let url = URL(string: "https://www.google.com/search?q=vehicle+co2+emissions+calculator") {
            UIApplication.shared.open(url)
        }
    }
    
    @objc
    func textFieldDidChange() {
        guard let plates = platesTextField.text, let emissionsStr = emissionsTextField.text else { return }
        
        let isValid = Double(emissionsStr) != nil && !plates.isEmpty
        continueButton.isEnabled = isValid
        keyboardView.isDisabled = !isValid
    }
    
    @objc private func onTap(_ gesture: UITapGestureRecognizer) {
        guard let label = gesture.view as? UILabel, let text = label.attributedText?.string else { return }

        let linkCo2 = "vehicle.enter.emissions.info.label.link.co2".localized
        let rangeCo2 = text.nsRange(of: linkCo2)
        let linkValue = "vehicle.enter.emissions.info.label.link.value".localized
        let rangeValue = text.nsRange(of: linkValue)

        if gesture.didTapAttributedTextInLabel(label: label, inRange: rangeCo2) {
            openCo2Url()
        } else if gesture.didTapAttributedTextInLabel(label: label, inRange: rangeValue) {
            performTypicalValue()
        }
    }

    @IBAction
    func onSkip() {
        delegate?.vehicleEnterEmissionsViewControllerDidTapSkip(self)
    }

    @IBAction
    func onContinue() {
        performSaveData()
    }
}

extension VehicleEnterEmissionsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == platesTextField.textField {
            emissionsTextField.textField.becomeFirstResponder()
        }
        
        return true
    }
}
