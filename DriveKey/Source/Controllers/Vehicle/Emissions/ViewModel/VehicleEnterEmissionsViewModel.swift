//
//  VehicleEnterEmissionsViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 03/11/2021.
//

import Foundation

class VehicleEnterEmissionsViewModel {
    private let userService: UserProvidable
    let screenSource: VehicleScreenSource
    let vehicleData: VehicleData?
    let vrm: String?
    let country: Country?
    let state: CountryState?
    private let isActiveVehiclePresent: Bool
    
    init(_ userService: UserProvidable, screenSource: VehicleScreenSource, vehicleData: VehicleData?, vrm: String?, country: Country?, state: CountryState?) {
        self.userService = userService
        self.screenSource = screenSource
        self.vehicleData = vehicleData
        self.vrm = vrm
        self.country = country
        self.state = state
        self.isActiveVehiclePresent = userService.currentUser?.activeVehicle != nil
    }
    
    var isFirstVehicle: Bool {
        !isActiveVehiclePresent
    }
}
