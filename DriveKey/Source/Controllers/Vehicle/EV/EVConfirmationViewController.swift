//
//  EVConfirmationViewController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 01/11/2021.
//

import UIKit

protocol EVConfirmationViewControllerDelegate: AnyObject {
    func evConfirmationViewControllerDidContinue(_ vc: EVConfirmationViewController)
    func evConfirmationViewControllerDidSkip(_ vc: EVConfirmationViewController)
}

class EVConfirmationViewController: ViewController {
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var content2Label: UILabel!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var skipButton: SmallButton!

    var shouldAllowSkip: Bool = false
    weak var delegate: EVConfirmationViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let navigationController = navigationController as? NavigationController {
            navigationController.setProfileBlackTheme()
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "close-icon"), style: .plain, target: self, action: #selector(tappedDismiss))

        contentLabel.text = "ev.confirmation.label".localized
        content2Label.text = "ev.confirmation.label.2".localized
        continueButton.setTitle("continue".localized, for: .normal)
        skipButton.setAttributedTitle("alert.cancel".localized.underline(), for: .normal)
        skipButton.isHidden = !shouldAllowSkip
    }

    @objc private func tappedDismiss() {
        delegate?.evConfirmationViewControllerDidContinue(self)
    }

    @IBAction private func didTapContinue(_: Any) {
        delegate?.evConfirmationViewControllerDidContinue(self)
    }

    @IBAction func didTapSkip(_: Any) {
        delegate?.evConfirmationViewControllerDidSkip(self)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
}
