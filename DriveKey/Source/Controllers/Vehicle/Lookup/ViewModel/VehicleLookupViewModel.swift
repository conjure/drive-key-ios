//
//  VehicleLookupViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Foundation

class VehicleLookupViewModel {
    private let userService: UserProvidable
    let screenSource: VehicleScreenSource
    let country: Country
    let state: CountryState?
    var vehicleData: VehicleData?
    var searchedForEV = false
    private let isActiveVehiclePresent: Bool
    
    init(_ userService: UserProvidable, screenSource: VehicleScreenSource, country: Country, state: CountryState?) {
        self.userService = userService
        self.screenSource = screenSource
        self.country = country
        self.state = state
        self.isActiveVehiclePresent = userService.currentUser?.activeVehicle != nil
    }
    
    var isFirstVehicle: Bool {
        !isActiveVehiclePresent
    }
}
