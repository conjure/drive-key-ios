//
//  VehicleLookupDataViewModel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2021.
//

import Foundation

struct VehicleLookupDataViewModel {
    private let vehicleData: VehicleData
    private let numberFormatter = NumberFormatter()

    init(vehicleData: VehicleData) {
        self.vehicleData = vehicleData
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 0
    }

    var co2Emissions: String? {
        guard let valueInGrams = vehicleData.co2Emissions else { return nil }
        let valueInKg = valueInGrams / 1000
        return String(valueInKg.formattedCO2) + " " + "vehicle.details.emissions.unit".localized
    }

    var model: String? {
        let make = vehicleData.make ?? ""
        let model = vehicleData.model ?? ""
        if make.isEmpty {
            return model
        }
        
        return make + " " + model
    }

    var year: String? {
        vehicleData.year
    }

    var weight: String? {
        guard let weight = vehicleData.weight, let formatted = numberFormatter.string(from: NSNumber(value: weight)) else { return nil }
        return formatted + " " + "vehicle.details.weight.unit".localized
    }

    var shouldHideCO2CalculationHeading: Bool {
        (fuelType == nil && fuelConsumption == nil) && (bhp == nil && engineCapacity == nil)
    }

    var fuelType: String? {
        vehicleData.fuelType
    }

    var fuelConsumption: String? {
        guard let min = vehicleData.fuelConsumptionMin, let max = vehicleData.fuelConsumptionMax else {
            return nil
        }
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        guard let minFuel = formatter.string(from: NSNumber(value: min)), let maxFuel = formatter.string(from: NSNumber(value: max)) else { return nil }
        return "\(minFuel)-\(maxFuel) \("vehicle.lookup.fuel.consumption.unit".localized)"
    }

    var bhp: String? {
        guard let bhp = vehicleData.bhp else { return nil }
        return numberFormatter.string(from: NSNumber(value: bhp)) ?? nil
    }

    var engineCapacity: String? {
        guard let valueInt = vehicleData.engineCapacity else { return nil }
        return String(valueInt) + " " + "vehicle.details.engine.capacity.unit".localized
    }
}
