//
//  VehicleLookupLockedViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

enum VehicleLookupErrorType {
    case noData
}

class VehicleLookupErrorViewController: UIViewController {
    @IBOutlet private var errorView: ErrorMessageView!
    var errorType: VehicleLookupErrorType = .noData

    override func viewDidLoad() {
        super.viewDidLoad()

        switch errorType {
        case .noData:
            let title = "vehicle.lookup.error.no.data.title".localized
            let message = "vehicle.lookup.error.no.data.label".localized
            errorView.updateContent(title: title, message: message)
        }
    }
}
