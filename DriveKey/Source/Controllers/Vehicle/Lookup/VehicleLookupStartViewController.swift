//
//  VehicleLookupStartViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

class VehicleLookupStartViewController: UIViewController {
    @IBOutlet private var infoLabel: UILabel!

    var showEVContent = false

    override func viewDidLoad() {
        super.viewDidLoad()

        infoLabel.font = UIFont.K2DRegular(16)
        infoLabel.textColor = .appColor(.navyBlue)
        infoLabel.textAlignment = .center
        infoLabel.numberOfLines = 0

        if showEVContent {
            infoLabel.text = "vehicle.lookup.start.searched.ev".localized
        } else {
            let attStr = "vehicle.lookup.start.label".localized.subscriptCO2(UIFont.K2DRegular(10)!) as! NSMutableAttributedString
            infoLabel.attributedText = attStr
        }
    }
}
