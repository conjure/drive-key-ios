//
//  VehicleLookupViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Combine
import UIKit

protocol VehicleLookupViewControllerDelegate: AnyObject {
    func vehicleLookupViewControllerDidTapSkip(_ viewController: VehicleLookupViewController)
    func vehicleLookupViewControllerDidTapEdit(_ viewController: VehicleLookupViewController, vehicleData: VehicleData)
    func vehicleLookupViewControllerDidTapContinue(_ viewController: VehicleLookupViewController)
    func vehicleLookupViewControllerDidTapEnterManually(_ viewController: VehicleLookupViewController, vehicleData: VehicleData?)
    func vehicleLookupViewControllerDidTapEnterManually(_ viewController: VehicleLookupViewController, vrm: String?, country: Country?, state: CountryState?)
    func vehicleLookupViewControllerWillShowEV(_ viewController: VehicleLookupViewController, allowSkip: Bool)
}

class VehicleLookupViewController: ViewController {
    @IBOutlet private var karaiImageView: UIImageView!
    @IBOutlet private var cardView: CardContainerView!
    @IBOutlet private var headerLabel: UILabel!
    @IBOutlet var vrmTextField: NamedTextField!
    @IBOutlet private var containerView: UIView!
    @IBOutlet private var skipButton: PlainButton!
    @IBOutlet private var continueButton: RoundedButton!
    @IBOutlet private var headerLabelBottomConstraint: NSLayoutConstraint!
    
    private var previousSearchedPlate: String?
    
    var viewModel: VehicleLookupViewModel!
    weak var delegate: VehicleLookupViewControllerDelegate?
    var api: APIProtocol!
    var authSerice: AuthService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.appColor(.navyBlue)
        view.sendSubviewToBack(cardView)
        view.sendSubviewToBack(karaiImageView)
        
        if UIScreen.main.bounds.height < 812 {
            headerLabelBottomConstraint.constant = 20
        }
        
        headerLabel.font = UIFont.K2DBold(28)
        headerLabel.textColor = .white
        headerLabel.textAlignment = .center
        headerLabel.numberOfLines = 2
        headerLabel.adjustsFontSizeToFitWidth = true
        headerLabel.minimumScaleFactor = 0.5
        headerLabel.text = "vehicle.lookup.header.label".localized
        
        vrmTextField.textField.delegate = self
        vrmTextField.textField.autocapitalizationType = .allCharacters
        vrmTextField.textField.autocorrectionType = .no
        vrmTextField.rightMargin = 40
        vrmTextField.textField.returnKeyType = .search
        vrmTextField.addRightButton(UIImage(named: "search-icon")!, size: CGSize(width: 21, height: 21))
        vrmTextField.onRightButton = { [weak self] in
            self?.onSearch()
        }
        
        continueButton.setTitle("continue".localized, for: .normal)
        continueButton.isEnabled = false
        let skipTitle = NSMutableAttributedString(attributedString: "skip".localized.underline())
        skipTitle.addAttributes([.font: UIFont.K2DMedium(16), .foregroundColor: UIColor.black], range: "skip".localized.nsRange(of: "skip".localized))
        skipButton.setAttributedTitle(skipTitle, for: .normal)
        
        if viewModel.screenSource == .bubble || viewModel.screenSource == .profile {
            karaiImageView.alpha = 0.2
            skipButton.isHidden = true
            continueButton.setTitle("vehicle.lookup.start.profile.flow.continue.button".localized, for: .normal)
        }
        
        showContentStart()
        
        hideKeyboardWhenTappedAround()
    }
    
    func update(_ data: VehicleData) {
        presentVehicleData(data)
    }
    
    private func performVehicleLookup() {
        let vrmStr = vrmTextField.text ?? ""
        let vrmStrEscaped = vrmStr.removingWhitespaces()
        
        if let previous = previousSearchedPlate {
            if previous.lowercased() == vrmStrEscaped.lowercased() {
                // Searching same plate
                return
            }
        }
        previousSearchedPlate = vrmStrEscaped
        
        showSpinner()
        vrmTextField.resignFirstResponder()
        
        api.lookupVehicle(vrmStrEscaped, country: viewModel.country.name, countryCode: viewModel.country.code, state: viewModel.state?.code) { result in
            self.hideSpinner()
            switch result {
            case let .success(message):
                print("[VehicleLookupViewController] performVehicleLookup success: \(message)")
                self.presentVehicleData(message.vehicle)
            case let .failure(error):
                print("[VehicleLookupViewController] performVehicleLookup error: \(error)")
                self.showContentError(.noData)
            }
        }
    }
    
    private func presentVehicleData(_ data: VehicleData) {
        viewModel.vehicleData = data
        
        if data.isElectricVehicle == true {
            if !viewModel.searchedForEV {
                viewModel.searchedForEV = true
                delegate?.vehicleLookupViewControllerWillShowEV(self, allowSkip: viewModel.screenSource == .onboarding)
            }
            vrmTextField.value = ""
            showContentEV()
        } else {
            showContentData(data)
        }
        dismissKeyboard()
    }
    
    private func showContentStart() {
        removeAllChildren()
        
        let startViewController: VehicleLookupStartViewController = UIStoryboard.instantiateViewController()
        add(child: startViewController, to: containerView, animated: false)
        
        continueButton.isHidden = true
        skipButton.isHidden = viewModel.screenSource == .bubble || viewModel.screenSource == .profile
    }
    
    private func showContentEV() {
        removeAllChildren()
        
        let startViewController: VehicleLookupStartViewController = UIStoryboard.instantiateViewController()
        startViewController.showEVContent = true
        add(child: startViewController, to: containerView, animated: false)
        
        continueButton.isHidden = true
    }
    
    private func showContentData(_ data: VehicleData) {
        removeAllChildren()
        
        let dataViewController: VehicleLookupDataViewController = UIStoryboard.instantiateViewController()
        dataViewController.viewModel = VehicleLookupDataViewModel(vehicleData: data)
        dataViewController.delegate = self
        add(child: dataViewController, to: containerView, animated: false)
        
        if data.co2Emissions == nil {
            continueButton.setTitle("continue".localized, for: .normal)
            skipButton.isHidden = viewModel.screenSource == .bubble || viewModel.screenSource == .profile
        } else {
            if viewModel.screenSource == .bubble || viewModel.screenSource == .profile {
                continueButton.setTitle("vehicle.lookup.start.profile.flow.continue.button".localized, for: .normal)
            } else {
                continueButton.setTitle("continue".localized, for: .normal)
            }
            skipButton.isHidden = true
        }
        
        continueButton.isHidden = false
        continueButton.isEnabled = true
    }
    
    private func showContentError(_ errorType: VehicleLookupErrorType) {
        removeAllChildren()
        
        let lockedViewController: VehicleLookupErrorViewController = UIStoryboard.instantiateViewController()
        lockedViewController.errorType = errorType
        add(child: lockedViewController, to: containerView, animated: false)
        
        continueButton.setTitle("vehicle.lookup.enter.manually.button".localized, for: .normal)
        
        continueButton.isHidden = false
        continueButton.isEnabled = true
        skipButton.isHidden = viewModel.screenSource == .bubble || viewModel.screenSource == .profile
        viewModel.vehicleData = nil
    }
    
    private func removeAllChildren() {
        children.forEach { $0.remove() }
    }
    
    func onSearch() {
        let vrmStr = vrmTextField.text ?? ""
        let vrmStrEscaped = vrmStr.removingWhitespaces()
        guard !vrmStrEscaped.isEmpty else { return }
        performVehicleLookup()
    }
    
    @IBAction
    func onSkip() {
        delegate?.vehicleLookupViewControllerDidTapSkip(self)
    }
    
    @IBAction
    func onContinue() {
        guard let data = viewModel.vehicleData else {
            let vrmStr = vrmTextField.text?.removingWhitespaces()
            delegate?.vehicleLookupViewControllerDidTapEnterManually(self, vrm: vrmStr, country: viewModel.country, state: viewModel.state)
            return
        }
        
        if data.co2Emissions == nil {
            delegate?.vehicleLookupViewControllerDidTapEnterManually(self, vehicleData: data)
            return
        }
        
        showSpinner()
        continueButton.isEnabled = false
        
        api.registerVehicle(data) { result in
            self.hideSpinner()
            self.continueButton.isEnabled = true
            switch result {
            case .success:
                AnalyticsService.trackEvent(.addVehicle)
                self.delegate?.vehicleLookupViewControllerDidTapContinue(self)
            case let .failure(error):
                self.showAlertError(error: error)
            }
        }
    }
}

extension VehicleLookupViewController: VehicleLookupDataViewControllerDelegate {
    func vehicleLookupDataViewControllerDidTapEdit(_: VehicleLookupDataViewController) {
        guard let vehicleData = viewModel.vehicleData else { return }
        delegate?.vehicleLookupViewControllerDidTapEdit(self, vehicleData: vehicleData)
    }
}

extension VehicleLookupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let vrm = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines), !vrm.isEmpty {
            print("Searching for plate... \(vrm)")
            performVehicleLookup()
        }
        textField.resignFirstResponder()
        return true
    }
}
