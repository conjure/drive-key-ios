//
//  VehicleLookupDataViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

protocol VehicleLookupDataViewControllerDelegate: AnyObject {
    func vehicleLookupDataViewControllerDidTapEdit(_ viewController: VehicleLookupDataViewController)
}

class VehicleLookupDataViewController: UIViewController {
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var emissionsLabel: NamedLabel!
    @IBOutlet private var vehicleLabel: NamedLabel!
    @IBOutlet private var yearLabel: NamedLabel!
    @IBOutlet private var weightLabel: NamedLabel!
    @IBOutlet private var calculationLabel: UILabel!
    @IBOutlet private var fuelTypeLabel: NamedLabel!
    @IBOutlet private var fuelConsumptionLabel: NamedLabel!
    @IBOutlet private var bhpLabel: NamedLabel!
    @IBOutlet private var engineCapacityLabel: NamedLabel!

    var viewModel: VehicleLookupDataViewModel!
    weak var delegate: VehicleLookupDataViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let emissions = viewModel.co2Emissions {
            emissionsLabel.attributedName = "vehicle.details.emissions".localized.subscriptCO2(.K2DMedium(9))
            emissionsLabel.value = emissions
        } else {
            emissionsLabel.isHidden = true
        }

        if let vehicle = viewModel.model {
            vehicleLabel.name = "vehicle.details.vehicle.label".localized
            vehicleLabel.value = vehicle
        } else {
            vehicleLabel.isHidden = true
        }

        if let year = viewModel.year {
            yearLabel.name = "vehicle.details.year.label".localized
            yearLabel.value = year
        } else {
            yearLabel.isHidden = true
        }

        if let weight = viewModel.weight {
            weightLabel.name = "vehicle.details.weight.label".localized
            weightLabel.value = weight
        } else {
            weightLabel.isHidden = true
        }

        calculationLabel.font = UIFont.K2DMedium(16)
        calculationLabel.textColor = UIColor.black
        calculationLabel.attributedText = "vehicle.details.calculation.label".localized.subscriptCO2(UIFont.K2DMedium(10))
        calculationLabel.isHidden = viewModel.shouldHideCO2CalculationHeading

        if let fuelType = viewModel.fuelType {
            fuelTypeLabel.name = "vehicle.details.fuel.type.label".localized
            fuelTypeLabel.value = fuelType
        } else {
            fuelTypeLabel.isHidden = true
        }

        if let consumption = viewModel.fuelConsumption {
            fuelConsumptionLabel.name = "vehicle.details.fuel.consumption.label".localized
            fuelConsumptionLabel.value = consumption
        } else {
            fuelConsumptionLabel.isHidden = true
        }

        if let bhp = viewModel.bhp {
            bhpLabel.name = "vehicle.details.bhp.label".localized
            bhpLabel.value = bhp
        } else {
            bhpLabel.isHidden = true
        }

        if let engine = viewModel.engineCapacity {
            engineCapacityLabel.name = "vehicle.details.engine.capacity.label".localized
            engineCapacityLabel.value = engine
        } else {
            engineCapacityLabel.isHidden = true
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.flashScrollIndicators()
    }

    @IBAction
    func onEdit() {
        // delegate?.vehicleLookupDataViewControllerDidTapEdit(self)
    }
}
