//
//  VehicleLookupTextValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/08/2021.
//

import Foundation

class VehicleLookupTextValidator: TextValidatable {
    static let TextLenght = 7
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }

    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let text = text else {
                return true
            }

            return text.removeAllSpaces().count == VehicleLookupTextValidator.TextLenght
        })
    }
}
