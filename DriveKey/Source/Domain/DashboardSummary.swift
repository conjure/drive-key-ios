//
//  DashboardSummary.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 26/01/2022.
//

import Foundation

struct DashboardSummary: Codable {
    let miles: Double?
    let trips: Int?
    let overallEcoDrivingScore: Double?
    let co2PerKiloGram: Double?
    let co2OffsetPerKiloGram: Double?
    let hardBrake: Double?
    let hardAcceleration: Double?
    let hardTurn: Double?
    let kiloMeters: Double?
}

extension DashboardSummary: Distancable {}
