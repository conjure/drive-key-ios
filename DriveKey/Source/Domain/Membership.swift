//
//  Membership.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 18/10/2021.
//

import Foundation

struct MembershipAttachResponse: Codable {
    let clientSecret: String?
}

struct MembershipPayResponse: Codable {
    let paymentIntent: PaymentIntent?

    struct PaymentIntent: Codable {
        let status: String?
        let clientSecret: String?
    }

    var state: PaymentIntentState {
        guard let status = paymentIntent?.status else { return .unknown }
        return PaymentIntentState(rawValue: status) ?? .unknown
    }
}

struct MembershipSetupIntentResponse: Codable {
    let clientSecret: String
}

enum MembershipState: String {
    case unknown
    case incomplete
    case active
    case trialing
    case incomplete_expired
    case past_due
    case canceled
    case unpaid
}

enum PaymentIntentState: String {
    case unknown
    case requires_action
    case requires_confirmation
    case requires_payment_method
    case requires_capture
    case canceled
    case succeeded
}

struct ConfirmIntentResponse: Codable {
    let status: String

    var state: PaymentIntentState {
        PaymentIntentState(rawValue: status) ?? .unknown
    }
}
