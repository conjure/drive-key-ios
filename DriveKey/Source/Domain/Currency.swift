//
//  Currency.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import Foundation

struct Currency: Codable {
    let code: String
    let name: String
    
    var iconName: String {
        "currency-icon-" + code.lowercased()
    }
}
