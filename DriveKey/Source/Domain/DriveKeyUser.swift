//
//  DriveKeyUser.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import Foundation

class DrivekeyUser: Codable {
    let uid: String
    var firstName: String?
    var lastName: String?
    let email: String
    let dateOfBirth: String?
    let addressLineOne: String?
    let addressLineTwo: String?
    let addressLineThree: String?
    let region: String?
    let postcode: String?
    let country: String?
    let countryCode: String?
    let state: String?
    var overallEcoDrivingScore: Double?
    var activeVehicle: VehicleData?
    var customerId: String?
    var notifications: Bool?
    var paymentMethodId: String?
    var dashboardSummary: DashboardSummary?
    let currency: String?
    let currencyCode: String?
    var isDebug: Bool?
    let standardRate: Double?
    let units: Units?
    let defaultTripType: TripType?
    
    init(userID: String, firstName: String? = nil, lastName: String? = nil, email: String, dateOfBirth: String? = nil, addressLineOne: String? = nil, addressLineTwo: String? = nil, addressLineThree: String? = nil, region: String? = nil, postcode: String? = nil, country: String? = nil, countryCode: String? = nil, state: String? = nil, createdAt _: Date? = nil, currency: String? = nil, currencyCode: String? = nil, standardRate: Double? = nil, units: Units? = nil, defaultTripType: TripType? = nil) {
        uid = userID
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.dateOfBirth = dateOfBirth
        self.addressLineOne = addressLineOne
        self.addressLineTwo = addressLineTwo
        self.addressLineThree = addressLineThree
        self.region = region
        self.postcode = postcode
        self.country = country
        self.countryCode = countryCode
        self.state = state
        self.currency = currency
        self.currencyCode = currencyCode
        self.standardRate = standardRate
        self.units = units
        self.defaultTripType = defaultTripType
    }
    
    var dateOfBirthDate: Date? {
        guard let dob = dateOfBirth else { return nil }
        let formatter = ISO8601DateFormatter()
        return formatter.date(from: dob)
    }
    
    var isCurrency: Bool {
        currencyCode != nil
    }
    
    var unitDistance: UnitDistance {
        guard let units = units, let distance = units.distance else { return .miles }
        
        return distance
    }
}

enum UnitDistance: String, Codable {
    case miles = "MILES"
    case kilometers = "KILOMETERS"
}

struct Units: Codable {
    let distance: UnitDistance?
}

extension UnitDistance {
    var localized: String {
        switch self {
        case .miles:
            return "unit.distance.mi".localized
        case .kilometers:
            return "unit.distance.km".localized
        }
    }
    
    var localizedPlural: String {
        switch self {
        case .miles:
            return "unit.distance.mi.plural".localized
        case .kilometers:
            return "unit.distance.km.plural".localized
        }
    }
    
    var localizedShort: String {
        switch self {
        case .miles:
            return "unit.distance.mi.short".localized
        case .kilometers:
            return "unit.distance.km.short".localized
        }
    }
}
