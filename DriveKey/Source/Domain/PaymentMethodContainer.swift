//
//  PaymentMethodContainer.swift
//  DriveKey
//
//  Created by Piotr Wilk on 19/10/2021.
//

import Foundation

// https://app.quicktype.io

// MARK: - PaymentMethodContainer

struct PaymentMethodContainer: Codable {
    let paymentMethods: [StripePaymentMethod]?
}

// MARK: - StripePaymentMethod

struct StripePaymentMethod: Codable {
    let id: String?
    let object: String?
    let billing_details: BillingDetails?
    let card: Card?
    let created: Int?
    let customer: String?
    let livemode: Bool?
    // let metadata: Metadata?
    let type: String?
}

// MARK: - BillingDetails

struct BillingDetails: Codable {
    let address: Address?
    let email: String?
    let name: String?
    let phone: String?
}

// MARK: - Address

struct Address: Codable {
    let city: String?
    let country: String?
    let line1: String?
    let line2: String?
    let postal_code: String?
    let state: String?
}

// MARK: - Card

struct Card: Codable {
    let brand: String?
    let checks: Checks?
    let country: String?
    let exp_month: Int?
    let exp_year: Int?
    let fingerprint: String?
    let funding: String?
    let generated_from: String?
    let last4: String?
    let networks: Networks?
    let three_d_secure_usage: ThreeDSecureUsage?
    let wallet: Wallet?
}

// MARK: - Checks

struct Checks: Codable {
    let address_line1_check: String?
    let address_postal_code_check: String?
    let cvc_check: String?
}

// MARK: - Networks

struct Networks: Codable {
    let available: [String]?
    let preferred: String?
}

// MARK: - ThreeDSecureUsage

struct ThreeDSecureUsage: Codable {
    let supported: Bool?
}

// MARK: - Wallet

struct Wallet: Codable {
    // let applePay: Metadata?
    let dynamic_last4: String?
    let type: String?
}
