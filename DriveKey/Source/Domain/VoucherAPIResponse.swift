//
//  VoucherAPIResponse.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 28/02/2022.
//

import Foundation

struct VoucherAPIResponse: Codable {
    let voucher: String?
    let amount: Double?
    let status: String?
    let type: String?
}
