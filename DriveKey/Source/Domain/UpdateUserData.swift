//
//  UpdateUserData.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/09/2021.
//
// swiftlint:disable cyclomatic_complexity

import Foundation

struct UpdateUserPayload {
    let firstName: String?
    let lastName: String?
    let email: String?
    let dateOfBirth: Date?
    let addressLineOne: String?
    let addressLineTwo: String?
    let addressLineThree: String?
    let region: String?
    let postcode: String?
    let country: String?
    let countryCode: String?
    let state: String?
    let notifications: Bool?
    let fcmToken: String?
    let currency: String?
    let currencyCode: String?
    let standardRate: Double?
    let units: Units?
    let defaultTripType: TripType?

    init(firstName: String? = nil, lastName: String? = nil, email: String? = nil, dateOfBirth: Date? = nil, addressLineOne: String? = nil, addressLineTwo: String? = nil, addressLineThree: String? = nil, region: String? = nil, postcode: String? = nil, country: String? = nil, countryCode: String? = nil, state: String? = nil, notifications: Bool? = nil, fcmToken: String? = nil, currency: String? = nil, currencyCode: String? = nil, standardRate: Double? = nil, units: Units? = nil, defaultTripType: TripType? = nil) {
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.dateOfBirth = dateOfBirth
        self.addressLineOne = addressLineOne
        self.addressLineTwo = addressLineTwo
        self.addressLineThree = addressLineThree
        self.region = region
        self.postcode = postcode
        self.country = country
        self.countryCode = countryCode
        self.state = state
        self.notifications = notifications
        self.fcmToken = fcmToken
        self.currency = currency
        self.currencyCode = currencyCode
        self.standardRate = standardRate
        self.units = units
        self.defaultTripType = defaultTripType
    }

    func toDict() -> [String: Any] {
        var data = [String: Any]()
        if let firstName = firstName {
            data["firstName"] = firstName
        }
        if let lastName = lastName {
            data["lastName"] = lastName
        }
        if let email = email {
            data["email"] = email
        }
        if let dateOfBirth = dateOfBirth {
            let formatter = ISO8601DateFormatter()
            let dob = formatter.string(from: dateOfBirth)
            data["dateOfBirth"] = dob
        } else {
            data["dateOfBirth"] = ""
        }
        if let addressLineOne = addressLineOne {
            data["addressLineOne"] = addressLineOne
        }
        if let addressLineTwo = addressLineTwo {
            data["addressLineTwo"] = addressLineTwo
        }
        if let addressLineThree = addressLineThree {
            data["addressLineThree"] = addressLineThree
        }
        if let region = region {
            data["region"] = region
        }
        if let postcode = postcode {
            data["postcode"] = postcode
        }
        if let country = country {
            data["country"] = country
        }
        if let countryCode = countryCode {
            data["countryCode"] = countryCode
        }
        if let state = state {
            data["state"] = state
        }
        if let notif = notifications {
            data["notifications"] = notif
        }
        if let token = fcmToken {
            data["fcmToken"] = token
        }
        if let currency = currency {
            data["currency"] = currency
        }
        if let currencyCode = currencyCode {
            data["currencyCode"] = currencyCode
        }
        if let standardRate = standardRate {
            data["standardRate"] = standardRate
        }
        if let units = units, let distance = units.distance {
            let dataUnits = ["distance": distance.rawValue]
            data["units"] = dataUnits
        }
       
        if let defaultTripType = defaultTripType {
            data["defaultTripType"] = defaultTripType.rawValue
        }
        
        return data
    }
}
