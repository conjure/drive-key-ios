//
//  PaymentMethod.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import Foundation
import Stripe
import UIKit

struct PaymentMethod {
    let identifier: String?
    let isApplePay: Bool
    let brand: String?
    let last4: String?
    let expMonth: Int?
    let expYear: Int?
    let billingDetails: BillingDetails?

    init(isApplePay: Bool, brand: String? = nil, last4: String? = nil, expMonth: Int? = nil, expYear: Int? = nil, billingDetails: BillingDetails? = nil) {
        self.identifier = UUID().uuidString
        self.isApplePay = isApplePay
        self.brand = brand
        self.last4 = last4
        self.expMonth = expMonth
        self.expYear = expYear
        self.billingDetails = billingDetails
    }

    init(_ stripePaymentMethod: StripePaymentMethod) {
        // https://stripe.com/docs/api/payment_methods/object#payment_method_object-type
        // https://stripe.com/docs/api/charges/object#charge_object-payment_method_details-card-wallet-type
        identifier = stripePaymentMethod.id 
        isApplePay = stripePaymentMethod.card?.wallet?.type == "apple_pay"
        brand = stripePaymentMethod.card?.brand
        last4 = stripePaymentMethod.card?.last4
        expMonth = stripePaymentMethod.card?.exp_month
        expYear = stripePaymentMethod.card?.exp_year
        billingDetails = stripePaymentMethod.billing_details
    }

    var cardNumber: String {
        guard let digits = last4 else { return "" }
        return "\u{2022}\u{2022}\u{2022}\u{2022} \(digits)"
    }

    // https://stripe.com/docs/api/tokens/object#token_object-card-brand
    var cardBrandIcon: UIImage? {
        guard !isApplePay else { return STPImageLibrary.applePayCardImage() }
        guard let brand = brand else { return nil }

        switch brand.lowercased() {
        case "amex":
            return STPImageLibrary.amexCardImage()
        case "diners":
            return STPImageLibrary.dinersClubCardImage()
        case "discover":
            return STPImageLibrary.discoverCardImage()
        case "jcb":
            return STPImageLibrary.jcbCardImage()
        case "mastercard":
            return STPImageLibrary.mastercardCardImage()
        case "unionpay":
            return STPImageLibrary.unionPayCardImage()
        case "visa":
            return STPImageLibrary.visaCardImage()
        default:
            return STPImageLibrary.unknownCardCardImage()
        }
    }

    static var card: String = "card"
    static var wallet: String = "wallet"
}

struct PaymentMethodCard {
    let number: String
    let expMonth: Int
    let expYear: Int
    let cvv: String

    public init(number: String, expMonth: Int, expYear: Int, cvv: String) {
        self.number = number
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvv = cvv
    }
}

extension PaymentMethodCard {
    var stripeCardParams: STPPaymentMethodCardParams {
        let result = STPPaymentMethodCardParams()
        result.number = number
        result.expMonth = NSNumber(value: expMonth)
        result.expYear = NSNumber(value: expYear)
        result.cvc = cvv

        return result
    }
}
