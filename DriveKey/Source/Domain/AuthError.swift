//
//  AuthError.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 05/08/2021.
//

import Foundation

enum AuthError: Error {
    case invalidEmail
    case emailInUse
    case weakPassword
    case invalidResetLink
    case expiredResetLink
    case networkError
    case userNotFound
    case wrongPassword
    case unknown(reason: String)
    case userDisabled
}

extension AuthError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidEmail:
            return "error.auth.invalidEmail".localized
        case .emailInUse:
            return "error.auth.emailInUse".localized
        case .expiredResetLink:
            return "error.auth.reset.password.link".localized
        case .weakPassword:
            return "error.auth.weakPassword".localized
        case .invalidResetLink:
            return "error.auth.reset.password.link".localized
        case .networkError:
            return "error.auth.network".localized
        case .unknown:
            return "error.auth.internal".localized
        case .userNotFound:
            return "error.auth.userNotFound".localized
        case .wrongPassword:
            return "error.auth.wrongPassword".localized
        case .userDisabled:
            return "error.auth.userDisabled".localized
        }
    }
}
