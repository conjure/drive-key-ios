//
//  MileageSummary.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/09/2022.
//

import Foundation
import Firebase

struct MileageSummary: Codable {
    var months: [MonthSummary]?
}

struct MonthSummary: Codable {
    let currencyCode: String?
    let kiloMeters: Double?
    let miles: Double?
    let month: String?
    let total: Double?
    let start: String?
    let startAt: Timestamp?
    let tripId: String?
    let tripIds: [String]?
    let trips: Double?
    let uid: String?
    let unitsOfDistance: String?
    let year: String?
    
    var startDate: Date? {
        startAt?.dateValue()
    }
}

extension MonthSummary: Distancable {}

struct MonthValueSummary: Codable {
    let currencyCode: String?
    let distance: String?
    let total: Double?
}
