//
//  Bubble.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/08/2021.
//

import Foundation

struct Bubble: Codable, Hashable {
    let bubbleId: String
    let title: String
    let message: String
    let type: String
    let dismissed: Bool
    let meta: Meta

    var bubbleType: BubbleType? {
        BubbleType(rawValue: type)
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(bubbleId)
    }

    static func == (lhs: Bubble, rhs: Bubble) -> Bool {
        lhs.bubbleId == rhs.bubbleId
    }

    init(bubbleType: BubbleType, title: String, message: String) {
        bubbleId = UUID().uuidString
        type = bubbleType.rawValue
        dismissed = false
        meta = Bubble.Meta(createdAt: Date())
        self.title = title
        self.message = message
    }

    struct Meta: Codable {
        let createdAt: Date
    }
}

extension Bubble: Comparable {
    static func < (lhs: Bubble, rhs: Bubble) -> Bool {
        guard let typeA = lhs.bubbleType, let typeB = rhs.bubbleType else { return false }
        return typeA.asInt() < typeB.asInt()
    }
}

extension Bubble {
    var imageName: String {
        switch bubbleType {
        case .vehicleReg:
            return "bubble_vehicle_reg"
        case .biometric:
            return "bubble_biometric"
        case .locationPermission:
           return "bubble_location"
        case .expiredCard:
            return "bubble_payment_error"
        case .none:
            return ""
        }
    }
}

extension Bubble {
    var shownOnDashboard: Bool {
        return bubbleType == .vehicleReg || bubbleType == .locationPermission
    }
}

enum BubbleType: String, CaseIterable {
    case expiredCard = "EXPIRED_CARD"
    case locationPermission
    case vehicleReg = "REGISTER_VEHICLE"
    case biometric

    static var asArray: [BubbleType] { allCases }

    func asInt() -> Int {
        BubbleType.asArray.firstIndex(of: self)!
    }
}
