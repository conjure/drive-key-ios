//
//  OneTimePaymentSummary.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/01/2022.
//

import Foundation

struct OneTimePaymentSummary: Codable {
    let totalCo2Generated: Double
    let netCo2Price: Double
    let co2UnitPrice: Double
    let currency: String?
    let paymentMethodExist: Bool
    let dkMargin: Double
    let minTransactionFee: Double
    let totalPrice: Double?

    func toDict() -> [String: Any] {
        var data = [String: Any]()

        data["totalCo2Generated"] = totalCo2Generated
        data["netCo2Price"] = netCo2Price
        data["co2UnitPrice"] = co2UnitPrice
        data["dkMargin"] = dkMargin
        data["minTransactionFee"] = minTransactionFee
        data["currency"] = currency ?? ""
        data["totalPrice"] = totalPrice

        return data
    }
}
