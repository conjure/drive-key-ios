//
//  Report.swift
//  DriveKey
//
//  Created by Piotr Wilk on 30/08/2022.
//

import Foundation

struct Report: Codable {
    var month: String?
    let potential: String
    let co2: String
    let trips: Int
    let distance: Double
    let unit: UnitDistance
    
    var summary: String {
        var result = ""
        if month != nil, !month!.isEmpty {
            result += month!
        } else if trips == 1 {
            result += "tray.report.text.trip".localized
        } else if trips > 1 {
            result += String(trips) + " " + "tray.report.text.trips".localized
        }
        
        return result + " " + "tray.report.info.summary".localized
    }
}
