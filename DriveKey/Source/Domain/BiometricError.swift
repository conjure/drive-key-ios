//
//  BiometricError.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 06/08/2021.
//

import Foundation

enum BiometricType {
    case none
    case touchID
    case faceID
    case unknown
}

enum BiometricStatus: Int {
    case unknown = 0
    case granted = 1
    case disabled = -1
    case denied = -2
    case dismissedBubble = -3
}

enum BiometricError: LocalizedError {
    case authenticationFailed
    case userCancel
    case userFallback
    case biometryNotAvailable
    case biometryNotEnrolled
    case biometryLockout
    case unknown

    var errorDescription: String? {
        switch self {
        case .authenticationFailed: return "error.biometric.failed".localized
        case .userCancel: return "error.biometric.cancel".localized
        case .userFallback: return "error.biometric.fallback".localized
        case .biometryNotAvailable: return "error.biometric.notAvailable".localized
        case .biometryNotEnrolled: return "error.biometric.notEnrolled".localized
        case .biometryLockout: return "error.biometric.lockout".localized
        case .unknown: return "error.biometric.unknown".localized
        }
    }
}
