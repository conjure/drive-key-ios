//
//  VehicleData.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Foundation

enum VehicleDataStatus: String, Codable {
    case new = "NEW"
    case active = "ACTIVE"
    case inactive = "INACTIVE"
}

struct VehicleData: Codable {
    let vehicleId: String?
    let licencePlate: String?
    let vrm: String?
    let co2Emissions: Double?
    let model: String?
    let year: String?
    let weight: Double?
    let fuelType: String?
    let fuelConsumptionMin: Double?
    let fuelConsumptionMax: Double?
    let bhp: Double?
    let engineCapacity: String?
    let isElectricVehicle: Bool?
    let make: String?
    let country: String?
    let countryCode: String?
    let state: String?
    let status: VehicleDataStatus?
    let primary: Bool?
    let vehicleRegistration: VehicleRegistration?
    
    var platesValue: String? {
        if let value = licencePlate {
            return value
        }
        return vrm
    }

    var makeValue: String? {
        if let value = make {
            return value
        }
        return vehicleRegistration?.Make
    }
    
    var modelValue: String? {
        if let value = model {
            return value
        }
        return vehicleRegistration?.Model
    }
    
    var co2Value: Double? {
        if let value = co2Emissions {
            return value
        }
        return vehicleRegistration?.Co2Emissions
    }
    
    var yearValue: String? {
        if let value = year {
            return value
        }
        return vehicleRegistration?.YearOfManufacture
    }
    
    var weightValue: Double? {
        if let value = weight {
            return value
        }
        return vehicleRegistration?.GrossWeight
    }
    
    var fuelValue: String? {
        if let value = fuelType {
            return value
        }
        return vehicleRegistration?.FuelType
    }
    
    var bhpValue: Double? {
        if let value = bhp {
            return value
        }
        return nil
    }
    
    var engineCapacityValue: String? {
        if let value = engineCapacity {
            return value
        }
        return vehicleRegistration?.EngineCapacity
    }
}

struct VehicleDataLookupResponse: Codable {
    let vehicle: VehicleData
}

// MARK: - VehicleRegistration
struct VehicleRegistration: Codable {
    let DateOfLastUpdate: String?
    let Colour: String?
    let VehicleClass: String?
    let CertificateOfDestructionIssued: Bool?
    let EngineNumber: String?
    let EngineCapacity: String?
    let TransmissionCode: String?
    let Exported: Bool?
    let YearOfManufacture: String?
    let WheelPlan: String?
    let DateExported: String?
    let Scrapped: Bool?
    let Transmission: String?
    let DateFirstRegisteredUk: String?
    let Model: String?
    let GearCount: Int?
    let ImportNonEu: Bool?
    let PreviousVrmGB: String?
    let GrossWeight: Double?
    let DoorPlanLiteral: String?
    let MvrisModelCode: String?
    let Vin: String?
    let Vrm: String?
    let DateFirstRegistered: String?
    let DateScrapped: String?
    let DoorPlan: String?
    let YearMonthFirstRegistered: String?
    let VinLast5: String?
    let VehicleUsedBeforeFirstRegistration: Bool?
    let MaxPermissibleMass: Int?
    let Make: String?
    let MakeModel: String?
    let TransmissionType: String?
    let SeatingCapacity: Int?
    let FuelType: String?
    let Co2Emissions: Double?
    let Imported: Bool?
    let MvrisMakeCode: String?
    let PreviousVrmNI: String?
    let VinConfirmationFlag: String?
}
