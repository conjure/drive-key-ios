//
//  APIResponse.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/10/2021.
//

import Foundation

struct APIMessage: Codable {
    let message: String?
}

struct APIStatusMessage: Codable {
    let status: String?
}

struct APIErrorMessage: Codable {
    let code: Int?
    let message: String?
    let success: Bool?
}

struct APIAuthCodeMessage: Codable {
    let authCode: String?
}
