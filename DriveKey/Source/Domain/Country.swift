//
//  Country.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import Foundation

struct Country: Codable {
    let code: String
    let name: String
    let states: [CountryState]
    
    var isOther: Bool {
        code == Country.other.code
    }
}

struct CountryState: Codable {
    let code: String
    let name: String
}

extension Country {
    static let other = Country(code: "other", name: "Other", states: [CountryState]())
}
