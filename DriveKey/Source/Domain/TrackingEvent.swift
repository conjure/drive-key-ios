//
//  TrackingEvent.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 16/06/2022.
//

import Foundation

enum TrackingEvent {
    case accountCreatedEmail
    case accountCreatedApple
    case addVehicle
    case manualCO2
    case allowLocation
    case allowMotion
    case allowPush
    case enableBiometrics

    case signInEmail
    case signInApple

    case clickFindOutMoreEcoDriving
    case clickFindOutMoreCarbon

    case viewProjectBrazilian
    case viewProjectTalas
    case viewProjectMidilli

    case viewTripHistory

    case clickedOffsetCarbon
    case clickedPayNow
    case makePurchase
    case successfulPurchase(amount: Double, currency: String)
}

extension TrackingEvent {

    var eventName: String? {
        switch self {
        case .accountCreatedEmail, .accountCreatedApple:
            return "account_created"
        case .addVehicle:
            return "add_vehicle"
        case .manualCO2:
            return "manual_co2"
        case .allowLocation:
            return "permission_location"
        case .allowMotion:
            return "permission_motion"
        case .allowPush:
            return "permission_push"
        case .enableBiometrics:
            return "enabled_biometrics"
        case .signInEmail, .signInApple:
            return "login"
        case .clickFindOutMoreEcoDriving:
            return "find_out_more_eco_driving"
        case .clickFindOutMoreCarbon:
            return "find_out_more_co2"
        case .viewProjectBrazilian, .viewProjectTalas, .viewProjectMidilli, .viewTripHistory:
            return nil
        case .clickedOffsetCarbon:
            return "click_offset_carbon"
        case .clickedPayNow:
            return "click_pay_now"
        case .makePurchase:
            return "make_purchase"
        case .successfulPurchase:
            return "purchase_complete"
        }
    }

    var eventParams: [String: Any]? {
        switch self {
        case .accountCreatedApple, .signInApple:
            return ["source": "apple"]
        case .accountCreatedEmail, .signInEmail:
            return ["source": "email"]
        default:
            return nil
        }
    }
}
