//
//  PaymentError.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 10/03/2022.
//

import Foundation

enum PaymentError: Error {
    case applePay
}

extension PaymentError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .applePay:
            return "error.payment.apple.pay".localized
        }
    }
}
