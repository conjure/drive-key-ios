//
//  PaymentAPI.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 25/01/2022.
//

import Foundation

struct OneTimePaymentAPIResponse: Codable {
    let requiresAction: Bool?
    let clientSecret: String?
}
