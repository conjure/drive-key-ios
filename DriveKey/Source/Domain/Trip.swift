//
//  Trip.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/10/2021.
//

import Foundation
import MapKit
import Firebase

protocol Distancable {
    var miles: Double? { get }
    var kiloMeters: Double? { get }
    func distance(_ unit: UnitDistance) -> Double?
}

enum TripType: String, Codable {
    case personal = "PERSONAL"
    case business = "BUSINESS"
}

enum TripUnits: String, Codable {
    case miles = "MILES"
    case kilometers = "KILOMETERS"
}

struct Trip: Codable {
    let id: String?
    let start: String?
    let startAt: Timestamp?
    let end: String?
    let endAt: Timestamp?
    let ecoDriverScore: Double?
    let miles: Double?
    let kiloMeters: Double?
    let tripEvents: Int?
    let adjustedCo2PerKiloGram: Double?
    let co2EstPrice: Double?
    let end_location: TripLocation?
    let estimatedRate: EstimatedRate?
    let start_location: TripLocation?
    let licencePlate: String?
    let vehicleId: String?
    let tripType: TripType?
    let businessNotes: String?
    let businessReason: String?
    let distance: Double?
    let waypoints: [Waypoint]?

    struct TripLocation: Codable {
        let longitude: Double?
        let latitude: Double?
    }
    
    struct EstimatedRate: Codable {
        let currencyCode: String?
        let rate: Double?
        let total: Double?
        let unitsOfDistance: TripUnits?
    }
    
    var startDate: Date? {
        if let startAt = startAt {
            return startAt.dateValue()
        }
        
        if let start = start {
            let formatter = DateFormatter.yyyyMMddTHHmmssXXXXX
            return formatter.date(from: start)
        }
        
        return nil
    }
    
    var monthYearComponents: DateComponents? {
        if let startDate = startDate {
            return startDate.get(.month, .year)
        }
        
        return nil
    }
    
    var endDate: Date? {
        if let endAt = endAt {
            return endAt.dateValue()
        }
        
        if let end = end {
            let formatter = DateFormatter.yyyyMMddTHHmmssXXXXX
            return formatter.date(from: end)
        }
        
        return nil
    }
    
    var name: String? {
        guard let tripType = tripType else { return nil }
        
        if tripType == .business, let reason = businessReason, !reason.isEmpty {
            return reason
        }

        return tripType.rawValue.capitalizingFirstLetter()
    }
    
    var co2Value: Double? {
        adjustedCo2PerKiloGram
    }
    
    var startCoordinate: CLLocationCoordinate2D? {
        if let start_location, let lat = start_location.latitude, let lon = start_location.longitude {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        
        if coordinates.count > 1, let start = coordinates.first {
            return start
        }
        
        return nil
    }
    
    var endCoordinate: CLLocationCoordinate2D? {
        if let end_location, let lat = end_location.latitude, let lon = end_location.longitude {
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }
        
        if coordinates.count > 1, let end = coordinates.first {
            return end
        }
        
        return nil
    }
    
    var coordinates: [CLLocationCoordinate2D] {
        if let waypoints {
            return waypoints.map({ CLLocationCoordinate2D(latitude: $0.latitude, longitude: $0.longitude) })
        }
        
        return [CLLocationCoordinate2D]()
    }
    
    var licensePlateValue: String {
        if let licencePlate {
            return licencePlate
        }
        
        return ""
    }
}

extension Trip: Distancable {}

extension Trip: Comparable {
    static func < (lhs: Trip, rhs: Trip) -> Bool {
        guard let start = lhs.start, let end = rhs.start else { return false }

        return start <= end
    }

    static func == (lhs: Trip, rhs: Trip) -> Bool {
        lhs.id == rhs.id
    }
}

extension Distancable {
    func distance(_ unit: UnitDistance) -> Double? {
        if unit == .miles, let distance = miles {
            return distance
        }
        
        if unit == .kilometers, let distance = kiloMeters {
            return distance
        }
        
        return nil
    }
}
