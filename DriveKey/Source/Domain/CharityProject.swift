//
//  Project.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/02/2022.
//

import Foundation

enum CharityProject: Identifiable, CaseIterable {
    case brazilianAmazon
    case talas
    case midilli

    var location: String {
        "projects.location.international".localized
    }

    var name: String {
        switch self {
        case .brazilianAmazon:
            return "projects.amazon".localized
        case .talas:
            return "projects.talas".localized
        case .midilli:
            return "projects.midilli".localized
        }
    }
    
    var screenTitle: String {
        switch self {
        case .brazilianAmazon:
            return "projects.amazon.detail.title".localized
        default:
            return name
        }
    }
    
    var info: String {
        switch self {
        case .brazilianAmazon:
            return "charity.project.info.brazil.amazon".localized
        case .talas:
            return "charity.project.info.talas".localized
        case .midilli:
            return "charity.project.info.midilli".localized
        }
    }
    
    var infoDescription: String {
        switch self {
        case .brazilianAmazon:
            return "charity.project.info.description.brazil.amazon".localized
        case .talas:
            return "charity.project.info.description.talas".localized
        case .midilli:
            return "charity.project.info.description.midilli".localized
        }
    }

    var imageName: String {
        switch self {
        case .brazilianAmazon:
            return "brazilianAmazon"
        case .talas:
            return "talasWindFarm"
        case .midilli:
            return "midilliHydroelectric"
        }
    }

    var projectImages: [String] {
        let baseImageName = imageName
        var imageNames: [String] = []
        switch self {
        case .talas, .midilli:
            for i in 1...4 {
                let image = "\(baseImageName)_\(i)"
                imageNames.append(image)
            }
        case .brazilianAmazon:
            for i in 1...3 {
                let image = "\(baseImageName)_\(i)"
                imageNames.append(image)
            }
        }
        return imageNames
    }

    var id: String {
        return UUID().uuidString
    }
}
