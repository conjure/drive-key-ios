//
//  UserProvidable.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 31/01/2022.
//

import Foundation
import Combine

protocol UserProvidable {
    var currentUser: DrivekeyUser? { get }
    var lastTrip: Trip? { get }
    var mileageSummary: MileageSummary? { get }
    var vehicles: [VehicleData]? { get }
    var vehiclesActive: [VehicleData]? { get }
    var syncedSubject: PassthroughSubject<DrivekeyUser, Never> { get }
    func setUser(_ user: DrivekeyUser)
    func reset()
}
