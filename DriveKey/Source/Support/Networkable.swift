//
//  Networkable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Alamofire
import Foundation

protocol Networkable: AnyObject {
    func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, completion: @escaping (Result<T, Error>) -> Void)
    func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, completion: @escaping (Result<T, Error>) -> Void)
    func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, tokenProvider: AuthTokenProvidable?, completion: @escaping (Result<T, Error>) -> Void)
}
