//
//  AppDataProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import Foundation
import Combine

protocol AppDataProvidable {
    var countries: [Country] { get }
    var currencies: [Currency] { get }
    var dataUpdated: PassthroughSubject<Bool, Never> { get }
}
