//
//  PickerPresentable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/05/2022.
//

import Foundation
import UIKit

protocol PickerPresentable: AnyObject {
    func showPicker(_ data: [String], completion: ((Int) -> Void)?)
}

extension PickerPresentable where Self: UIViewController {
    func showPicker(_ data: [String], completion: ((Int) -> Void)?) {
        let pickerViewController = PickerViewController()
        pickerViewController.data = data
        pickerViewController.completion = completion
        
        pickerViewController.modalPresentationStyle = .overCurrentContext
        modalPresentationStyle = .currentContext
        definesPresentationContext = true
        
        present(pickerViewController, animated: true)
    }
}
