//
//  TrayPresentable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 12/07/2022.
//

import UIKit
import OverlayContainer

protocol TrayPresentable: AnyObject {
    func showTrayError(_ message: String?, completion: (() -> Void)?)
    func showTrayLogout(_ completion: (() -> Void)?)
    func showTrayDelete(_ completion: (() -> Void)?)
    func showTrayDeleteInfo(_ completion: (() -> Void)?)
    func showTrayDeleteConfirmation(_ completion: (() -> Void)?)
    func showTrayRemove(_ completion: (() -> Void)?)
    func showTrayRemoveLast(_ completion: (() -> Void)?)
    func showTrayRemoveVehicleConfirmation(_ completion: (() -> Void)?)
    func showTrayPrimarySet(_ vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?)
    func showTrayPrimaryUpdate(_ vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?)
    func showTrayPrimaryUpdate(_ preselected: VehicleData, vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?)
    func showTrayRemoveTrip(_ completion: ((String) -> Void)?)
    func showTrayRemoveTripConfirmation(_ completion: (() -> Void)?)
    func showTrayReport(_ report: Report?, completion: (() -> Void)?)
    func showTrayReportConfirmation(_ email: String?, completion: (() -> Void)?)
}

extension TrayPresentable where Self: UIViewController {
    func showTrayError(_ message: String?, completion: (() -> Void)?) {
        let trayError: ErrorTrayViewController = UIStoryboard.instantiateViewController()
        trayError.message = message
        trayError.onContinue = {
            completion?()
        }
        
        presentTray(trayError)
    }
    
    func showTrayLogout(_ completion: (() -> Void)?) {
        let tray: LogoutTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayDelete(_ completion: (() -> Void)?) {
        let tray: DeleteTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayDeleteInfo(_ completion: (() -> Void)?) {
        let tray: DeleteInfoTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayDeleteConfirmation(_ completion: (() -> Void)?) {
        let tray: DeleteConfirmationTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayRemove(_ completion: (() -> Void)?) {
        let tray: RemoveTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayRemoveLast(_ completion: (() -> Void)?) {
        let tray: RemoveLastTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }

    func showTrayConfirmCurrency(_ completion: (() -> Void)?) {
        let tray: ConfirmCurrencyTrayViewController = UIStoryboard.instantiateViewController()
        tray.onContinue = {
            completion?()
        }

        presentTray(tray)
    }
    
    func showTrayRemoveVehicleConfirmation(_ completion: (() -> Void)?) {
        let tray: RemoveConfirmationTrayViewController = UIStoryboard.instantiateViewController()
        tray.type = .vehicle
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayPrimarySet(_ vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?) {
        let tray: PrimaryTrayViewController = UIStoryboard.instantiateViewController()
        tray.shouldDismiss = false
        tray.isUpdate = false
        tray.vehicles = vehicles
        tray.onSelect = { (vehicle: VehicleData) in
            completion?(vehicle)
        }
        tray.onAddVehicle = {
            addVehicle?()
        }
        
        presentTray(tray)
    }
    
    func showTrayPrimaryUpdate(_ vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?) {
        let tray: PrimaryTrayViewController = UIStoryboard.instantiateViewController()
        tray.vehicles = vehicles
        tray.onSelect = { (vehicle: VehicleData) in
            completion?(vehicle)
        }
        tray.onAddVehicle = {
            addVehicle?()
        }
        
        presentTray(tray)
    }
    
    func showTrayPrimaryUpdate(_ preselected: VehicleData, vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?) {
        let tray: PrimaryTrayViewController = UIStoryboard.instantiateViewController()
        tray.vehicles = vehicles
        tray.preselectedVehicle = preselected
        tray.onSelect = { (vehicle: VehicleData) in
            completion?(vehicle)
        }
        tray.onAddVehicle = {
            addVehicle?()
        }
        
        presentTray(tray)
    }
    
    func showTrayPrimaryChange(_ preselected: VehicleData?, vehicles: [VehicleData], completion: ((VehicleData) -> Void)?, addVehicle: (() -> Void)?) {
        let tray: PrimaryTrayViewController = UIStoryboard.instantiateViewController()
        tray.vehicles = vehicles
        tray.preselectedVehicle = preselected
        tray.isChange = true
        tray.onSelect = { (vehicle: VehicleData) in
            completion?(vehicle)
        }
        tray.onAddVehicle = {
            addVehicle?()
        }
        
        presentTray(tray)
    }
    
    func showTrayRemoveTrip(_ completion: ((String) -> Void)?) {
        let tray: RemoveTripTrayViewController = UIStoryboard.instantiateViewController()
        tray.onSelect = { (reason: String) in
            completion?(reason)
        }
        
        presentTray(tray)
    }
    
    func showTrayRemoveTripConfirmation(_ completion: (() -> Void)?) {
        let tray: RemoveConfirmationTrayViewController = UIStoryboard.instantiateViewController()
        tray.type = .trip
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayReport(_ report: Report?, completion: (() -> Void)?) {
        let tray: ReportTrayViewController = UIStoryboard.instantiateViewController()
        tray.report = report
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayReportConfirmation(_ email: String?, completion: (() -> Void)?) {
        let tray: ReportConfirmationTrayViewController = UIStoryboard.instantiateViewController()
        tray.email = email
        tray.onContinue = {
            completion?()
        }
        
        presentTray(tray)
    }
    
    func showTrayMore(_ completion: ((MoreTrayOption) -> Void)?) {
        let tray: MoreTrayViewController = UIStoryboard.instantiateViewController()
        tray.onSelect = { option in
            completion?(option)
        }
        
        presentTray(tray)
    }
    
    private func presentTray(_ tray: TrayViewController) {
        let container = OverlayContainerViewController()
        container.viewControllers = [tray]
        container.delegate = tray
        container.moveOverlay(toNotchAt: 1, animated: false)
        container.transitioningDelegate = tray.presenter
        container.modalPresentationStyle = .custom
        present(container, animated: true, completion: nil)
    }
}
