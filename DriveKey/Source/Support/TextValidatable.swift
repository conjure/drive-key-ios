//
//  TextValidatable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2021.
//

import Foundation

typealias TextValidationClosure = (String?) -> Bool

protocol TextValidatable {
    var validationClosure: TextValidationClosure { get }
    func validate(_ text: String?) -> Bool
}

extension TextValidatable {
    func validate(_ text: String?) -> Bool {
        validationClosure(text)
    }
}
