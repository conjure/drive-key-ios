//
//  AlertPresentable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import Alamofire
import UIKit

protocol AlertPresentable: AnyObject {
    func showAlert(_ title: String, message: String, completion: ((UIAlertAction) -> Void)?)
    func showAlertError(title: String, error: Error, completion: ((UIAlertAction) -> Void)?)
    func showAlertError(_ message: String, andCheckConnection error: Error, completion: ((UIAlertAction) -> Void)?)
}

extension AlertPresentable where Self: UIViewController {
    func showAlert(_ title: String, message: String, completion: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "ok".localized, style: .default, handler: completion)
        alert.addAction(actionOk)

        present(alert, animated: true)
    }

    func showAlertError(title: String = "error".localized, error: Error, completion: ((UIAlertAction) -> Void)? = nil) {
        if case let NetworkManagerError.errorMessage(message) = error {
            showAlert(title, message: message ?? "", completion: completion)
            return
        }

        showAlert(title, message: error.localizedDescription, completion: completion)
    }

    func showAlertError(_ message: String, andCheckConnection error: Error, completion: ((UIAlertAction) -> Void)? = nil) {
        if let errorAlamo = error as? AFError {
            if let errorUrl = errorAlamo.underlyingError as? URLError, errorUrl.code == .notConnectedToInternet {
                showAlertError(error: errorUrl, completion: completion)
                return
            }

            if let errorNs = errorAlamo.underlyingError as NSError?, errorNs.domain == NSPOSIXErrorDomain, errorNs.code == 50 || errorNs.code == 57 {
                showAlertError(error: errorNs, completion: completion)
                return
            }
        }

        showAlert("error".localized, message: message, completion: completion)
    }
}
