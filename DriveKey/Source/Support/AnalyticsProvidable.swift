//
//  AnalyticsProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 27/05/2022.
//

import UIKit
import FirebaseAnalytics

protocol AnalyticsProvidable: AnyObject {
    func logScreenEvent()
}

extension AnalyticsProvidable where Self: UIViewController {
    func logScreenEvent() {
        AnalyticsService.trackScreenEvent(className)
    }
}
