//
//  PermissionProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/10/2021.
//

import Foundation

protocol PermissionProvidable: AnyObject {
    func requestPermission()
    func isPermissionGranted(_ completion: ((Bool) -> Void)?)
    var isAuthorized: Bool { get }
    var isPermissionAsked: Bool { get }
}
