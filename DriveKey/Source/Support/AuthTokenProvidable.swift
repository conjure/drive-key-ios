//
//  AuthTokenProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 17/08/2021.
//

import Foundation

protocol AuthTokenProvidable: AnyObject {
    func getToken(_ completion: ((String?) -> Void)?)
}
