//
//  VehicleProvidable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 29/07/2022.
//

import Foundation
import Combine

protocol VehicleProvidable: AnyObject {
    var data: PassthroughSubject<[VehicleData], Never> { get }
    var error: PassthroughSubject<Error, Never> { get }
    func registerListener(_ userId: String)
    func unregisterListener()
}
