//
//  Storable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2021.
//

import Foundation

protocol Storable: AnyObject {
    associatedtype StorageKey: CaseIterable

    func remove(forKey key: StorageKey)
    func clearAll()
}

extension Storable {
    func clearAll() {
        for key in StorageKey.allCases {
            remove(forKey: key)
        }
    }
}

protocol StringStorable: Storable {
    func setString(_ value: String, forKey key: StorageKey)
    func string(forKey key: StorageKey) -> String?
}

protocol IntStorable: Storable {
    func setInt(_ value: Int, forKey key: StorageKey)
    func int(forKey key: StorageKey) -> Int?
}
