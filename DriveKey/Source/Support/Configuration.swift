//
//  Configuration.swift
//  DriveKey
//
//  Created by Piotr Wilk on 13/10/2021.
//

import Foundation

// TODO: As an improvement it's possible to use a separate plist file instead of the app info plist
// https://betterprogramming.pub/how-to-create-development-staging-and-production-configs-in-xcode-ec58b2cc1df4
struct Configuration {
    static let isLocal = false
    
    private enum ConfigKeys {
        static let APIBaseURL               = "CONFIG_API_BASE_URL"
        static let GoogleServiceFileName    = "CONFIG_GOOGLE_SERVICE_FILE_NAME"
        static let GoogleMapsAPIKey         = "CONFIG_GOOGLE_MAPS_API_KEY"
        static let DynamicLinkDomain        = "CONFIG_DYNAMIC_LINK_DOMAIN"
        static let DynamicLinkResetPassword = "CONFIG_DYNAMIC_LINK_RESET_PASSWORD"
        static let StripePublishableKey     = "CONFIG_STRIPE_PUBLISHABLE_KEY"
        static let MerchantIdentifier       = "CONFIG_MERCHANT_IDENTIFIER"
    }

    static var APIBaseURL: String {
        if isLocal {
            return "http://127.0.0.1:5051/karai-dev/europe-west2"
        }
        
        let result = Configuration.getValue(ConfigKeys.APIBaseURL)
        return result
    }

    static var GoogleServiceFileName: String {
        let result = Configuration.getValue(ConfigKeys.GoogleServiceFileName)
        return result
    }
    
    static var GoogleMapsAPIKey: String {
        let result = Configuration.getValue(ConfigKeys.GoogleMapsAPIKey)
        return result
    }

    static var DynamicLinkDomain: String {
        let result = Configuration.getValue(ConfigKeys.DynamicLinkDomain)
        return result
    }

    static var DynamicLinkResetPassword: String {
        let result = Configuration.getValue(ConfigKeys.DynamicLinkResetPassword)
        return result
    }

    static var StripePublishableKey: String {
        let result = Configuration.getValue(ConfigKeys.StripePublishableKey)
        return result
    }

    static var MerchantIdentifier: String {
        let result = Configuration.getValue(ConfigKeys.MerchantIdentifier)
        return result
    }

    private static func getValue(_ key: String) -> String {
        guard let valueStr = Configuration.infoDictionary[key] as? String else {
            fatalError("[Configuration] Value not set in plist for this key: \(key)")
        }

        return valueStr
    }

    private static var infoDictionary: [String: Any] {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("[Configuration] Plist file not found")
        }

        return dict
    }
}
