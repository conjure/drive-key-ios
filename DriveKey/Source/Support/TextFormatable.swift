//
//  TextFormatable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/09/2021.
//

import Foundation

protocol TextFormatable {
    func format(_ str: String?) -> String?
}
