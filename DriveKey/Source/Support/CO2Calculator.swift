//
//  CO2Calculator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/11/2023.
//

import Foundation

class CO2Calculator {
    //`distance` in miles, `duration` in seconds, `emissions` in g/km
    static func co2InKgPerKm(_ distance: Double, duration: Double, emissions: Double) -> Double {
        guard distance > 0 else { return 0 }
        guard duration > 0 else { return 0 }
        guard emissions > 0 else { return 0 }
        
        let distanceInMi = Measurement(value: distance, unit: UnitLength.miles)
        let distanceInKm = distanceInMi.converted(to: .kilometers)
        
        let durationInMin = Measurement(value: duration, unit: UnitDuration.seconds)
        let durationInHour = durationInMin.converted(to: .hours)
        
        //https://conjure.myjetbrains.com/youtrack/issue/DKEY-602/DK-Enhanced-Co2-Calculation-Scaled-using-Emissions-Data
        let speed = distanceInKm.value / durationInHour.value
        var co2Factor: Double = 1
        if speed <= 10 {
            co2Factor = 2
        } else if speed <= 20 {
            co2Factor = 1.5
        } else if speed <= 30 {
            co2Factor = 1.25
        } else if speed <= 90 {
            co2Factor = 1
        } else if speed <= 120 {
            co2Factor = 1.25
        } else if speed <= 150 {
            co2Factor = 1.5
        } else {
            co2Factor = 2
        }
        let co2InGr = Measurement(value: emissions * distanceInKm.value * co2Factor, unit: UnitMass.grams)
        
        return co2InGr.converted(to: .kilograms).value
    }
}
