//
//  BubbleProvidable.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 08/02/2022.
//

import Foundation
import Combine

protocol BubbleProvidable {
    var bubbles: [Bubble] { get }
    var syncedSubject: PassthroughSubject<[Bubble], Never> { get }
    func sync(for user: DrivekeyUser)
    func refresh()
    func reset()
}

extension BubbleProvidable {
    func reset() {}
    func refresh() {}
}
