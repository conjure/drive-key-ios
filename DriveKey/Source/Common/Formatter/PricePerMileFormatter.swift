//
//  PricePerMileFormatter.swift
//  DriveKey
//
//  Created by Piotr Wilk on 15/09/2022.
//

import Foundation
import AnyFormatKit

class PricePerMileFormatter: TextFormatable {
    let formatter: DefaultTextInputFormatter
    let currencySymbol: String
    
    init(_ currencySymbol: String) {
        let pattern = currencySymbol + "#####"
        self.formatter = DefaultTextInputFormatter(textPattern: pattern)
        self.currencySymbol = currencySymbol
    }
    
    func format(_ str: String?) -> String? {
        let unformattedText = formatter.unformat(str)
        return formatter.format(unformattedText)
    }
}
