//
//  ViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

class ViewController: UIViewController, AlertPresentable, AnalyticsProvidable {
    private var spinnerView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)

    override func viewDidLoad() {
        super.viewDidLoad()

        spinnerView.hidesWhenStopped = true
        spinnerView.color = UIColor.black.withAlphaComponent(0.7)
        view.addSubview(spinnerView)

        navigationItem.backButtonTitle = ""
        navigationController?.navigationBar.barStyle = .black
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.bringSubviewToFront(spinnerView)
        logScreenEvent()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        spinnerView.center = view.center
    }

    func showSpinner() {
        spinnerView.startAnimating()
        self.view.isUserInteractionEnabled = false
        self.navigationController?.view.isUserInteractionEnabled = false
    }

    func hideSpinner() {
        spinnerView.stopAnimating()
        self.view.isUserInteractionEnabled = true
        self.navigationController?.view.isUserInteractionEnabled = true
    }

    override public var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}
