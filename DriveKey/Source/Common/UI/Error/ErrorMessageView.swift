//
//  ErrorMessageView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/11/2021.
//

import UIKit

class ErrorMessageView: BaseView {
    private let iconView = Subviews.iconView
    private let titleLabel = Subviews.titleLabel
    private let messageLabel = Subviews.messageLabel

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        layer.borderColor = UIColor(hex: 0xD50434).cgColor
        layer.borderWidth = 2.0

        addSubview(iconView)
        addSubview(titleLabel)
        addSubview(messageLabel)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        NSLayoutConstraint.activate([
            iconView.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            iconView.widthAnchor.constraint(equalToConstant: 13),
            iconView.heightAnchor.constraint(equalToConstant: 13),
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 9),

            titleLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 8),
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12),

            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 1),
            messageLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15)
        ])
    }

    func updateContent(title: String, message: String) {
        titleLabel.text = title
        messageLabel.text = message
    }
}

private struct Subviews {
    static var iconView: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "error"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var titleLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(15)
        label.textColor = .appColor(.errorRed)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true

        return label
    }

    static var messageLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(14)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 0

        return label
    }
}
