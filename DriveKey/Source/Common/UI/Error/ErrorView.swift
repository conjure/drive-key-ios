//
//  ErrorView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 18/08/2021.
//

import UIKit

class ErrorView: XibView {
    @IBOutlet private var headingLabel: UILabel!

    override func setup() {
        super.setup()

        layer.borderColor = UIColor(hex: 0xD50434).cgColor
        layer.borderWidth = 2.0
    }

    func configure(message: String) {
        headingLabel.text = message
    }
}
