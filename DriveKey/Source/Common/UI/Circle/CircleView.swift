//
//  CircleView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 29/10/2021.
//

import UIKit

class CircleView: UIView {
    var fillColor = UIColor(hex: 0x061A32)
    var topOffset: CGFloat = 0
    var padding: CGFloat = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

    func setup() {
        backgroundColor = .clear
    }

    override func draw(_ rect: CGRect) {
        var path = UIBezierPath()
        let width = rect.width - padding * 2
        path = UIBezierPath(ovalIn: CGRect(x: padding, y: padding + topOffset, width: width, height: width))
        fillColor.setFill()
        path.lineWidth = 0
        path.stroke()
        path.fill()
    }
}
