//
//  NamedSwitchField.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/12/2022.
//

import UIKit

class NamedSwitchField: BaseView {
    private let nameLabel = Subviews.nameLabel
    private var textField = Subviews.textField
    private var toggleSwitch = Subviews.toggleSwitch
    private let separatorView = Subviews.separatorView
    var theme: NamedFieldTheme = .dark
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        addSubview(nameLabel)
        addSubview(textField)
        addSubview(toggleSwitch)
        addSubview(separatorView)
        
        textField.isEnabled = false
        nameLabel.alpha = 0.69
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            nameLabel.heightAnchor.constraint(equalToConstant: 14),
            textField.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: -2),
            textField.heightAnchor.constraint(greaterThanOrEqualToConstant: 40),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            toggleSwitch.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -10),
            toggleSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            separatorView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 2)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var name: String? {
        didSet {
            guard let name = name else { return }
            nameLabel.text = name
            let attributes = [
                NSAttributedString.Key.foregroundColor: theme.placeholderText,
                NSAttributedString.Key.font: UIFont.K2DMedium(18)
            ]
            textField.attributedPlaceholder = NSAttributedString(string: name,
                                                                 attributes: attributes)
        }
    }
    
    var attributedName: NSAttributedString? {
        didSet {
            guard let attributedName = attributedName else { return }
            nameLabel.attributedText = attributedName
            let attributes = [
                NSAttributedString.Key.foregroundColor: theme.placeholderText,
                NSAttributedString.Key.font: UIFont.K2DMedium(18)
            ]
            
            let attStr = NSMutableAttributedString(attributedString: attributedName)
            attStr.addAttributes(attributes, range: attributedName.string.nsRange(of: attributedName.string))
            attStr.updateCO2(for: UIFont.K2DMedium(18), colour: theme.placeholderText)
            textField.attributedPlaceholder = attStr
        }
    }
    
    var label: String? {
        didSet {
            textField.text = label
        }
    }
    
    var value: Bool? {
        didSet {
            guard let value = value else { return }
            toggleSwitch.isOn = value
        }
    }
    
    var isOn: Bool {
        toggleSwitch.isOn
    }
    
    var isEnabled: Bool? {
        didSet {
            if isEnabled == true {
                toggleSwitch.isEnabled = true
            } else {
                toggleSwitch.isEnabled = false
            }
        }
    }
}

private struct Subviews {
    static var nameLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(12)
        label.textColor = .black
        label.textAlignment = .left
        label.alpha = 0
        label.numberOfLines = 1
        
        return label
    }
    
    static var textField: InsetTextField {
        let textField = InsetTextField(frame: CGRect.zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.K2DMedium(16)
        textField.textColor = .black
        textField.textAlignment = .left
        textField.adjustsFontSizeToFitWidth = true
        
        return textField
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        
        return view
    }
    
    static var toggleSwitch: UISwitch {
        let toggle = UISwitch(frame: .zero)
        toggle.translatesAutoresizingMaskIntoConstraints = false
        toggle.onTintColor = UIColor(hex: 0x26B787)
        
        return toggle
    }
}
