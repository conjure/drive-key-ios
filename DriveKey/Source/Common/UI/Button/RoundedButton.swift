//
//  RoundedButton.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import UIKit

class RoundedButton: UIButton {
    static var baseFont = UIFont.K2DBold(16)
    private let disabledColour = UIColor(hex: 0xB2B2B2)

    // MARK: - Initialization

    required init?(coder: NSCoder) {
        super.init(coder: coder)

        setup()
    }

    override required init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    private func setup() {
        layer.cornerRadius = 8

        backgroundColor = .appColor(.green)
        titleLabel?.font = RoundedButton.baseFont
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.1
        setTitleColor(.appColor(.navyBlue), for: .normal)
        setTitleColor(.appColor(.navyBlue), for: .selected)
        setTitleColor(.appColor(.navyBlue), for: .highlighted)
    }

    override var isEnabled: Bool {
        get {
            super.isEnabled
        }
        set {
            super.isEnabled = newValue
            backgroundColor = isEnabled ? .appColor(.green) : disabledColour
            titleLabel?.font = RoundedButton.baseFont
        }
    }
}
