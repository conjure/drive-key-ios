//
//  AppleSignInButton.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import UIKit

class AppleSignInButton: UIButton {
    // MARK: - Initialization

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        // style
        setup()
    }

    override required init(frame: CGRect) {
        super.init(frame: frame)

        // style
        setup()
    }

    private func setup() {
        layer.cornerRadius = 8.0
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1

        backgroundColor = .clear
        titleLabel?.font = UIFont.K2DMedium(16)
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.1

        setTitle("signin.apple.button".localized, for: .normal)
        setTitleColor(.black, for: .normal)
        setImage(UIImage(named: "apple_signin")?.withRenderingMode(.alwaysTemplate), for: .normal)
        tintColor = .black
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        moveImageLeftTextCenter()
    }

    private func moveImageLeftTextCenter(imagePadding: CGFloat = 30.0) {
        guard let imageViewWidth = imageView?.frame.width else { return }
        guard let titleLabelWidth = titleLabel?.intrinsicContentSize.width else { return }
        contentHorizontalAlignment = .left
        contentVerticalAlignment = .center
        imageEdgeInsets = UIEdgeInsets(top: 0, left: imagePadding - imageViewWidth / 2, bottom: 0, right: 0.0)
        titleEdgeInsets = UIEdgeInsets(top: 1.0, left: (bounds.width - titleLabelWidth) / 2 - imageViewWidth, bottom: 0.0, right: 0.0)
    }
}
