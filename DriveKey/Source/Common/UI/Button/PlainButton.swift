//
//  PlainButton.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

class PlainButton: UIButton {
    public enum Constants {
        static var height: CGFloat = 50
    }

    override var isEnabled: Bool {
        get {
            super.isEnabled
        }
        set {
            super.isEnabled = newValue
            alpha = newValue ? 1 : 0.4
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setUp()
        setUpLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setUp()
        setUpLayout()
    }

    public func setUp() {
        titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        setTitleColor(.black, for: .normal)
        setTitleColor(.black.withAlphaComponent(0.4), for: .selected)
        setTitleColor(.black.withAlphaComponent(0.4), for: .highlighted)
        backgroundColor = .clear
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

    public func setUpLayout() {
        let heightConstraint = heightAnchor.constraint(equalToConstant: Constants.height)
        heightConstraint.priority = .defaultHigh
        NSLayoutConstraint.activate([heightConstraint])
    }
}
