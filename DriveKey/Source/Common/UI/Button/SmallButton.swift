//
//  SmallButton.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/08/2021.
//

import UIKit

class SmallButton: UIButton {
    override func point(inside point: CGPoint, with _: UIEvent?) -> Bool {
        bounds.insetBy(dx: -15, dy: -15).contains(point)
    }
}
