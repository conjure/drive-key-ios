//
//  BorderedButton.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 04/08/2021.
//

import UIKit

class BorderedButton: UIButton {
    // MARK: - Initialization

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        // style
        setup()
    }

    override required init(frame: CGRect) {
        super.init(frame: frame)

        // style
        setup()
    }

    private func setup() {
        layer.cornerRadius = 8.0
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1

        backgroundColor = .clear
        titleLabel?.font = UIFont.K2DMedium(16)
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.1

        setTitleColor(.black, for: .normal)
    }
}
