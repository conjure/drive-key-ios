//
//  AssetsColor.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/08/2021.
//
// swiftlint:disable cyclomatic_complexity

import UIKit
import SwiftUI

enum AssetColor {
    case lightGray
    case purple
    case navyBlue
    case lightBlue
    case lightWhite
    case green
    case errorRed
    case red
    case chartEmpty
    case chartBlue
    case chartDarkBlue
    case chartFlashBlue
    case chartGreen
    case chartYellow
    case chartOrange
    case chartRed
    case pastelBlue
    case pastelDarkBlue
    case notificationBlue
    case pickerGray
    case pickerDarkGray
    case pickerSeparatorGray
    case darkGray
    case separatorGray
    case separatorDarkGray
    case trayLightGray
}

extension UIColor {
    static func appColor(_ name: AssetColor) -> UIColor {
        switch name {
        case .lightGray:
            return UIColor(named: "lightGray")!
        case .green:
            return UIColor(named: "green")!
        case .purple:
            return UIColor(named: "purple")!
        case .lightBlue:
            return UIColor(named: "lightBlue")!
        case .lightWhite:
            return UIColor(named: "lightWhite")!
        case .navyBlue:
            return UIColor(named: "navyBlue")!
        case .errorRed:
            return UIColor(named: "errorRed")!
        case .red:
            return UIColor(named: "red")!
        case .chartEmpty:
            return UIColor(named: "chartEmpty")!
        case .chartBlue:
            return UIColor(named: "chartBlue")!
        case .chartDarkBlue:
            return UIColor(named: "chartDarkBlue")!
        case .chartFlashBlue:
            return UIColor(named: "chartFlashBlue")!
        case .chartGreen:
            return UIColor(named: "chartGreen")!
        case .chartYellow:
            return UIColor(named: "chartYellow")!
        case .chartOrange:
            return UIColor(named: "chartOrange")!
        case .chartRed:
            return UIColor(named: "chartRed")!
        case .pastelBlue:
            return UIColor(named: "pastelBlue")!
        case .pastelDarkBlue:
            return UIColor(named: "pastelDarkBlue")!
        case .notificationBlue:
            return UIColor(named: "notificationBlue")!
        case .pickerGray:
            return UIColor(named: "pickerGray")!
        case .pickerDarkGray:
            return UIColor(named: "pickerDarkGray")!
        case .pickerSeparatorGray:
            return UIColor(named: "pickerSeparatorGray")!
        case .darkGray:
            return UIColor(named: "darkGray")!
        case .separatorGray:
            return UIColor(named: "separatorGray")!
        case .separatorDarkGray:
            return UIColor(named: "separatorDarkGray")!
        case .trayLightGray:
            return UIColor(named: "trayLightGray")!
        }
    }
}

extension Color {
    static func appColor(_ name: AssetColor) -> Color {
        switch name {
        case .lightGray:
            return Color("lightGray")
        case .green:
            return Color("green")
        case .purple:
            return Color("purple")
        case .lightBlue:
            return Color("lightBlue")
        case .lightWhite:
            return Color("lightWhite")
        case .navyBlue:
            return Color("navyBlue")
        case .errorRed:
            return Color("errorRed")
        case .red:
            return Color("red")
        case .chartEmpty:
            return Color("chartEmpty")
        case .chartBlue:
            return Color("chartBlue")
        case .chartDarkBlue:
            return Color("chartDarkBlue")
        case .chartFlashBlue:
            return Color("chartFlashBlue")
        case .chartGreen:
            return Color("chartGreen")
        case .chartYellow:
            return Color("chartYellow")
        case .chartOrange:
            return Color("chartOrange")
        case .chartRed:
            return Color("chartRed")
        case .pastelBlue:
            return Color("pastelBlue")
        case .pastelDarkBlue:
            return Color("pastelDarkBlue")
        case .notificationBlue:
            return Color("notificationBlue")
        case .pickerGray:
            return Color("pickerGray")
        case .pickerDarkGray:
            return Color("pickerDarkGray")
        case .pickerSeparatorGray:
            return Color("pickerSeparatorGray")
        case .darkGray:
            return Color("darkGray")
        case .separatorGray:
            return Color("separatorGray")
        case .separatorDarkGray:
            return Color("separatorDarkGray")
        case .trayLightGray:
            return Color("trayLightGray")
        }
    }
}
