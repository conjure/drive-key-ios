//
//  Fonts.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/08/2021.
//

import UIKit
import SwiftUI

extension UIFont {
    static var heading1: UIFont {
        UIFont(name: "K2D-Bold", size: 28)!
    }

    static var heading2: UIFont {
        UIFont(name: "K2D-Medium", size: 24)!
    }

    static var heading3: UIFont {
        UIFont(name: "K2D-Medium", size: 20)!
    }

    static var paragraph1: UIFont {
        UIFont(name: "K2D-Regular", size: 16)!
    }

    static var paragraph2: UIFont {
        UIFont(name: "K2D-Regular", size: 14)!
    }

    static var link: UIFont {
        UIFont(name: "K2D-Medium", size: 16)!
    }

    static var label: UIFont {
        K2DMedium(12)
    }

    class func K2DLight(_ size: CGFloat) -> UIFont {
        UIFont(name: "K2D-Light", size: size)!
    }

    class func K2DLightItalic(_ size: CGFloat) -> UIFont? {
        UIFont(name: "K2D-LightItalic", size: size)
    }

    class func K2DRegular(_ size: CGFloat) -> UIFont? {
        UIFont(name: "K2D-Regular", size: size)
    }

    class func K2DMedium(_ size: CGFloat) -> UIFont {
        UIFont(name: "K2D-Medium", size: size)!
    }

    class func K2DSemiBold(_ size: CGFloat) -> UIFont? {
        UIFont(name: "K2D-SemiBold", size: size)
    }

    class func K2DBold(_ size: CGFloat) -> UIFont? {
        UIFont(name: "K2D-Bold", size: size)
    }
}

extension Font {
    static func K2DBold(_ size: CGFloat) -> Font {
        Font.custom("K2D-Bold", size: size)
    }

    static func K2DLightItalic(_ size: CGFloat) -> Font {
        Font.custom("K2D-LightItalic", size: size)
    }

    static func K2DRegular(_ size: CGFloat) -> Font {
        Font.custom("K2D-Regular", size: size)
    }

    static func K2DMedium(_ size: CGFloat) -> Font {
        Font.custom("K2D-Medium", size: size)
    }

    static func K2DSemiBold(_ size: CGFloat) -> Font {
        Font.custom( "K2D-SemiBold", size: size)
    }
}
