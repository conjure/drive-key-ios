//
//  NavigationController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 14/09/2021.
//

import UIKit

class NavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.directionalLayoutMargins.leading = 24
        navigationBar.directionalLayoutMargins.trailing = 24

        UILabel.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).adjustsFontSizeToFitWidth = true
        
        setDefaultTheme()
    }
    
    func setDefaultTheme() {
        navigationBar.tintColor = .white
        navigationBar.barStyle = .black
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.K2DBold(18)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        let largeFontSize: CGFloat = UIScreen.main.bounds.width <= 320 ? 26 : 28
        navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.font: UIFont.K2DBold(largeFontSize)!,
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
    }
    
    // TODO: Add white label theme
    func setProfileBlackTheme() {
        navigationBar.tintColor = .black
        navigationBar.barStyle = .black
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.K2DBold(18)!,
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.font: UIFont.K2DBold(28)!,
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        topViewController?.preferredStatusBarStyle ?? .default
    }
}
