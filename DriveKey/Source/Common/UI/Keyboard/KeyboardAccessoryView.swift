//
//  KeyboardAccessoryView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/09/2021.
//

import UIKit

class KeyboardAccessoryView: BaseView {
    private let saveButton = Subviews.saveButton
    var onButton: (() -> Void)?

    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .appColor(.green)

        addSubview(saveButton)

        saveButton.addTarget(self, action: #selector(onSaveButton), for: .touchUpInside)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            saveButton.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            saveButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            saveButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            saveButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var buttonTitle: String? {
        didSet {
            saveButton.setTitle(buttonTitle, for: .normal)
        }
    }

    var isDisabled: Bool? {
        didSet {
            if isDisabled == true {
                saveButton.isEnabled = false
                saveButton.alpha = 0.5
                backgroundColor = UIColor(hex: 0xB2B2B2)
            } else {
                saveButton.isEnabled = true
                saveButton.alpha = 1
                backgroundColor = .appColor(.green)
            }
        }
    }

    @objc
    private func onSaveButton() {
        onButton?()
    }
}

private struct Subviews {
    static var saveButton: UIButton {
        let button = UIButton(frame: CGRect.zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("save".localized, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.black.withAlphaComponent(0.5), for: .highlighted)
        button.titleLabel?.font = UIFont.K2DMedium(18)
        button.titleLabel?.textAlignment = .center

        return button
    }
}
