//
//  KeyboardHandler.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/08/2021.
//

import UIKit

protocol KeyboardHandler: AnyObject {
    var keyboardConstraint: NSLayoutConstraint? { get set }
    var keyboardConstraintOffset: CGFloat? { get set }
    var keyboardObservables: [Any]? { get set }

    func keyboardHeight(_ notification: Notification) -> CGFloat
    func keyboardWillShow(_ notification: Notification)
    func keyboardWillHide(_ notification: Notification)
    func startObservingKeyboardChanges()
    func stopObservingKeyboardChanges()
    func setupHideKeyboardOnTap() -> UIGestureRecognizer
}

extension KeyboardHandler where Self: UIViewController {
    func startObservingKeyboardChanges() {
        keyboardConstraintOffset = keyboardConstraint?.constant ?? 0

        // show
        let showId = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                                            object: nil,
                                                            queue: nil) { [weak self] notification in
            self?.keyboardWillShow(notification)
        }
        keyboardObservables?.append(showId)

        // rotations
        let frameId = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                                             object: nil,
                                                             queue: nil) { [weak self] notification in
            self?.keyboardWillShow(notification)
        }
        keyboardObservables?.append(frameId)

        // keyboard change -> emoji, numerical, etc.
        let inputId = NotificationCenter.default.addObserver(forName: UITextInputMode.currentInputModeDidChangeNotification,
                                                             object: nil,
                                                             queue: nil) { [weak self] notification in
            self?.keyboardWillShow(notification)
        }
        keyboardObservables?.append(inputId)

        // hide
        let hideId = NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                                            object: nil,
                                                            queue: nil) { [weak self] notification in
            self?.keyboardWillHide(notification)
        }
        keyboardObservables?.append(hideId)
    }

    func stopObservingKeyboardChanges() {
        keyboardObservables?.forEach { NotificationCenter.default.removeObserver($0) }
    }

    func keyboardHeight(_ notification: Notification) -> CGFloat {
        guard let value = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return 0
        }

        return value.cgRectValue.height
    }

    func keyboardWillShow(_ notification: Notification) {
        let verticalPadding: CGFloat = 10
        guard let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
              let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt else { return }

        keyboardConstraint?.constant = keyboardHeight(notification) + verticalPadding - keyboardConstraintOffset!

        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions(rawValue: curve), animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func keyboardWillHide(_ notification: Notification) {
        guard let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
              let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt else { return }

        keyboardConstraint?.constant = keyboardConstraintOffset ?? 0

        UIView.animate(withDuration: duration, delay: 0.0, options: UIView.AnimationOptions(rawValue: curve), animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    @discardableResult
    func setupHideKeyboardOnTap() -> UIGestureRecognizer {
        let tapGesture = UITapGestureRecognizer(target: view, action: #selector(view.endEditing(_:)))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)

        return tapGesture
    }
}
