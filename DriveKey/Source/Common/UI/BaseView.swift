//
//  BaseView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit

class BaseView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setup()
    }

    func setup() {
        setUpSubviews()
        setUpLayout()
    }

    func setUpSubviews() {}

    func setUpLayout() {}
}
