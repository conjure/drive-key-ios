//
//  PickerViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 23/05/2022.
//

import UIKit

class PickerViewController: UIViewController {
    private var pickerView: UIPickerView!
    private var selectedRow: Int = 0
    var data = [String]()
    var completion: ((_ index: Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let separatorView = UIView(frame: .zero)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = .darkGray
        view.addSubview(separatorView)
        
        let navigationBar = UINavigationBar()
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.barTintColor = .appColor(.lightWhite)
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [
            NSAttributedString.Key.font: UIFont.K2DMedium(16),
            NSAttributedString.Key.foregroundColor: UIColor.appColor(.pickerDarkGray)
        ]
        navigationBar.shadowImage = UIImage()
        
        let navigationItem = UINavigationItem()
        let doneItem = UIBarButtonItem(title: "picker.view.done".localized, style: .done, target: self, action: #selector(onDone))
        doneItem.setTitleTextAttributes([.foregroundColor: UIColor.appColor(.pickerDarkGray)], for: .normal)
        navigationItem.rightBarButtonItem = doneItem

        let cancelItem = UIBarButtonItem(title: "picker.view.cancel".localized, style: .done, target: self, action: #selector(onCancel))
        cancelItem.setTitleTextAttributes([.foregroundColor: UIColor.appColor(.pickerDarkGray)], for: .normal)
        navigationItem.leftBarButtonItem = cancelItem

        navigationBar.items = [navigationItem]
        view.addSubview(navigationBar)
        
        pickerView = UIPickerView()
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.backgroundColor = .appColor(.pickerGray)
        view.addSubview(pickerView)
        
        pickerView.delegate = self
        pickerView.dataSource = self

        let constraints = [
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            separatorView.bottomAnchor.constraint(equalTo: navigationBar.topAnchor),
            navigationBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            navigationBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            navigationBar.bottomAnchor.constraint(equalTo: pickerView.topAnchor),
            pickerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pickerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pickerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        pickerView.reloadAllComponents()
    }

    @objc private func onCancel() {
        dismiss(animated: true)
    }
    
    @objc
    private func onDone() {
        dismiss(animated: true)
        completion?(selectedRow)
    }
}

extension PickerViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = row
    }
}

extension PickerViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        data.count
    }
}
