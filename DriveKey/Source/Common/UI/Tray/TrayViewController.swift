//
//  TrayViewController.swift
//  DriveKey
//
//  Created by Piotr Wilk on 12/07/2022.
//

import UIKit
import OverlayContainer

class TrayViewController: UIViewController {
    private(set) var presenter = TrayPresenter()
    private var draggerView: UIImageView!
    
    var onContinue: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.clipsToBounds = true
        view.layer.cornerRadius = 12
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        addDragger()
    }
    
    private func addDragger() {
        draggerView = UIImageView()
        draggerView.translatesAutoresizingMaskIntoConstraints = false
        draggerView.image = UIImage(named: "dragger")
        draggerView.contentMode = .center
        draggerView.isUserInteractionEnabled = true
        view.addSubview(draggerView)
        
        let constraints = [
            draggerView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
            draggerView.widthAnchor.constraint(equalToConstant: 40),
            draggerView.heightAnchor.constraint(equalToConstant: 4),
            draggerView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var contentHeight: CGFloat {
        200
    }
    
    var shouldDismissOnTap: Bool {
        true
    }
    
    var shouldDismissOnDrag: Bool {
        true
    }
    
    func willDismissOnTap() {
        /* ovveride */
    }
    
    func willDismissOnSwipe() {
        /* ovveride */
    }
}

extension TrayViewController: OverlayContainerViewControllerDelegate {
    func numberOfNotches(in containerViewController: OverlayContainerViewController) -> Int {
        2
    }
    
    func overlayContainerViewController(_ containerViewController: OverlayContainerViewController, heightForNotchAt index: Int, availableSpace: CGFloat) -> CGFloat {
        switch index {
        case 1:
            return contentHeight
        default:
            return 0
        }
    }
    
    func overlayContainerViewController(_ containerViewController: OverlayContainerViewController, willMoveOverlay overlayViewController: UIViewController, toNotchAt index: Int) {
        if index == 0 {
            willDismissOnSwipe()
        }
    }
    
    func overlayContainerViewController(_ containerViewController: OverlayContainerViewController, canReachNotchAt index: Int, forOverlay overlayViewController: UIViewController) -> Bool {
        
        if index == 0 {
            return shouldDismissOnDrag
        }
        
        return true
    }
}
