//
//  TrayPresenter.swift
//  DriveKey
//
//  Created by Piotr Wilk on 12/07/2022.
//

import UIKit
import OverlayContainer

class TrayPresenter: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        let dimmingView = TransparentOverlayContainerSheetDimmingView()
        dimmingView.minimumAlpha = 0.4
        dimmingView.maximumAlpha = 0.4
        let controller = OverlayContainerSheetPresentationController(dimmingView: dimmingView,
                                                                     presentedViewController: presented,
                                                                     presenting: presenting)
        controller.sheetDelegate = self
        
        return controller
    }
}

extension TrayPresenter: OverlayContainerSheetPresentationControllerDelegate {
    func overlayContainerSheetDismissalPolicy(for presentationController: OverlayContainerSheetPresentationController) -> OverlayContainerSheetDismissalPolicy {
        var policy = ThresholdOverlayContainerSheetDismissalPolicy()
        policy.dismissingVelocity = .value(2400)
        policy.dismissingPosition = .notch(index: 1)
        
        return policy
    }
    
    func overlayContainerSheetPresentationControllerShouldDismissOnTap(_ presentationController: OverlayContainerSheetPresentationController) -> Bool {
        if let overlay = presentationController.presentedViewController as? OverlayContainerViewController {
            if let tray = overlay.viewControllers.first(where: { $0 is TrayViewController }) as? TrayViewController {
                if tray.shouldDismissOnTap {
                    tray.willDismissOnTap()
                }
                
                return tray.shouldDismissOnTap
            }
        }
        
        return true
    }
}
