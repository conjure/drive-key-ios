//
//  AccordionView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

protocol AccordionViewDataSource: AnyObject {
    func numberOfSections(_ accordionView: AccordionView) -> Int
    func accordionView(_ accordionView: AccordionView, viewForSection index: Int) -> AccordionSectionable
    func accordionView(_ accordionView: AccordionView, viewForSectionContent index: Int) -> UIView
}

class AccordionView: BaseView {
    let tableView = Subviews.tableView
    private var sections = [AccordionSectionable]()
    
    public weak var dataSource: AccordionViewDataSource?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        addSubview(tableView)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNonzeroMagnitude))
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 20
        tableView.estimatedSectionFooterHeight = 0
        tableView.showsVerticalScrollIndicator = false
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.separatorStyle = .none
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func reloadData() {
        sections.removeAll()
        tableView.reloadData()
    }
    
    private func toggleSection(_ index: Int) {
        // toggle
        let section = sections[index]
        section.toggle()
        
        // content
        if section.isSelected {
            showContentForSection(true, index: section.sectionIndex)
        } else {
            showContentForSection(false, index: section.sectionIndex)
        }
    }
    
    private func showContentForSection(_ show: Bool, index: Int) {
        tableView.beginUpdates()
        let indexPath = IndexPath(row: 0, section: index)
        if show {
            tableView.insertRows(at: [indexPath], with: .fade)
        } else {
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        tableView.endUpdates()
    }
    
    func open(_ index: Int, scrollToVisible: Bool = false) {
        guard sections.count - 1 >= index else { return }
        
        let section = sections[index]
        if !section.isSelected {
            toggleSection(index)
        }
        
        if section.isSelected, scrollToVisible {
            let indexPath = IndexPath(row: 0, section: index)
            tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    func close(_ index: Int) {
        guard sections.count - 1 >= index else { return }
        
        let section = sections[index]
        if section.isSelected {
            toggleSection(index)
        }
    }
    
    @objc
    func onSectionTap(_ tapRecognizer: UITapGestureRecognizer) {
        if let section = tapRecognizer.view as? AccordionSectionable {
            toggleSection(section.sectionIndex)
        }
    }
    
    func setHeaderView(_ headerView: UIView) {
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingExpandedSize)
        let frame = CGRect(x: 0, y: 0, width: bounds.width, height: size.height)
        headerView.frame = frame
        
        // workaround for this: https://gist.github.com/marcoarment/1105553afba6b4900c10
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.tableView.tableHeaderView = headerView
        }
    }
}

extension AccordionView: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource?.numberOfSections(self) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard !sections.isEmpty else { return 0 }
        let section = sections[section]
        return section.isSelected ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // cache
        if sections.count > section {
            return sections[section]
        }
        
        // section
        let sectionView = dataSource?.accordionView(self, viewForSection: section)
        sectionView?.sectionIndex = section
        if sectionView != nil {
            sections.insert((sectionView! as AccordionSectionable), at: section)
        }
        
        // tap
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(onSectionTap))
        tapRecognizer.numberOfTapsRequired = 1
        sectionView?.addGestureRecognizer(tapRecognizer)
        
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // cell
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = .none
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
        
        // content
        if let contentView = dataSource?.accordionView(self, viewForSectionContent: indexPath.section) {
            cell.contentView.addSubview(contentView)
            contentView.translatesAutoresizingMaskIntoConstraints = false
            let constraints = [
                contentView.topAnchor.constraint(equalTo: cell.contentView.topAnchor),
                contentView.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor),
                contentView.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor),
                contentView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor)
            ]
            NSLayoutConstraint.activate(constraints)
        }
        
        cell.setNeedsLayout()
        
        return cell
    }
}

extension AccordionView: UITableViewDelegate {}

private struct Subviews {
    static var tableView: UITableView {
        let view = UITableView(frame: CGRect.zero, style: .grouped)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.backgroundView = UIView()
        view.backgroundView?.backgroundColor = .clear
        
        return view
    }
}
