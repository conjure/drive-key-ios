//
//  AccordionSectionable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 18/01/2022.
//

import UIKit

protocol AccordionSectionable where Self: UIView {
    var sectionIndex: Int { get set }
    var isSelected: Bool { get }
    func toggle()
}
