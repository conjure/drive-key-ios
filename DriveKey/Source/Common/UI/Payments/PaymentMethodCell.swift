//
//  PaymentMethodCell.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import UIKit

class PaymentMethodCell: UITableViewCell {
    static let reuseidentifier = "paymentMethodCell"

    let nameLabel = Subviews.nameLabel
    private let separator = Subviews.separatorView
    let disclosureIcon = Subviews.disclosureIndicator
    private let iconView = Subviews.brandImageView

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white

        addSubview(iconView)
        addSubview(nameLabel)
        addSubview(separator)
        addSubview(disclosureIcon)

        NSLayoutConstraint.activate([
            iconView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            iconView.widthAnchor.constraint(equalToConstant: 37),
            iconView.heightAnchor.constraint(equalToConstant: 37),
            iconView.bottomAnchor.constraint(equalTo: separator.topAnchor, constant: -13.0),

            nameLabel.centerYAnchor.constraint(equalTo: iconView.centerYAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: iconView.trailingAnchor, constant: 12),

            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            separator.heightAnchor.constraint(equalToConstant: 1.0),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor),

            disclosureIcon.heightAnchor.constraint(equalToConstant: 16),
            disclosureIcon.widthAnchor.constraint(equalToConstant: 8),
            disclosureIcon.centerYAnchor.constraint(equalTo: centerYAnchor),
            disclosureIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
        ])
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(for paymentMethod: PaymentMethod, hideDisclosure: Bool = false) {
        iconView.image = paymentMethod.cardBrandIcon
        nameLabel.text = paymentMethod.cardNumber
        disclosureIcon.isHidden = hideDisclosure
    }
}

private struct Subviews {
    static var brandImageView: UIImageView {
        let imageView = UIImageView(frame: .zero)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .appColor(.navyBlue)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }

    static var nameLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.font = UIFont.K2DRegular(18)
        label.textColor = .appColor(.navyBlue)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    static var separatorView: UIView {
        let separator = UIView(frame: .zero)
        separator.backgroundColor = .black.withAlphaComponent(0.2)
        separator.translatesAutoresizingMaskIntoConstraints = false
        return separator
    }

    static var disclosureIndicator: UIImageView {
        let imageView = UIImageView(image: UIImage(named: "cell_disclosure"))
        imageView.tintColor = .appColor(.navyBlue)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }
}
