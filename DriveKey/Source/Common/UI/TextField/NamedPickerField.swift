//
//  NamedPickerField.swift
//  DriveKey
//
//  Created by Piotr Wilk on 20/05/2022.
//

import UIKit

class NamedPickerField: BaseView {
    private let highlightView = Subviews.highlightView
    private let nameLabel = Subviews.nameLabel
    private(set) var textField = Subviews.textField
    private let separatorView = Subviews.separatorView
    private let tapRecognizer = UITapGestureRecognizer()
    
    private let iconSpacing: CGFloat = 12
    
    var onRightButton: (() -> Void)?
    var selectedIndex: Int = 0
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        addSubview(highlightView)
        addSubview(nameLabel)
        addSubview(textField)
        addSubview(separatorView)
        
        textField.isEnabled = false
        textField.delegate = self
        
        tapRecognizer.addTarget(self, action: #selector(onTap))
        tapRecognizer.numberOfTapsRequired = 1
        addGestureRecognizer(tapRecognizer)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            highlightView.topAnchor.constraint(equalTo: topAnchor),
            highlightView.leadingAnchor.constraint(equalTo: leadingAnchor),
            highlightView.trailingAnchor.constraint(equalTo: trailingAnchor),
            highlightView.bottomAnchor.constraint(equalTo: separatorView.topAnchor),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 9),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            nameLabel.heightAnchor.constraint(equalToConstant: 14),
            textField.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: -2),
            textField.heightAnchor.constraint(greaterThanOrEqualToConstant: 40),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            separatorView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 2)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var rightMargin: CGFloat? {
        didSet {
            guard let rightMargin = rightMargin else {
                textField.rightView = nil
                textField.rightViewMode = .never
                return
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: rightMargin, height: textField.bounds.height))
            textField.rightView = view
            textField.rightViewMode = .always
        }
    }
    
    var name: String? {
        didSet {
            guard let name = name else { return }
            nameLabel.text = name
        }
    }
    
    var value: String? {
        didSet {
            textField.text = value
        }
    }
    
    var rightIcon: UIImage? {
        didSet {
            if let rightIcon = rightIcon {
                let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: rightIcon.size.width, height: rightIcon.size.height))
                iconView.image = rightIcon
                iconView.contentMode = .scaleAspectFit
                
                textField.textInputInsets = UIEdgeInsets(top: 0, left: textField.textInputInsets.left, bottom: 0, right: iconSpacing)
                textField.rightView = iconView
                textField.rightViewMode = .always
            } else {
                textField.textInputInsets = UIEdgeInsets.zero
                textField.rightView = nil
                textField.rightViewMode = .never
            }
        }
    }
    
    func addRightIcon(_ icon: UIImage, size: CGSize) {
        let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        iconView.image = icon
        iconView.contentMode = .scaleAspectFit
        
        textField.textInputInsets = UIEdgeInsets(top: 0, left: textField.textInputInsets.left, bottom: 0, right: iconSpacing)
        textField.rightView = iconView
        textField.rightViewMode = .always
    }
    
    func removeRightIcon() {
        textField.textInputInsets = UIEdgeInsets.zero
        textField.rightView = nil
        textField.rightViewMode = .never
    }
    
    func addRightButton(_ icon: UIImage, size: CGSize) {
        let button = SmallButton(type: .system)
        button.setImage(icon.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView?.contentMode = .center
        button.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        button.addTarget(self, action: #selector(onButton), for: .touchUpInside)
        
        textField.textInputInsets = UIEdgeInsets(top: 0, left: textField.textInputInsets.left, bottom: 0, right: iconSpacing)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    @objc
    private func onButton() {
        onRightButton?()
    }
    
    @objc
    func onTap() {
        onRightButton?()
    }
    
    var leftIcon: UIImage? {
        didSet {
            if let leftIcon = leftIcon {
                let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: leftIcon.size.width, height: leftIcon.size.height))
                iconView.image = leftIcon
                iconView.contentMode = .scaleAspectFit
                
                textField.textInputInsets = UIEdgeInsets(top: 0, left: iconSpacing, bottom: 0, right: textField.textInputInsets.right)
                textField.leftView = iconView
                textField.leftViewMode = .always
            } else {
                textField.textInputInsets = UIEdgeInsets.zero
                textField.leftView = nil
                textField.leftViewMode = .never
            }
        }
    }
    
    var text: String? {
        textField.text
    }
    
    var isEnabled: Bool? {
        didSet {
            if isEnabled == true {
                tapRecognizer.isEnabled = true
                textField.rightView?.isHidden = false
            } else {
                tapRecognizer.isEnabled = false
                textField.rightView?.isHidden = true
            }
        }
    }
    
    var highlightColor: UIColor? {
        didSet {
            highlightView.backgroundColor = highlightColor
        }
    }
    
    func updateHighlight(_ color: UIColor) {
        UIView.transition(with: self, duration: 0.15, options: .transitionCrossDissolve, animations: { [unowned self] in
            self.highlightView.backgroundColor = color
        }, completion: nil)
        layoutIfNeeded()
    }
    
    var nameColor: UIColor? {
        didSet {
            guard let color = nameColor else { return }
            nameLabel.textColor = color
        }
    }
    
    var textColor: UIColor? {
        didSet {
            guard let color = textColor else { return }
            textField.textColor = color
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder ?? "",
                                                                 attributes: [.foregroundColor: color.withAlphaComponent(0.3)])
        }
    }
    
    var placeholderColor: UIColor? {
        didSet {
            guard let color = placeholderColor else { return }
            textField.attributedPlaceholder = NSAttributedString(string: textField.placeholder ?? "",
                                                                 attributes: [.foregroundColor: color])
        }
    }
    
    var separatorColor: UIColor? {
        didSet {
            guard let color = separatorColor else { return }
            separatorView.backgroundColor = color
        }
    }
}

extension NamedPickerField: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        false
    }
}

private struct Subviews {
    static var highlightView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }
    
    static var nameLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(12)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 1
        
        return label
    }
    
    static var textField: InsetTextField {
        let textField = InsetTextField(frame: CGRect.zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.K2DMedium(16)
        textField.textColor = .black
        textField.textAlignment = .left
        textField.adjustsFontSizeToFitWidth = true
        
        return textField
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        
        return view
    }
}
