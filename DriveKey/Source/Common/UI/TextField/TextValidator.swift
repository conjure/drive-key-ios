//
//  TextValidator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2021.
//

import Foundation

class TextValidator: TextValidatable {
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }
}
