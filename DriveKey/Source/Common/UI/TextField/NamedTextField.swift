//
//  NamedTextField.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/08/2021.
//

import UIKit

enum NamedTextFieldState {
    case `default`
    case focus
    case error
}

class NamedTextField: BaseView {
    private let highlightView = Subviews.highlightView
    private let nameLabel = Subviews.nameLabel
    private(set) var textField = Subviews.textField
    private let separatorView = Subviews.separatorView
    private let errorLabel = Subviews.errorLabel
    private let errorIconView = Subviews.errorIconView
    
    private var _state = NamedTextFieldState.default
    private let iconSpacing: CGFloat = 12
    
    var formatter: TextFormatable?
    var onRightButton: (() -> Void)?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        addSubview(highlightView)
        addSubview(nameLabel)
        addSubview(textField)
        addSubview(separatorView)
        addSubview(errorLabel)
        addSubview(errorIconView)
        
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraints = [
            highlightView.topAnchor.constraint(equalTo: topAnchor),
            highlightView.leadingAnchor.constraint(equalTo: leadingAnchor),
            highlightView.trailingAnchor.constraint(equalTo: trailingAnchor),
            highlightView.bottomAnchor.constraint(equalTo: separatorView.topAnchor),
            nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 6),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            nameLabel.heightAnchor.constraint(equalToConstant: 14),
            textField.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: -2),
            textField.heightAnchor.constraint(greaterThanOrEqualToConstant: 40),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            separatorView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 0),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 2),
            errorLabel.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 4),
            errorLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            errorLabel.trailingAnchor.constraint(equalTo: errorIconView.leadingAnchor, constant: -4),
            errorLabel.heightAnchor.constraint(equalToConstant: 14),
            errorLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            errorIconView.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 4),
            errorIconView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            errorIconView.widthAnchor.constraint(equalToConstant: 13),
            errorIconView.heightAnchor.constraint(equalToConstant: 13)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    var rightMargin: CGFloat? {
        didSet {
            guard let rightMargin = rightMargin else {
                textField.rightView = nil
                textField.rightViewMode = .never
                return
            }
            
            let view = UIView(frame: CGRect(x: 0, y: 0, width: rightMargin, height: textField.bounds.height))
            textField.rightView = view
            textField.rightViewMode = .always
        }
    }
    
    var name: String? {
        didSet {
            guard let name = name else { return }
            nameLabel.text = name
            let attributes = [
                NSAttributedString.Key.foregroundColor: theme.placeholderText,
                NSAttributedString.Key.font: UIFont.K2DMedium(18)
            ]
            textField.attributedPlaceholder = NSAttributedString(string: name,
                                                                 attributes: attributes)
        }
    }
    
    var attributedName: NSAttributedString? {
        didSet {
            guard let attributedName = attributedName else { return }
            nameLabel.attributedText = attributedName
            let attributes = [
                NSAttributedString.Key.foregroundColor: theme.placeholderText,
                NSAttributedString.Key.font: UIFont.K2DMedium(18)
            ]
            
            let attStr = NSMutableAttributedString(attributedString: attributedName)
            attStr.addAttributes(attributes, range: attributedName.string.nsRange(of: attributedName.string))
            attStr.updateCO2(for: UIFont.K2DMedium(18), colour: theme.placeholderText)
            textField.attributedPlaceholder = attStr
        }
    }
    
    var value: String? {
        didSet {
            textField.text = value
            textFieldDidChange()
        }
    }
    
    var rightIcon: UIImage? {
        didSet {
            if let rightIcon = rightIcon {
                let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: rightIcon.size.width, height: rightIcon.size.height))
                iconView.image = rightIcon
                iconView.contentMode = .scaleAspectFit
                
                textField.textInputInsets = UIEdgeInsets(top: 0, left: textField.textInputInsets.left, bottom: 0, right: iconSpacing)
                textField.rightView = iconView
                textField.rightViewMode = .always
            } else {
                textField.textInputInsets = UIEdgeInsets.zero
                textField.rightView = nil
                textField.rightViewMode = .never
            }
        }
    }
    
    func addRightButton(_ icon: UIImage, size: CGSize) {
        let button = SmallButton(type: .system)
        button.setImage(icon.withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView?.contentMode = .center
        button.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        button.addTarget(self, action: #selector(onButton), for: .touchUpInside)
        
        textField.textInputInsets = UIEdgeInsets(top: 0, left: textField.textInputInsets.left, bottom: 0, right: iconSpacing)
        textField.rightView = button
        textField.rightViewMode = .always
    }
    
    @objc
    private func onButton() {
        onRightButton?()
    }
    
    var leftIcon: UIImage? {
        didSet {
            if let leftIcon = leftIcon {
                let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: leftIcon.size.width, height: leftIcon.size.height))
                iconView.image = leftIcon
                iconView.contentMode = .scaleAspectFit
                
                textField.textInputInsets = UIEdgeInsets(top: 0, left: iconSpacing, bottom: 0, right: textField.textInputInsets.right)
                textField.leftView = iconView
                textField.leftViewMode = .always
            } else {
                textField.textInputInsets = UIEdgeInsets.zero
                textField.leftView = nil
                textField.leftViewMode = .never
            }
        }
    }
    
    var text: String? {
        textField.text
    }
    
    var errorMessage: String? {
        didSet {
            errorLabel.text = errorMessage
        }
    }
    
    var validator: TextValidatable? {
        didSet {
            setNeedsLayout()
        }
    }
    
    @discardableResult
    func validate() -> Bool {
        if let validator = validator {
            if validator.validate(text) {
                state = .default
            } else {
                state = .error
            }
        } else {
            state = .default
        }
        
        return state != .error
    }
    
    private var state: NamedTextFieldState {
        get {
            _state
        }
        set {
            guard _state != newValue else {
                return
            }
            
            _state = newValue
            updateState()
        }
    }
    
    func setError(_ isError: Bool) {
        state = isError ? .error : .default
    }
    
    var isError: Bool {
        return state == .error
    }
    
    var isEnabled: Bool? {
        didSet {
            if isEnabled == true {
                textField.isEnabled = true
                state = .focus
            } else {
                textField.isEnabled = false
                state = .default
                textFieldDidChange()
            }
        }
    }
    
    var isDisabled: Bool? {
        didSet {
            if isDisabled == true {
                textField.isEnabled = false
                state = .default
                highlightView.backgroundColor = .clear
                alpha = 0.5
            } else {
                textField.isEnabled = false
                state = .default
                highlightView.backgroundColor = .clear
                alpha = 1
            }
        }
    }
    
    var theme: NamedFieldTheme = .dark {
        didSet {
            updateState()
        }
    }
    
    private func updateState() {
        textField.textColor = theme.text
        
        UIView.transition(with: self, duration: 0.15, options: .transitionCrossDissolve, animations: { [unowned self] in
            switch self.state {
            case .error:
                self.nameLabel.textColor = UIColor(named: "errorRed")
                self.separatorView.backgroundColor = UIColor(named: "errorRed")
                self.errorLabel.alpha = 1
                self.errorIconView.alpha = 1
            case .focus:
                self.highlightView.backgroundColor = self.theme.highlight
                self.nameLabel.textColor = self.theme.name
                self.separatorView.backgroundColor = self.theme.separator
                self.errorLabel.alpha = 0
                self.errorIconView.alpha = 0
            case .default:
                self.nameLabel.textColor = self.theme.name
                self.separatorView.backgroundColor = self.theme.separator
                self.errorLabel.alpha = 0
                self.errorIconView.alpha = 0
            }
        }, completion: nil)
        layoutIfNeeded()
    }
    
    @objc
    func textFieldDidChange() {
        if let formatter = formatter {
            textField.text = formatter.format(textField.text)
        }
        
        if !textField.isEnabled {
            highlightView.backgroundColor = .clear
            return
        }
        
        var shouldHideName = true
        var color = UIColor.clear
        if let text = textField.text, !text.isEmpty {
            color = theme.highlight
            shouldHideName = false
        }
        
        if highlightView.backgroundColor != color {
            UIView.transition(with: self, duration: 0.15, options: .transitionCrossDissolve, animations: { [unowned self] in
                self.highlightView.backgroundColor = color
                self.nameLabel.alpha = shouldHideName ? 0 : 0.69
            }, completion: nil)
        }
        
        if !shouldHideName, nameLabel.alpha == 0 {
            nameLabel.alpha = 1
        }
    }
}

private struct Subviews {
    static var highlightView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        
        return view
    }
    
    static var nameLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(12)
        label.textColor = .black
        label.textAlignment = .left
        label.alpha = 0
        label.numberOfLines = 1
        
        return label
    }
    
    static var textField: InsetTextField {
        let textField = InsetTextField(frame: CGRect.zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.K2DMedium(16)
        textField.textColor = .black
        textField.textAlignment = .left
        textField.adjustsFontSizeToFitWidth = true
        
        return textField
    }
    
    static var errorLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(14)
        label.textColor = UIColor(named: "errorRed")
        label.textAlignment = .left
        label.alpha = 0
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }
    
    static var errorIconView: UIImageView {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        view.image = UIImage(named: "text-validation-error-icon")
        view.alpha = 0
        
        return view
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        
        return view
    }
}

struct NamedFieldTheme {
    let text: UIColor
    let name: UIColor
    let placeholderText: UIColor
    let highlight: UIColor
    let separator: UIColor
}

extension NamedFieldTheme {
    static var dark: NamedFieldTheme {
        let theme = NamedFieldTheme(text: .black,
                                    name: .black,
                                    placeholderText: .black,
                                    highlight: UIColor(hex: 0xE6E8EA),
                                    separator: .black)
        return theme
    }
    
    static var light: NamedFieldTheme {
        let theme = NamedFieldTheme(text: .white,
                                    name: .white.withAlphaComponent(0.7),
                                    placeholderText: .white.withAlphaComponent(0.5),
                                    highlight: .appColor(.notificationBlue),
                                    separator: .white)
        return theme
    }
}
