//
//  InsetTextField.swift
//  DriveKey
//
//  Created by Piotr Wilk on 28/09/2021.
//

import UIKit

class InsetTextField: UITextField {
    var textInputInsets = UIEdgeInsets.zero

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        super.textRect(forBounds: bounds.inset(by: textInputInsets))
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        super.editingRect(forBounds: bounds.inset(by: textInputInsets))
    }
}
