//
//  NamedLabel.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import UIKit

class NamedLabel: BaseView {
    private let nameLabel = Subviews.nameLabel
    private let valueLabel = Subviews.valueLabel
    private let separatorView = Subviews.separatorView

    override func setUpSubviews() {
        super.setUpSubviews()

        addSubview(nameLabel)
        addSubview(valueLabel)
        addSubview(separatorView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            valueLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 2),
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.topAnchor.constraint(equalTo: valueLabel.bottomAnchor, constant: 10),
            separatorView.leadingAnchor.constraint(equalTo: leadingAnchor),
            separatorView.trailingAnchor.constraint(equalTo: trailingAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1),
            separatorView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var name: String? {
        didSet {
            nameLabel.text = name
        }
    }

    var attributedName: NSAttributedString? {
        didSet {
            nameLabel.attributedText = attributedName
        }
    }

    var value: String? {
        didSet {
            valueLabel.text = value
        }
    }
}

private struct Subviews {
    static var nameLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(12)
        label.textColor = .black
        label.textAlignment = .left
        label.alpha = 0.69
        label.numberOfLines = 1

        return label
    }

    static var valueLabel: UILabel {
        let label = UILabel(frame: CGRect.zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DMedium(16)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true

        return label
    }

    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        view.alpha = 0.2

        return view
    }
}
