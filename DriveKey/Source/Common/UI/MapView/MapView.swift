//
//  MapView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 07/02/2022.
//

import UIKit
import GoogleMaps

class MapView: BaseView {
    private var mapView = Subviews.mapView
    private var coverView = Subviews.coverView
    private var markers = [GMSMarker]()
    
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(mapView)
        addSubview(coverView)
        mapView.delegate = self
        
        // mapView.stopRendering()
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor),
            coverView.topAnchor.constraint(equalTo: topAnchor),
            coverView.leadingAnchor.constraint(equalTo: leadingAnchor),
            coverView.trailingAnchor.constraint(equalTo: trailingAnchor),
            coverView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func addMarker(_ marker: GMSMarker) {
        markers.append(marker)
        marker.map = mapView
    }
    
    func showAllMarkers(_ padding: CGFloat) {
        var bounds = GMSCoordinateBounds()
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let insets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        runOnMainThreadAsyncAfter(0.2) {
            if let camera = self.mapView.camera(for: bounds, insets: insets) {
                self.mapView.camera = camera
            }
        }
    }
    
    func removeAllMarkers() {
        markers.forEach({ $0.map = nil })
        markers.removeAll()
    }
    
    func clear() {
        markers.removeAll()
        mapView.clear()
    }
    
    private func hideCoverView() {
        UIView.animate(withDuration: 0.25) {
            self.coverView.alpha = 0
        }
    }
    
    func setStyle(_ jsonName: String) {
        mapView.setStyle(jsonName)
    }
    
    var camera: GMSCameraPosition? {
        didSet {
            guard let camera = camera else { return }
            mapView.camera = camera
        }
    }
    
    var map: GMSMapView {
        mapView
    }
}

private struct Subviews {
    static var mapView: GMSMapView {
        let view = GMSMapView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = .clear
        
        return view
    }
    
    static var coverView: UIView {
        let view = UIView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor(hex: 0xE5E5E5)
        
        return view
    }
}

extension MapView: GMSMapViewDelegate {
    func mapViewDidStartTileRendering(_ mapView: GMSMapView) {
        runOnMainThreadAsyncAfter(0.15) {
            self.hideCoverView()
        }
    }
}
