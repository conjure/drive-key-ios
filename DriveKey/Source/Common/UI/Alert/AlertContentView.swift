//
//  AlertContentView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 21/09/2021.
//

import UIKit

class AlertContentView: BaseView {
    private let headingLabel = Subviews.headingLabel
    private let messageLabel = Subviews.messageLabel
    private let confirmButton = Subviews.confirmButton
    private let cancelButton = Subviews.cancelButton

    var confirmCallback: (() -> Void)?
    var dismissCallback: (() -> Void)?

    override func setUpSubviews() {
        super.setUpSubviews()

        addSubview(headingLabel)
        addSubview(messageLabel)
        addSubview(confirmButton)
        addSubview(cancelButton)

        confirmButton.addTarget(self, action: #selector(didConfirm), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
    }

    override func setUpLayout() {
        super.setUpLayout()

        NSLayoutConstraint.activate([
            headingLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20),
            headingLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 34),
            headingLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -34),

            messageLabel.topAnchor.constraint(equalTo: headingLabel.bottomAnchor, constant: 16),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),

            confirmButton.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 32),
            confirmButton.trailingAnchor.constraint(equalTo: trailingAnchor),
            confirmButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            confirmButton.heightAnchor.constraint(equalToConstant: 61),
            confirmButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            cancelButton.leadingAnchor.constraint(equalTo: leadingAnchor),
            cancelButton.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5),
            cancelButton.heightAnchor.constraint(equalToConstant: 61),
            cancelButton.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    var type: AlertStyle! {
        didSet {
            switch type {
            case .cancelMembership:
                headingLabel.text = "membership.cancel".localized
                messageLabel.text = "alert.cancel.membership.message".localized
                confirmButton.setTitle("membership.cancel".localized, for: .normal)
            case .changePaymentMethod:
                headingLabel.text = "payments.change.method".localized
                messageLabel.text = "payments.change.method.alert.message".localized
                confirmButton.setTitle("payments.change.method.confirm".localized, for: .normal)
            case .removeCard:
                headingLabel.text = "payments.remove.method.alert.title".localized
                messageLabel.text = "payments.remove.method.alert.message".localized
                confirmButton.setTitle("payments.remove.method.confirm".localized, for: .normal)
            default:
                print("Not implemented yet")
            }
        }
    }

    @objc private func didConfirm() {
        confirmCallback?()
    }

    @objc private func dismissAlert() {
        dismissCallback?()
    }

    override var intrinsicContentSize: CGSize {
        let superSize = super.intrinsicContentSize
        return CGSize(width: superSize.width, height: 200)
    }
}

private struct Subviews {
    static var headingLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.font = .K2DBold(18)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    static var messageLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = .K2DRegular(16)
        label.textColor = .black
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }

    static var confirmButton: UIButton {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.K2DBold(16)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.backgroundColor = .appColor(.green)
        
        return button
    }

    static var cancelButton: UIButton {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.K2DBold(16)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .appColor(.lightGray)
        button.setTitle("alert.cancel".localized, for: .normal)
        
        return button
    }
}
