//
//  AlertPopupView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 21/09/2021.
//

import UIKit

public enum AlertStyle {
    case cancelMembership
    case removeCard
    case changePaymentMethod
    case addCard
}

protocol AlertDelegate: AnyObject {
    func alertDidConfirm(_ vc: AlertPopupView, for type: AlertStyle)
    func alertDidDismiss(_ vc: AlertPopupView)
}

class AlertPopupView: UIViewController {
    private let backgroundTransparency: CGFloat = 0.4
    private var contentView = Subviews.alertView

    var type: AlertStyle!
    weak var delegate: AlertDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupContent()
        animateAlert()
    }

    fileprivate func setupContent() {
        view.backgroundColor = UIColor.black.withAlphaComponent(backgroundTransparency)
        contentView.type = type

        contentView.dismissCallback = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.delegate?.alertDidDismiss(strongSelf)
        }

        contentView.confirmCallback = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.delegate?.alertDidConfirm(strongSelf, for: strongSelf.type)
        }

        view.addSubview(contentView)

        NSLayoutConstraint.activate([
            contentView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40.0),
            contentView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40.0),
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    private func animateAlert() {
        view.alpha = 0.0
        contentView.alpha = 0.0
        UIView.animate(withDuration: 0.1, animations: {
            self.view.alpha = 1.0
            self.contentView.alpha = 1.0
        })
    }
}

private struct Subviews {
    static var alertView: AlertContentView {
        let view = AlertContentView(frame: .zero)
        view.backgroundColor = .white
        view.layer.cornerRadius = 12.0
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
}
