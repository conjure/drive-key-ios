//
//  TripStatsView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 11/08/2022.
//

import UIKit

class TripStatsView: BaseView {
    private let scoreBackgroundView = Subviews.scoreBackgroundView
    private let scoreLabel = Subviews.scoreLabel
    private let platesLabel = Subviews.platesLabel
    private let milesLabel = Subviews.milesLabel
    private let separatorView = Subviews.separatorView
    private let co2Label = Subviews.co2Label
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .clear
        
        addSubview(scoreBackgroundView)
        addSubview(scoreLabel)
        addSubview(platesLabel)
        addSubview(milesLabel)
        addSubview(separatorView)
        addSubview(co2Label)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            scoreBackgroundView.topAnchor.constraint(equalTo: topAnchor),
            scoreBackgroundView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scoreBackgroundView.heightAnchor.constraint(equalToConstant: 48),
            scoreBackgroundView.widthAnchor.constraint(equalToConstant: 48),
            scoreBackgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),
            scoreLabel.topAnchor.constraint(equalTo: scoreBackgroundView.topAnchor),
            scoreLabel.leadingAnchor.constraint(equalTo: scoreBackgroundView.leadingAnchor),
            scoreLabel.trailingAnchor.constraint(equalTo: scoreBackgroundView.trailingAnchor),
            scoreLabel.bottomAnchor.constraint(equalTo: scoreBackgroundView.bottomAnchor),
            platesLabel.topAnchor.constraint(equalTo: scoreBackgroundView.topAnchor, constant: 1),
            platesLabel.leadingAnchor.constraint(equalTo: scoreBackgroundView.trailingAnchor, constant: 10),
            platesLabel.heightAnchor.constraint(equalToConstant: 20),
            platesLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            milesLabel.topAnchor.constraint(equalTo: platesLabel.bottomAnchor),
            milesLabel.leadingAnchor.constraint(equalTo: scoreBackgroundView.trailingAnchor, constant: 10),
            milesLabel.heightAnchor.constraint(equalToConstant: 25),
            separatorView.topAnchor.constraint(equalTo: platesLabel.bottomAnchor, constant: 7),
            separatorView.heightAnchor.constraint(equalToConstant: 12),
            separatorView.widthAnchor.constraint(equalToConstant: 2),
            separatorView.leadingAnchor.constraint(equalTo: milesLabel.trailingAnchor, constant: 9),
            co2Label.topAnchor.constraint(equalTo: platesLabel.bottomAnchor),
            co2Label.leadingAnchor.constraint(equalTo: milesLabel.trailingAnchor, constant: 18),
            co2Label.heightAnchor.constraint(equalToConstant: 25)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    func update(_ data: Trip?, unit: UnitDistance) {
        guard let data = data else { return }
        platesLabel.text = data.licensePlateValue
        score = data.ecoDriverScore
        co2 = data.co2Value
        guard let distance = data.distance(unit) else { return }
        guard let distanceStr = NumberFormatter.distance.string(from: NSNumber(value: distance)) else { return }
        let distanceUnitStr = !distanceStr.isEmpty ? distanceStr + " " + unit.localizedShort.capitalizingFirstLetter() : ""
        miles = distanceUnitStr
    }
    
    var scoreBackgroundColor: UIColor = UIColor.appColor(.navyBlue) {
        didSet {
            scoreBackgroundView.backgroundColor = scoreBackgroundColor
        }
    }
    
    var score: Double? {
        didSet {
            guard let score = score, score > 0 else {
                scoreLabel.attributedText = nil
                scoreLabel.text = "trip.details.stats.no.score".localized
                return
            }
            
            let scoreInt = Int(score * 100)
            let suffixStr = "trip.details.stats.score.bottom".localized
            let scoreStr = "\(scoreInt)"
            let descriptionStr = scoreStr + "\n" + suffixStr
            let fontTop = UIFont.K2DSemiBold(16) ?? UIFont.systemFont(ofSize: 16, weight: .semibold)
            let fontBottom = UIFont.K2DSemiBold(8) ?? UIFont.systemFont(ofSize: 8, weight: .semibold)
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 0.83
            paragraphStyle.alignment = .center
            
            let rangeParagraph = NSRange(location: 0, length: descriptionStr.count)
            let rangeTop = descriptionStr.nsRange(of: scoreStr)
            let rangeBottom = descriptionStr.nsRange(of: suffixStr)
            
            let descriptionAttStr = NSMutableAttributedString(string: descriptionStr)
            descriptionAttStr.addAttribute(.font, value: fontTop, range: rangeTop)
            descriptionAttStr.addAttribute(.font, value: fontBottom, range: rangeBottom)
            descriptionAttStr.addAttribute(.paragraphStyle, value: paragraphStyle, range: rangeParagraph)
            
            scoreLabel.attributedText = descriptionAttStr
        }
    }

    var plates: String? {
        didSet {
            guard let plates = plates else {
                platesLabel.text = nil
                return
            }
            
            platesLabel.text = plates
        }
    }
    
    var miles: String? {
        didSet {
            guard let miles = miles else {
                milesLabel.text = nil
                return
            }
            
            milesLabel.text = miles
        }
    }
    
    var co2: Double? {
        didSet {
            guard let co2 = co2 else {
                co2Label.attributedText = nil
                return
            }
            
            let descriptionStr = "\(co2.formattedCO2)" + "trip.details.stats.kg.co2".localized
            
            let fontSubscript = UIFont.K2DBold(10) ?? UIFont.systemFont(ofSize: 10, weight: .bold)
            let range2 = NSRange(location: descriptionStr.count - 1, length: 1)
            
            let descriptionAttStr = NSMutableAttributedString(string: descriptionStr)
            descriptionAttStr.addAttribute(.font, value: fontSubscript, range: range2)
            
            co2Label.attributedText = descriptionAttStr
        }
    }
}

private struct Subviews {
    static var scoreBackgroundView: UIView {
        let view = UIView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.backgroundColor = UIColor.appColor(.navyBlue)
        
        return view
    }
    
    static var scoreLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DSemiBold(14)
        label.numberOfLines = 2
        label.textColor = .white
        label.textAlignment = .center
        
        return label
    }
    
    static var platesLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DBold(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        
        return label
    }
    
    static var milesLabel: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }
    
    static var separatorView: UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .appColor(.lightGray)
        
        return view
    }
    
    static var co2Label: UILabel {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.K2DRegular(14)
        label.numberOfLines = 1
        label.textColor = .white
        label.textAlignment = .left
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }
}
