//
//  EstimatedTotalView.swift
//  DriveKey
//
//  Created by Ade Adegoke on 15/08/2022.
//

import UIKit

class EstimatedTotalView: XibView {

    @IBOutlet private var estimateTotalLabel: UILabel!

    override func setup() {
        super.setup()
        layer.borderWidth = 1
        layer.borderColor = UIColor.appColor(.green).cgColor
        layer.cornerRadius = 12
        estimateTotalLabel.font = UIFont.K2DBold(14)
        estimateTotalLabel.backgroundColor = .clear
        estimateTotalLabel.textColor = UIColor.appColor(.green)
        backgroundColor = .clear
    }

    func updateView(with price: Double?, currency: String?) {
        guard let currency = currency else { return }
        let totalPrice = price ?? 0.01
        
        estimateTotalLabel.text = totalPrice.formatForCurrency(currency)
    }
}
