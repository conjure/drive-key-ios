//
//  TripMapView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 10/10/2022.
//

import UIKit
import MapKit
import GoogleMaps
import Polyline

class TripMapView: BaseView {
    private let mapView = Subviews.mapView
    private var polyline: GMSPolyline?
    private var polylineMotion: GMSPolyline?
        
    override func setUpSubviews() {
        super.setUpSubviews()

        backgroundColor = .clear
        addSubview(mapView)
    }

    override func setUpLayout() {
        super.setUpLayout()

        let constraints = [
            mapView.topAnchor.constraint(equalTo: topAnchor),
            mapView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    var style: String? {
        didSet {
            guard let style = style else { return }
            mapView.setStyle(style)
        }
    }
    
    func updateLocation(_ locations: [CLLocationCoordinate2D]) {
        clearMap()
        guard locations.count > 1 else { return }
        addTripMarkers(locations.first!, end: locations.last!)
        
        guard locations.count > 2 else {
            mapView.showAllMarkers(40)
            return
        }
        
        let path = GMSMutablePath()
        locations.forEach({ path.add($0) })
        
        polylineMotion = GMSPolyline(path: path)
        polylineMotion?.strokeWidth = 2
        polylineMotion?.strokeColor = .appColor(.green)
        polylineMotion?.zIndex = 0
        polylineMotion?.map = mapView.map
        mapView.showAllMarkers(40)
    }
    
    private func addTripMarkers(_ start: CLLocationCoordinate2D, end: CLLocationCoordinate2D) {
        let markerStart = GMSMarker()
        markerStart.position = start
        markerStart.icon = UIImage(named: "map-start-icon")
        mapView.addMarker(markerStart)
        
        let markerStop = GMSMarker()
        markerStop.position = end
        markerStop.icon = UIImage(named: "map-end-icon")
        mapView.addMarker(markerStop)
    }
    
    private func clearMap() {
        polyline?.map = nil
        polyline = nil
        polylineMotion?.map = nil
        polylineMotion = nil
        mapView.clear()
    }
}

private struct Subviews {
    static var mapView: MapView {
        let view = MapView(frame: CGRect.zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        view.isUserInteractionEnabled = false
        
        return view
    }
}
