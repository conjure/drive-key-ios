//
//  TripToolbarView.swift
//  DriveKey
//
//  Created by Piotr Wilk on 28/10/2022.
//

import UIKit

protocol TripToolbarViewDelegate: AnyObject {
    func tripToolbarViewDidTapRemove(_ toolbarView: TripToolbarView)
    func tripToolbarViewDidTapVehicle(_ toolbarView: TripToolbarView)
    func tripToolbarViewDidTapBusiness(_ toolbarView: TripToolbarView)
}

class TripToolbarView: BaseView {
    private let removeButton = Subviews.removeButton
    private let vehicleButton = Subviews.vehicleButton
    private let businessButton = Subviews.businessButton
    
    weak var delegate: TripToolbarViewDelegate?
    
    override func setUpSubviews() {
        super.setUpSubviews()
        
        backgroundColor = .appColor(.notificationBlue)
        layer.cornerRadius = 12
        layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        addSubview(removeButton)
        addSubview(vehicleButton)
        addSubview(businessButton)
        
        removeButton.addTarget(self, action: #selector(onRemove), for: .touchUpInside)
        vehicleButton.addTarget(self, action: #selector(onVehicle), for: .touchUpInside)
        businessButton.addTarget(self, action: #selector(onBusiness), for: .touchUpInside)
    }
    
    override func setUpLayout() {
        super.setUpLayout()
        
        let constraint = [
            removeButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 6),
            removeButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            removeButton.widthAnchor.constraint(equalToConstant: 60),
            removeButton.heightAnchor.constraint(equalToConstant: 40),
            vehicleButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            vehicleButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            vehicleButton.widthAnchor.constraint(equalToConstant: 60),
            vehicleButton.heightAnchor.constraint(equalToConstant: 40),
            businessButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -6),
            businessButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            businessButton.widthAnchor.constraint(equalToConstant: 60),
            businessButton.heightAnchor.constraint(equalToConstant: 40)
        ]
        NSLayoutConstraint.activate(constraint)
    }
    
    var businessHighlighted: Bool? {
        didSet {
            guard let businessHighlighted = businessHighlighted else { return }
            businessButton.isSelected = businessHighlighted
        }
    }
    
    @objc
    private func onRemove() {
        delegate?.tripToolbarViewDidTapRemove(self)
    }
    
    @objc
    private func onVehicle() {
        delegate?.tripToolbarViewDidTapVehicle(self)
    }
    
    @objc
    private func onBusiness() {
        delegate?.tripToolbarViewDidTapBusiness(self)
    }
}

private struct Subviews {
    static var removeButton: UIButton {
        let button = UIButton(frame: CGRect.zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "trip-bin-icon"), for: .normal)
        button.setImage(UIImage(named: "trip-bin-icon-selected"), for: .highlighted)
        button.setImage(UIImage(named: "trip-bin-icon-selected"), for: .selected)
        button.contentMode = .center
        
        return button
    }
    
    static var vehicleButton: UIButton {
        let button = UIButton(frame: CGRect.zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "trip-car-icon"), for: .normal)
        button.setImage(UIImage(named: "trip-car-icon-selected"), for: .highlighted)
        button.setImage(UIImage(named: "trip-car-icon-selected"), for: .selected)
        button.contentMode = .center
        
        return button
    }
    
    static var businessButton: UIButton {
        let button = UIButton(frame: CGRect.zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "trip-suitcase-icon"), for: .normal)
        button.setImage(UIImage(named: "trip-suitcase-icon-selected"), for: .highlighted)
        button.setImage(UIImage(named: "trip-suitcase-icon-selected"), for: .selected)
        button.contentMode = .center
        
        return button
    }
}
