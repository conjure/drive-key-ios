//
//  UnknownRepresentable.swift
//  DriveKey
//
//  Created by Piotr Wilk on 09/08/2021.
//

import Foundation

protocol UnknownRepresentable: RawRepresentable, CaseIterable where RawValue: Equatable {
    static var unknown: Self { get }
}

extension UnknownRepresentable {
    init(rawValue: RawValue) {
        let value = Self.allCases.first(where: { $0.rawValue == rawValue })
        self = value ?? Self.unknown
    }
}
