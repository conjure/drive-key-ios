//
//  EmailAddressValidator.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/08/2021.
//

import Foundation

class EmailAddressValidator: TextValidatable {
    var validationClosure: TextValidationClosure

    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }

    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let email = text else {
                return true
            }

            let rawEmail = email.trimmingCharacters(
                in: .whitespaces
            )

            var isValidEmail: Bool {
                NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: rawEmail)
            }

            return isValidEmail
        })
    }
}
