//
//  PostCodeValidator.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 08/12/2021.
//

import Foundation

class PostCodeValidator: TextValidatable {
    var validationClosure: TextValidationClosure
    
    init(validationClosure: @escaping TextValidationClosure) {
        self.validationClosure = validationClosure
    }
    
    convenience init() {
        self.init(validationClosure: { text -> Bool in
            guard let postcode = text else {
                return false
            }
            
            // UK + USA
            let pattern = "(^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$)|(^[0-9]{5}(?:-[0-9]{4})?$)"
            let range = NSRange(location: 0, length: postcode.utf16.count)
            
            do {
                let regex = try NSRegularExpression(pattern: pattern)
                return regex.firstMatch(in: postcode, options: [], range: range) != nil
            } catch {
                print("Error validating postcode - \(error)")
                return false
            }
        })
    }
}
