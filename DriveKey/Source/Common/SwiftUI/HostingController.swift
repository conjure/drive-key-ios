//
//  HostingController.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 19/01/2022.
//

import SwiftUI

class HostingController: UIHostingController<AnyView> {
    private var hideNavigationBar: Bool = false
    private var prefersLargeTitles: Bool = false
    var statusBarStyle = UIStatusBarStyle.default

    // UIKit seems to observe changes on this, perhaps with KVO?
    // In any case, I found changing `statusBarStyle` was sufficient
    // and no other method calls were needed to force the status bar to update
    override var preferredStatusBarStyle: UIStatusBarStyle {
        statusBarStyle
    }

    init<T: View>(wrappedView: T, hideNavigationBar: Bool = false, prefersLargeTitles: Bool = true) {
        // This observer is necessary to break a dependency cycle - without it
        // onPreferenceChange would need to use self but self can't be used until
        // super.init is called, which can't be done until after onPreferenceChange is set up etc.
        let observer = Observer()

        let observedView = AnyView(wrappedView.onPreferenceChange(StatusBarStyleKey.self) { style in
            observer.value?.statusBarStyle = style
        })

        super.init(rootView: observedView)
        observer.value = self
        self.hideNavigationBar = hideNavigationBar
        self.prefersLargeTitles = prefersLargeTitles
    }

    private class Observer {
        weak var value: HostingController?
        init() {}
    }

    @available(*, unavailable) required init?(coder aDecoder: NSCoder) {
        // We aren't using storyboards, so this is unnecessary
        fatalError("Unavailable")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let backBarButtonItem = UIBarButtonItem(title: nil, style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if hideNavigationBar {
            navigationController?.setNavigationBarHidden(true, animated: false)
        } else {
            navigationController?.navigationBar.prefersLargeTitles = prefersLargeTitles
        }
    }
}
