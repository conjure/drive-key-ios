//
//  ProjectPickerDelegate.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/02/2022.
//

import Combine

class ProjectPickerDelegate: ObservableObject {
    var willChange = PassthroughSubject<ProjectPickerDelegate, Never>()
    var didChange = PassthroughSubject<ProjectPickerDelegate, Never>()

    var project: CharityProject? {
        willSet {
            willChange.send(self)
        }

        didSet {
            didChange.send(self)
        }
    }
}
