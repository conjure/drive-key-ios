//
//  ProjectTileView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/02/2022.
//

// swiftlint:disable weak_delegate

import SwiftUI

struct ProjectTileView: View {
    var project: CharityProject
    @ObservedObject var delegate: ProjectPickerDelegate
    
    var body: some View {
        VStack(spacing: 5) {
            Text(project.name)
                .foregroundColor(.white)
                .font(.K2DBold(16))
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 16)

            Text(project.location)
                .foregroundColor(.white)
                .font(.K2DSemiBold(14))
                .frame(maxWidth: .infinity, alignment: .leading)

            Spacer()
        }
        .padding(.leading, 17)
        .padding(.trailing, 10)
        .background(
            Image(project.imageName)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 245, height: 160)
                .overlay(GradientOverlayView(colors: [
                    Color(red: 0.124, green: 0.243, blue: 0.396, opacity: 1),
                    Color(red: 0.093, green: 0.195, blue: 0.326, opacity: 0),
                    Color(red: 0.059, green: 0.141, blue: 0.247, opacity: 0.47)
                  ]))
        )
        .cornerRadius(12)
        .contentShape(Rectangle())
        .onTapGesture {
            self.delegate.project = project
        }
    }
}

struct ProjectTileView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectTileView(project: .brazilianAmazon, delegate: ProjectPickerDelegate())
            .frame(width: 245, height: 160)

        ProjectTileView(project: .talas, delegate: ProjectPickerDelegate())
            .frame(width: 245, height: 160)
        
        ProjectTileView(project: .midilli, delegate: ProjectPickerDelegate())
            .frame(width: 245, height: 160)
    }
}
