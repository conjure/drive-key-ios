//
//  ProjectCarouselView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/02/2022.
//

// swiftlint:disable weak_delegate

import SwiftUI

class ProjectCarouselViewModel: ObservableObject {
    @Published var charity: CharityProject?

    init(charityProject: CharityProject?) {
        self.charity = charityProject
    }
}

struct ProjectCarouselView: View {
    private var projects: [CharityProject] {
        var charities: [CharityProject] = []
        for proj in CharityProject.allCases {
            if proj.name == model.charity?.name {
                continue
            } else {
                charities.append(proj)
            }
        }
        return charities
    }

    var heading: String

    @ObservedObject var model: ProjectCarouselViewModel
    @ObservedObject var delegate: ProjectPickerDelegate

    init(heading: String,
         model: ProjectCarouselViewModel,
         delegate: ProjectPickerDelegate) {
        self.heading = heading
        self.model = model
        self.delegate = delegate
    }

    var body: some View {
        VStack(spacing: 16) {
            if !heading.isEmpty {
                Text(heading)
                    .foregroundColor(.white)
                    .font(.K2DBold(16))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 16)
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 16) {
                    ForEach(projects, id: \.self) { project in
                        ProjectTileView(project: project, delegate: delegate)
                            .frame(width: 245, height: 160)
                    }
                }
                .padding(.horizontal, 24)
            }
            .frame(width: UIScreen.main.bounds.width, height: 160)
            Spacer()
        }
        .statusBar(style: .lightContent)
    }
}

struct ProjectCarouselView_Previews: PreviewProvider {
    static var delegate = ProjectPickerDelegate()

    static var previews: some View {
        return GeometryReader { geo in
            ProjectCarouselView(heading: "projects.carousel.heading.dashboard".localized, model: .init(charityProject: nil), delegate: delegate)
                .frame(width: geo.size.width, height: 248)
                .background(Color.appColor(.pastelDarkBlue))
        }
    }
}
