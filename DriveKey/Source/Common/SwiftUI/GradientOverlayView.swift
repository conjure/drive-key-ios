//
//  GradientOverlayView.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/02/2022.
//

import SwiftUI

struct GradientOverlayView: View {
    var colors: [Color]
    var body: some View {
        ZStack {
            Text(" ")
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(
            LinearGradient(gradient: Gradient(colors: colors), startPoint: .top, endPoint: .bottom)
                .edgesIgnoringSafeArea(.all)
        )
    }
}
