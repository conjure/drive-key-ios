//
//  AppDependencies.swift
//  DriveKeyMVP
//
//  Created by Dinesh Vijaykumar on 01/06/2021.
//

import Foundation

class AppDependencies {
    let api: APIProtocol
    let authService: AuthService
    var dynamicLinkService: DynamicLinkService
    var biometricService: BiometricService
    let userService: UserProvidable
    let bubbleService: BubbleProvidable
    let permissionService: PermissionService
    let appDataService: AppDataProvidable
    let tripsMonitor: TripsMonitor
    
    init(api: APIProtocol, authService: AuthService, dynamicLinkService: DynamicLinkService, biometricService: BiometricService, userService: UserProvidable, bubbleService: BubbleProvidable, permissionService: PermissionService, appDataService: AppDataProvidable, tripsMonitor: TripsMonitor) {
        self.api = api
        self.authService = authService
        self.dynamicLinkService = dynamicLinkService
        self.biometricService = biometricService
        self.userService = userService
        self.bubbleService = bubbleService
        self.permissionService = permissionService
        self.appDataService = appDataService
        self.tripsMonitor = tripsMonitor
    }
}
