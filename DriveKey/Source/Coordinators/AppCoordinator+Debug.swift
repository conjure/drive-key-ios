//
//  AppCoordinator+Debug.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 01/11/2021.
//

import UIKit
import Firebase

// MARK: - Debug
extension AppCoordinator {
    func presentDashboardDebug() {
        let dashboardDebugViewController: DashboardDebugViewController = UIStoryboard.instantiateViewController()
        dashboardDebugViewController.delegate = self
        dashboardDebugViewController.api = dependencies.api
        dashboardDebugViewController.location = dependencies.tripsMonitor.locationProvider
        dashboardDebugViewController.motion = dependencies.tripsMonitor.motionProvider
        dashboardDebugViewController.user = dependencies.userService
        
        let modalNavigationController = UINavigationController(rootViewController: dashboardDebugViewController)
        
        navigationController.present(modalNavigationController, animated: true, completion: nil)
    }

    func debugShowVehicleList() {
        let user = dependencies.userService.currentUser
        presentVehicleList(user)
    }
    
    func debugShowVehicleDetails() {
        let data = dependencies.userService.currentUser?.activeVehicle
        presentVehicleDetails(data, activeVehicles: nil)
    }

    func debugShowVehicleAdd(_: Bool) {
        presentVehicleAdd(from: .onboarding)
    }

    func debugShowVehicleLookup(isBubble _: Bool, isProfile _: Bool) {
        presentVehicleLookup(from: .onboarding, country: Country.other, state: nil)
    }

    func debugShowVehicleEnterEmissions(_: Bool) {
        presentVehicleEnterEmissions(from: .onboarding, vehicleData: nil, vrm: nil)
    }

    func debugShowPersonalData() {
        let userData = dependencies.userService.currentUser
        presentPersonalData(userData)
    }

    func debugShowAddPayment() {
        presentProfile()
        runOnMainThreadAsyncAfter(0.5) {
            self.presentAddNewPaymentMethod(previousMethod: nil)
        }
    }

    func debugShowPaymentMethods() {
        let user = dependencies.userService.currentUser
        presentPaymentMethods(user)
    }
    
    func debugShowTripHistory() {
        presentTripHistory()
    }
    
    func debugShowShare(_ item: URL) {
        let activityViewController = UIActivityViewController(activityItems: [item], applicationActivities: nil)
        activityViewController.completionWithItemsHandler = {(_, _, _, _) in
            try? FileManager.default.removeItem(at: item)
        }
        
        let modalNavigationController = UINavigationController(rootViewController: activityViewController)
        modalNavigationController.isNavigationBarHidden = false
        modalNavigationController.modalPresentationStyle = .formSheet
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.present(modalNavigationController, animated: true)
        } else {
            navigationController.present(modalNavigationController, animated: true)
        }
    }
}

extension AppCoordinator: DashboardDebugViewControllerDelegate {
    func dashboardDebugViewController(_ viewController: DashboardDebugViewController, didTapShare item: URL) {
        runOnMainThreadAsyncAfter(0.3) {
            self.debugShowShare(item)
        }
    }
}
