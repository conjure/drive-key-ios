//
//  AppCoordinator.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//
import UIKit
import Combine
import Pulley

class AppCoordinator: Coordinator {
    var parent: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var dependencies: AppDependencies
    private var tasks = Set<AnyCancellable>()
    var pendingDeepLink: DeepLink?
    
    required init(_ navigationController: UINavigationController, dependencies: AppDependencies?) {
        self.navigationController = navigationController
        self.dependencies = dependencies!
        
        // dynamic links
        dependencies?.dynamicLinkService.linkActionSubject
            .sink(receiveValue: { [weak self] event in
                switch event {
                case let .resetPassword(code):
                    self?.presentResetPassword(with: code)
                }
            })
            .store(in: &tasks)
    }
    
    func start() {
        if dependencies.authService.isUserLoggedIn {
            presentLoggedInUser()
        } else {
            presentWalkthrough()
        }
    }
    
    private func presentWalkthrough() {
        let walkthrough: CarouselViewController = UIStoryboard.instantiateViewController()
        walkthrough.delegate = self
        navigationController.pushViewController(walkthrough, animated: true)
    }
    
    func presentLoginRoute() {
        let viewController: LoginRouteViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = LoginRouteViewModel(authService: dependencies.authService, userService: dependencies.userService, api: dependencies.api)
        viewController.delegate = self
        
        navigationController.setViewControllers([viewController], animated: true)
    }
    
    private func presentSignup() {
        let viewController: SignupViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = SignupViewModel(authService: dependencies.authService, userService: dependencies.userService)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentLogin() {
        let viewController: LoginViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = LoginViewModel(authService: dependencies.authService, userService: dependencies.userService, api: dependencies.api)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func presentHTMLContent(type: HTMLContentType, modally: Bool) {
        let viewController: HTMLViewController = UIStoryboard.instantiateViewController()
        viewController.contentType = type
        
        var root: UINavigationController
        if let nav = navigationController.presentedViewController as? UINavigationController {
            root = nav
        } else {
            root = navigationController
        }
        
        if modally {
            let nav = NavigationController(rootViewController: viewController)
            nav.modalPresentationStyle = .fullScreen
            root.present(nav, animated: true, completion: nil)
        } else {
            root.pushViewController(viewController, animated: true)
        }
    }
    
    private func presentForgotPassword() {
        let viewController: ForgotPasswordViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = ForgotPasswordViewModel(authService: dependencies.authService)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentEmailConfirmation(emailType: EmailType) {
        let viewController: EmailConfirmationViewController = UIStoryboard.instantiateViewController()
        viewController.emailType = emailType
        viewController.delegate = self
        
        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    func presentResetPassword(with oobCode: String) {
        let viewController: ResetPasswordViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = ResetPasswordViewModel(authService: dependencies.authService, oobCode: oobCode)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentAddCurrency() {
        let viewController: AddCurrencyViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = AddCurrencyViewModel(dependencies.api,
                                                        userService: dependencies.userService,
                                                        appService: dependencies.appDataService)
        viewController.delegate = self
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentBiometricSetup(fromBubble: Bool = false) {
        if dependencies.biometricService.canEvaluate() {
            let viewController: BiometricViewController = UIStoryboard.instantiateViewController()
            viewController.viewModel = BiometricViewModel(biometricService: dependencies.biometricService, fromBubble: fromBubble)
            viewController.delegate = self
            
            if fromBubble {
                viewController.modalPresentationStyle = .fullScreen
                navigationController.present(viewController, animated: true, completion: nil)
            } else {
                navigationController.pushViewController(viewController, animated: true)
            }
        } else {
            let countries = dependencies.appDataService.countries
            presentVehicleCountry(from: .onboarding, countries: countries)
        }
    }
    
    private func presentBiometricLogin() {
        let viewController: BiometricLoginViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = BiometricLoginViewModel(biometricService: dependencies.biometricService)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentBiometricOrDashboard() {
        if UserDefaults.standard.integer(forKey: UserDefaultKeys.biometricStatus) == BiometricStatus.unknown.rawValue {
            presentBiometricSetup()
        } else {
            presentLoggedInUser()
        }
    }
    
    private func presentLoggedInUser() {        
        let grantedBiometricPermission = (UserDefaults.standard.integer(forKey: UserDefaultKeys.biometricStatus) == BiometricStatus.granted.rawValue) && dependencies.biometricService.canEvaluate()
        if let user = dependencies.userService.currentUser, !user.isCurrency {
            presentAddCurrency()
        } else if grantedBiometricPermission {
            presentBiometricLogin()
        } else {
            presentDashboard()
        }
    }
    
    func presentCurrencyOrPermissionsOrDashboard() {
        if let user = dependencies.userService.currentUser, !user.isCurrency {
            presentAddCurrency()
        } else if dependencies.permissionService.shouldShowPermissions {
            presentPermissions()
        } else {
            presentDashboard()
        }
    }
    
    private func presentPermissions() {
        let viewController: PermissionsViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = PermissionsViewModel(permissionService: dependencies.permissionService)
        viewController.delegate = self
        navigationController.pushViewController(viewController, animated: true)
    }
    
    private func presentDashboard() {
        let viewController: DashboardViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = DashboardViewModel(userService: dependencies.userService,
                                                      notificationService: dependencies.permissionService.notificationService as! NotificationService,
                                                      api: dependencies.api,
                                                      bubbleProvider: dependencies.bubbleService, tripsMonitor: dependencies.tripsMonitor)
        viewController.carbonStateProvider = DashboardCarbonStateProvider()
        viewController.delegate = self
        
        let drawerContentVC: DashboardDrawerViewController = UIStoryboard.instantiateViewController()
        drawerContentVC.viewModel = DashboardDrawerViewModel(bubbleService: dependencies.bubbleService, userService: dependencies.userService)
        drawerContentVC.delegate = self
        
        let pulleyController = PulleyViewController(contentViewController: viewController, drawerViewController: drawerContentVC)
        pulleyController.initialDrawerPosition = .closed
        pulleyController.animationDuration = 0.5
        pulleyController.drawerCornerRadius = 22.0
        pulleyController.navigationItem.backButtonTitle = ""
        navigationController.setViewControllers([pulleyController], animated: true)
    }
    
    func presentPersonalData(_ data: DrivekeyUser?) {
        let personalDataViewController: PersonalDataViewController = UIStoryboard.instantiateViewController()
        personalDataViewController.viewModel = PersonalDataViewModel(userData: data, countries: dependencies.appDataService.countries)
        personalDataViewController.api = dependencies.api
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(personalDataViewController, animated: true)
        } else {
            navigationController.pushViewController(personalDataViewController, animated: true)
        }
    }
    
    func presentBusinessInfo() {
        let businessInfoViewController: BusinessInfoViewController = UIStoryboard.instantiateViewController()
        businessInfoViewController.viewModel = BusinessInfoViewModel(dependencies.api, userService: dependencies.userService)
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(businessInfoViewController, animated: true)
        } else {
            navigationController.pushViewController(businessInfoViewController, animated: true)
        }
    }
    
    func presentUnits() {
        let unitsViewController: UnitsViewController = UIStoryboard.instantiateViewController()
        unitsViewController.viewModel = UnitsViewModel(dependencies.userService)
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(unitsViewController, animated: true)
        } else {
            navigationController.pushViewController(unitsViewController, animated: true)
        }
    }
    
    func presentErrorConfirmation(_ confirmationType: ErrorConfirmationType) {
        let viewController: ErrorViewController = UIStoryboard.instantiateViewController()
        viewController.delegate = self
        viewController.confirmationType = confirmationType
        
        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
    
    func presentInfo(_ openSection: InfoSection?) {
        let infoViewController: InfoViewController = UIStoryboard.instantiateViewController()
        infoViewController.viewModel = InfoViewModel(section: openSection)
        
        presentModal(infoViewController)
    }
    
    func presentTripHistory(_ filter: TripFilterType? = nil) {
        let tripHistoryViewController: TripHistoryViewController = UIStoryboard.instantiateViewController()
        let tripsFetcher = TripHistoryFetcher(user: dependencies.userService.currentUser)
        tripHistoryViewController.viewModel = TripHistoryViewModel(userService: dependencies.userService,
                                                                   api: dependencies.api,
                                                                   tripsFetcher: tripsFetcher,
                                                                   filter: filter)
        tripHistoryViewController.delegate = self
        
        presentModal(tripHistoryViewController)
    }
    
    func presentTripDetails(_ trip: Trip) {
        let tripDetailsViewController: TripDetailsViewController = UIStoryboard.instantiateViewController()
        tripDetailsViewController.viewModel = TripDetailsViewModel(dependencies.userService,
                                                                   api: dependencies.api,
                                                                   trip: trip)
        tripDetailsViewController.delegate = self
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(tripDetailsViewController, animated: true)
        } else {
            navigationController.pushViewController(tripDetailsViewController, animated: true)
        }
    }
    
    func showPopupAlert(_ type: AlertStyle, from: AlertDelegate) {
        let screen = AlertPopupView()
        screen.type = type
        screen.modalPresentationStyle = .overCurrentContext
        screen.modalTransitionStyle = .crossDissolve
        screen.delegate = from
        
        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.present(screen, animated: true, completion: nil)
        } else {
            navigationController.present(screen, animated: true, completion: nil)
        }
    }
    
    func presentModal(_ viewController: UIViewController) {
        let modalNavigation = NavigationController(rootViewController: viewController)
        modalNavigation.modalPresentationStyle = .fullScreen
        navigationController.present(modalNavigation, animated: true, completion: nil)
    }
}

// MARK: - CarouselViewControllerDelegate

extension AppCoordinator: CarouselViewControllerDelegate {
    func walkthroughViewControllerDidFinish(_: CarouselViewController) {
        presentLoginRoute()
    }
}

// MARK: - LoginRouteViewControllerDelegate

extension AppCoordinator: LoginRouteViewControllerDelegate {
    func loginRouteWillLogin(_: LoginRouteViewController) {
        presentLogin()
    }
    
    func loginRouteWillSignup(_: LoginRouteViewController) {
        presentSignup()
    }
    
    func loginRouteDidSignupWithApple(_: LoginRouteViewController) {
        presentAddCurrency()
    }
    
    func loginRouteDidSigninWithApple(_: LoginRouteViewController) {
        presentCurrencyOrPermissionsOrDashboard()
    }
}

// MARK: - SignupViewControllerDelegate

extension AppCoordinator: SignupViewControllerDelegate {
    func signupViewControllerDidSignUp(_: SignupViewController) {
        presentAddCurrency()
    }
    
    func signupViewControllerWillShowHTMLContent(_: SignupViewController, content: HTMLContentType) {
        presentHTMLContent(type: content, modally: true)
    }
}

// MARK: - LoginViewControllerDelegate

extension AppCoordinator: LoginViewControllerDelegate {
    func loginViewControllerDidLogin(_: LoginViewController) {
        presentCurrencyOrPermissionsOrDashboard()
    }
    
    func loginViewControllerDidSignupWithApple(_: LoginViewController) {
        presentAddCurrency()
    }
    
    func loginViewControllerDidForgotPassword(_: LoginViewController) {
        presentForgotPassword()
    }
}

// MARK: - BiometricViewControllerDelegate

extension AppCoordinator: BiometricViewControllerDelegate {
    func biometricViewContollerDidActionBubble(_ vc: BiometricViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
    
    func biometricViewControllerWillActionLater(_: BiometricViewController) {
        let countries = dependencies.appDataService.countries
        presentVehicleCountry(from: .onboarding, countries: countries)
    }
    
    func biometricViewControllerDidEnable(_: BiometricViewController) {
        let countries = dependencies.appDataService.countries
        presentVehicleCountry(from: .onboarding, countries: countries)
    }
}

// MARK: - BiometricLoginViewControllerDelegate

extension AppCoordinator: BiometricLoginViewControllerDelegate {
    func biometricLoginViewControllerWillLoginWithEmail(_: BiometricLoginViewController) {
        pendingDeepLink = nil
        presentLogin()
    }
    
    func biometricLoginViewControllerDidLogin(_: BiometricLoginViewController) {
        presentDashboard()
        if let option = pendingDeepLink {
            handleDeepLink(option)
        }
    }
}

// MARK: - ForgotPasswordViewControllerDelegate

extension AppCoordinator: ForgotPasswordViewControllerDelegate {
    func forgotPasswordViewControllerWillShowConfirmation(_: ForgotPasswordViewController) {
        presentEmailConfirmation(emailType: .resetPassword)
    }
}

// MARK: - EmailConfirmationViewControllerDelegate

extension AppCoordinator: EmailConfirmationViewControllerDelegate {
    func emailConfirmationViewController(_ vc: EmailConfirmationViewController, emailType: EmailType) {
        switch emailType {
        case .resetPassword:
            presentLoginRoute()
        case .emailVerification:
            // navigate to profile
            vc.navigationController?.popToRootViewController(animated: true)
        }
    }
}

// MARK: - ResetPasswordViewControllerDelegate

extension AppCoordinator: ResetPasswordViewControllerDelegate {
    func resetPasswordViewControllerDidResetPassword(_: ResetPasswordViewController) {
        presentLoginRoute()
    }
}

// MARK: - AddCurrencyViewControllerDelegate
extension AppCoordinator: AddCurrencyViewControllerDelegate {
    func addCurrencyViewControllerDidUpdateCurrency(_ viewController: AddCurrencyViewController) {
        presentBiometricOrDashboard()
    }
}

// MARK: - PermissionsViewControllerDelegate

extension AppCoordinator: PermissionsViewControllerDelegate {
    func permissionsViewControllerWillProceed(_: PermissionsViewController) {
        presentDashboard()
    }
}

// MARK: - DashboardViewControllerDelegate

extension AppCoordinator: DashboardViewControllerDelegate {
    func dashboardViewControllerWillPresentProfile(_: DashboardViewController) {
        presentProfile()
    }
    
    func dashboardViewControllerDidDidTapInfo(_ viewController: DashboardViewController) {
        presentInfo(nil)
    }
    
    func dashboardViewControllerDidTripleTap(_: DashboardViewController) {
        presentDashboardDebug()
    }
    
    func dashboardViewControllerWillPresentTrips(_ vc: DashboardViewController) {
        presentTripHistory()
    }
    
    func dashboardViewControllerWillPresent(_ vc: DashboardViewController, info: InfoSection?) {
        presentInfo(info)
    }
    
    func dashboardViewControllerWillPresentPayment(_ vc: DashboardViewController) {
        presentOneTimePayment()
    }
    
    func dashboardViewControllerWillBubble(_ vc: DashboardViewController, bubbleType: BubbleType) {
        self.presentBubble(bubbleType)
    }
    
    private func presentBubble(_ type: BubbleType) {
        switch type {
        case .vehicleReg:
            presentVehicleDetailsFlow(from: .bubble)
        case .biometric:
            presentBiometricSetup(fromBubble: true)
        case .locationPermission:
            let locationService = dependencies.permissionService.locationService as! LocationService
            if !locationService.isLocationServicesEnabled {
                locationService.requestLocation()
            } else if locationService.isPermissionAsked {
                openAppSettings()
            } else {
                dependencies.permissionService.requestPermissionLocation()
            }
        case .expiredCard:
            let user = dependencies.userService.currentUser
            presentPaymentMethods(user)
        }
    }
    
    func dashboardViewControllerDidTap(_ viewController: DashboardViewController, charityProject: CharityProject) {
        presentCharityProject(charityProject, source: .dashboard)
    }
    
    func dashboardViewControllerDidDidTapAddVehicle(_ viewController: DashboardViewController) {
        presentVehicleAdd(from: .bubble)
    }
    
    func dashboardViewControllerDidDidTapMileageSummary(_ viewController: DashboardViewController) {
        presentTripHistory(.business)
    }
}

extension AppCoordinator: DashboardDrawerViewControllerDelegate {
    func dashboardDrawerViewController(_: DashboardDrawerViewController, willPresent bubble: Bubble) {
        guard let type = bubble.bubbleType else { return }
        presentBubble(type)
    }
    
    private func openAppSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { success in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

// MARK: - VerifyEmailViewControllerDelegate

extension AppCoordinator: VerifyEmailViewControllerDelegate {
    func verifyEmailViewControllerDidError(_: VerifyEmailViewController) {
        presentErrorConfirmation(.errEmailVerification)
    }
    
    func verifyEmailViewControllerDidSendEmail(_: VerifyEmailViewController) {
        presentEmailConfirmation(emailType: .emailVerification)
    }
}

// MARK: - ErrorViewControllerDelegate

extension AppCoordinator: ErrorViewControllerDelegate {
    func errorViewController(_ vc: ErrorViewController, didConfirm type: ErrorConfirmationType) {
        switch type {
        case .paymentError:
            vc.navigationController?.popViewController(animated: true)
        default:
            if let nav = navigationController.presentedViewController as? UINavigationController {
                nav.dismiss(animated: true, completion: nil)
            } else {
                navigationController.popToRootViewController(animated: true)
            }
        }
    }
}

// MARK: - TripHistoryViewController

extension AppCoordinator: TripHistoryViewControllerDelegate {
    func tripHistoryViewControllerDidFailLoading(_ viewController: TripHistoryViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func tripHistoryViewControllerDidTapAddVehicle(_ viewController: TripHistoryViewController) {
        presentVehicleAdd(from: .tripHistory)
    }
    
    func tripHistoryViewController(_ viewController: TripHistoryViewController, didSelect trip: Trip) {
        presentTripDetails(trip)
    }
}

extension AppCoordinator: TripDetailsViewControllerDelegate {
    func tripDetailsViewControllerDidTapAddVehicle(_ viewController: TripDetailsViewController) {
        presentVehicleAdd(from: .tripHistory)
    }
    
    func tripDetailsViewControllerDidRemoveTrip(_ viewController: TripDetailsViewController) {
        popToTripHistory()
    }
}

// MARK: - Debug
extension AppCoordinator {
    func debugShowInfo(_ section: InfoSection?) {
        presentInfo(section)
    }
}
