//
//  AppCoordinator+Vehicle.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 01/11/2021.
//

import UIKit

enum VehicleScreenSource {
    case onboarding
    case bubble
    case profile
    case tripHistory
    case tripDetails
    case deepLink
}

extension AppCoordinator {
    func presentVehicleDetailsFlow(from: VehicleScreenSource) {
        if let user = dependencies.userService.currentUser, user.activeVehicle != nil {
            presentVehicleList(user)
        } else {
            presentVehicleAdd(from: from)
        }
    }
    
    func presentVehicleList(_ user: DrivekeyUser?) {
        let vehicleListViewController: VehicleListViewController = UIStoryboard.instantiateViewController()
        vehicleListViewController.viewModel = VehicleListViewModel(user, api: dependencies.api, vehicleFetcher: VehicleFetcher())
        vehicleListViewController.delegate = self
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(vehicleListViewController, animated: true)
        } else {
            navigationController.pushViewController(vehicleListViewController, animated: true)
        }
    }
    
    func popToVehicleListOrLanding(_ isLastActive: Bool) {
        if isLastActive {
            popToVehicleAdd()
        } else {
            popToVehicleList()
        }
    }
    
    func popToVehicleList() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }
        
        if let vehicleListViewController = currentNavigationController?.viewControllers.first(where: { $0 is VehicleListViewController }) {
            currentNavigationController?.popToViewController(vehicleListViewController, animated: true)
        }
    }
    
    func popToVehicleAdd() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }
        
        if currentNavigationController?.viewControllers.first(where: { $0 is ProfileDetailViewController }) != nil {
            var viewControllers = currentNavigationController!.viewControllers
            viewControllers.removeAll(where: { $0 is VehicleListViewController })
            viewControllers.removeAll(where: { $0 is VehicleDetailsViewController })
            
            let vehicleAddViewController: VehicleAddViewController = UIStoryboard.instantiateViewController()
            vehicleAddViewController.viewModel = VehicleAddViewModel(.profile)
            vehicleAddViewController.delegate = self
            viewControllers.append(vehicleAddViewController)
            
            currentNavigationController?.setViewControllers(viewControllers, animated: true)
        }
    }
    
    func presentVehicleDetails(_ selectedVehicle: VehicleData?, activeVehicles: [VehicleData]?) {
        let vehicleDetailsViewController: VehicleDetailsViewController = UIStoryboard.instantiateViewController()
        vehicleDetailsViewController.viewModel = VehicleDetailsViewModel(selectedVehicle,
                                                                         activeVehicles: activeVehicles,
                                                                         countries: dependencies.appDataService.countries,
                                                                         api: dependencies.api)
        vehicleDetailsViewController.delegate = self
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(vehicleDetailsViewController, animated: true)
        } else {
            navigationController.pushViewController(vehicleDetailsViewController, animated: true)
        }
    }
    
    func presentVehicleAdd(from: VehicleScreenSource) {
        let vehicleAddViewController: VehicleAddViewController = UIStoryboard.instantiateViewController()
        vehicleAddViewController.viewModel = VehicleAddViewModel(from)
        vehicleAddViewController.delegate = self
        
        if from == .bubble {
            let presentedNavigationController = NavigationController(rootViewController: vehicleAddViewController)
            presentedNavigationController.modalPresentationStyle = .fullScreen
            navigationController.present(presentedNavigationController, animated: true, completion: nil)
        } else {
            if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
                presentedNavigationController.pushViewController(vehicleAddViewController, animated: true)
            } else {
                navigationController.pushViewController(vehicleAddViewController, animated: true)
            }
        }
    }
    
    func presentVehicleCountry(from: VehicleScreenSource, countries: [Country]) {
        let vehicleCountryViewController: VehicleCountryViewController = UIStoryboard.instantiateViewController()
        vehicleCountryViewController.viewModel = VehicleCountryViewModel(countries, screenSource: from)
        vehicleCountryViewController.delegate = self
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(vehicleCountryViewController, animated: true)
        } else {
            navigationController.pushViewController(vehicleCountryViewController, animated: true)
        }
    }
    
    func presentVehicleLookup(from: VehicleScreenSource, country: Country, state: CountryState?) {
        let vehicleLookupViewController: VehicleLookupViewController = UIStoryboard.instantiateViewController()
        vehicleLookupViewController.viewModel = VehicleLookupViewModel(dependencies.userService,
                                                                       screenSource: from,
                                                                       country: country,
                                                                       state: state)
        vehicleLookupViewController.delegate = self
        vehicleLookupViewController.api = dependencies.api
        vehicleLookupViewController.authSerice = dependencies.authService
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(vehicleLookupViewController, animated: true)
        } else {
            navigationController.pushViewController(vehicleLookupViewController, animated: true)
        }
    }
    
    func presentVehicleEnterEmissions(from: VehicleScreenSource, vehicleData: VehicleData?, vrm: String?, country: Country? = nil, state: CountryState? = nil) {
        let vehicleEnterEmissionsViewController: VehicleEnterEmissionsViewController = UIStoryboard.instantiateViewController()
        vehicleEnterEmissionsViewController.viewModel = VehicleEnterEmissionsViewModel(dependencies.userService,
                                                                                       screenSource: from,
                                                                                       vehicleData: vehicleData,
                                                                                       vrm: vrm,
                                                                                       country: country,
                                                                                       state: state)
        vehicleEnterEmissionsViewController.delegate = self
        vehicleEnterEmissionsViewController.api = dependencies.api
        
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(vehicleEnterEmissionsViewController, animated: true)
        } else {
            navigationController.pushViewController(vehicleEnterEmissionsViewController, animated: true)
        }
    }
    
    func updateVehicleLookup(_ vehicleData: VehicleData) {
        if let vehicleLookupViewController = navigationController.viewControllers.first(where: { $0 is VehicleLookupViewController }) as? VehicleLookupViewController {
            vehicleLookupViewController.update(vehicleData)
        }
    }
    
    func presentVehicleLookupEdit(_ vehicleData: VehicleData) {
        let vehicleLookupEditViewController: VehicleLookupEditViewController = UIStoryboard.instantiateViewController()
        vehicleLookupEditViewController.viewModel = VehicleLookupEditViewModel(vehicleData: vehicleData)
        vehicleLookupEditViewController.delegate = self
        navigationController.pushViewController(vehicleLookupEditViewController, animated: true)
    }
    
    func presentEVVehicleConfirmation(ableToSkip: Bool) {
        let viewController: EVConfirmationViewController = UIStoryboard.instantiateViewController()
        viewController.shouldAllowSkip = ableToSkip
        viewController.delegate = self
        let navController = NavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .fullScreen
        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.present(navController, animated: true)
        } else {
            navigationController.present(navController, animated: true)
        }
    }
    
    func popVehicleLookup(_ viewController: UIViewController, from: VehicleScreenSource) {
        switch from {
        case .profile:
            popToProfile()
        case .bubble:
            viewController.navigationController?.dismiss(animated: true, completion: nil)
        case .onboarding:
            presentCurrencyOrPermissionsOrDashboard()
        case .tripHistory:
            popToTripHistory()
        case .tripDetails:
            popToTripDetails()
        case .deepLink:
            popToMyVehicles()
        }
    }
}

// MARK: - VehicleListViewControllerDelegate

extension AppCoordinator: VehicleListViewControllerDelegate {
    
    func vehicleListViewControllerDidTapAddVehicle(_ viewController: VehicleListViewController) {
        let source: VehicleScreenSource = navigationController.presentedViewController == nil ? .deepLink : .profile
        presentVehicleAdd(from: source)
    }
    
    func vehicleListViewController(_ viewController: VehicleListViewController, didSelect vehicle: VehicleData, activeVehicles: [VehicleData]) {
        presentVehicleDetails(vehicle, activeVehicles: activeVehicles)
    }
}

// MARK: - VehicleDetailsViewControllerDelegate

extension AppCoordinator: VehicleDetailsViewControllerDelegate {
    func vehicleDetailsViewControllerDidRemoveVehicle(_ viewController: VehicleDetailsViewController) {
        popToVehicleListOrLanding(viewController.viewModel.isLastActive)
    }
    
    func vehicleDetailsViewControllerDidTapAddVehicle(_ viewController: VehicleDetailsViewController) {
        popToVehicleAdd()
    }
}

// MARK: - VehicleAddViewControllerDelegate

extension AppCoordinator: VehicleAddViewControllerDelegate {
    func vehicleAddViewControllerDidTapClose(_ viewController: VehicleAddViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func vehicleAddViewControllerDidTapDismissPermanently(_ viewController: VehicleAddViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func vehicleAddViewControllerDidTapAdd(_: VehicleAddViewController, source: VehicleScreenSource) {
        let countries = dependencies.appDataService.countries
        presentVehicleCountry(from: source, countries: countries)
    }
    
    func vehicleAddViewControllerWillAddCO2(_ viewController: VehicleAddViewController) {
        presentVehicleEnterEmissions(from: viewController.viewModel.source, vehicleData: nil, vrm: nil)
    }
}

// MARK: - VehicleCountryViewControllerDelegate

extension AppCoordinator: VehicleCountryViewControllerDelegate {
    func vehicleCountryViewControllerDidTapSkip(_ viewController: VehicleCountryViewController) {
        presentCurrencyOrPermissionsOrDashboard()
    }
    
    func vehicleCountryViewControllerDidTapContinue(_ viewController: VehicleCountryViewController, country: Country, state: CountryState?) {
        if country.isOther {
            presentVehicleEnterEmissions(from: viewController.viewModel.screenSource, vehicleData: nil, vrm: nil)
        } else {
            presentVehicleLookup(from: viewController.viewModel.screenSource, country: country, state: state)
        }
    }
}

// MARK: - VehicleLookupViewControllerDelegate

extension AppCoordinator: VehicleLookupViewControllerDelegate {
    func vehicleLookupViewControllerDidTapSkip(_: VehicleLookupViewController) {
        presentCurrencyOrPermissionsOrDashboard()
    }
    
    func vehicleLookupViewControllerDidTapEdit(_: VehicleLookupViewController, vehicleData: VehicleData) {
        presentVehicleLookupEdit(vehicleData)
    }
    
    func vehicleLookupViewControllerDidTapContinue(_ viewController: VehicleLookupViewController) {
        popVehicleLookup(viewController, from: viewController.viewModel.screenSource)
    }
    
    func vehicleLookupViewControllerDidTapEnterManually(_ viewController: VehicleLookupViewController, vehicleData: VehicleData?) {
        guard let viewModel = viewController.viewModel else { return }
        presentVehicleEnterEmissions(from: viewModel.screenSource, vehicleData: vehicleData, vrm: nil)
    }
    
    func vehicleLookupViewControllerDidTapEnterManually(_ viewController: VehicleLookupViewController, vrm: String?, country: Country?, state: CountryState?) {
        guard let viewModel = viewController.viewModel else { return }
        presentVehicleEnterEmissions(from: viewModel.screenSource, vehicleData: nil, vrm: vrm, country: country, state: state)
    }
    
    func vehicleLookupViewControllerWillShowEV(_: VehicleLookupViewController, allowSkip: Bool) {
        presentEVVehicleConfirmation(ableToSkip: allowSkip)
    }
}

// MARK: - VehicleEnterEmissionsViewControllerDelegate

extension AppCoordinator: VehicleEnterEmissionsViewControllerDelegate {
    func vehicleEnterEmissionsViewControllerDidTapSkip(_ viewController: VehicleEnterEmissionsViewController) {
        popVehicleLookup(viewController, from: viewController.viewModel.screenSource)
    }
    
    func vehicleEnterEmissionsViewControllerDidTapContinue(_ viewController: VehicleEnterEmissionsViewController) {
        popVehicleLookup(viewController, from: viewController.viewModel.screenSource)
    }
}

// MARK: - VehicleLookupEditViewControllerDelegate

extension AppCoordinator: VehicleLookupEditViewControllerDelegate {
    func vehicleLookupEditViewControllerDidTapSave(_: VehicleLookupEditViewController, vehicleData: VehicleData) {
        updateVehicleLookup(vehicleData)
        navigationController.popViewController(animated: true)
    }
}

// MARK: - EVConfirmationViewControllerDelegate

extension AppCoordinator: EVConfirmationViewControllerDelegate {
    func evConfirmationViewControllerDidContinue(_ vc: EVConfirmationViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
    
    func evConfirmationViewControllerDidSkip(_ vc: EVConfirmationViewController) {
        presentCurrencyOrPermissionsOrDashboard()
        vc.dismiss(animated: true, completion: nil)
    }
}
