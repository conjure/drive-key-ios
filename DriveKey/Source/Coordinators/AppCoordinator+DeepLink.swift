//
//  AppCoordinator+DeepLink.swift
//  DriveKey
//
//  Created by Piotr Wilk on 06/10/2022.
//

import UIKit
import Pulley
import OverlayContainer

enum DeepLink: String {
    case carbonOffset               = "offset-carbon"
    case dashboard                  = "dashboard"
    case ecoDrivingTips             = "eco-driving-tips"
    case myVehicles                 = "my-vehicles"
    case redeemVoucher              = "redeem-voucher"
    case tripHistory                = "trip-history"
    case projectBrazilianAmazon     = "project/brazilian-amazon"
    case projectTalas               = "project/talas"
    case projectMidilli             = "project/midilli"
}

extension AppCoordinator {
    func handleDeepLink(_ option: DeepLink) {
        if isBiometricLoginPresent {
            pendingDeepLink = option
            return
        }
        pendingDeepLink = nil
        
        if isDashboardPresent {
            popToDashboard()
            
            runOnMainThreadAsyncAfter(1) {
                self.presentDeepLink(option)
            }
        }
    }
    
    private func presentDeepLink(_ option: DeepLink) {
        switch option {
        case .carbonOffset:
            presentOneTimePayment()
        case .dashboard:
            popToDashboard()
        case .ecoDrivingTips:
            presentInfo(.ecoDrivingTips)
        case .myVehicles:
            let user = dependencies.userService.currentUser
            presentVehicleList(user)
        case .redeemVoucher:
            presentVoucherRedeem()
        case .tripHistory:
            presentTripHistory()
        case .projectBrazilianAmazon:
            presentCharityProject(.brazilianAmazon, source: .deepLink)
        case .projectTalas:
            presentCharityProject(.talas, source: .deepLink)
        case .projectMidilli:
            presentCharityProject(.midilli, source: .deepLink)
        }
    }
    
    func popToDashboard() {
        // overlay on modal
        if navigationController.presentedViewController?.presentedViewController is OverlayContainerViewController {
            navigationController.presentedViewController?.dismiss(animated: false)
        }
        
        // overlay on dashboard
        if navigationController.presentedViewController is OverlayContainerViewController {
            navigationController.presentedViewController?.dismiss(animated: true)
        }
        
        // modal
        if navigationController.presentedViewController is NavigationController {
            navigationController.presentedViewController?.dismiss(animated: true)
        }
        
        if let pulleyViewController = navigationController.viewControllers.first(where: { $0 is PulleyViewController }) {
            navigationController.popToViewController(pulleyViewController, animated: false)
        }
    }
    
    var isDashboardPresent: Bool {
        navigationController.viewControllers.first(where: { $0 is PulleyViewController }) != nil
    }
    
    var isBiometricLoginPresent: Bool {
        navigationController.visibleViewController is BiometricLoginViewController
    }
}
