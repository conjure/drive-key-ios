//
//  AppCoordinator+Payments.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 23/09/2021.
//

import Foundation
import UIKit
import SwiftUI

extension AppCoordinator {
    func presentPaymentMethods(_ user: DrivekeyUser?) {
        let viewController: PaymentMethodListViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = PaymentMethodsViewModel(userData: user)
        viewController.delegate = self
        viewController.api = dependencies.api

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    func presentAddNewPaymentMethod(previousMethod: PaymentMethod?, summary: OneTimePaymentSummary? = nil) {
        let viewController: AddPaymentMethodMasterViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = AddPaymentMethodMasterViewModel(userService: dependencies.userService, paymentMethod: previousMethod,
                                                                   summary: summary,
                                                                   api: dependencies.api)
        viewController.delegate = self

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    func presentOneTimePayment() {
        let configuration = OneTimePaymentView.Configuration()
        configuration.delegate = self
        let viewModel = OneTimePaymentViewModel(user: dependencies.userService.currentUser!, api: dependencies.api)
        let vc = HostingController(wrappedView: OneTimePaymentView(viewModel: viewModel, configuration: configuration), prefersLargeTitles: false)
        let nav = NavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        navigationController.present(nav, animated: true)
    }

    func presentPaymentMethod(_ method: PaymentMethod) {
        let viewController: PaymentMethodDetailViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = PaymentMethodDetailViewModel(method: method, api: dependencies.api)
        viewController.delegate = self

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    func presentExistingPaymentMethod(with summary: OneTimePaymentSummary) {
        let viewController: ExistingPaymentMethodViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = ExistingPaymentMethodViewModel(user: dependencies.userService.currentUser!, api: dependencies.api, summary: summary)
        viewController.delegate = self

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }
}

extension AppCoordinator: OneTimePaymentViewDelegate {
    func oneTimePaymentView(_ view: OneTimePaymentView, willAddCard forSummary: OneTimePaymentSummary) {
        presentAddNewPaymentMethod(previousMethod: nil, summary: forSummary)
    }

    func oneTimePaymentView(_ view: OneTimePaymentView, willConfirmCard forSummary: OneTimePaymentSummary) {
        presentExistingPaymentMethod(with: forSummary)
    }

    func oneTimePaymentView(_ view: OneTimePaymentView, willPresent content: HTMLContentType) {
        presentHTMLContent(type: content, modally: true)
    }

    func oneTimePaymentViewWillDismiss() {
        navigationController.dismiss(animated: true, completion: nil)
    }

    func oneTimePaymentView(_ view: OneTimePaymentView, willPresent project: CharityProject) {
        presentCharityProject(project, source: .offsetCarbon)
    }
}

extension AppCoordinator: PaymentMethodListViewControllerDelegate {
    func paymentMethodsViewController(_: PaymentMethodListViewController, didSelect method: PaymentMethod) {
        presentPaymentMethod(method)
    }
}

extension AppCoordinator: PaymentMethodDetailViewControllerDelegate {
    func paymentMethodDetailViewControllerWillShowAlert(_ vc: PaymentMethodDetailViewController) {
        showPopupAlert(.removeCard, from: vc)
    }

    func paymentMethodDetailViewControllerDidRemove(_ vc: PaymentMethodDetailViewController, method: PaymentMethod) {
        let viewController: PaymentMethodDetailViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = PaymentMethodDetailViewModel(method: method, api: dependencies.api)
        viewController.delegate = self
        viewController.showNoMethodsState = true

        if let nav = navigationController.presentedViewController as? UINavigationController {
            var newControllers = nav.viewControllers
            newControllers.remove(at: newControllers.count - 2)
            newControllers.removeLast()
            newControllers.append(viewController)
            nav.setViewControllers(newControllers, animated: true)
        } else {
            var newControllers = navigationController.viewControllers
            newControllers.remove(at: newControllers.count - 2)
            newControllers.removeLast()
            newControllers.append(viewController)
            navigationController.setViewControllers(newControllers, animated: true)
        }
    }
}

extension AppCoordinator: AddPaymentMethodMasterViewControllerDelegate {
    func addPaymentMethodMasterViewController(_ vc: AddPaymentMethodMasterViewController, didCompletePaymentFor: Int) {
        presentOffsetConfirmation(for: didCompletePaymentFor)
    }

    func addPaymentMethodMasterViewController(_ vc: AddPaymentMethodMasterViewController, didError: ErrorConfirmationType) {
        presentErrorConfirmation(didError)
    }
}

extension AppCoordinator: ExistingPaymentMethodViewControllerDelegate {
    func existingPaymentMethodViewControllerWillAddNewMethod(_ vc: ExistingPaymentMethodViewController, summary: OneTimePaymentSummary, previousMethod: PaymentMethod) {
        presentAddNewPaymentMethod(previousMethod: previousMethod, summary: summary)
    }

    func existingPaymentMethodViewControllerDidCompletePayment(_ vc: ExistingPaymentMethodViewController, amount: Double) {
        self.presentOffsetConfirmation(for: Int(amount))
    }

    func existingPaymentMethodViewControllerDidError(_ vc: ExistingPaymentMethodViewController, error: Error) {
        let errorType = ErrorConfirmationType.paymentError(message: error.localizedDescription)
        self.presentErrorConfirmation(errorType)
    }

    func existingPaymentMethodViewControllerWillShowHTMLContent(_ vc: ExistingPaymentMethodViewController, content: HTMLContentType) {
        presentHTMLContent(type: content, modally: true)
    }
}
