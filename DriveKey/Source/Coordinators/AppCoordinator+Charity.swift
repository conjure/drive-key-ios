//
//  AppCoordinator+Charity.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 07/03/2022.
//

import UIKit
import SafariServices

enum ProjectScreenSource {
    case dashboard
    case offsetCarbon
    case deepLink
}

extension AppCoordinator: CharityProjectViewControllerDelegate {
    func presentCharityProject(_ project: CharityProject?, picker: ProjectPickerDelegate = ProjectPickerDelegate(), source: ProjectScreenSource) {
        let charityProjectViewController: CharityProjectViewController = UIStoryboard.instantiateViewController()
        charityProjectViewController.delegate = self
        charityProjectViewController.pickerDelegate = picker
        charityProjectViewController.viewModel = CharityProjectViewModel(charityProject: project, screenSource: source)
        
        if source == .offsetCarbon {
            if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
                presentedNavigationController.pushViewController(charityProjectViewController, animated: true)
            } else {
                navigationController.pushViewController(charityProjectViewController, animated: true)
            }
        } else {
            presentModal(charityProjectViewController)
        }
    }

    func charityProjectViewController(_ vc: CharityProjectViewController, didTap url: URL) {
        let config = SFSafariViewController.Configuration()
        config.entersReaderIfAvailable = true

        let vc = SFSafariViewController(url: url, configuration: config)
        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.present(vc, animated: true)
        } else {
            navigationController.present(vc, animated: true)
        }
    }
}
