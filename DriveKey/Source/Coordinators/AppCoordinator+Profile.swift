//
//  AppCoordinator+Profile.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 27/06/2022.
//

import UIKit

extension AppCoordinator {
    func presentProfile() {
        let viewController: ProfileViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = ProfileViewModel(authService: dependencies.authService,
                                                    userService: dependencies.userService,
                                                    biometricService: dependencies.biometricService,
                                                    bubbleService: dependencies.bubbleService,
                                                    api: dependencies.api, tripMonitor: dependencies.tripsMonitor)
        viewController.delegate = self

        let nav = NavigationController(rootViewController: viewController)
        nav.modalPresentationStyle = .fullScreen
        navigationController.present(nav, animated: true, completion: nil)
    }

    func popToProfile() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }

        if let profileViewController = currentNavigationController?.viewControllers.first(where: { $0 is ProfileViewController }) {
            currentNavigationController?.popToViewController(profileViewController, animated: true)
        }
    }
    
    func popToTripHistory() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }

        if let tripHistoryViewController = currentNavigationController?.viewControllers.first(where: { $0 is TripHistoryViewController }) {
            currentNavigationController?.popToViewController(tripHistoryViewController, animated: true)
        }
    }
    
    func popToTripDetails() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }

        if let tripDetailsViewController = currentNavigationController?.viewControllers.first(where: { $0 is TripDetailsViewController }) {
            currentNavigationController?.popToViewController(tripDetailsViewController, animated: true)
        }
    }
    
    func popToMyVehicles() {
        var currentNavigationController = navigationController.presentedViewController as? UINavigationController
        if currentNavigationController == nil {
            currentNavigationController = navigationController
        }

        if let vehicleListViewController = currentNavigationController?.viewControllers.first(where: { $0 is VehicleListViewController }) {
            currentNavigationController?.popToViewController(vehicleListViewController, animated: true)
        }
    }

    func presentProfileDetail(for section: ProfileSection) {
        let viewController: ProfileDetailViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = ProfileDetailViewModel(section,
                                                          userService: dependencies.userService,
                                                          biometricService: dependencies.biometricService,
                                                          api: dependencies.api,
                                                          authService: dependencies.authService,
                                                          bubbleService: dependencies.bubbleService)
        viewController.delegate = self

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    private func presentAboutKarai() {
        let viewController: AboutKaraiViewController = UIStoryboard.instantiateViewController()

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    private func presentCurrency(_ currencies: [Currency], canUpdateCurrency: Bool) {
        let currencyViewController: CurrencyViewController = UIStoryboard.instantiateViewController()
        currencyViewController.viewModel = CurrencyViewModel(dependencies.userService, currencies: currencies)

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(currencyViewController, animated: true)
        } else {
            navigationController.pushViewController(currencyViewController, animated: true)
        }
    }

    private func showContent(_ content: ProfileContentRow) {
        switch content {
        case .about:
            presentAboutKarai()
        case .personalInfo:
            let userData = dependencies.userService.currentUser
            presentPersonalData(userData)
        case .myVehicles:
            presentVehicleDetailsFlow(from: .profile)
        case .businessInfo:
            presentBusinessInfo()
        case .units:
            presentUnits()
        case .payment:
            let user = dependencies.userService.currentUser
            presentPaymentMethods(user)
        case .currency:
            let currencies = dependencies.appDataService.currencies
            presentCurrency(currencies, canUpdateCurrency: false)
        case .termsConditions:
            presentHTMLContent(type: .termsOfUse, modally: false)
        case .privacyPolicy:
            presentHTMLContent(type: .privacyPolicy, modally: false)
        case .vouchers:
            presentVoucherRedeem()
        default:
            return
        }
    }
}

// MARK: - ProfileViewControllerDelegate
extension AppCoordinator: ProfileViewControllerDelegate {
    func profileViewController(_: ProfileViewController, didSelect row: ProfileSection) {
        presentProfileDetail(for: row)
    }

    private func presentVerifyEmail() {
        let viewController: VerifyEmailViewController = UIStoryboard.instantiateViewController()
        viewController.viewModel = VerifyEmailViewModel(authService: dependencies.authService, userService: dependencies.userService)
        viewController.delegate = self

        if let nav = navigationController.presentedViewController as? UINavigationController {
            nav.pushViewController(viewController, animated: true)
        } else {
            navigationController.pushViewController(viewController, animated: true)
        }
    }

    func profileViewControllerDidLogout(_ vc: ProfileViewController) {
        self.presentLoginRoute()
        navigationController.dismiss(animated: true)
    }

    func profileViewControllerWillVerify(_: ProfileViewController) {
        presentVerifyEmail()
    }

    func profileViewControllerDidTapAddName(_: ProfileViewController) {
        let userData = dependencies.userService.currentUser
        presentPersonalData(userData)
    }
}

// MARK: - ProfileDetailViewControllerDelegate
extension AppCoordinator: ProfileDetailViewControllerDelegate {
    func profileDetailViewController(_ vc: ProfileDetailViewController, didTapRow: ProfileContentRow) {
        self.showContent(didTapRow)
    }
    
    func profileDetailViewControllerDidRemoveAccount(_ vc: ProfileDetailViewController) {
        self.presentLoginRoute()
        navigationController.dismiss(animated: true)
    }
}
