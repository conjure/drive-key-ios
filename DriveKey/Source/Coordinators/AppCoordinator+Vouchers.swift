//
//  AppCoordinator+Vouchers.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 24/02/2022.
//

import UIKit

extension AppCoordinator {
    func presentVoucherRedeem() {
        let voucherRedeemVC: RedeemVoucherViewController = UIStoryboard.instantiateViewController()
        voucherRedeemVC.viewModel = RedeemVoucherViewModel(api: dependencies.api)
        voucherRedeemVC.delegate = self

        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(voucherRedeemVC, animated: true)
        } else {
            navigationController.pushViewController(voucherRedeemVC, animated: true)
        }
    }

    func presentOffsetConfirmation(for amount: Int) {
        let confirmation: OffsetConfirmationViewController = UIStoryboard.instantiateViewController()
        confirmation.offsetAmount = amount
        confirmation.delegate = self

        if let presentedNavigationController = navigationController.presentedViewController as? UINavigationController {
            presentedNavigationController.pushViewController(confirmation, animated: true)
        } else {
            navigationController.pushViewController(confirmation, animated: true)
        }
    }
}

extension AppCoordinator: RedeemVoucherViewControllerDelegate {
    func redeemVoucherViewController(_ vc: RedeemVoucherViewController, didRedeem amount: Int) {
        presentOffsetConfirmation(for: amount)
    }
}

extension AppCoordinator: OffsetConfirmationViewControllerDelegate {
    func offsetConfirmationViewControllerDidConfirm(_ vc: OffsetConfirmationViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}
