//
//  ReportsEndpoint.swift
//  DriveKey
//
//  Created by Piotr Wilk on 31/08/2022.
//

import Foundation
import Alamofire

enum ReportsEndpoint {
    case expense
}

extension ReportsEndpoint: Endpoint {
    var path: String {
        switch self {
        case .expense:
            return "reports-api/v1/expense"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .expense:
            return .post
        }
    }
}
