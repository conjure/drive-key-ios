//
//  PaymentEndpoint.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 25/01/2022.
//

import Foundation
import Alamofire

enum PaymentEndpoint {
    case oneTimePayment
    case paymentAttach(paymentMethodId: String)
    case removeMethod(paymentMethodId: String)
    case paymentMethods
    case confirmIntent
}

extension PaymentEndpoint: Endpoint {
    var path: String {
        switch self {
        case .oneTimePayment:
            return "payment-api/v1/one-time"
        case let .paymentAttach(paymentMethodId):
            return "payment-api/v1/payment-methods/\(paymentMethodId)/attach"
        case .paymentMethods:
            return "payment-api/v1/payment-methods/"
        case .confirmIntent:
            return "payment-api/v1/confirm-payment-intent"
        case .removeMethod(let paymentMethodId):
            return "payment-api/v1/payment-methods/\(paymentMethodId)/detach"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .oneTimePayment, .paymentAttach, .paymentMethods, .confirmIntent:
            return .post
        case .removeMethod:
            return .delete
        }
    }
}
