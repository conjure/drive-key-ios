//
//  CarbonEndpoint.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/01/2022.
//

import Foundation
import Alamofire

enum CarbonEndpoint {
    case oneTimePaymentSummary
    case redeemVoucher
}

extension CarbonEndpoint: Endpoint {
    var path: String {
        switch self {
        case .oneTimePaymentSummary:
            return "carbon-api/v1/checkout-summary"
        case .redeemVoucher:
            return "carbon-api/v1/redeem-voucher"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .oneTimePaymentSummary:
            return .get
        case .redeemVoucher:
            return .post
        }
    }
}
