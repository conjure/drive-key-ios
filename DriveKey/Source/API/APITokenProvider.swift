//
//  APITokenProvider.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/10/2021.
//

import FirebaseAuth
import Foundation

class APITokenProvider: AuthTokenProvidable {
    func getToken(_ completion: ((String?) -> Void)?) {
        if Auth.auth().currentUser != nil {
            Auth.auth().currentUser?.getIDTokenForcingRefresh(true, completion: { token, error in
                if let error = error {
                    print("[APITokenProvider] getToken ERROR - \(error)")
                    completion?(nil)
                } else {
                    print("[APITokenProvider] getToken token: \(token ?? "NIL")")
                    completion?(token!)
                }
            })
        } else {
            print("[APITokenProvider] getToken ERROR - NO USER")
            completion?(nil)
        }
    }
}
