//
//  BubbleEndpoint.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 20/09/2021.
//

import Alamofire
import Foundation

enum BubbleEndpoint {
    case dismiss(bubble: Bubble)
}

extension BubbleEndpoint: Endpoint {
    var path: String {
        switch self {
        case let .dismiss(bubble):
            return "bubble-api/v1/dismiss/\(bubble.bubbleId)"
        }
    }

    var httpMethod: HTTPMethod {
        .post
    }
}
