//
//  Endpoint.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Alamofire
import Foundation

public protocol Endpoint {
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var url: URL { get }
    var encoding: ParameterEncoding { get }
    var timeout: TimeInterval { get }
}

public extension Endpoint {
    internal var baseURL: String {
        Configuration.APIBaseURL
    }

    var headers: HTTPHeaders? {
        [
            "Content-Type": "application/json"
        ]
    }

    var httpMethod: HTTPMethod {
        .get
    }

    var encoding: ParameterEncoding {
        JSONEncoding.default
    }

    var timeout: TimeInterval {
        15
    }

    internal var url: URL {
        var urlStr = baseURL + "/" + path
        urlStr = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        return URL(string: urlStr)!
    }
}
