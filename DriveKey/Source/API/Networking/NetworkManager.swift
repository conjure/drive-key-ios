//
//  NetworkManager.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Alamofire
import Foundation

public enum NetworkManagerError: Error {
    case errorMessage(String?)
    case noConnection
    case internalServerError
}

class NetworkManager: Networkable {
    static let shared = NetworkManager(Session(), tokenProvider: APITokenProvider())
    private let session: Session
    private let requestTokenProvider: AuthTokenProvidable
    
    private init(_ session: Session, tokenProvider: AuthTokenProvidable) {
        self.session = session
        requestTokenProvider = tokenProvider
    }
    
    func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, completion: @escaping (Result<T, Error>) -> Void) {
        fetchData(endpoint: endpoint, parameters: parameters, decodingStrategy: nil, authenticate: true) { result in
            completion(result)
        }
    }
    
    func fetchData<T>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, completion: @escaping (Result<T, Error>) -> Void) where T: Decodable {
        fetchData(endpoint: endpoint, parameters: parameters, decodingStrategy: decodingStrategy, authenticate: true) { result in
            completion(result)
        }
    }
    
    func fetchData<T>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, authenticate: Bool, completion: @escaping (Result<T, Error>) -> Void) where T: Decodable {
        let tokenProvider: AuthTokenProvidable? = authenticate ? requestTokenProvider : nil
        fetchData(endpoint: endpoint, parameters: parameters, decodingStrategy: decodingStrategy, tokenProvider: tokenProvider) { result in
            completion(result)
        }
    }
    
    func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, tokenProvider: AuthTokenProvidable?, completion: @escaping (Result<T, Error>) -> Void) {
        if let tokenProvider = tokenProvider {
            authorizeRequest(tokenProvider) { token in
                self.fetchData(endpoint: endpoint, parameters: parameters, decodingStrategy: decodingStrategy, authToken: token) { result in
                    completion(result)
                }
            }
        } else {
            fetchData(endpoint: endpoint, parameters: parameters, decodingStrategy: decodingStrategy, authToken: nil) { result in
                completion(result)
            }
        }
    }
    
    private func fetchData<T: Decodable>(endpoint: Endpoint, parameters: Parameters?, decodingStrategy: JSONDecoder.KeyDecodingStrategy?, authToken: String?, completion: @escaping (Result<T, Error>) -> Void) {
        var headers = endpoint.headers
        if authToken != nil {
            headers?.add(HTTPHeader(name: "Authorization", value: "Bearer \(authToken!)"))
        }
        let decoder = JSONDecoder()
        if decodingStrategy != nil {
            decoder.keyDecodingStrategy = decodingStrategy!
        }
        decoder.dateDecodingStrategy = .iso8601WithOptionalMilliseconds
        
        session.request(endpoint.url,
                        method: endpoint.httpMethod,
                        parameters: parameters,
                        encoding: endpoint.encoding,
                        headers: headers,
                        requestModifier: { request in
            request.timeoutInterval = endpoint.timeout
        })
            .validate()
            .responseDecodable(of: T.self, decoder: decoder, completionHandler: { data in
                switch data.result {
                case .success(let result):
                    completion(.success(result))
                case let .failure(error):
                    if let jsonData = data.data {
                        let decoder = JSONDecoder()
                        if let errorMessage = try? decoder.decode(APIErrorMessage.self, from: jsonData) {
                            completion(.failure(NetworkManagerError.errorMessage(errorMessage.message)))
                            return
                        }
                        completion(.failure(NetworkManagerError.internalServerError))
                    } else {
                        print("[NetworkManager] Error: \(error)")
                        guard let error = (error.underlyingError as? URLError) else {
                            completion(.failure(NetworkManagerError.internalServerError))
                            return
                        }
                    
                        switch error.code {
                        case .timedOut, .notConnectedToInternet:
                            completion(.failure(NetworkManagerError.noConnection))
                        default:
                            completion(.failure(NetworkManagerError.internalServerError))
                        }
                    }
                }
            })
    }
    
    private func authorizeRequest(_ tokenProvider: AuthTokenProvidable, completion: @escaping ((String?) -> Void)) {
        tokenProvider.getToken { token in
            completion(token)
        }
    }
}

extension NetworkManagerError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .errorMessage(let message):
            return message
        case .noConnection:
            return "error.auth.network".localized
        case .internalServerError:
            return "error.auth.internal".localized
        }
    }
}
