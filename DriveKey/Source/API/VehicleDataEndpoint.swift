//
//  VehicleDataEndpoint.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Alamofire
import Foundation

enum VehicleDataEndpoint {
    case lookup(plate: String)
    case register
    case setInactive(vehicleId: String)
    case setPrimary(vehicleId: String)
}

extension VehicleDataEndpoint: Endpoint {
    var path: String {
        switch self {
        case .lookup:
            return "vehicle-api/v1/look-up"
        case .register:
            return "vehicle-api/v1/register"
        case .setInactive(let vehicleId):
            return "vehicle-api/v1/\(vehicleId)/set-inactive"
        case .setPrimary(let vehicleId):
            return "vehicle-api/v1/\(vehicleId)/set-primary"
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        case .lookup, .register:
            return .post
        case .setInactive, .setPrimary:
            return .put
        }
    }
}
