//
//  APIVehicleDataProtocol.swift
//  DriveKey
//
//  Created by Piotr Wilk on 05/08/2021.
//

import Alamofire
import Foundation

protocol APIProtocol: APIVehicleDataProtocol, APIUserProtocol, APIBubbleProtocol, APICarbonProtocol, APIPaymentProtocol, APITripProtocol, APIReportsProtocol {}

protocol APIUserProtocol: AnyObject {
    func userData(_ data: UpdateUserPayload, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func disableAccount(_ authCode: String?, completion: @escaping (Result<APIMessage, Error>) -> Void)
}

protocol APIBubbleProtocol: AnyObject {
    func dismiss(_ bubble: Bubble, completion: @escaping (Result<APIMessage, Error>) -> Void)
}

protocol APIVehicleDataProtocol: AnyObject {
    func lookupVehicle(_ vrm: String, country: String, countryCode: String, state: String?, completion: @escaping (Result<VehicleDataLookupResponse, Error>) -> Void)
    func registerVehicle(_ vehicleData: VehicleData, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func removeVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func primaryVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
}

protocol APICarbonProtocol: AnyObject {
    func oneTimePaymentSummary(_ completion: @escaping (Result<OneTimePaymentSummary, Error>) -> Void)
    func redeemVoucher(_ voucherCode: String, completion: @escaping(Result<VoucherAPIResponse, Error>) -> Void)
}

protocol APIPaymentProtocol: AnyObject {
    func confirm(_ paymentIntentId: String, completion: @escaping (Result<ConfirmIntentResponse, Error>) -> Void)
    func oneTimePayment(data: OneTimePaymentSummary, completion: @escaping (Result<OneTimePaymentAPIResponse, Error>) -> Void)
    func removePaymentMethod(_ id: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func paymentAttach(paymentMethodId: String, type: String, completion: @escaping (Result<MembershipAttachResponse, Error>) -> Void)
    func paymentMethods(_ customerId: String, completion: @escaping (Result<PaymentMethodContainer, Error>) -> Void)
}

protocol APITripProtocol: AnyObject {
    func changeVehicle(_ tripId: String, vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func removeTrip(_ tripId: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func changeTrip(_ tripId: String, type: TripType, completion: @escaping (Result<APIMessage, Error>) -> Void)
    func updateBusiness(_ tripId: String, rate: Double, notes: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void)
}

protocol APIReportsProtocol: AnyObject {
    func generateExpense(_ trips: [String], completion: @escaping (Result<APIMessage, Error>) -> Void)
}

extension NetworkManager: APIProtocol {}

extension NetworkManager: APIUserProtocol {
    func userData(_ data: UpdateUserPayload, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: UserEndpoint.updateDetails, parameters: data.toDict(), decodingStrategy: nil) { result in
            completion(result)
        }
    }
    
    func disableAccount(_ authCode: String?, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: UserEndpoint.disable(authCode: authCode), parameters: nil, completion: completion)
    }
}

extension NetworkManager: APIBubbleProtocol {
    func dismiss(_ bubble: Bubble, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: BubbleEndpoint.dismiss(bubble: bubble), parameters: nil, decodingStrategy: nil) { result in
            completion(result)
        }
    }
}

extension NetworkManager: APIVehicleDataProtocol {
    func lookupVehicle(_ vrm: String, country: String, countryCode: String, state: String?, completion: @escaping (Result<VehicleDataLookupResponse, Error>) -> Void) {
        var params = ["vrm": vrm, "country": country, "countryCode": countryCode]
        if state != nil {
            params["state"] = state!
        }
        fetchData(endpoint: VehicleDataEndpoint.lookup(plate: vrm), parameters: params, decodingStrategy: nil) { result in
            completion(result)
        }
    }
    
    func registerVehicle(_ vehicleData: VehicleData, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: VehicleDataEndpoint.register, parameters: vehicleData.dictionary) { result in
            completion(result)
        }
    }
    
    func removeVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: VehicleDataEndpoint.setInactive(vehicleId: vehicleId), parameters: nil) { result in
            completion(result)
        }
    }
    
    func primaryVehicle(_ vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: VehicleDataEndpoint.setPrimary(vehicleId: vehicleId), parameters: nil) { result in
            completion(result)
        }
    }
}

extension NetworkManager: APICarbonProtocol {
    func oneTimePaymentSummary(_ completion: @escaping (Result<OneTimePaymentSummary, Error>) -> Void) {
        fetchData(endpoint: CarbonEndpoint.oneTimePaymentSummary, parameters: nil) { result in
            completion(result)
        }
    }
    
    func redeemVoucher(_ voucherCode: String, completion: @escaping (Result<VoucherAPIResponse, Error>) -> Void) {
        let params = ["voucher": voucherCode]
        fetchData(endpoint: CarbonEndpoint.redeemVoucher, parameters: params) { result in
            completion(result)
        }
    }
}

extension NetworkManager: APIPaymentProtocol {
    func paymentAttach(paymentMethodId: String, type: String, completion: @escaping (Result<MembershipAttachResponse, Error>) -> Void) {
        fetchData(endpoint: PaymentEndpoint.paymentAttach(paymentMethodId: paymentMethodId), parameters: ["defaultPaymentMethod": true, "type": type]) { result in
            completion(result)
        }
    }
    
    func paymentMethods(_ customerId: String, completion: @escaping (Result<PaymentMethodContainer, Error>) -> Void) {
        fetchData(endpoint: PaymentEndpoint.paymentMethods, parameters: ["customerId": customerId]) { result in
            completion(result)
        }
    }
    
    func confirm(_ paymentIntentId: String, completion: @escaping (Result<ConfirmIntentResponse, Error>) -> Void) {
        fetchData(endpoint: PaymentEndpoint.confirmIntent, parameters: ["paymentIntentId": paymentIntentId]) { result in
            completion(result)
        }
    }
    
    func oneTimePayment(data: OneTimePaymentSummary, completion: @escaping (Result<OneTimePaymentAPIResponse, Error>) -> Void) {
        fetchData(endpoint: PaymentEndpoint.oneTimePayment, parameters: data.toDict()) { result in
            completion(result)
        }
    }
    
    func removePaymentMethod(_ id: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        fetchData(endpoint: PaymentEndpoint.removeMethod(paymentMethodId: id), parameters: nil) { result in
            completion(result)
        }
    }
}

extension NetworkManager: APITripProtocol {
    func changeVehicle(_ tripId: String, vehicleId: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        let params = ["vehicleId": vehicleId]
        fetchData(endpoint: TripEndpoint.changeVehicle(tripId: tripId), parameters: params, completion: completion)
    }
    
    func removeTrip(_ tripId: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        let params = ["reason": reason]
        fetchData(endpoint: TripEndpoint.setInactive(tripId: tripId), parameters: params, completion: completion)
    }
    
    func changeTrip(_ tripId: String, type: TripType, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        let params = ["tripType": type.rawValue.uppercased()]
        fetchData(endpoint: TripEndpoint.setType(tripId: tripId), parameters: params, completion: completion)
    }
    
    func updateBusiness(_ tripId: String, rate: Double, notes: String, reason: String, completion: @escaping (Result<APIMessage, Error>) -> Void) {
        let params: [String: Any] = ["rate": rate, "notes": notes, "reason": reason]
        fetchData(endpoint: TripEndpoint.updateBusiness(tripId: tripId), parameters: params, completion: completion)
    }
}

extension NetworkManager: APIReportsProtocol {
    func generateExpense(_ trips: [String], completion: @escaping (Result<APIMessage, Error>) -> Void) {
        let params = ["trips": trips]
        fetchData(endpoint: ReportsEndpoint.expense, parameters: params, completion: completion)
    }
}
