//
//  UserEndpoint.swift
//  DriveKey
//
//  Created by Dinesh Vijaykumar on 17/09/2021.
//

import Alamofire
import Foundation

enum UserEndpoint {
    case updateDetails
    case disable(authCode: String?)
}

extension UserEndpoint: Endpoint {
    var path: String {
        switch self {
        case .updateDetails:
            return "user-api/v1/me"
        case .disable:
            return "user-api/v1/disable"
        }
    }
    
    var headers: HTTPHeaders? {
        var headers: HTTPHeaders = ["Content-Type": "application/json"]
        if case .disable(let authCode) = self, let authCode = authCode {
            headers["apple-auth-code"] = authCode
        }
        
        return headers
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .updateDetails, .disable:
            return .post
        }
    }
}
