//
//  TripEndpoint.swift
//  DriveKey
//
//  Created by Piotr Wilk on 02/08/2022.
//

import Alamofire
import Foundation

enum TripEndpoint {
    case changeVehicle(tripId: String)
    case setInactive(tripId: String)
    case setType(tripId: String)
    case updateBusiness(tripId: String)
}

extension TripEndpoint: Endpoint {
    var path: String {
        switch self {
        case .changeVehicle(let tripId):
            return "vehicle-api/v1/trips/\(tripId)/change-vehicle"
        case .setInactive(let tripId):
            return "vehicle-api/v1/trips/\(tripId)/set-inactive"
        case .setType(let tripId):
            return "vehicle-api/v1/trips/\(tripId)/set-trip-type"
        case .updateBusiness(let tripId):
            return "vehicle-api/v1/trips/\(tripId)/update-business-trip"
        }
    }

    var httpMethod: HTTPMethod {
        .put
    }
}
