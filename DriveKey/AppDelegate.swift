//
//  AppDelegate.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseMessaging
import Stripe
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    lazy var userService = UserService(vehicleFetcher: VehicleFetcher())

    func application(_: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        StripeAPI.defaultPublishableKey = Configuration.StripePublishableKey

        let googleFilePath = Bundle.main.path(forResource: Configuration.GoogleServiceFileName, ofType: nil)
        guard let fileopts = FirebaseOptions(contentsOfFile: googleFilePath!)
        else { return false }
        FirebaseApp.configure(options: fileopts)
        
        if Configuration.isLocal {
            let settings = Firestore.firestore().settings
            settings.host = "127.0.0.1:8081"
            settings.isPersistenceEnabled = false
            settings.isSSLEnabled = false
            Firestore.firestore().settings = settings
            Auth.auth().useEmulator(withHost: "127.0.0.1", port: 9098)
        }
        
        GMSServices.provideAPIKey(Configuration.GoogleMapsAPIKey)

        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options _: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_: UIApplication, didDiscardSceneSessions _: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func application(_: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
}
