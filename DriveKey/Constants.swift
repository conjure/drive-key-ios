//
//  Constants.swift
//  DriveKey
//
//  Created by Piotr Wilk on 04/08/2021.
//

import Foundation

struct UserDefaultKeys {
    static let loggedIn             = "isUserLoggedIn"
    static let didLaunchAppBefore   = "didLaunchAppBefore"
    static let currentUser          = "currentUser"
    static let mileageSummary       = "mileageSummary"
    static let lastTrip             = "lastTrip"
    static let biometricStatus      = "biometricStatus"
    static let shouldTrackTrips     = "shouldTrackTrips"
}

struct DeepLinkConfig {
    static let scheme = "karai"
}
